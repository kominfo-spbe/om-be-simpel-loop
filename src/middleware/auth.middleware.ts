import {Next} from '@loopback/core';
import {HttpErrors, Middleware, MiddlewareContext} from '@loopback/rest';
import {AuthService} from '../services';
import http from "http";

const jwt = require('jsonwebtoken');

export const authMiddleware: Middleware = async (
    middlewareCtx: MiddlewareContext,
    next: Next,
) => {
    const {request} = middlewareCtx;
    //console.log('Request: %s %s', request.headers, request.method, request.originalUrl)

    try {
        // Proceed with next middleware
        const result = await next();

        //console.log(request.headers)
        //console.log(request.headers.referer)
        //console.log(request.originalUrl)
        //console.log(request.originalUrl.includes("/tx-laporan/tracking"))

        const whiteList = [
            '/',
            '/explorer/openapi.json',
            '/explorer/favicon-32x32.png',
            '/inspect-token',
            '/ping',
            '/files',
            //"/files/*",
            '/explorer/*',
            '/tx-laporan',
            '/tx-laporan/tracking',
            '/tx-laporan-by-email',
            '/tx-laporan-by-token',
            '/tx-file-upload',
        ];

        if (
            request.originalUrl.includes('/tx-laporan-by-token') ||
            request.originalUrl.includes('/tx-laporan-by-email') ||
            request.originalUrl.includes('/tx-laporan/request-otp') ||
            request.originalUrl.includes('/tx-laporan/verify-otp')
        ) {
            return result;
        }

        //if (!request.headers.referer && !request.headers.referer?.includes("explorer")) {
        //         && !request.originalUrl.includes("files/")
        if (
            !whiteList.includes(<string>request.originalUrl) &&
            !request.originalUrl.includes('/tx-laporan/tracking')
            // && !request.originalUrl.includes("files/")
        ) {
            const headerAuth = request.headers.authorization;
            const token =
                headerAuth !== null && headerAuth?.startsWith('Bearer')
                    ? headerAuth?.substring(7, headerAuth.length)
                    : undefined;

            let isvalid = await AuthService.validateJwt(token);
            if (!isvalid) {
                throw new HttpErrors.Unauthorized("Unauthorized");
            }
        }

        // Process response
        //console.log('Response received for %s %s', request.method, request.originalUrl)
        return result;
    } catch (err) {
        // Catch errors from downstream middleware
        console.error(
            'Error received for %s %s %s',
            request.method,
            request.originalUrl,
            err,
        );
        throw err;
    }
};
