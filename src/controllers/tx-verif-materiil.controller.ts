import {inject} from "@loopback/core";
import {
    Filter,
    FilterExcludingWhere,
    repository
} from '@loopback/repository';
import {
    del, get,
    getModelSchemaRef, param, patch, post, requestBody, response
} from '@loopback/rest';
import {TxVerifMateriil} from '../models';
import {MLookupRepository, TxLaporanRepository, TxVerifMateriilRepository} from '../repositories';
import {PvlService} from "../services";

export class TxVerifMateriilController {
    public include = {"include": ["TxVerifMateriilKronologi", "TxVerifMateriilDokumen"]};
    public relations = ["TxVerifMateriilKronologi", "TxVerifMateriilDokumen"];

    constructor(
        @repository(TxVerifMateriilRepository)
        public txVerifMateriilRepository: TxVerifMateriilRepository,
        @repository(MLookupRepository)
        public mLookupRepository: MLookupRepository,
        @inject('services.PvlService')
        public pvlService: PvlService,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository
    ) {
    }

    @post('/tx-verif-materiil')
    @response(200, {
        description: 'Input Transaksi Verifikasi Materiil',
        content: {'application/json': {schema: getModelSchemaRef(TxVerifMateriil)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxVerifMateriil, {
                        title: 'Transaksi Verifikasi Materiil Baru',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txVerifMateriil: Omit<TxVerifMateriil, 'id'>,
    ): Promise<TxVerifMateriil> {
        let savedData = await this.txVerifMateriilRepository.create(txVerifMateriil);
        if (savedData) {
            let objLaporan = {
                substansi: txVerifMateriil.substansi,
                pokok_masalah: txVerifMateriil.pokok_permasalahan
            }

            await this.txLaporanRepository.updateById(savedData.id_tx_laporan, objLaporan);
        }

        return savedData
    }

    @get('/tx-verif-materiil')
    @response(200, {
        description: 'List Transaksi Verifikasi Materiil',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxVerifMateriil, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxVerifMateriil,) filter?: Filter<TxVerifMateriil>,
    ): Promise<{ data: (TxVerifMateriil)[]; message: string; status: boolean }> {
        var data = filter;
        if (data) {
            data.include = this.relations;
        } else {
            data = this.include;
        }

        let listUraian = ['substansi', 'legal_standing_pelapor']
        let resultData = await this.pvlService.getMLookUpFindAll(data, listUraian, this.txVerifMateriilRepository)

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: resultData
        };
    }

    @get('/tx-verif-materiil/{id}')
    @response(200, {
        description: 'Detail Transaksi Verifikasi Materiil',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxVerifMateriil, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxVerifMateriil, {exclude: 'where'}) filter?: FilterExcludingWhere<TxVerifMateriil>
    ): Promise<object> {
        var data = filter
        if (data) {
            data.include = this.relations
        } else {
            data = this.include
        }

        let listUraian = ['substansi', 'legal_standing_pelapor']
        let resultData = await this.pvlService.getMLookUpFindById(id, data, listUraian, this.txVerifMateriilRepository)

        return {
            status: true,
            message: "Data berhasil didapatkan",
            data: resultData
        };
    }

    @patch('/tx-verif-materiil/{id}')
    @response(200, {
        description: 'TxVerifMateriil PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxVerifMateriil, {partial: true}),
                },
            },
        })
            txVerifMateriil: TxVerifMateriil,
    ): Promise<object> {
        let currentData = await this.txVerifMateriilRepository.findById(id);
        let isUpdated = false;

        if (currentData) {
            txVerifMateriil.updated_date = new Date().toISOString();
            await this.txVerifMateriilRepository.updateById(id, txVerifMateriil);
            isUpdated = true;

            if (txVerifMateriil.substansi != null || txVerifMateriil.pokok_permasalahan != null) {
                let objLaporan = {
                    substansi: txVerifMateriil.substansi,
                    pokok_masalah: txVerifMateriil.pokok_permasalahan
                }

                await this.txLaporanRepository.updateById(currentData.id_tx_laporan, objLaporan);
            }
        }

        let newData = await this.txVerifMateriilRepository.findById(id);

        return {
            status: isUpdated,
            message: isUpdated ? 'Data berhasil diubah' : 'Data gagal diubah',
            data: {"data_lama": currentData, "data_baru": newData}
        };
    }

    @del('/tx-verif-materiil/{id}')
    @response(204, {
        description: 'TxVerifMateriil DELETE success',
    })
    async deleteById(
        @param.path.string('id') id: string,
    ): Promise<object> {

        let currentData = await this.txVerifMateriilRepository.findById(id);
        let isDeleted = false;
        let deletedDate = new Date().toISOString();

        if (currentData) {
            await this.txVerifMateriilRepository.updateById(id, {"deleted_date": deletedDate});
            isDeleted = true;
        }

        let newData = await this.txVerifMateriilRepository.findById(id);

        return {
            status: isDeleted,
            message: isDeleted ? 'Data berhasil dihapus' : 'Data gagal dihapus',
            data: {"id": id, "deleted_date": deletedDate, "data": newData}
        };
    }

}
