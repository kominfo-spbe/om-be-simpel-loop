import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxHistoryStatusLaporan} from '../models';
import {TxHistoryStatusLaporanRepository, TxLaporanRepository} from '../repositories';
import {PvlService} from "../services";
import {inject} from "@loopback/core";

export class TxHistoryStatusLaporanController {
    constructor(
        @repository(TxHistoryStatusLaporanRepository)
        public txHistoryStatusLaporanRepository: TxHistoryStatusLaporanRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository,
        @inject('services.PvlService')
        public pvlService: PvlService
    ) {
    }

    @post('/tx-history-status-laporan')
    @response(200, {
        description: 'TxHistoryStatusLaporan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxHistoryStatusLaporan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxHistoryStatusLaporan, {
                        title: 'NewTxHistoryStatusLaporan',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txHistoryStatusLaporan: Omit<TxHistoryStatusLaporan, 'id'>,
    ):
        Promise<{ data: TxHistoryStatusLaporan; message: string; status: boolean }> {

        if (txHistoryStatusLaporan.no_agenda == null) {
            let laporan = await this.txLaporanRepository.findById(txHistoryStatusLaporan.id_tx_laporan)
            txHistoryStatusLaporan.no_agenda = laporan.no_agenda
        }

        //save change status
        txHistoryStatusLaporan.created_date = new Date().toISOString()
        let savedData = await this.txHistoryStatusLaporanRepository.create(txHistoryStatusLaporan);

        let rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: await this.txHistoryStatusLaporanRepository.findById(savedData.id)

        }

        return rs;
    }

    @get('/tx-history-status-laporan/count')
    @response(200, {
        description: 'TxHistoryStatusLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxHistoryStatusLaporan) where?: Where<TxHistoryStatusLaporan>,
    ): Promise<Count> {
        return this.txHistoryStatusLaporanRepository.count(where);
    }

    @get('/tx-history-status-laporan')
    @response(200, {
        description: 'Array of TxHistoryStatusLaporan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxHistoryStatusLaporan, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxHistoryStatusLaporan) filter?: Filter<TxHistoryStatusLaporan>,
    ): Promise<object> {
        const resultData = await this.pvlService.getHistoryRelation(filter, this.txHistoryStatusLaporanRepository)
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: resultData
        };
    }

    @get('/tx-history-status-laporan/{id}')
    @response(200, {
        description: 'TxHistoryStatusLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxHistoryStatusLaporan, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxHistoryStatusLaporan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxHistoryStatusLaporan>
    ): Promise<{ data: TxHistoryStatusLaporan; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txHistoryStatusLaporanRepository.findById(id, filter)
        };
    }

}
