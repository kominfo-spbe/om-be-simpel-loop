import {inject} from "@loopback/core";
import {Count, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, Request, response, RestBindings} from '@loopback/rest';
import {TxLaporan} from '../models';
import {
    MKantorRepository, MPelaporRepository, TdUserRepository, TxDokumenRepository,
    TxHistoryStatusLaporanRepository, TxLaporanHistoryRepository, TxLaporanRepository, TxOtpRepository
} from '../repositories';
import {AuthService, EmailService, OtpService, PvlService} from "../services";

export class ChartBoardController {
    constructor(
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository,
        @repository(MPelaporRepository)
        public mPelaporRepository: MPelaporRepository,
        @repository(MKantorRepository)
        public mKantorRepository: MKantorRepository,
        @repository(TxLaporanHistoryRepository)
        public txLaporanHistoryRepository: TxLaporanHistoryRepository,
        @repository(TdUserRepository)
        public tdUserRepository: TdUserRepository,
        @repository(TxHistoryStatusLaporanRepository)
        public txdHistoryStatusLaporanRepository: TxHistoryStatusLaporanRepository,
        @repository(TxOtpRepository)
        public txOtpRepository: TxOtpRepository,
        @repository(TxDokumenRepository)
        public txDokumenRepository: TxDokumenRepository,
        @inject('services.EmailService')
        public emailService: EmailService,
        @inject('services.PvlService')
        public pvlService: PvlService,
        @inject('services.OtpService')
        public otpService: OtpService,
        @inject(RestBindings.Http.REQUEST) private request: Request
    ) {
    }

    @get('/laporan-jenis-klasifikasi')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async laporanJenisKlasifikasi(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("kantor") kantor: string,
                @param.query.string("ku") ku: string
            ): Promise<any> {
                let sql = "select b.id_tx_laporan,(array_agg(a.status_laporan)) [1] as status_laporan , "+
                          "(array_agg(b.klasifikasi_laporan order by b.created_date desc)) [1] as klasifikasi_laporan "+
                          "from simpel_4.tx_laporan a "+
                          "join simpel_4.tx_klasifikasi_lm b "+
                          "on a.id = b.id_tx_laporan "+
                          "join simpel_4.tx_pleno c "+
                          "on a.id  = c.id_tx_laporan "+
                          "where a.is_suspended is null ";
                const currentYear = new Date().getFullYear();

                if(startDate != null && endDate != null){
                    sql = sql +"and a.tgl_agenda between '" +startDate+ "' and '"+ endDate +"' ";
                }else{
                    sql = sql +"and EXTRACT(YEAR FROM a.tgl_agenda) =" +currentYear+" ";
                }
                if(kantor != null){
                    sql = sql + "and a.kode_kantor = '" + kantor + "' ";
                }
                if(ku != null){
                    sql = sql + "and c.tim_pemeriksaan = '" + ku + "' ";
                }

                sql = sql + "group by b.id_tx_laporan";
                const result = await this.txLaporanRepository.dataSource.execute(sql);

                // @ts-ignore
                const sederhanaProses = result.filter(x => {
                    return (x.status_laporan != "LT" && x.status_laporan != "TMSM" && x.status_laporan != "TMSF" && x.status_laporan != "LTRSM" && x.status_laporan != "LTRKS" ) && x.klasifikasi_laporan == "klasifikasi_laporan_sederhana"
                })

                // @ts-ignore
                const sederhanaTutup = result.filter(x => {
                    return (x.status_laporan == "LT" || x.status_laporan == "TMSM" || x.status_laporan == "TMSF" || x.status_laporan == "LTRSM" || x.status_laporan == "LTRKS" ) && x.klasifikasi_laporan == "klasifikasi_laporan_sederhana"
                })

                // @ts-ignore
                const sedangProses = result.filter(x => {
                    return (x.status_laporan != "LT" && x.status_laporan != "TMSM" && x.status_laporan != "TMSF" && x.status_laporan != "LTRSM" && x.status_laporan != "LTRKS" ) && x.klasifikasi_laporan  == "klasifikasi_laporan_sedang"
                })

                // @ts-ignore
                const sedangTutup = result.filter(x => {
                    return (x.status_laporan == "LT" || x.status_laporan == "TMSM" || x.status_laporan == "TMSF" || x.status_laporan == "LTRSM" || x.status_laporan == "LTRKS" ) && x.klasifikasi_laporan == "klasifikasi_laporan_sedang"
                })

                // @ts-ignore
                const beratProses = result.filter(x => {
                    return (x.status_laporan != "LT" && x.status_laporan != "TMSM" && x.status_laporan != "TMSF" && x.status_laporan != "LTRSM" && x.status_laporan != "LTRKS" ) && x.klasifikasi_laporan == "klasifikasi_laporan_berat"
                })

                // @ts-ignore
                const beratTutup = result.filter(x => {
                    return (x.status_laporan == "LT" || x.status_laporan == "TMSM" || x.status_laporan == "TMSF" || x.status_laporan == "LTRSM" || x.status_laporan == "LTRKS" ) && x.klasifikasi_laporan == "klasifikasi_laporan_berat"
                })

                const countSedangProses: Count = {
                    count: sedangProses.length
                }

                const countSedangTutup: Count = {
                    count: sedangTutup.length
                }

                const countSederhanaProses: Count = {
                    count: sederhanaProses.length
                }

                const countSederhanaTutup: Count = {
                    count: sederhanaTutup.length
                }

                const countBeratProses: Count = {
                    count: beratProses.length
                }

                const countBeratTutup: Count = {
                    count: beratTutup.length
                }

                type ChartModel = {
                    name: String;
                    data: number[];
                };

                type ChartModelXaxis = {
                    categories: String[];
                };

                type Chart = {
                    series : ChartModel[];
                    xaxis : ChartModelXaxis;
                };

                let klasifikasi_lap:string[];
                klasifikasi_lap = ["Laporan Sederhana", "Laporan Sedang", "Laporan Berat"]

                const chart1: ChartModel = {
                    name: "Proses",
                    data: [countSederhanaProses.count, countSedangProses.count, countBeratProses.count]
                };
                const chart2: ChartModel = {
                    name: "Ditutup",
                    data: [countSederhanaTutup.count, countSedangTutup.count, countBeratTutup.count]
                };
                const chart4: ChartModelXaxis = {
                    categories : klasifikasi_lap
                }

                let series:ChartModel[];
                series = [chart1, chart2]

                const hasil : Chart = {
                    series : series,
                    xaxis : chart4
                }



                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: hasil
                };
        }


        @get('/jumlah-tugas-asisten')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async jumlahTugasAsisten(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("kantor") kantor: string,
                @param.query.string("team") team: string
            ): Promise<any> {
                const headerAuth = this.request.headers['authorization']

                let roles = []
                if (headerAuth) {
                    const decodedToken = await AuthService.parseJwt(headerAuth)

                    if (decodedToken.roles && decodedToken.roles.length > 0) {
                        roles = decodedToken.roles
                    }
                }

                // console.log(roles);



                let sql = "select * from simpel_4.tx_laporan a "+
                          "join apps_manager.td_user b "+
                          "on a.officer_by = b.id_user "+
                          "where a.is_suspended is null ";


                // @ts-ignore
                let jabatan = roles.find(element => element.indexOf('Anggota Tim KKU Riksa') > -1 || (element.indexOf('Kepala Keasistenan Tim Riksa') > -1 && element !== 'Kepala Keasistenan Tim Riksa Kantor Perwakilan' ) || element.indexOf('Kepala Keasistenan Utama Riksa') > -1  ||  element.indexOf('Resmon') > -1 || element === 'Anggota Tim PVL Kantor Pusat' || element === 'Kepala Keasistenan Utama PVL' || element === 'Kepala Keasistenan PVL Pusat' );


                let grup;
                let riksaUtama;
                let riksaTim;
                let riksaAnggota;

                if(jabatan != null){
                    if(jabatan.indexOf('Riksa') >= 0){
                        grup = jabatan.substring(jabatan.length - 1 , jabatan.length);
                        riksaUtama = "Kepala Keasistenan Utama Riksa " + grup;
                        riksaTim = "Kepala Keasistenan Tim Riksa " + grup;
                        riksaAnggota = "Anggota Tim KKU Riksa " + grup;
                        jabatan = "Riksa";

                    }else if(jabatan.indexOf('Resmon') >= 0){
                        jabatan = "Resmon";

                    }
                }

                let teamGrup;
                let teamRiksaUtama;
                let teamRiksaTim;
                let teamRiksaAnggota;

                if(team != null){
                    if(team.indexOf('Riksa') >= 0){
                        teamGrup = team.substring(team.length - 1 , team.length);
                        teamRiksaUtama = "Kepala Keasistenan Utama Riksa " + teamGrup;
                        teamRiksaTim = "Kepala Keasistenan Tim Riksa " + teamGrup;
                        teamRiksaAnggota = "Anggota Tim KKU Riksa " + teamGrup;
                        team = "Riksa";

                    }else if(team.indexOf('Resmon') >= 0){
                        team = "Resmon";

                    }
                }

                let query = "";



                if(kantor == "JKT" && team != null){
                    if(team == 'Kepala Keasistenan Utama PVL' || team == 'Anggota Tim PVL Kantor Pusat'){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and (c.nama_role = 'Kepala Keasistenan Utama PVL' or c.nama_role = 'Anggota Tim PVL Kantor Pusat' or c.nama_role = 'Kepala Keasistenan PVL Pusat') "+
                            "group by a.nama ";
                    }else if(team == 'Riksa' ){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and (c.nama_role = '"+teamRiksaAnggota+"' or c.nama_role = '"+teamRiksaTim+"' or c.nama_role = '"+teamRiksaUtama+"') "+
                            "group by a.nama ";
                    }else if(team == 'Resmon'){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and c.nama_role like '%Resmon%' "+
                            "group by a.nama ";
                    }
                }else if(kantor != null){
                    query = "select * from apps_manager.td_user where flag_aktivasi = '1' and kode_kantor = '"+kantor+"' ";
                }else{
                    if(jabatan == 'Kepala Keasistenan Utama PVL' || jabatan == 'Anggota Tim PVL Kantor Pusat'){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and (c.nama_role = 'Kepala Keasistenan Utama PVL' or c.nama_role = 'Anggota Tim PVL Kantor Pusat' or c.nama_role = 'Kepala Keasistenan PVL Pusat') "+
                            "group by a.nama ";
                    }else if(jabatan == 'Riksa' ){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and (c.nama_role = '"+riksaAnggota+"' or c.nama_role = '"+riksaTim+"' or c.nama_role = '"+riksaUtama+"') "+
                            "group by a.nama ";
                    }else if(jabatan == 'Resmon'){
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and c.nama_role like '%Resmon%' "+
                            "group by a.nama ";
                    }else{
                        query = "select a.nama, (array_agg(a.id_user)) [1] as id_user from apps_manager.td_user a "+
                            "join apps_manager.td_user_detail b "+
                            "on a.id_user = b.id_user "+
                            "join apps_manager.td_role c "+
                            "on c.id_role = b.id_role "+
                            "where a.flag_aktivasi = '1' and c.nama_role ='"+jabatan+"' "+
                            "group by a.nama ";
                    }
                }

                // console.log(query);


                const currentYear = new Date().getFullYear();

                if(startDate != null && endDate != null){
                    sql = sql +"and a.tgl_agenda between '" +startDate+ "' and '"+ endDate +"' ";
                }else{
                    sql = sql +"and EXTRACT(YEAR FROM a.tgl_agenda) =" +currentYear+" ";
                }
                if(kantor != null){
                    sql = sql +"and b.kode_kantor = '"+kantor+"' ";
                }

                query = query + "ORDER BY nama ";
                sql = sql +"ORDER BY officer_by DESC ";

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                const result2 = await this.txLaporanRepository.dataSource.execute(query);



                type ChartModel = {
                    name: String;
                    data: number[];
                };

                type ChartModelXaxis = {
                    type: String;
                    categories: String[];
                };

                type Chart = {
                    series : ChartModel[];
                    xaxis : ChartModelXaxis;
                };

                const arrProses: number[] = [];
                const arrDitutup: number[] = [];
                const arrName: string[] = [];

                // @ts-ignore
                result2.forEach(x => {
                    arrName.push(x.nama);

                    // @ts-ignore
                    const dataTutup = result.filter( y => {
                        return (y.status_laporan == "LT" || y.status_laporan == "TMSM" || y.status_laporan == "TMSF" || y.status_laporan == "LTRSM" || y.status_laporan == "LTRKS" ) && y.officer_by == x.id_user
                    })

                    // @ts-ignore
                    const dataProses = result.filter( y => {
                        return (y.status_laporan != "LT" && y.status_laporan != "TMSM" && y.status_laporan != "TMSF" && y.status_laporan != "LTRSM" && y.status_laporan != "LTRKS" ) && y.officer_by == x.id_user
                    })

                    const jumProses: Count = {
                        count: dataProses.length
                    }

                    const jumTutup: Count = {
                        count: dataTutup.length
                    }

                    arrDitutup.push(jumTutup.count);
                    arrProses.push(jumProses.count);

                });

                const chartProses: ChartModel = {
                    name: "Proses",
                    data: arrProses
                };

                const chartTutup: ChartModel = {
                    name: "Ditutup",
                    data: arrDitutup
                };

                const chartXaxis: ChartModelXaxis = {
                    type : "Asisten",
                    categories : arrName
                };

                let seriess:ChartModel[];
                seriess = [chartProses, chartTutup]

                const hasil: Chart = {
                    series : seriess,
                    xaxis : chartXaxis
                };

                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: hasil
                };
        }

        @get('/sebaran-laporan')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async sebaranLaporan(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("key") key: string,
                @param.query.string("kantor") kantor: string,
                @param.query.string("pokokPermasalahan") pokokPermasalahan: string,
                @param.query.string("triwulan") triwulan: string
            ): Promise<any> {

                let sql = "";
                let query = "";
                const currentYear = new Date().getFullYear();
                if(startDate != null && endDate != null){
                    startDate = startDate;
                    endDate = endDate;
                }else{
                    startDate = currentYear.toString();
                    endDate = currentYear.toString();
                }
                let monthStart = 1;
                let monthEnd = 12;

                if(triwulan != null){
                    if(triwulan == "1"){
                        monthStart = 1;
                        monthEnd = 3;
                    }else if(triwulan == "2"){
                        monthStart = 4;
                        monthEnd = 6;
                    }else if(triwulan == "3"){
                        monthStart = 7;
                        monthEnd = 9;
                    }else if(triwulan == "4"){
                        monthStart = 10;
                        monthEnd = 12;
                    }
                }


                if(key == "cara_penyampaian"){
                    sql = "select b.lookup_name,b.lookup_code, EXTRACT(YEAR FROM a.tgl_agenda) as tahun from simpel_4.tx_laporan a "+
                          "join referensi.m_lookup b "+
                          "on a.cara_penyampaian = b.lookup_code "+
                          "where a.is_suspended is null "+
                          "and EXTRACT(YEAR FROM a.tgl_agenda) between '" +startDate+ "' and '"+ endDate +"' ";

                    query = "select * from referensi.m_lookup where lookup_group_name = '"+ key +"' order by lookup_code";

                }else if(key == "dugaan_maladministrasi"){
                    sql = "select b.lookup_name,b.lookup_code, EXTRACT(YEAR FROM a.tgl_agenda) as tahun from simpel_4.tx_laporan a "+
                          "join simpel_4.tx_riksa c "+
                          "on a.id = c.id_tx_laporan "+
                          "join referensi.m_lookup b "+
                          "on b.lookup_code = c.dugaan_maladministrasi "+
                          "where a.is_suspended is null "+
                          "and EXTRACT(YEAR FROM a.tgl_agenda) between '" +startDate+ "' and '"+ endDate +"' ";

                          query = "select * from referensi.m_lookup where lookup_group_name = '"+ key +"' order by lookup_code";
                }else if(key == "tipe_laporan"){
                    sql = "select b.lookup_name,b.lookup_code, EXTRACT(YEAR FROM a.tgl_agenda) as tahun from simpel_4.tx_laporan a "+
                          "join referensi.m_lookup b "+
                          "on a.tipe_laporan = b.lookup_code "+
                          "where a.is_suspended is null "+
                          "and EXTRACT(YEAR FROM a.tgl_agenda) between '" +startDate+ "' and '"+ endDate +"' ";

                    query = "select * from referensi.m_lookup where lookup_group_name = '"+ key +"' order by lookup_code";

                }else if(key == "kelompok_instansi"){
                    sql = "select b.lookup_name,b.lookup_code, EXTRACT(YEAR FROM a.tgl_agenda) as tahun from simpel_4.tx_laporan a "+
                          "join referensi.m_lookup b "+
                          "on a.kelompok_instansi_terlapor = b.lookup_code "+
                          "where a.is_suspended is null "+
                          "and EXTRACT(YEAR FROM a.tgl_agenda) between '" +startDate+ "' and '"+ endDate +"' ";

                    query = "select * from referensi.m_lookup where lookup_group_name = '"+ key +"' order by lookup_code";

                }else if(pokokPermasalahan == "pokok_permasalahan"){
                    sql = "select a.pokok_masalah as lookup_code ,EXTRACT(YEAR FROM a.tgl_agenda) as tahun from simpel_4.tx_laporan a "+
                          "where a.is_suspended is null "+
                          "and EXTRACT(YEAR FROM a.tgl_agenda) between '" +startDate+ "' and '"+ endDate +"' ";

                          query = "select * from referensi.m_lookup where lookup_group_name = '"+ pokokPermasalahan +"' and parent_key = '"+ key+"' order by lookup_code";
                }

                if(triwulan != null){
                    sql = sql + "and EXTRACT(MONTH FROM a.tgl_agenda) between '" +monthStart+ "' and '"+ monthEnd +"' ";
                }

                if(kantor != null){
                    sql = sql + "and a.kode_kantor = '" + kantor + "' ";
                }

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                const lookup = await this.txLaporanRepository.dataSource.execute(query);


                const numberStartDate = Number(startDate);
                const numberEndDate = Number(endDate);

                type ChartModel = {
                    name: String;
                    data: number[];
                };

                type ChartModelXaxis = {
                    type: String;
                    categories: String[];
                };

                type Chart = {
                    series : ChartModel[];
                    xaxis : ChartModelXaxis;
                };

                const arrCount: number[] = [];
                const arrName: string[] = [];

                const seriess:ChartModel[] = [];


                for (let index = numberStartDate; index <= numberEndDate; index++) {
                    // @ts-ignore
                    const tamp = result.filter(x => {
                        return x.tahun == index
                    })

                    arrCount.splice(0, lookup.length);
                    arrName.splice(0, lookup.length);

                    const hakk = tamp;

                    // @ts-ignore
                    lookup.forEach(z => {
                        arrName.push(z.lookup_name);
                        // @ts-ignore
                        const dataPerAkses = hakk.filter(y => {
                            return y.lookup_code == z.lookup_code
                        })

                        const countPerAkses: Count = {
                            count: dataPerAkses.length
                        }
                        arrCount.push(countPerAkses.count);

                        });

                    const arr: number[] = [];
                    arrCount.forEach( c => {
                        arr.push(c)
                    })

                    const chartOne: ChartModel = {
                        name: index.toString(),
                        data: arr
                    };

                    seriess.push(chartOne);

                }

                const xaxis: ChartModelXaxis = {
                    type : "Akses",
                    categories : arrName
                }

                const hasil : Chart = {
                    series : seriess,
                    xaxis : xaxis
                }

                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data : hasil
                };
        }

        @get('/laporan-status-penyelesaian')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async laporanStatusPenyelesaian(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const headerAuth = this.request.headers['authorization']
                let kodeKantor : string | undefined;
                if (headerAuth) {
                    const decodedToken = await AuthService.parseJwt(headerAuth)

                    const id = decodedToken.user.id;
                    const result = await this.tdUserRepository.findById(id);

                    const a = result.kodeKantor;
                    kodeKantor = a;

                }


                const currentYear = new Date().getFullYear();
                const sql = `SELECT
                                D.lookup_name
                            --    , E.id
                                ,SUM (CASE WHEN E.id IS NOT NULL AND (E.alasan_penutupan = D.tipe_laporan or E.alasan_pencabutan = D.tipe_laporan) THEN 1 ELSE 0 END) AS count
                            FROM
                            (SELECT
                                C.lookup_name,
                                C.lookup_code as tipe_laporan FROM referensi.m_lookup C
                                WHERE lower(C.lookup_code) like '%alasan_penutup%' or lower(lookup_code) like '%alasan_pencabutan%'
                            ) AS D
                            LEFT JOIN (
                                select G.id, (array_agg(H.alasan_penutupan)) [1] as alasan_penutupan, (array_agg(H.alasan_pencabutan)) [1] as alasan_pencabutan FROM simpel_4.tx_laporan G
                                left JOIN simpel_4.tx_penutupan_laporan H ON H.id_tx_laporan = G.id
                                where G.status is not null and (H.alasan_penutupan is not null or H.alasan_pencabutan is not null)
                                ${startDate != null && endDate != null ?
                                                    `AND G.created_date between '${startDate}' AND '${endDate}' `
                                        : `AND EXTRACT(YEAR FROM G.created_date) = ${currentYear} `
                                    }
                                AND G.kode_kantor = '${kodeKantor}'
                                group by G.id
                            ) E ON E.alasan_penutupan = D.tipe_laporan
                            GROUP BY D.lookup_name
                            ORDER BY D.lookup_name`;

                const result = await this.txLaporanRepository.dataSource.execute(sql);


                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data : result
                };
        }


        @get('/laporan-antar-kota-priode')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async laporanAntarKota(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("kantor1") kantor1: string,
                @param.query.string("kantor2") kantor2: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear()
                const sql = `SELECT
                        A.lookup_name,
                        A.nama_kantor AS kode_kantor,
                        SUM (CASE WHEN E.id IS NOT NULL AND E.tipe_laporan = A.lookup_code AND A.kode_kantor = E.kode_kantor THEN 1 ELSE 0 END) AS count
                    FROM (
                        SELECT B.lookup_name, B.lookup_code, C.kode_kantor, C.nama_kantor FROM referensi.m_lookup B
                        CROSS JOIN (SELECT D.kode_kantor, D.nama_kantor FROM referensi.m_kantor D WHERE (D.kode_kantor = '${kantor1}' ${kantor2 != null ? `OR D.kode_kantor = '${kantor2}'` : ''})) C
                        WHERE B.lookup_code IN ('IN','LM','RCO')
                    ) A
                    LEFT JOIN (
                        SELECT F.* FROM simpel_4.tx_laporan F

                        ${penyelesaian != null ?
                            `JOIN simpel_4.tx_penutupan_laporan G ON G.id_tx_laporan = F.id AND (G.alasan_penutupan = '${penyelesaian}' OR G.alasan_pencabutan = '${penyelesaian}')`
                            : ''
                        }

                        ${startDate != null && endDate != null ?
                            `WHERE DATE(F.created_date) BETWEEN '${startDate}' AND '${endDate}'`
                            : `WHERE EXTRACT(YEAR FROM F.created_date) = ${currentYear}`
                        }
                    ) E ON E.tipe_laporan = A.lookup_code
                    GROUP BY A.lookup_name, A.nama_kantor
                    ORDER BY A.lookup_name`;

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: result
                };
        }


        @get('/laporan-antar-riksa')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async flaporanAntarRiksa(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear();

                const sql = `select  v.tipe as role,
                SUM ( v.RCO  ) AS RCO,
                SUM ( v.LM  ) AS LM,
                SUM ( v.IN  ) AS in,
                SUM ( v.Jumlah ) as Jumlah
        from(
            select
                        case
                            when lower(a.role) like '%riksa 1' then 'R1'
                             when lower(a.role) like '%riksa 2' then 'R2'
                             when lower(a.role) like '%riksa 3' then 'R3'
                             when lower(a.role) like '%riksa 4' then 'R4'
                             when lower(a.role) like '%riksa 5' then 'R5'
                             when lower(a.role) like '%riksa 6' then 'R6'
                             when lower(a.role) like '%riksa 7' then 'R7'
                             when lower(a.role) like '%pvl%' then 'R8'
                             when lower(a.role) like '%resmon%' then 'R9'
                         end as tipe,
                        A.role,
                        SUM ( CASE WHEN B.tipe_laporan = 'RCO' THEN 1 ELSE 0 END ) AS RCO,
                        SUM ( CASE WHEN B.tipe_laporan = 'LM' THEN 1 ELSE 0 END ) AS LM,
                        SUM ( CASE WHEN B.tipe_laporan = 'IN' THEN 1 ELSE 0 END ) AS IN,
                        COUNT ( B.id_tx_laporan ) AS Jumlah
                    FROM
                        (
                        SELECT
                            A.id_user AS id_user,
                            A.nama AS nama_user,
                            ( ARRAY_AGG ( C.nama_role ) ) [ 1 ] AS role
                        FROM
                            apps_manager.td_user
                            A JOIN apps_manager.td_user_detail b ON A.id_user = b.id_user
                            JOIN apps_manager.td_role C ON C.id_role = b.id_role
                        WHERE
                            C.nama_role LIKE'%Anggota Tim PVL%'
                            OR (C.nama_role LIKE'%Kepala Keasistenan %' and c.nama_role != 'Kepala Keasistenan Tim Riksa Kantor Perwakilan')
                            OR C.nama_role LIKE'%Kepala Keasistenan Utama PVL%'
                            OR C.nama_role LIKE'%Anggota Tim KKU Riksa%'
                            OR (C.nama_role LIKE'%Kepala Keasistenan Tim Riksa%' and c.nama_role != 'Kepala Keasistenan Tim Riksa Kantor Perwakilan')
                            OR C.nama_role LIKE'%Resmon%'
                        GROUP BY
                            A.id_user
                        )
                        AS A LEFT JOIN (
                        SELECT A
                            .ID AS id_tx_laporan,
                            A.officer_by,
                            A.tipe_laporan,
                            B.lookup_name
                        FROM
                            simpel_4.tx_laporan
                            A JOIN referensi.m_lookup B ON B.lookup_code = A.tipe_laporan
                            AND A.tipe_laporan IN ( 'IN', 'LM', 'RCO' )
                            and A.is_suspended is null
                            ${startDate != null && endDate != null ?
                                `and A.tgl_agenda between '${startDate}' AND '${endDate}'`
                                : `and EXTRACT(YEAR FROM A.tgl_agenda) = ${currentYear}`
                            }
                        ) B ON B.officer_by = A.id_user
                        GROUP BY A.role
            ) v
            group by v.tipe`

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                let namaRole:string[];
                namaRole = ["Keasistenan Utama Riksa 1", "Keasistenan Utama Riksa 2", "Keasistenan Utama Riksa 3", "Keasistenan Utama Riksa 4", "Keasistenan Utama Riksa 5", "Keasistenan Utama Riksa 6", "Keasistenan Utama Riksa 7", "Tim PVL", "Tim Resmon"]

                let tipeLaporan:string[];
                tipeLaporan = ["Respon Cepat", "Laporan Masyarakat", "Investigasi atas Prakarsa Sendiri" ];

                type ChartModel = {
                    nama: String;
                    lookupName: String;
                    count : number;
                };

                const series:ChartModel[] = [];


                for (let a = 0; a < result.length; a++) {
                    for (let b = 0; b < 3; b++) {
                        const nama = namaRole[a];
                        const tipe = tipeLaporan[b];
                        let jumlah = 0;
                        if(b == 0){
                            jumlah = result[a].rco;
                        }else if(b == 1){
                            jumlah = result[a].lm;
                        }else if(b == 2){
                            jumlah = result[a].in;
                        }

                        const chart1: ChartModel = {
                            nama : nama,
                            lookupName : tipe,
                            count : jumlah
                        };

                        series.push(chart1);



                      }
                }


                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: series
                };
        }

        @get('/laporan-antar-kota-tahun')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async laporanAntarKotaTahun(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("kantor1") kantor1: string,
                @param.query.string("kantor2") kantor2: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear()
                const sql = `SELECT
                        A.lookup_name,
                        A.nama_kantor AS kode_kantor,
                        SUM (CASE WHEN E.id IS NOT NULL AND E.tipe_laporan = A.lookup_code AND A.kode_kantor = E.kode_kantor THEN 1 ELSE 0 END) AS count
                    FROM (
                        SELECT B.lookup_name, B.lookup_code, C.kode_kantor, C.nama_kantor FROM referensi.m_lookup B
                        CROSS JOIN (SELECT D.kode_kantor, D.nama_kantor FROM referensi.m_kantor D WHERE (D.kode_kantor = '${kantor1}' ${kantor2 != null ? `OR D.kode_kantor = '${kantor2}'` : ''})) C
                        WHERE B.lookup_code IN ('IN','LM','RCO')
                    ) A
                    LEFT JOIN (
                        SELECT F.* FROM simpel_4.tx_laporan F

                        ${penyelesaian != null ?
                            `JOIN simpel_4.tx_penutupan_laporan G ON G.id_tx_laporan = F.id AND (G.alasan_penutupan = '${penyelesaian}' OR G.alasan_pencabutan = '${penyelesaian}')`
                            : ''
                        }

                        ${startDate != null && endDate != null ?
                            `WHERE EXTRACT(YEAR FROM F.created_date) BETWEEN '${startDate}' AND '${endDate}'`
                            : `WHERE EXTRACT(YEAR FROM F.created_date) = ${currentYear}`
                        }
                    ) E ON E.tipe_laporan = A.lookup_code
                    GROUP BY A.lookup_name, A.nama_kantor
                    ORDER BY A.lookup_name`;

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: result
                };
        }


        @get('/laporan-ditutup-maladministrasi')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async laporanDitutupMaladministrasi(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("kantor") kantor: string,
                @param.query.string("ku") ku: string
            ): Promise<any> {
                 // const currentYear = new Date().getFullYear()
                //  const headerAuth = this.request.headers['authorization']
                //  let kodeKantor : string | undefined;
                //  if (headerAuth) {
                //      const decodedToken = await AuthService.parseJwt(headerAuth)

                //      const id = decodedToken.user.id;
                //      const result = await this.tdUserRepository.findById(id);

                //       const a = result.kodeKantor;
                //      kodeKantor = a;

                //  }

                const currentYear = new Date().getFullYear();

                let join = "where a.id in (select a.id from simpel_4.tx_laporan a left join simpel_4.tx_pleno c on a.id  = c.id_tx_laporan group by a.id) ";

                 let sql = "select b.putusan_dugaan_maladministrasi, "+
                             "count(a.id) from simpel_4.tx_laporan a "+
                             "join simpel_4.tx_riksa b "+
                             "on a.id = b.id_tx_laporan ";

                if(ku != null){
                    join = "where a.id in (select a.id from simpel_4.tx_laporan a left join simpel_4.tx_pleno c on a.id = c.id_tx_laporan where c.tim_pemeriksaan = '" + ku + "' group by a.id) ";
                    sql = sql + join;
                }else{
                    sql = sql + join;
                }

                sql = sql + "and b.putusan_dugaan_maladministrasi is not null ";

                if(startDate != null && endDate != null){
                    sql = sql +"and a.tgl_agenda between '" +startDate+ "' and '"+ endDate +"' ";
                }else{
                    sql = sql +"and EXTRACT(YEAR FROM a.tgl_agenda) =" +currentYear+" ";
                }

                 if(kantor != null){
                     sql = sql + "and a.kode_kantor = '"+kantor+"' ";
                 }

                 sql = sql + "group by b.putusan_dugaan_maladministrasi order by b.putusan_dugaan_maladministrasi ";


                const result = await this.txLaporanRepository.dataSource.execute(sql);


                type ChartModel = {
                    status: String;
                    jumlah: number;
                };

                let satu = 0;
                let dua = 0;
                let tiga = 0;
                // @ts-ignore
                result.forEach(x => {
                    if(x.putusan_dugaan_maladministrasi == '1'){
                        satu = x.count
                    }else if (x.putusan_dugaan_maladministrasi == '2'){
                        dua = x.count
                    }else if (x.putusan_dugaan_maladministrasi == '3'){
                        tiga = x.count
                    }


                });

                const series:ChartModel[] = [];
                const listNumber : number[] = [satu, dua, tiga];
                const listName : string[] = ["Ada Maladministrasi", "Tidak Ada Maladministrasi", "Terdapat Maladministrasi Tetapi Sudah Diselesaikan"];

                for (let index = 0; index < 3; index++) {
                    const data: ChartModel = {
                        status: listName[index],
                        jumlah: listNumber[index]
                    };
                    series.push(data);

                }

                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: series
                };
        }

        @get('/laporan-antar-riksa-kolom')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async flaporanAntarRiksaKolom(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear();

                const sql = `select  v.tipe as role,
                            SUM ( v.proses  ) AS proses,
                            SUM ( v.tutup  ) AS tutup,
                            SUM ( v.Jumlah ) as Jumlah
                        from(
                        select
                        case
                            when lower(a.role) like '%riksa 1' then 'R1'
                             when lower(a.role) like '%riksa 2' then 'R2'
                             when lower(a.role) like '%riksa 3' then 'R3'
                             when lower(a.role) like '%riksa 4' then 'R4'
                             when lower(a.role) like '%riksa 5' then 'R5'
                             when lower(a.role) like '%riksa 6' then 'R6'
                             when lower(a.role) like '%riksa 7' then 'R7'
                             when lower(a.role) like '%pvl%' then 'R8'
                             when lower(a.role) like '%resmon%' then 'R9'
                         end as tipe,
                        A.role,
                        SUM ( CASE
                                  WHEN B.status_laporan != 'LT'
                                  AND B.status_laporan != 'TMSM'
                                  AND B.status_laporan != 'TMSF'
                                  AND B.status_laporan != 'LTRSM'
                                  AND B.status_laporan != 'LTRKS'
                                  THEN 1 ELSE 0
                                END ) AS proses,
                        SUM ( CASE
                                  WHEN B.status_laporan = 'LT'   THEN 1
                                  WHEN B.status_laporan = 'TMSM' THEN 1
                                  WHEN B.status_laporan = 'TMSF'   THEN 1
                                  WHEN B.status_laporan = 'LTRSM' THEN 1
                                  WHEN B.status_laporan = 'LTRKS'   THEN 1
                                ELSE 0
                                END ) AS tutup,
                        COUNT ( B.id_tx_laporan ) AS Jumlah
                    FROM
                        (
                        SELECT
                            A.id_user AS id_user,
                            A.nama AS nama_user,
                            ( ARRAY_AGG ( C.nama_role ) ) [ 1 ] AS role
                        FROM
                            apps_manager.td_user
                            A JOIN apps_manager.td_user_detail b ON A.id_user = b.id_user
                            JOIN apps_manager.td_role C ON C.id_role = b.id_role
                        WHERE
                            C.nama_role LIKE'%Anggota Tim PVL%'
                            OR (C.nama_role LIKE'%Kepala Keasistenan %' and c.nama_role != 'Kepala Keasistenan Tim Riksa Kantor Perwakilan')
                            OR C.nama_role LIKE'%Kepala Keasistenan Utama PVL%'
                            OR C.nama_role LIKE'%Anggota Tim KKU Riksa%'
                            OR (C.nama_role LIKE'%Kepala Keasistenan Tim Riksa%' and c.nama_role != 'Kepala Keasistenan Tim Riksa Kantor Perwakilan')
                            OR C.nama_role LIKE'%Resmon%'
                        GROUP BY
                            A.id_user
                        )
                        AS A LEFT JOIN (
                        SELECT A
                            .ID AS id_tx_laporan,
                            A.officer_by,
                            A.tipe_laporan,
                            A.status_laporan,
                            B.lookup_name
                        FROM
                            simpel_4.tx_laporan
                            A JOIN referensi.m_lookup B ON B.lookup_code = A.tipe_laporan
                            AND A.tipe_laporan IN ( 'IN', 'LM', 'RCO' )
                            and A.is_suspended is null
                            ${startDate != null && endDate != null ?
                                `and A.tgl_agenda between '${startDate}' AND '${endDate}'`
                                : `and EXTRACT(YEAR FROM A.tgl_agenda) = ${currentYear}`
                            }
                        ) B ON B.officer_by = A.id_user
                        GROUP BY A.role
                       ) v
                        group by v.tipe`

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                let namaRole:string[];
                namaRole = ["Keasistenan Utama Riksa 1", "Keasistenan Utama Riksa 2", "Keasistenan Utama Riksa 3", "Keasistenan Utama Riksa 4", "Keasistenan Utama Riksa 5", "Keasistenan Utama Riksa 6", "Keasistenan Utama Riksa 7", "Tim PVL", "Tim Resmon"]

                type ChartModel = {
                    name: String;
                    processed: number;
                    closed : number;
                    sum : number;
                };

                const series:ChartModel[] = [];


                for (let a = 0; a < result.length; a++) {

                    const nama = namaRole[a];

                    const chart1: ChartModel = {
                        name : nama,
                        processed : result[a].proses,
                        closed : result[a].tutup,
                        sum :result[a].jumlah
                    };

                    series.push(chart1);


                }
                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: series
                };
        }

        @get('/laporan-antar-kantor-perwakilan')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async flaporanAntarKantorPerwakilan(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear();

                const sql = `select
                A.nama_kantor as kantor,
                SUM ( CASE WHEN B.tipe_laporan = 'RCO' THEN 1 ELSE 0 END ) AS RCO,
                SUM ( CASE WHEN B.tipe_laporan = 'LM' THEN 1 ELSE 0 END ) AS LM,
                SUM ( CASE WHEN B.tipe_laporan = 'IN' THEN 1 ELSE 0 END ) AS IN,
                COUNT ( B.id_tx_laporan ) AS Jumlah
            FROM
                (
                SELECT
                    A.kode_kantor,
                    A.nama_kantor,
                    A.id_provinsi
                    from referensi.m_kantor A
                    where A.kode_kantor != 'JKT'
                    group by a.kode_kantor
                    order by A.id_provinsi
                )
                AS A LEFT JOIN (
                SELECT A
                    .ID AS id_tx_laporan,
                    A.kode_kantor,
                    A.tipe_laporan,
                    B.lookup_name
                FROM
                    simpel_4.tx_laporan
                    A JOIN referensi.m_lookup B ON B.lookup_code = A.tipe_laporan
                    AND A.tipe_laporan IN ( 'IN', 'LM', 'RCO' )
                    and A.is_suspended is null
                    ${startDate != null && endDate != null ?
                        `and A.tgl_agenda between '${startDate}' AND '${endDate}'`
                        : `and EXTRACT(YEAR FROM A.tgl_agenda) = ${currentYear}`
                    }
                ) B ON B.kode_kantor = A.kode_kantor
                GROUP BY A.nama_kantor`

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                let tipeLaporan:string[];
                tipeLaporan = ["Respon Cepat", "Laporan Masyarakat", "Investigasi atas Prakarsa Sendiri" ];

                type ChartModel = {
                    nama: String;
                    lookupName: String;
                    count : number;
                };

                const series:ChartModel[] = [];


                for (let a = 0; a < result.length; a++) {
                    const b = a;
                    for (let b = 0; b < 3; b++) {
                        const nama = result[a].kantor;
                        const tipe = tipeLaporan[b];
                        let jumlah = 0;
                        if(b == 0){
                            jumlah = result[a].rco;
                        }else if(b == 1){
                            jumlah = result[a].lm;
                        }else if(b == 2){
                            jumlah = result[a].in;
                        }

                        const chart1: ChartModel = {
                            nama : nama,
                            lookupName : tipe,
                            count : jumlah
                        };

                        series.push(chart1);

                      }
                }


                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: series
                };
        }

        @get('/laporan-antar-kantor-perwakilan-kolom')
        @response(200, {
            description: 'TxLaporan model by token',
            content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
                },
            })
            async flaporanAntarKotaPerwakilanKolom(
                @param.query.string("startDate") startDate: string,
                @param.query.string("endDate") endDate: string,
                @param.query.string("penyelesaian") penyelesaian: string
            ): Promise<any> {
                const currentYear = new Date().getFullYear();

                const sql = `select
                A.nama_kantor as kantor,
                SUM ( CASE
                          WHEN B.status_laporan != 'LT'
                          AND B.status_laporan != 'TMSM'
                          AND B.status_laporan != 'TMSF'
                          AND B.status_laporan != 'LTRSM'
                          AND B.status_laporan != 'LTRKS'
                          THEN 1 ELSE 0
                        END ) AS proses,
                SUM ( CASE
                          WHEN B.status_laporan = 'LT'   THEN 1
                          WHEN B.status_laporan = 'TMSM' THEN 1
                          WHEN B.status_laporan = 'TMSF'   THEN 1
                          WHEN B.status_laporan = 'LTRSM' THEN 1
                          WHEN B.status_laporan = 'LTRKS'   THEN 1
                        ELSE 0
                        END ) AS tutup,
                COUNT ( B.id_tx_laporan ) AS Jumlah
            FROM
                (
                SELECT
                    A.kode_kantor,
                    A.nama_kantor,
                    A.id_provinsi
                    from referensi.m_kantor A
                    where A.kode_kantor != 'JKT'
                    group by a.kode_kantor
                    order by A.id_provinsi
                )
                AS A LEFT JOIN (
                SELECT
                    A.ID AS id_tx_laporan,
                    A.kode_kantor,
                    A.tipe_laporan,
                    B.lookup_name,
                    A.status_laporan
                FROM
                    simpel_4.tx_laporan
                    A JOIN referensi.m_lookup B ON B.lookup_code = A.tipe_laporan
                    AND A.tipe_laporan IN ( 'IN', 'LM', 'RCO' )
                    and A.is_suspended is null
                    ${startDate != null && endDate != null ?
                        `and A.tgl_agenda between '${startDate}' AND '${endDate}'`
                        : `and EXTRACT(YEAR FROM A.tgl_agenda) = ${currentYear}`
                    }
                ) B ON B.kode_kantor = A.kode_kantor
                GROUP BY A.nama_kantor`

                const result = await this.txLaporanRepository.dataSource.execute(sql);

                type ChartModel = {
                    name: String;
                    processed: number;
                    closed : number;
                    sum : number;
                };

                const series:ChartModel[] = [];


                for (let a = 0; a < result.length; a++) {
                    const c = a;
                    const nama = result[a].kantor;

                    const chart1: ChartModel = {
                        name : nama,
                        processed : result[a].proses,
                        closed : result[a].tutup,
                        sum :result[a].jumlah
                    };

                    series.push(chart1);


                }
                return {
                    status: true,
                    message: "Data berhasil ditampilkan",
                    data: series
                };
        }

}
