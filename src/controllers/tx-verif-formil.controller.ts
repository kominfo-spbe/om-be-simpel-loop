import {inject, service} from "@loopback/core";
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {TxVerifFormil} from '../models';
import {TxVerifFormilRepository} from '../repositories';
import {LogService, PvlService} from "../services";

export class TxVerifFormilController {
  constructor(
    @repository(TxVerifFormilRepository)
    public txVerifFormilRepository: TxVerifFormilRepository,
    @inject('services.PvlService')
    public pvlService: PvlService,

    @service(LogService) public logService: LogService
  ) { }

  @post('/tx-verif-formil')
  @response(200, {
    description: 'TxVerifFormil model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxVerifFormil)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifFormil, {
            title: 'NewTxVerifFormil',
            exclude: ['id'],
          }),
        },
      },
    })
    txVerifFormil: Omit<TxVerifFormil, 'id'>,
  ): Promise<{data: TxVerifFormil; message: string; status: boolean}> {

    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: await this.txVerifFormilRepository.create(txVerifFormil)
    }

    //send to log


    return rs;
  }

  @get('/tx-verif-formil/count')
  @response(200, {
    description: 'TxVerifFormil model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxVerifFormil) where?: Where<TxVerifFormil>,
  ): Promise<Count> {
    return this.txVerifFormilRepository.count(where);
  }

  @get('/tx-verif-formil')
  @response(200, {
    description: 'Array of TxVerifFormil model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxVerifFormil, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxVerifFormil) filter?: Filter<TxVerifFormil>,
  ): Promise<object> {

    let listUraian = ['kriteria_pelapor', 'kategori_pelapor', 'klasifikasi_pelapor', 'status_verif_formil']
    let resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txVerifFormilRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  @get('/tx-verif-formil/{id}')
  @response(200, {
    description: 'TxVerifFormil model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxVerifFormil, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxVerifFormil, {exclude: 'where'}) filter?: FilterExcludingWhere<TxVerifFormil>
  ): Promise<object> {

    let listUraian = ['kriteria_pelapor', 'kategori_pelapor', 'klasifikasi_pelapor', 'status_verif_formil']
    let resultData = await this.pvlService.getMLookUpFindById(id, filter, listUraian, this.txVerifFormilRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  @patch('/tx-verif-formil/{id}')
  @response(204, {
    description: 'TxVerifFormil PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifFormil, {partial: true}),
        },
      },
    })
    txVerifFormil: TxVerifFormil,
  ): Promise<{data: any; message: string; status: boolean}> {
    await this.txVerifFormilRepository.updateById(id, txVerifFormil);

    return {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txVerifFormilRepository.findById(id)
    };
  }

  @del('/tx-verif-formil/{id}')
  @response(204, {
    description: 'TxVerifFormil DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txVerifFormilRepository.deleteById(id);
  }

  // @patch('/tx-verif-formil')
  // @response(200, {
  //   description: 'TxVerifFormil PATCH success count',
  //   content: {'application/json': {schema: CountSchema}},
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(TxVerifFormil, {partial: true}),
  //       },
  //     },
  //   })
  //   txVerifFormil: TxVerifFormil,
  //   @param.where(TxVerifFormil) where?: Where<TxVerifFormil>,
  // ): Promise<Count> {
  //   return this.txVerifFormilRepository.updateAll(txVerifFormil, where);
  // }

  // @put('/tx-verif-formil/{id}')
  // @response(204, {
  //   description: 'TxVerifFormil PUT success',
  // })
  // async replaceById(
  //   @param.path.string('id') id: string,
  //   @requestBody() txVerifFormil: TxVerifFormil,
  // ): Promise<void> {
  //   await this.txVerifFormilRepository.replaceById(id, txVerifFormil);
  // }
}
