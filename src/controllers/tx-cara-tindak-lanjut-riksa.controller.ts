import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  oas,
  param,
  patch,
  post,
  Request,
  requestBody,
  Response,
  response,
  RestBindings
} from '@loopback/rest';
import {TxCaraTindakLanjutRiksa, TxDokumen} from '../models';
import {TxCaraTindakLanjutRiksaRepository, TxDokumenRepository, TxLaporanRepository} from '../repositories';

import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from '../keys';
import {FileUploadHandler} from '../types';

import multer from 'multer';

import {inject, service} from "@loopback/core";
import path from "path";
import fs from "fs";
import https from "https";
import {listeners} from "cluster";
import {GenerateFileService} from "../services";

export class TxCaraTindakLanjutRiksaController {
  constructor(
    @repository(TxCaraTindakLanjutRiksaRepository)
    public TxCaraTindakLanjutRiksaRepository : TxCaraTindakLanjutRiksaRepository,
    @repository(TxDokumenRepository)
    public txDokumenRepository: TxDokumenRepository,
    @repository(TxLaporanRepository)
    public txLaporanRepository: TxLaporanRepository,
    @service(GenerateFileService)
    public dokumenService: GenerateFileService,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @inject(STORAGE_DIRECTORY) private storageDirectory: string,
    @inject(RestBindings.Http.REQUEST) private request: Request
  ) {}

  @post('/tx-cara-tindak-lanjut-riksa')
  @response(200, {
    description: 'TxCaraTindakLanjutRiksa model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxCaraTindakLanjutRiksa)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {
            title: 'newTxCaraTindakLanjutRiksa',
            exclude: ['id'],
          }),
        },
      },
    })
    TxCaraTindakLanjutRiksa: Omit<TxCaraTindakLanjutRiksa, 'id'>,
  ): Promise<{ data: TxCaraTindakLanjutRiksa; message: string; status: boolean }> {

    const dataAll = await this.TxCaraTindakLanjutRiksaRepository.find()
    let idUpdate  = ""

    let laporan = await this.txLaporanRepository.findById(TxCaraTindakLanjutRiksa.id_tx_laporan);
    if (laporan.kode_kantor !== 'JKT') {
      TxCaraTindakLanjutRiksa.jenis_surat += '_perwakilan';
    }
    dataAll.forEach(x => {
      if(TxCaraTindakLanjutRiksa["id_tx_laporan"] == x.id_tx_laporan && TxCaraTindakLanjutRiksa["jenis_surat"] == x.jenis_surat)
      {
        idUpdate = x.id
      }

    });
    let hasil : any ;
    let massage : string ;
    if (idUpdate == "") {
      hasil = await this.TxCaraTindakLanjutRiksaRepository.create(TxCaraTindakLanjutRiksa)
      massage = "Data berhasil ditambahkan"
    } else {
      await this.TxCaraTindakLanjutRiksaRepository.updateById(idUpdate, TxCaraTindakLanjutRiksa)
      hasil = await this.TxCaraTindakLanjutRiksaRepository.findById(idUpdate)
      massage = "Data berhasil diubah"
    }

    return await this.getFileFromReport(TxCaraTindakLanjutRiksa.jenis_surat, laporan.id, laporan.status_laporan ?? "", laporan.no_agenda?? "", laporan.kode_kantor ?? "").then(() => {
      return {
            status: true,
            message: massage,
            data: hasil
      }
    }).catch(() => {
      return {
        status: true,
        message: massage,
        data: hasil
      }
    });
  }

  @get('/tx-cara-tindak-lanjut-riksa')
    @response(200, {
        description: 'Array of TxCaraTindakLanjutRiksa model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxCaraTindakLanjutRiksa, {includeRelations: true}),
                },
            },
        },
    })
    async find(
      @param.filter(TxCaraTindakLanjutRiksa) filter?: Filter<TxCaraTindakLanjutRiksa>,
    ): Promise<{ data: (TxCaraTindakLanjutRiksa)[]; message: string; status: boolean }> {
    let param: any = filter;
    let jenisSurat = param.where.jenis_surat;
    let idTxLaporan = param.where.id_tx_laporan;
    let laporan = await this.txLaporanRepository.findById(idTxLaporan);
    if (laporan != null && laporan.kode_kantor !== "JKT") {
      jenisSurat += "_perwakilan";
      param.where.jenis_surat = jenisSurat;
    }
      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxCaraTindakLanjutRiksaRepository.find(param)
      };

    }

  @get('/tx-cara-tindak-lanjut-riksa/{id}')
  @response(200, {
    description: 'TxCaraTindakLanjutRiksa model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxCaraTindakLanjutRiksa, {exclude: 'where'}) filter?: FilterExcludingWhere<TxCaraTindakLanjutRiksa>
  ): Promise<{ data: TxCaraTindakLanjutRiksa; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxCaraTindakLanjutRiksaRepository.findById(id, filter)
      };
  }

  @patch('/tx-cara-tindak-lanjut-riksa/{id}')
  @response(204, {
    description: 'TxCaraTindakLanjutRiksa PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {partial: true}),
        },
      },
    })
    txRiksaMonitoring: TxCaraTindakLanjutRiksa,
  ): Promise<{ data: any; message: string; status: boolean }> {

      await this.TxCaraTindakLanjutRiksaRepository.updateById(id, txRiksaMonitoring)
      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.TxCaraTindakLanjutRiksaRepository.findById(id)
      };
  }

  @patch('/tx-cara-tindak-lanjut-riksa/by-laporan-id')
  @response(204, {
    description: 'TxCaraTindakLanjutRiksa PATCH success',
  })
  async updateByIdLaporan(
      @requestBody({
        content: {
          'application/json': {
            schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {partial: true}),
          },
        },
      })
          txRiksaMonitoring: TxCaraTindakLanjutRiksa,
  ): Promise<{ data: any; message: string; status: boolean }> {

    let idTxLaporan = txRiksaMonitoring?.id_tx_laporan;
    // @ts-ignore
    let laporan = await this.txLaporanRepository.findById(idTxLaporan);
    if (laporan.kode_kantor !== "JKT") {
      txRiksaMonitoring.jenis_surat += "_perwakilan";
    }

    const TxRiksa = await this.TxCaraTindakLanjutRiksaRepository.find({
      where: {
        id_tx_laporan: txRiksaMonitoring.id_tx_laporan,
        jenis_surat: txRiksaMonitoring.jenis_surat
      },
      limit: 1
    });

    // @ts-ignore
    await this.TxCaraTindakLanjutRiksaRepository.updateById(TxRiksa[0].id, txRiksaMonitoring)
    return {
      status: true,
      message: "Data berhasil diubah",
      // @ts-ignore
      data: await this.TxCaraTindakLanjutRiksaRepository.findById(TxRiksa[0].id)
    };
  }

  @del('/tx-cara-tindak-lanjut-riksa/{id}')
    @response(204, {
        description: 'TxCaraTindakLanjutRiksa DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.TxCaraTindakLanjutRiksaRepository.deleteById(id);
    }

  @get('/tx-cara-tindak-lanjut-riksa-by-laporan/{idLaporan}')
  @response(200, {
    description: 'TxCaraTindakLanjutRiksa model by laporan',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {includeRelations: true}),
      },
    },
  })
  async findByLaporan(
    @param.path.string('idLaporan') idLaporan: string,
    @param.filter(TxCaraTindakLanjutRiksa, {exclude: 'where'}) filter?: FilterExcludingWhere<TxCaraTindakLanjutRiksa>
  ): Promise<{ data: TxCaraTindakLanjutRiksa; message: string; status: boolean }> {

    const sql = `select r.id, r.id_tx_laporan, r.id_tx_riksa, r.data, r.created_date, r.filename, 
                  case l.kode_kantor
                  when 'JKT' then r.jenis_surat
                  else (case 
                  when r.jenis_surat like 'ba_penutupan_laporan_riksaperwakilan%' then 'ba_penutupan_laporan_riksaperwakilan' 
                  when r.jenis_surat like '%_perwakilan_perwakilan' then concat(split_part(r.jenis_surat, '_perwakilan', 1), '_perwakilan') 
                  when r.jenis_surat like '%_perwakilan' then split_part(r.jenis_surat, '_perwakilan', 1) else r.jenis_surat end)
                  end 
                  from simpel_4.tx_cara_tindak_lanjut_riksa r 
                  join simpel_4.tx_laporan l on r.id_tx_laporan = l.id  
                  where r.id_tx_laporan = '${idLaporan}'`;

    const result = await this.TxCaraTindakLanjutRiksaRepository.dataSource.execute(sql);

    // const resultFinal = result[0]

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: result
      };
  }

  @get('/tx-cara-tindak-lanjut-riksa-by-riksa/{idRiksa}')
  @response(200, {
    description: 'TxCaraTindakLanjutRiksa model by riksa',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxCaraTindakLanjutRiksa, {includeRelations: true}),
      },
    },
  })
  async findByRiksa(
    @param.path.string('idRiksa') idRiksa: string,
    @param.filter(TxCaraTindakLanjutRiksa, {exclude: 'where'}) filter?: FilterExcludingWhere<TxCaraTindakLanjutRiksa>
  ): Promise<{ data: TxCaraTindakLanjutRiksa; message: string; status: boolean }> {

    const sql = `select * from simpel_4.tx_cara_tindak_lanjut_riksa\n`+
                `where id_tx_riksa = '${idRiksa}'\n`+
                `limit 1`

    const result = await this.TxCaraTindakLanjutRiksaRepository.dataSource.execute(sql);

    const resultFinal = result[0]

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: resultFinal
      };
  }

  @get('/tx-cara-tindak-lanjut-riksa/download')
  @oas.response.file()
  async findFile(
      @inject(RestBindings.Http.RESPONSE) response: Response,
      @param.filter(TxCaraTindakLanjutRiksa) filter?: Filter<TxCaraTindakLanjutRiksa>
  ) {
    const data = await this.TxCaraTindakLanjutRiksaRepository.find(filter);
    let surat: TxCaraTindakLanjutRiksa;

    if(data.length > 0){
      surat = data[0];
    }

    // @ts-ignore
    if(surat.filename != null){
      // @ts-ignore
      const file = path.resolve(this.storageDirectory + '/surat-tindak-lanjut/', surat.filename);
      // @ts-ignore
      response.download(file, surat.filename);
      return response;
    }

    return {
      status: false,
      message: "Data tidak ditemukan",
      data: null
    };

  }

  @post('/tx-cara-tindak-lanjut-riksa/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async tindakLanjutUpload(
      @requestBody.file()
          request: Request,
      @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    const storage = multer.diskStorage(
        {
          destination: './public/uploads/surat-tindak-lanjut/',
          filename: function ( req, file, cb ) {
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            cb( null, req.body.jenis_surat + '_' + req.body.id_tx_laporan + file.originalname.substring(file.originalname.lastIndexOf('.')));
          }
        }
    );
    const upload = multer({storage});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, (err: unknown) => {
        if (err) reject(err);
        else {
          resolve(TxCaraTindakLanjutRiksaController.modifyTindakLanjutFile(request));
        }
      });
    });
  }

  private static modifyTindakLanjutFile(request: Request) {
    const uploadedFiles = request.files;
    const data = request.body
    const mapper = (f: globalThis.Express.Multer.File) => ({
      fieldname: f.fieldname,
      originalname: data.jenis_surat + '_' + data.id_tx_laporan + f.originalname.substring(f.originalname.lastIndexOf('.')),
      encoding: f.encoding,
      mimetype: f.mimetype,
      size: f.size,
    });
    let files: object[] = [];
    if (Array.isArray(uploadedFiles)) {
      files = uploadedFiles.map(mapper);
    } else {
      for (const filename in uploadedFiles) {
        files.push(...uploadedFiles[filename].map(mapper));
      }
    }
    return {files, fields: request.body};
  }

  private async getFileFromReport(namafile: string, idtxlaporan: string, status: string, nomor_agenda: string, kode_kantor: string) {
    const endpoint = 'cara_tindak_lanjut';
    let version = 1

    const tDokumen = await this.txDokumenRepository.find({
      where: {
        id_tx_laporan: idtxlaporan,
        tipe_dokumen: namafile
      },
      order: ['version DESC'],
      limit: 1
    });

    if (tDokumen.length > 0) {
      version = ((tDokumen[0].version ? tDokumen[0].version : 0) + 1);
    }

    // await this.getDoc(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, template);
    await this.dokumenService.generateDokumen(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, namafile)
  }

  // private async getDoc(endpoint: string, namafile: string, idtxlaporan: string, version: number, nomor_agenda: string, status: string, template: string) {
  //   return await new Promise((resolve,reject) => {
  //     const folderPath = './public/uploads/dokumen/' + idtxlaporan;
  //     if (!fs.existsSync(folderPath)) {
  //       fs.mkdirSync(folderPath, { recursive: true });
  //     }
  //
  //     const headers = {...this.request.headers};
  //     const token = headers.authorization !== null && headers.authorization?.startsWith("Bearer") ?
  //         headers.authorization?.substring(7, headers.authorization.length) : undefined;
  //     // var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsInN1YnN0YW5zaSI6WyJzdWJzdGFuc2lfMyIsInN1YnN0YW5zaV8zIiwic3Vic3RhbnNpXzEiLCJzdWJzdGFuc2lfMSIsInN1YnN0YW5zaV80Iiwic3Vic3RhbnNpXzQiLCJzdWJzdGFuc2lfNSIsInN1YnN0YW5zaV81Iiwic3Vic3RhbnNpXzYiLCJzdWJzdGFuc2lfOSIsInN1YnN0YW5zaV8xMSIsInN1YnN0YW5zaV8xMyIsInN1YnN0YW5zaV8xNSIsInN1YnN0YW5zaV8xNiIsInN1YnN0YW5zaV8xOCIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8yNiIsInN1YnN0YW5zaV8zMCIsInN1YnN0YW5zaV8zMSIsInN1YnN0YW5zaV8zMiIsInN1YnN0YW5zaV8zNCIsInN1YnN0YW5zaV8xMiIsInN1YnN0YW5zaV8zMyIsInN1YnN0YW5zaV8zNyIsInN1YnN0YW5zaV80OCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MiIsInN1YnN0YW5zaV84Il0sInBlcm1pc3Npb25zIjpbIlBldHVnYXMgUmlrc2EiLCJSZWdpc3RyYXNpLVJlZ2lzdHJhc2ktVGFtYmFoIiwiUmVnaXN0cmFzaS1PbWJ1ZHNtYW4gT25saW5lLVJlZ2lzdHJhc2lrYW4iLCJQZXR1Z2FzIFBWTCIsIlJlZ2lzdHJhc2ktUmVnaXN0cmFzaS1QZW51Z2FzYW4iXSwia2luZCI6ImFjY2VzcyIsInJvbGVzIjpbIktlcGFsYSBUaW0gUmlrc2EgMyIsIktlcGFsYSBUaW0gUmlrc2EgNyIsIlRpbSBQVkwiLCJLZXBhbGEgS2Vhc2lzdGVuYW4gUmlrc2EiLCJLZXBhbGEgVGltIFJpa3NhIDYiLCJBbmdnb3RhIFJpa3NhIDEiLCJLZXBhbGEgS2Vhc2lzdGVuYW4iLCJLZXBhbGEgVGltIFJpa3NhIDIiLCJLZXBhbGEgVGltIFJpa3NhIDQiLCJLZXBhbGEgVGltIFJpa3NhIDUgVGVzdCIsIktlcGFsYSBUaW0gUmlrc2EgMSJdLCJleHAiOjE2NjY3NzI2NDYsInVzZXIiOnsiaWQiOiJlYWM5NzllZC1mNTBkLTQ3MmYtODUwYy1mOWQxNjg1ZWRlYWQiLCJ1c2VybmFtZSI6ImNob2lydWRkaW5AZ21haWwuY29tIiwiZW1haWwiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDMifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNyJ9LHsiYXV0aG9yaXR5IjoiVGltIFBWTCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIFJpa3NhIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDYifSx7ImF1dGhvcml0eSI6IkFuZ2dvdGEgUmlrc2EgMSJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDIifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIFRpbSBSaWtzYSA1IFRlc3QifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgMSJ9XSwiZW5hYmxlZCI6dHJ1ZSwiYWNjb3VudE5vbkxvY2tlZCI6dHJ1ZSwiY3JlZGVudGlhbHNOb25FeHBpcmVkIjp0cnVlLCJhY2NvdW50Tm9uRXhwaXJlZCI6dHJ1ZX0sImlhdCI6MTY2NjY4NjI0Nn0.lhEQneVY6t9xC0YzYXlxl2bPALqhLwT56qLeawBr2hqEq6D4HcwARh35W0qFhmt6LHKR4m-NblNS_lO1OeUzrg'
  //
  //     const options = {
  //       hostname: 'api-report.ombudsman.dev.layanan.go.id',
  //       path: `/v1/report/${endpoint}?jenisSurat=${namafile}&template=${template}&idLaporan=${idtxlaporan}&lang=en&tte=&nik=&passphrase=`,
  //       headers: {
  //         Authorization: 'Bearer ' + token
  //       }
  //     }
  //     const filename: string = folderPath + '/' + namafile + '_' + idtxlaporan + '_' + version + '.pdf'
  //     const file = fs.createWriteStream(filename)
  //
  //     https.get(options, res => {
  //       if (res.statusCode !== 200) {
  //         fs.unlink(filename, () => {
  //           console.log(res.statusCode);
  //           // reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
  //         });
  //         resolve(true)
  //         return;
  //       }
  //
  //       res.pipe(file);
  //       res.on('data', chunk => {
  //         // data.push(chunk);
  //       });
  //       file.on("finish", () => {
  //         file.close();
  //         // @ts-ignore
  //         const dataTxDokumen: TxDokumen = {
  //           filename,
  //           id_tx_laporan: idtxlaporan,
  //           tipe_dokumen: template,
  //           nomor_agenda: nomor_agenda,
  //           status_dokumen: status,
  //           version
  //         }
  //
  //         this.txDokumenRepository.save(dataTxDokumen)
  //         resolve(true);
  //       });
  //     })
  //         .on("error", listeners => {
  //           reject(false);
  //         });
  //   });
  // }

}
