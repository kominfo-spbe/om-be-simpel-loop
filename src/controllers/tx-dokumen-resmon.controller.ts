import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  oas,
  param,
  patch,
  post,
  Request,
  requestBody,
  response,
  Response,
  RestBindings
} from '@loopback/rest';
import {TxDokumen, TxDokumenResmon} from '../models';
import {TxDokumenRepository, TxDokumenResmonRepository, TxLaporanRepository} from '../repositories';
import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from '../keys';
import {FileUploadHandler} from '../types';

import multer from 'multer';

import {inject, service} from "@loopback/core";
import path from "path";
import fs from "fs";
import https from "https";
import {GenerateFileService} from "../services";

export class TxDokumenResmoniController {
  constructor(
    @repository(TxDokumenResmonRepository)
    public TxDokumenResmonRepository : TxDokumenResmonRepository,
    @repository(TxDokumenRepository)
    public txDokumenRepository: TxDokumenRepository,
    @repository(TxLaporanRepository)
    public txLaporanRepository: TxLaporanRepository,
    @service(GenerateFileService)
    public dokumenService: GenerateFileService,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @inject(STORAGE_DIRECTORY) private storageDirectory: string,
    @inject(RestBindings.Http.REQUEST) private request: Request
  ) {}

  @post('/tx-dokumen-resmon')
  @response(200, {
    description: 'TxDokumenResmon model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxDokumenResmon)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDokumenResmon, {
            title: 'TxDokumenResmon',
            exclude: ['id'],
          }),
        },
      },
    })
    TxDokumenResmon: Omit<TxDokumenResmon, 'id'>,
  ): Promise<{ data: TxDokumenResmon; message: string; status: boolean }> {

    const dataAll = await this.TxDokumenResmonRepository.find()
    let idUpdate  = ""
    dataAll.forEach(x => {
      if(TxDokumenResmon["id_tx_laporan"] == x.id_tx_laporan && TxDokumenResmon["jenis_surat"] == x.jenis_surat)
      {
        idUpdate = x.id
      }
    })

    let hasil : any ;
    let massage : string  = "";
    if(idUpdate == ""){
      hasil = await this.TxDokumenResmonRepository.create(TxDokumenResmon)
      massage = "Data berhasil ditambahkan"
    }else{
      await this.TxDokumenResmonRepository.updateById(idUpdate, TxDokumenResmon)
      hasil = await this.TxDokumenResmonRepository.findById(idUpdate)
      massage = "Data berhasil diubah"
    }

    let laporan = await this.txLaporanRepository.findById(TxDokumenResmon.id_tx_laporan);
    let namafile = TxDokumenResmon.jenis_surat;

    await this.getFileFromReport(namafile, laporan.id, laporan.status_laporan ?? "", laporan.no_agenda ?? "");

    return {
        status: true,
        message: massage,
        data: hasil
      };

  }

  @get('/tx-dokumen-resmon')
    @response(200, {
        description: 'Array of TxDokumenResmon model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDokumenResmon, {includeRelations: true}),
                },
            },
        },
    })
    async find(
      @param.filter(TxDokumenResmon) filter?: Filter<TxDokumenResmon>,
    ): Promise<{ data: (TxDokumenResmon)[]; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxDokumenResmonRepository.find(filter)
      };

    }

  @get('/tx-dokumen-resmon/{id}')
  @response(200, {
    description: 'TxDokumenResmon model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDokumenResmon, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxDokumenResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDokumenResmon>
  ): Promise<{ data: TxDokumenResmon; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxDokumenResmonRepository.findById(id, filter)
      };
  }

  @patch('/tx-dokumen-resmon/{id}')
  @response(204, {
    description: 'TxDokumenResmon PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDokumenResmon, {partial: true}),
        },
      },
    })
    txRiksaMonitoring: TxDokumenResmon,
  ): Promise<{ data: any; message: string; status: boolean }> {

      await this.TxDokumenResmonRepository.updateById(id, txRiksaMonitoring)
      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.TxDokumenResmonRepository.findById(id)
      };
  }

  @del('/tx-dokumen-resmon/{id}')
    @response(204, {
        description: 'TxDokumenResmon DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.TxDokumenResmonRepository.deleteById(id);
    }

  @get('/tx-dokumen-resmon-by-laporan/{idLaporan}')
  @response(200, {
    description: 'TxDokumenResmon model by laporan',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDokumenResmon, {includeRelations: true}),
      },
    },
  })
  async findByLaporan(
    @param.path.string('idLaporan') idLaporan: string,
    @param.filter(TxDokumenResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDokumenResmon>
  ): Promise<{ data: TxDokumenResmon; message: string; status: boolean }> {

    const sql = `select * from simpel_4.tx_dokumen_resmon\n`+
                `where id_tx_laporan = '${idLaporan}'`

    const result = await this.TxDokumenResmonRepository.dataSource.execute(sql);

    // const resultFinal = result[0]

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: result
      };
  }

  @get('/tx-dokumen-resmon-by-riksa/{idRiksa}')
  @response(200, {
    description: 'TxDokumenResmon model by riksa',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDokumenResmon, {includeRelations: true}),
      },
    },
  })
  async findByRiksa(
    @param.path.string('idRiksa') idRiksa: string,
    @param.filter(TxDokumenResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDokumenResmon>
  ): Promise<{ data: TxDokumenResmon; message: string; status: boolean }> {

    const sql = `select * from simpel_4.tx_dokumen_resmon\n`+
                `where id_tx_riksa = '${idRiksa}'\n`+
                `limit 1`

    const result = await this.TxDokumenResmonRepository.dataSource.execute(sql);

    const resultFinal = result[0]

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: resultFinal
      };
  }

  @get('/tx-dokumen-resmon/download')
  @oas.response.file()
  async findFile(
      @inject(RestBindings.Http.RESPONSE) response: Response,
      @param.filter(TxDokumenResmon) filter?: Filter<TxDokumenResmon>
  ) {
    const data = await this.TxDokumenResmonRepository.find(filter);
    let surat: TxDokumenResmon;

    if(data.length > 0){
      surat = data[0];
    }

    // @ts-ignore
    if(surat.filename != null){
      // @ts-ignore
      const file = path.resolve(this.storageDirectory + '/dokumen-resmon/', surat.filename);
      // @ts-ignore
      response.download(file, surat.filename);
      return response;
    }

    return {
      status: false,
      message: "Data tidak ditemukan",
      data: null
    };

  }

  @post('/tx-dokumen-resmon/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async resmonUpload(
      @requestBody.file()
          request: Request,
      @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    const storage = multer.diskStorage(
        {
          destination: './public/uploads/dokumen-resmon/',
          filename: function ( req, file, cb ) {
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            cb( null, req.body.jenis_surat + '_' + req.body.id_tx_laporan + file.originalname.substring(file.originalname.lastIndexOf('.')));
          }
        }
    );
    const upload = multer({storage});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, (err: unknown) => {
        if (err) reject(err);
        else {
          resolve(TxDokumenResmoniController.modifyResmonFile(request));
        }
      });
    });
  }

  private static modifyResmonFile(request: Request) {
    const uploadedFiles = request.files;
    const data = request.body
    const mapper = (f: globalThis.Express.Multer.File) => ({
      fieldname: f.fieldname,
      originalname: data.jenis_surat + '_' + data.id_tx_laporan + f.originalname.substring(f.originalname.lastIndexOf('.')),
      encoding: f.encoding,
      mimetype: f.mimetype,
      size: f.size,
    });
    let files: object[] = [];
    if (Array.isArray(uploadedFiles)) {
      files = uploadedFiles.map(mapper);
    } else {
      for (const filename in uploadedFiles) {
        files.push(...uploadedFiles[filename].map(mapper));
      }
    }
    return {files, fields: request.body};
  }

  private async getFileFromReport(namafile: string, idtxlaporan: string, status: string, nomor_agenda: string) {
    let endpoint = "dokumen_resmon";
    let version = 1

    const tDokumen = await this.txDokumenRepository.find({
      where: {
        id_tx_laporan: idtxlaporan,
        tipe_dokumen: namafile
      },
      order: ['version DESC'],
      limit: 1
    });

    if (tDokumen.length > 0) {
      version = ((tDokumen[0].version ? tDokumen[0].version : 0) + 1);
    }

    // await this.getDoc(endpoint, namafile, idtxlaporan, version, nomor_agenda, status);

    await this.dokumenService.generateDokumen(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, "")
  }

  // private async getDoc(endpoint: string, namafile: string, idtxlaporan: string, version: number, nomor_agenda: string, status: string) {
  //   return await new Promise((resolve,reject) => {
  //     const folderPath = './public/uploads/dokumen/' + idtxlaporan;
  //     if (!fs.existsSync(folderPath)) {
  //       fs.mkdirSync(folderPath, { recursive: true });
  //     }
  //
  //     const headers = {...this.request.headers};
  //     const token = headers.authorization !== null && headers.authorization?.startsWith("Bearer") ?
  //         headers.authorization?.substring(7, headers.authorization.length) : undefined;
  //     // var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsInN1YnN0YW5zaSI6WyJzdWJzdGFuc2lfMyIsInN1YnN0YW5zaV8zIiwic3Vic3RhbnNpXzEiLCJzdWJzdGFuc2lfMSIsInN1YnN0YW5zaV80Iiwic3Vic3RhbnNpXzQiLCJzdWJzdGFuc2lfNSIsInN1YnN0YW5zaV81Iiwic3Vic3RhbnNpXzYiLCJzdWJzdGFuc2lfOSIsInN1YnN0YW5zaV8xMSIsInN1YnN0YW5zaV8xMyIsInN1YnN0YW5zaV8xNSIsInN1YnN0YW5zaV8xNiIsInN1YnN0YW5zaV8xOCIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8yNiIsInN1YnN0YW5zaV8zMCIsInN1YnN0YW5zaV8zMSIsInN1YnN0YW5zaV8zMiIsInN1YnN0YW5zaV8zNCIsInN1YnN0YW5zaV8xMiIsInN1YnN0YW5zaV8zMyIsInN1YnN0YW5zaV8zNyIsInN1YnN0YW5zaV80OCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MiIsInN1YnN0YW5zaV84Il0sInBlcm1pc3Npb25zIjpbIlBldHVnYXMgUmlrc2EiLCJSZWdpc3RyYXNpLVJlZ2lzdHJhc2ktVGFtYmFoIiwiUmVnaXN0cmFzaS1PbWJ1ZHNtYW4gT25saW5lLVJlZ2lzdHJhc2lrYW4iLCJQZXR1Z2FzIFBWTCIsIlJlZ2lzdHJhc2ktUmVnaXN0cmFzaS1QZW51Z2FzYW4iXSwia2luZCI6ImFjY2VzcyIsInJvbGVzIjpbIktlcGFsYSBUaW0gUmlrc2EgMyIsIktlcGFsYSBUaW0gUmlrc2EgNyIsIlRpbSBQVkwiLCJLZXBhbGEgS2Vhc2lzdGVuYW4gUmlrc2EiLCJLZXBhbGEgVGltIFJpa3NhIDYiLCJBbmdnb3RhIFJpa3NhIDEiLCJLZXBhbGEgS2Vhc2lzdGVuYW4iLCJLZXBhbGEgVGltIFJpa3NhIDIiLCJLZXBhbGEgVGltIFJpa3NhIDQiLCJLZXBhbGEgVGltIFJpa3NhIDUgVGVzdCIsIktlcGFsYSBUaW0gUmlrc2EgMSJdLCJleHAiOjE2NjY3NzI2NDYsInVzZXIiOnsiaWQiOiJlYWM5NzllZC1mNTBkLTQ3MmYtODUwYy1mOWQxNjg1ZWRlYWQiLCJ1c2VybmFtZSI6ImNob2lydWRkaW5AZ21haWwuY29tIiwiZW1haWwiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDMifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNyJ9LHsiYXV0aG9yaXR5IjoiVGltIFBWTCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIFJpa3NhIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDYifSx7ImF1dGhvcml0eSI6IkFuZ2dvdGEgUmlrc2EgMSJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDIifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIFRpbSBSaWtzYSA1IFRlc3QifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgMSJ9XSwiZW5hYmxlZCI6dHJ1ZSwiYWNjb3VudE5vbkxvY2tlZCI6dHJ1ZSwiY3JlZGVudGlhbHNOb25FeHBpcmVkIjp0cnVlLCJhY2NvdW50Tm9uRXhwaXJlZCI6dHJ1ZX0sImlhdCI6MTY2NjY4NjI0Nn0.lhEQneVY6t9xC0YzYXlxl2bPALqhLwT56qLeawBr2hqEq6D4HcwARh35W0qFhmt6LHKR4m-NblNS_lO1OeUzrg'
  //
  //     const options = {
  //       hostname: 'api-report.ombudsman.dev.layanan.go.id',
  //       path: `/v1/report/${endpoint}?jenisSurat=${namafile}&idLaporan=${idtxlaporan}&lang=en&tte=&nik=&passphrase=`,
  //       headers: {
  //         Authorization: 'Bearer ' + token
  //       }
  //     }
  //     const filename: string = folderPath + '/' + namafile + '_' + idtxlaporan + '_' + version + '.pdf'
  //     const file = fs.createWriteStream(filename)
  //
  //     https.get(options, res => {
  //       if (res.statusCode !== 200) {
  //         fs.unlink(filename, () => {
  //           console.log(res.statusCode);
  //           // reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
  //         });
  //         resolve(true)
  //         return;
  //       }
  //
  //       res.pipe(file);
  //       res.on('data', chunk => {
  //         // data.push(chunk);
  //       });
  //       file.on("finish", () => {
  //         file.close();
  //         // @ts-ignore
  //         const dataTxDokumen: TxDokumen = {
  //           filename,
  //           id_tx_laporan: idtxlaporan,
  //           tipe_dokumen: namafile,
  //           nomor_agenda: nomor_agenda,
  //           status_dokumen: status,
  //           version
  //         }
  //
  //         this.txDokumenRepository.save(dataTxDokumen)
  //         resolve(true);
  //       });
  //     })
  //         .on("error", listeners => {
  //           reject(false);
  //         });
  //   });
  // }

}
