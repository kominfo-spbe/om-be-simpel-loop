import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxLaporan, TxLaporanPerubahan} from '../models';
import {TxLaporanPerubahanRepository} from '../repositories';

export class TxLaporanPerubahanController {
  constructor(
    @repository(TxLaporanPerubahanRepository)
    public txLaporanPerubahanRepository : TxLaporanPerubahanRepository,
  ) {}

  @post('/tx-laporan-perubahan')
  @response(200, {
    description: 'TxLaporanPerubahan model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxLaporanPerubahan)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxLaporanPerubahan, {
            title: 'NewTxLaporanPerubahan',
            exclude: ['id'],
          }),
        },
      },
    })
    txLaporanPerubahan: Omit<TxLaporanPerubahan, 'id'>,
  ): Promise<TxLaporanPerubahan> {
    return this.txLaporanPerubahanRepository.create(txLaporanPerubahan);
  }

  @get('/tx-laporan-perubahan/count')
  @response(200, {
    description: 'TxLaporanPerubahan model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxLaporanPerubahan) where?: Where<TxLaporanPerubahan>,
  ): Promise<Count> {
    return this.txLaporanPerubahanRepository.count(where);
  }

  @get('/tx-laporan-perubahan')
  @response(200, {
    description: 'Array of TxLaporanPerubahan model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxLaporanPerubahan, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxLaporanPerubahan) filter?: Filter<TxLaporanPerubahan>,
  ):Promise<{ data: (TxLaporanPerubahan)[]; message: string; status: boolean }> {
      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txLaporanPerubahanRepository.find(filter)
      };
  }

  @patch('/tx-laporan-perubahan')
  @response(200, {
    description: 'TxLaporanPerubahan PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxLaporanPerubahan, {partial: true}),
        },
      },
    })
    txLaporanPerubahan: TxLaporanPerubahan,
    @param.where(TxLaporanPerubahan) where?: Where<TxLaporanPerubahan>,
  ): Promise<Count> {
    return this.txLaporanPerubahanRepository.updateAll(txLaporanPerubahan, where);
  }

  @get('/tx-laporan-perubahan/{id}')
  @response(200, {
    description: 'TxLaporanPerubahan model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxLaporanPerubahan, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxLaporanPerubahan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxLaporanPerubahan>
  ): Promise<TxLaporanPerubahan> {
    return this.txLaporanPerubahanRepository.findById(id, filter);
  }

  @patch('/tx-laporan-perubahan/{id}')
  @response(204, {
    description: 'TxLaporanPerubahan PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxLaporanPerubahan, {partial: true}),
        },
      },
    })
    txLaporanPerubahan: TxLaporanPerubahan,
  ): Promise<{ data: TxLaporanPerubahan; message: string; status: boolean }> {

      // update data tx_laporan
      await this.txLaporanPerubahanRepository.updateById(id, txLaporanPerubahan);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txLaporanPerubahanRepository.findById(id)
      };
  }

  @put('/tx-laporan-perubahan/{id}')
  @response(204, {
    description: 'TxLaporanPerubahan PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txLaporanPerubahan: TxLaporanPerubahan,
  ): Promise<void> {
    await this.txLaporanPerubahanRepository.replaceById(id, txLaporanPerubahan);
  }

  @del('/tx-laporan-perubahan/{id}')
  @response(204, {
    description: 'TxLaporanPerubahan DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txLaporanPerubahanRepository.deleteById(id);
  }
}
