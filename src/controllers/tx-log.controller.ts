import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxLog} from '../models';
import {TxLogRepository} from '../repositories';

export class TxLogController {
    constructor(
        @repository(TxLogRepository)
        public txLogRepository: TxLogRepository,
    ) {
    }

    @post('/tx-log')
    @response(200, {
        description: 'TxLog model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxLog)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLog, {
                        title: 'NewTxLog',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txLog: Omit<TxLog, 'id'>,
    ): Promise<TxLog> {
        return this.txLogRepository.create(txLog);
    }

    @get('/tx-log/count')
    @response(200, {
        description: 'TxLog model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxLog) where?: Where<TxLog>,
    ): Promise<Count> {
        return this.txLogRepository.count(where);
    }

    @get('/tx-log')
    @response(200, {
        description: 'Array of TxLog model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxLog, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxLog) filter?: Filter<TxLog>,
    ): Promise<{ data: (TxLog)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txLogRepository.find(filter)
        };
    }

    @patch('/tx-log')
    @response(200, {
        description: 'TxLog PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLog, {partial: true}),
                },
            },
        })
            txLog: TxLog,
        @param.where(TxLog) where?: Where<TxLog>,
    ): Promise<Count> {
        return this.txLogRepository.updateAll(txLog, where);
    }

    @get('/tx-log/{id}')
    @response(200, {
        description: 'TxLog model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLog, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxLog, {exclude: 'where'}) filter?: FilterExcludingWhere<TxLog>
    ): Promise<TxLog> {
        return this.txLogRepository.findById(id, filter);
    }

    @patch('/tx-log/{id}')
    @response(204, {
        description: 'TxLog PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLog, {partial: true}),
                },
            },
        })
            txLog: TxLog,
    ): Promise<void> {
        await this.txLogRepository.updateById(id, txLog);
    }

    @put('/tx-log/{id}')
    @response(204, {
        description: 'TxLog PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txLog: TxLog,
    ): Promise<void> {
        await this.txLogRepository.replaceById(id, txLog);
    }

    @del('/tx-log/{id}')
    @response(204, {
        description: 'TxLog DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txLogRepository.deleteById(id);
    }
}
