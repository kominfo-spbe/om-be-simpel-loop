import {repository} from '@loopback/repository';
import {get, getModelSchemaRef, response} from '@loopback/rest';
import {TxFileUpload} from '../models';
import {TxFileUploadRepository} from '../repositories';

export class TxFileUploadController {
    constructor(
        @repository(TxFileUploadRepository)
        public txFileUploadRepository: TxFileUploadRepository
    ) {
    }

    @get('/tx-file-upload')
    @response(200, {
        description: 'TxFileUpload model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxFileUpload, {includeRelations: true}),
            },
        },
    })
    async find(
    ): Promise<{ data: TxFileUpload; message: string; status: boolean }> {
      const model = new TxFileUpload();
      const savedData = await this.txFileUploadRepository.create(model);
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: savedData
        };
    }




}
