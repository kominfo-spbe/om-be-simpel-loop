import {inject} from "@loopback/core";
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, oas, param, post, Request, requestBody, Response, response, RestBindings} from '@loopback/rest';
import axios from "axios";
import FormData from "form-data";
import fs from "fs";
import path from "path";
import * as process from "process";
import {Tte, TxDokumen} from '../models';
import {TdTtdSpesimenRepository, TxDokumenRepository, TxDokumenTteRepository} from '../repositories';

export class TxDokumenTteController {
    constructor(
        @repository(TxDokumenTteRepository)
        public TxDokumenTteRepository : TxDokumenTteRepository,
        @repository(TxDokumenRepository)
        public TxDokumenRepository : TxDokumenRepository,
        @repository(TdTtdSpesimenRepository)
        public TdTtdSpesimenRepository : TdTtdSpesimenRepository,
        @inject(RestBindings.Http.REQUEST) private request: Request
    ) {}

    @post('/tx-dokumen-tte')
    @oas.response.file()
    async create(
        @inject(RestBindings.Http.RESPONSE) resp: Response,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(Tte, {
                        title: 'Tte',
                    }),
                },
            },
        })
            tte: Tte,
    ) {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
        const doc = await this.TxDokumenRepository.find({
            where: {
                id_tx_laporan: tte.id_tx_laporan,
                tipe_dokumen: tte.tipe_dokumen
            },
            order: ['version DESC'],
            limit: 1
        });

        const existingDoc: TxDokumen = doc[0]

        const halaman = existingDoc.page;

        const filter = {
            where: {
                user_id: tte.user_id
            }
        }
        const exist = await this.TdTtdSpesimenRepository.find(filter);

        const folderPath = './public/uploads/dokumen/' + tte.id_tx_laporan

        // @ts-ignore
        const filename = path.basename(existingDoc.filename, path.extname(existingDoc.filename)) + path.extname(existingDoc.filename)
// @ts-ignore
        const filele = fs.readFileSync(existingDoc.filename);

        const ttdPath = './public/uploads/spesimen/'

        let x = '80'
        let y = '80'
        let w = '150'
        let h = '150'
        switch (tte.urutan_penandatangan){
            case 2:
                x = '80'
                y = '80'
                w = '150'
                h = '150'
                existingDoc.penandatangan_2 = tte.user_id
                break;
            case 3:
                x = '260'
                y = '80'
                w = '330'
                h = '150'
                existingDoc.penandatangan_3 = tte.user_id
                break;
            case 1:
                x = '450'
                y = '80'
                w = '520'
                h = '150'
                existingDoc.penandatangan_1 = tte.user_id
                break;
            default:
                existingDoc.penandatangan_1 = tte.user_id
                break;
        }
        const filterDownload = {"order":["version DESC"],"limit":1,"where":{"id_tx_laporan":tte.id_tx_laporan,"tipe_dokumen":tte.tipe_dokumen}}
        const linkDownload = process.env.API_URL + '/tx-dokumen/download?filter=' + JSON.stringify(filterDownload)

// @ts-ignore
        const formData = new FormData();
        // @ts-ignore
        formData.append('nik',tte.nik);
        // @ts-ignore
        formData.append('passphrase', tte.passphrase);
        // @ts-ignore
        formData.append('file', filele, '123.pdf');
        // formData.append('halaman', 'TERAKHIR');
        formData.append('page', `${halaman}`);
        formData.append('tampilan', 'VISIBLE');
        formData.append('linkQR', linkDownload);
        formData.append('yAxis', y);
        formData.append('xAxis', x);
        formData.append('width', w);
        formData.append('height', h);

        if(exist && exist.length > 0){
            const ttdFilename = exist[0].spesimen_path
            const fileTtd = fs.readFileSync(ttdPath + ttdFilename);
            formData.append('imageTTD', fileTtd, 'ttd.png');
        }

        const username = 'esign'
        const password = 'wrjcgX6526A2dCYSAV6u'
        const auth = Buffer.from(`${username}:${password}`).toString('base64')

        const headers = {
            'Authorization' : 'Basic ' + auth,
            ...formData.getHeaders()
        }

        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath, { recursive: true });
        }
        const file = fs.createWriteStream(folderPath + '/' + filename);

        await axios({
            responseType: 'stream',
            method: 'post',
            url: 'https://esign-dev.layanan.go.id/api/sign/pdf',
            headers: headers,
            data: formData
        }).then(response => {
            response.data.pipe(file)

            this.TxDokumenRepository.update(existingDoc)
        }).catch(e => console.log("------------------------------", e));

       await new Promise((resolve, reject) => {
            file.on('finish', () => {
                // @ts-ignore
                resolve();
            }).on('error', err => {

                reject(err);
            });
        });

        // file.on('finish', () => {
        //     //@ts-ignore
        //     file.
        // });
        //
        // @ts-ignore
        resp.download(folderPath + '/' + filename, path.basename(folderPath + '/' + filename));
        return resp;

    }

    @get('/tx-dokumen-tte/not-tte')
    @response(200, {
        description: 'Array of TxDokumen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDokumen, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.query.string("idrole") idrole: any,
        @param.query.string("id_user") iduser: any,
        @param.query.string("kode_kantor") kode_kantor: any,
        @param.query.string("no_agenda") no_agenda: any,
        @param.query.string("tipe_dokumen") tipe_dokumen: any,
        @param.query.string("status_dokumen") status_dokumen: any,
        @param.query.string("skip") skip: any,
        @param.query.string("limit") limit: any,
        @param.query.string("order_by") orderBy: any,
        @param.filter(TxDokumen) filter?: Filter<TxDokumen>,
    ): Promise<{ data: any; message: string; status: boolean }> {

        let  wherekota = ``;
        if (kode_kantor != null && kode_kantor != '') {
            wherekota = ` and tl.kode_kantor = '${kode_kantor}' \n`;
        }

        let whereFilter = ``;
        if (no_agenda != null && no_agenda != '') {
            whereFilter = ` where td.nomor_agenda like '%${no_agenda}%' `;
        }

        if (tipe_dokumen != null && tipe_dokumen != '') {
            whereFilter = ` where lower(td.tipe_dokumen) like lower('%${tipe_dokumen}%') `;
        }

        if (status_dokumen != null && status_dokumen != '') {
            whereFilter = ` where lower(td.status_dokumen) like lower('%${status_dokumen}%') `;
        }

        let  limitFilter = ``;
        if(orderBy != null && orderBy != ''){
            limitFilter = ` order by td.${orderBy} \n`;
        }else{
            limitFilter = `order by td.nomor_agenda asc`;
        }

        if(limit != null && limit != ''){
            limitFilter = limitFilter + ` limit ${limit} \n`;
        }

        if(skip != null && skip != ''){
            limitFilter = limitFilter + ` offset ${skip} `;
        }

        const tte = JSON.parse(decodeURI(idrole))
        const idroles = "'" + tte.join("', '") + "'"

            // const query: string = "select td.*, tl.kode_kantor from simpel_4.tx_dokumen td\n" +
            //     "join simpel_4.tx_laporan tl\n" +
            //     "    on td.id_tx_laporan = tl.id\n" + wherekota +
            //     "inner join simpel_4.tx_manajemen_dokumen tmd\n" +
            //     `on td.tipe_dokumen = tmd.nama_dokumen and (tmd.penandatangan_1 in (${idroles}) or tmd.penandatangan_2 in (${idroles}) or tmd.penandatangan_3 in (${idroles}))\n` +
            //     `where (td.penandatangan_1 is null or td.penandatangan_1 != '${iduser}') or\n` +
            //     `      (td.penandatangan_2 is null or td.penandatangan_2  != '${iduser}') or\n` +
            //     `      (td.penandatangan_3 is null or td.penandatangan_3  != '${iduser}')`
        // const query: string = "select  \n" +
        //     "td.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, max(td.version), td.nomor_agenda\n" +
        //     "from simpel_4.tx_dokumen td\n" +
        //     "join simpel_4.tx_laporan tl on td.id_tx_laporan = tl.id \n" + wherekota +
        //     "inner join simpel_4.tx_manajemen_dokumen tmd on td.tipe_dokumen = tmd.nama_dokumen and \n" +
        //     "(\n" +
        //     `tmd.penandatangan_1 in (${idroles}) or \n` +
        //     `tmd.penandatangan_2 in (${idroles}) or \n` +
        //     `tmd.penandatangan_3 in (${idroles})\n` +
        //     ")\n" +
        //     `where (((td.penandatangan_1 is null or td.penandatangan_1 != '${iduser}') and tmd.penandatangan_1 is not null) or\n` +
        //     `((td.penandatangan_2 is null or td.penandatangan_2  != '${iduser}') and tmd.penandatangan_2 is not null) or\n` +
        //     `((td.penandatangan_3 is null or td.penandatangan_3  != '${iduser}') and tmd.penandatangan_3 is not null)) \n` + whereFilter +
        //     `group by td.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, td.nomor_agenda\n` +
        //     `order by td.nomor_agenda asc`;

        const query = "select td.* \n" +
            "from simpel_4.tx_dokumen td\n" +
            "join (\n" +
            "\tselect  \n" +
            "\ttd.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, max(td.version) as version, td.nomor_agenda\n" +
            "\tfrom simpel_4.tx_dokumen td\n" +
            "\tjoin simpel_4.tx_laporan tl on td.id_tx_laporan = tl.id \n" + wherekota +
            "\tinner join simpel_4.tx_manajemen_dokumen tmd on td.tipe_dokumen = tmd.nama_dokumen and \n" +
            "\t   (\n" +
            `\t\t\ttmd.penandatangan_1 in (${idroles}) or \n` +
            `\t\t\ttmd.penandatangan_2 in (${idroles}) or \n` +
            `\t\t\ttmd.penandatangan_3 in (${idroles})\n` +
            "\t  )\n" +
            `\twhere ((td.penandatangan_1 is null or td.penandatangan_1 != '${iduser}') and tmd.penandatangan_1 is not null) or\n` +
            `\t((td.penandatangan_2 is null or td.penandatangan_2  != '${iduser}') and tmd.penandatangan_2 is not null) or\n` +
            `\t((td.penandatangan_3 is null or td.penandatangan_3  != '${iduser}') and tmd.penandatangan_3 is not null)\n` +
            "\tgroup by td.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, td.nomor_agenda\n" +
            // "\torder by td.nomor_agenda asc\n" +
            ") d on \n" +
            "td.id_tx_laporan = d.id_tx_laporan and \n" +
            "td.tipe_dokumen = d.tipe_dokumen and \n" +
            "td.status_dokumen = d.status_dokumen and \n" +
            "td.version = d.version and \n" +
            "td.nomor_agenda = d.nomor_agenda\n " + whereFilter +" "+ limitFilter;

        // console.log(query)
        // // @ts-ignore
        const result = await this.TxDokumenRepository.dataSource.execute(query);

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };

    }

    @get('/tx-dokumen-tte/tte')
    @response(200, {
        description: 'Array of TxDokumen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDokumen, {includeRelations: true}),
                },
            },
        },
    })
    async findTte(
        @param.query.string("idrole") idrole: any,
        @param.query.string("id_user") iduser: any,
        @param.query.string("kode_kantor") kode_kantor: any,
        @param.query.string("no_agenda") no_agenda: any,
        @param.query.string("tipe_dokumen") tipe_dokumen: any,
        @param.query.string("status_dokumen") status_dokumen: any,
        @param.query.string("skip") skip: any,
        @param.query.string("limit") limit: any,
        @param.query.string("order_by") orderBy: any
    ): Promise<{ data: any; message: string; status: boolean }> {

        let  wherekota = ``;
        if(kode_kantor != null && kode_kantor != ''){
            wherekota = ` and tl.kode_kantor = '${kode_kantor}' \n`;
        }
        let whereFilter = ``;
        if (no_agenda != null && no_agenda != '') {
            whereFilter = ` and td.nomor_agenda like '%${no_agenda}%' `;
        }

        if (tipe_dokumen != null && tipe_dokumen != '') {
            whereFilter = ` and lower(td.tipe_dokumen) like lower('%${tipe_dokumen}%') `;
        }

        if (status_dokumen != null && status_dokumen != '') {
            whereFilter = ` and lower(td.status_dokumen) like lower('%${status_dokumen}%') `;
        }

        let  limitFilter = ``;
        if(orderBy != null && orderBy != ''){
            limitFilter = ` order by td.${orderBy} \n`;
        }

        if(limit != null && limit != ''){
            limitFilter = limitFilter + ` limit ${limit} \n`;
        }

        if(skip != null && skip != ''){
            limitFilter = limitFilter + ` offset ${skip} `;
        }


        const tte = JSON.parse(decodeURI(idrole))
        const idroles = "'" + tte.join("', '") + "'"

        const query: string = "select td.*, tl.kode_kantor from simpel_4.tx_dokumen td\n" +
            "join simpel_4.tx_laporan tl\n" +
            "    on td.id_tx_laporan = tl.id\n" + wherekota + whereFilter+
            "inner join simpel_4.tx_manajemen_dokumen tmd\n" +
            `on td.tipe_dokumen = tmd.nama_dokumen and (tmd.penandatangan_1 in (${idroles}) or tmd.penandatangan_2 in (${idroles}) or tmd.penandatangan_3 in (${idroles}))\n` +
            `where (td.penandatangan_1 = '${iduser}') or\n` +
            `      (td.penandatangan_2 = '${iduser}') or\n` +
            `      (td.penandatangan_3 = '${iduser}') ${limitFilter} ` ;
        //console.log(query)
        // // @ts-ignore


        const result = await this.TxDokumenRepository.dataSource.execute(query);

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };

    }

    @get('/tx-dokumen/download')
    @oas.response.file()
    async findFile(
        @inject(RestBindings.Http.RESPONSE) response: Response,
        @param.filter(TxDokumen) filter?: Filter<TxDokumen>
    ) {
        const data = await this.TxDokumenRepository.find(filter);
        let surat: TxDokumen;

        if(data.length > 0){
            surat = data[0];

            // let kode_kantor = data[0].kode_kantor;
            // if (kode_kantor !== 'JKT') {
            //     if (status !== 'VFL') {
            //         if (status === 'PLRKS') {
            //             surat.filename += '_riksaperwakilan';
            //         } else {
            //             surat.filename += '_perwakilan';
            //         }
            //     }
            // } else {
            //     if (status === 'PLRKS') {
            //         surat.filename += '_riksapusat';
            //     }
            // }
        }

        // @ts-ignore
        if(surat != null && surat.filename != null){
            // @ts-ignore
            const file = path.resolve(surat.filename);
            // @ts-ignore
            response.download(file, path.basename(surat.filename));
            return response;
        }

        return {
            status: false,
            message: "Data tidak ditemukan",
            data: null
        };

    }

    @get('/tx-dokumen-tte/count/not-tte')
    @response(200, {
        description: 'Array of TxDokumen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDokumen, {includeRelations: true}),
                },
            },
        },
    })
    async countNotTte(
        @param.query.string("idrole") idrole: any,
        @param.query.string("id_user") iduser: any,
        @param.query.string("kode_kantor") kode_kantor: any,
        @param.query.string("no_agenda") no_agenda: any,
        @param.query.string("tipe_dokumen") tipe_dokumen: any,
        @param.query.string("status_dokumen") status_dokumen: any,
        @param.query.string("skip") skip: any,
        @param.query.string("limit") limit: any,
        @param.query.string("order_by") orderBy: any,
        @param.filter(TxDokumen) filter?: Filter<TxDokumen>,
    ): Promise<any> {

        let  wherekota = ``;
        if (kode_kantor != null && kode_kantor != '') {
            wherekota = ` and tl.kode_kantor = '${kode_kantor}' \n`;
        }

        let whereFilter = ``;
        if (no_agenda != null && no_agenda != '') {
            whereFilter = ` where td.nomor_agenda like '%${no_agenda}%' `;
        }

        if (tipe_dokumen != null && tipe_dokumen != '') {
            whereFilter = ` where lower(td.tipe_dokumen) like lower('%${tipe_dokumen}%') `;
        }

        if (status_dokumen != null && status_dokumen != '') {
            whereFilter = ` where lower(td.status_dokumen) like lower('%${status_dokumen}%') `;
        }

        let  limitFilter = ``;
        if(orderBy != null && orderBy != ''){
            limitFilter = ` order by td.${orderBy} \n`;
        }else{
            limitFilter = `order by td.nomor_agenda asc`;
        }

        if(limit != null && limit != ''){
            limitFilter = limitFilter + ` limit ${limit} \n`;
        }

        if(skip != null && skip != ''){
            limitFilter = limitFilter + ` offset ${skip} `;
        }

        const tte = JSON.parse(decodeURI(idrole))
        const idroles = "'" + tte.join("', '") + "'"

        const query = "select td.* \n" +
            "from simpel_4.tx_dokumen td\n" +
            "join (\n" +
            "\tselect  \n" +
            "\ttd.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, max(td.version) as version, td.nomor_agenda\n" +
            "\tfrom simpel_4.tx_dokumen td\n" +
            "\tjoin simpel_4.tx_laporan tl on td.id_tx_laporan = tl.id \n" + wherekota +
            "\tinner join simpel_4.tx_manajemen_dokumen tmd on td.tipe_dokumen = tmd.nama_dokumen and \n" +
            "\t   (\n" +
            `\t\t\ttmd.penandatangan_1 in (${idroles}) or \n` +
            `\t\t\ttmd.penandatangan_2 in (${idroles}) or \n` +
            `\t\t\ttmd.penandatangan_3 in (${idroles})\n` +
            "\t  )\n" +
            `\twhere ((td.penandatangan_1 is null or td.penandatangan_1 != '${iduser}') and tmd.penandatangan_1 is not null) or\n` +
            `\t((td.penandatangan_2 is null or td.penandatangan_2  != '${iduser}') and tmd.penandatangan_2 is not null) or\n` +
            `\t((td.penandatangan_3 is null or td.penandatangan_3  != '${iduser}') and tmd.penandatangan_3 is not null)\n` +
            "\tgroup by td.id_tx_laporan, td.tipe_dokumen, td.status_dokumen, tl.kode_kantor, td.nomor_agenda\n" +
            "\torder by td.nomor_agenda asc\n" +
            ") d on \n" +
            "td.id_tx_laporan = d.id_tx_laporan and \n" +
            "td.tipe_dokumen = d.tipe_dokumen and \n" +
            "td.status_dokumen = d.status_dokumen and \n" +
            "td.version = d.version and \n" +
            "td.nomor_agenda = d.nomor_agenda\n " + whereFilter +" "+ limitFilter;

        // console.log(query)
        // // @ts-ignore
        const resultData = await this.TxDokumenRepository.dataSource.execute(query);

        return {
            count : resultData.length
        };

    }

    @get('/tx-dokumen-tte/count/tte')
    @response(200, {
        description: 'Array of TxDokumen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDokumen, {includeRelations: true}),
                },
            },
        },
    })
    async countTte(
        @param.query.string("idrole") idrole: any,
        @param.query.string("id_user") iduser: any,
        @param.query.string("kode_kantor") kode_kantor: any,
        @param.query.string("no_agenda") no_agenda: any,
        @param.query.string("tipe_dokumen") tipe_dokumen: any,
        @param.query.string("status_dokumen") status_dokumen: any,
        @param.query.string("skip") skip: any,
        @param.query.string("limit") limit: any,
        @param.query.string("order_by") orderBy: any
    ): Promise<any> {

        let  wherekota = ``;
        if(kode_kantor != null && kode_kantor != ''){
            wherekota = ` and tl.kode_kantor = '${kode_kantor}' \n`;
        }
        let whereFilter = ``;
        if (no_agenda != null && no_agenda != '') {
            whereFilter = ` and td.nomor_agenda like '%${no_agenda}%' `;
        }

        if (tipe_dokumen != null && tipe_dokumen != '') {
            whereFilter = ` and lower(td.tipe_dokumen) like lower('%${tipe_dokumen}%') `;
        }

        if (status_dokumen != null && status_dokumen != '') {
            whereFilter = ` and lower(td.status_dokumen) like lower('%${status_dokumen}%') `;
        }

        let  limitFilter = ``;
        if(orderBy != null && orderBy != ''){
            limitFilter = ` order by td.${orderBy} \n`;
        }

        if(limit != null && limit != ''){
            limitFilter = limitFilter + ` limit ${limit} \n`;
        }

        if(skip != null && skip != ''){
            limitFilter = limitFilter + ` offset ${skip} `;
        }


        const tte = JSON.parse(decodeURI(idrole))
        const idroles = "'" + tte.join("', '") + "'"

        const query: string = "select td.*, tl.kode_kantor from simpel_4.tx_dokumen td\n" +
            "join simpel_4.tx_laporan tl\n" +
            "    on td.id_tx_laporan = tl.id\n" + wherekota + whereFilter+
            "inner join simpel_4.tx_manajemen_dokumen tmd\n" +
            `on td.tipe_dokumen = tmd.nama_dokumen and (tmd.penandatangan_1 in (${idroles}) or tmd.penandatangan_2 in (${idroles}) or tmd.penandatangan_3 in (${idroles}))\n` +
            `where (td.penandatangan_1 = '${iduser}') or\n` +
            `      (td.penandatangan_2 = '${iduser}') or\n` +
            `      (td.penandatangan_3 = '${iduser}') ${limitFilter} ` ;
        //console.log(query)
        // // @ts-ignore
        const resultData = await this.TxDokumenRepository.dataSource.execute(query);

        return {
            count : resultData.length
        };

    }
}
