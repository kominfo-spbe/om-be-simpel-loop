import {
  Filter,
  FilterExcludingWhere,
  repository
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {TxVerifMateriilDokumen} from '../models';
import {TxVerifMateriilDokumenRepository} from '../repositories';

export class TxVerifMateriilDokumenController {
  constructor(
    @repository(TxVerifMateriilDokumenRepository)
    public txVerifMateriilDokumenRepository: TxVerifMateriilDokumenRepository,
  ) { }

  @post('/tx-verif-materiil-dokumen')
  @response(200, {
    description: 'TxVerifMateriilDokumen model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxVerifMateriilDokumen)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifMateriilDokumen, {
            title: 'NewTxVerifMateriilDokumen',
            exclude: ['id'],
          }),
        },
      },
    })
    txVerifMateriilDokumen: Omit<TxVerifMateriilDokumen, 'id'>,
  ): Promise<TxVerifMateriilDokumen> {
    return this.txVerifMateriilDokumenRepository.create(txVerifMateriilDokumen);
  }

  @get('/tx-verif-materiil-dokumen')
  @response(200, {
    description: 'Array of TxVerifMateriilDokumen model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxVerifMateriilDokumen, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxVerifMateriilDokumen) filter?: Filter<TxVerifMateriilDokumen>,
  ): Promise<TxVerifMateriilDokumen[]> {
    return this.txVerifMateriilDokumenRepository.find(filter);
  }

  @get('/tx-verif-materiil-dokumen/{id}')
  @response(200, {
    description: 'TxVerifMateriilDokumen model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxVerifMateriilDokumen, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxVerifMateriilDokumen, {exclude: 'where'}) filter?: FilterExcludingWhere<TxVerifMateriilDokumen>
  ): Promise<TxVerifMateriilDokumen> {
    return this.txVerifMateriilDokumenRepository.findById(id, filter);
  }

  @patch('/tx-verif-materiil-dokumen/{id}')
  @response(204, {
    description: 'TxVerifMateriilDokumen PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifMateriilDokumen, {partial: true}),
        },
      },
    })
    txVerifMateriilDokumen: TxVerifMateriilDokumen,
  ): Promise<void> {
    await this.txVerifMateriilDokumenRepository.updateById(id, txVerifMateriilDokumen);
  }

  @del('/tx-verif-materiil-dokumen/{id}')
  @response(204, {
    description: 'TxVerifMateriilDokumen DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
  ): Promise<object> {

    let currentData = await this.txVerifMateriilDokumenRepository.findById(id);
    let isDeleted = false;
    let deletedDate = new Date().toISOString();

    if (currentData) {
      await this.txVerifMateriilDokumenRepository.updateById(id, {"deleted_date": deletedDate});
      isDeleted = true;
    }

    let newData = await this.txVerifMateriilDokumenRepository.findById(id);

    return {
      status: isDeleted,
      message: isDeleted ? 'Data berhasil dihapus' : 'Data gagal dihapus',
      data: {"id": id, "deleted_date": deletedDate, "data": newData}
    };
  }
}
