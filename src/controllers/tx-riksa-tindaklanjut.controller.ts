import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get, getModelSchemaRef, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {TxLampiran, TxRiksaTindaklanjut} from '../models';
import {TxLampiranRepository, TxRiksaTindaklanjutRepository} from '../repositories';

export class TxRiksaTindaklanjutController {
  constructor(
    @repository(TxRiksaTindaklanjutRepository)
    public txRiksaTindaklanjutRepository: TxRiksaTindaklanjutRepository,
    @repository(TxLampiranRepository)
    public txLampiranRepository: TxLampiranRepository,
  ) { }

  @post('/tx-riksa-tindaklanjut')
  @response(200, {
    description: 'TxRiksaTindaklanjut model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxRiksaTindaklanjut)}},
  })

  async create(
    @requestBody({
      content: {
        'application/json': {
          // schema: getModelSchemaRef(TxRiksaTindaklanjut, {
          //   title: 'NewTxRiksaTindaklanjut',
          //   exclude: ['id'],
          // }),
          schema: {
            type: 'object',
            properties: {
              txRiksaTindaklanjut: getModelSchemaRef(TxRiksaTindaklanjut, {
                title: 'Tx Riksa Tindak Lanjut',
                exclude: ['id'],
              }),
              txLampiran: {
                type: 'array',
                items: getModelSchemaRef(TxLampiran, {
                  title: 'Tx Riksa Tindak Lanjut Lampiran',
                  exclude: ['id'],
                }),
              }
            }
          }
        },
      },
    })
    dataPost: any,
  ): Promise<object> {

    //Simpan ke TxRiksaTindakLanjut
    let tindakLanjut = await this.txRiksaTindaklanjutRepository.create(dataPost.txRiksaTindaklanjut)
    // let tindakLanjut = dataPost.txRiksaTindaklanjut
    let tLampiran: any = []

    //Simpan ke Tindak Lanjut dengan loop
    for (const lampiran of dataPost.txLampiran) {

      lampiran.id_tx_laporan = tindakLanjut.id_tx_laporan
      lampiran.id_reference = tindakLanjut.id

      tLampiran.push(await this.txLampiranRepository.create(lampiran))

    }

    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: {tindakLanjut, tLampiran}
    }

    return rs;
  }

  @get('/tx-riksa-tindaklanjut/count')
  @response(200, {
    description: 'TxRiksaTindaklanjut model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxRiksaTindaklanjut) where?: Where<TxRiksaTindaklanjut>,
  ): Promise<Count> {
    return this.txRiksaTindaklanjutRepository.count(where);
  }

  @get('/tx-riksa-tindaklanjut')
  @response(200, {
    description: 'Array of TxRiksaTindaklanjut model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxRiksaTindaklanjut, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxRiksaTindaklanjut) filter?: Filter<TxRiksaTindaklanjut>,
  ): Promise<{data: (TxRiksaTindaklanjut)[]; message: string; status: boolean}> {

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txRiksaTindaklanjutRepository.find(filter)
    };
  }

  @get('/tx-riksa-tindaklanjut/{id}')
  @response(200, {
    description: 'TxRiksaTindaklanjut model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxRiksaTindaklanjut, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxRiksaTindaklanjut, {exclude: 'where'}) filter?: FilterExcludingWhere<TxRiksaTindaklanjut>
  ): Promise<{data: TxRiksaTindaklanjut; message: string; status: boolean}> {

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txRiksaTindaklanjutRepository.findById(id, filter)
    };
  }

  @patch('/tx-riksa-tindaklanjut/{id}')
  @response(204, {
    description: 'TxRiksaTindaklanjut PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaTindaklanjut, {partial: true}),
        },
      },
    })
    txRiksaTindaklanjut: TxRiksaTindaklanjut,
  ): Promise<{data: any; message: string; status: boolean}> {

    await this.txRiksaTindaklanjutRepository.updateById(id, txRiksaTindaklanjut)

    return {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txRiksaTindaklanjutRepository.findById(id)
    };

  }

  @del('/tx-riksa-tindaklanjut/{id}')
  @response(204, {
    description: 'TxRiksaTindaklanjut DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txRiksaTindaklanjutRepository.deleteById(id);
  }

  // @post('/tx-riksa-tindaklanjut/tindakLanjut')
  // @response(200, {
  //   description: 'TxRiksaTindaklanjut model instance',
  //   content: {'application/json': {schema: getModelSchemaRef(TxRiksaTindaklanjut)}},
  // })
  // async createKronologi(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         // schema: getModelSchemaRef(TxRiksaTindaklanjut, {
  //         //   title: 'NewTxRiksaTindaklanjut',
  //         //   exclude: ['id'],
  //         // }),
  //         schema: {
  //           type: 'object',
  //           properties: {
  //             txRiksaTindaklanjut: getModelSchemaRef(TxRiksaTindaklanjut, {
  //               title: 'Tx Riksa Tindak Lanjut',
  //               exclude: ['id'],
  //             }),
  //             txLampiran: {
  //               type: 'array',
  //               items: getModelSchemaRef(TxLampiran, {
  //                 title: 'Tx Riksa Tindak Lanjut Lampiran',
  //                 exclude: ['id'],
  //               }),
  //             }
  //           }
  //         }
  //       },
  //     },
  //   })
  //   dataPost: any,
  // ): Promise<object> {

  //   //Simpan ke TxRiksaTindakLanjut
  //   let tindakLanjut = await this.txRiksaTindaklanjutRepository.create(dataPost.txRiksaTindaklanjut)
  //   // let tindakLanjut = dataPost.txRiksaTindaklanjut
  //   let tLampiran: any = []

  //   //Simpan ke Tindak Lanjut dengan loop
  //   for (const lampiran of dataPost.txLampiran) {

  //     lampiran.id_tx_laporan = tindakLanjut.id_tx_laporan
  //     lampiran.id_reference = tindakLanjut.id

  //     tLampiran.push(await this.txLampiranRepository.create(lampiran))

  //   }

  //   let rs = {
  //     status: true,
  //     message: "Data berhasil ditambahkan",
  //     data: {tindakLanjut, tLampiran}
  //   }

  //   return rs;
  // }

}
