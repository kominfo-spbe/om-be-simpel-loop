import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, requestBody, response} from '@loopback/rest';
import {TxKeteranganResmonRepository} from '../repositories';
import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from '../keys';
import {FileUploadHandler} from '../types';

import {inject} from "@loopback/core";
import {TxKeteranganResmon} from "../models/tx-keterangan-resmon.model";

export class TxKeteranganResmonController {
  constructor(
    @repository(TxKeteranganResmonRepository)
    public TxKeteranganResmonRepository : TxKeteranganResmonRepository,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @inject(STORAGE_DIRECTORY) private storageDirectory: string
  ) {}

  @post('/tx-keterangan-resmon')
  @response(200, {
    description: 'TxKeteranganResmon model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxKeteranganResmon)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKeteranganResmon, {
            title: 'TxKeteranganResmon',
            exclude: ['id'],
          }),
        },
      },
    })
    TxKeteranganResmon: Omit<TxKeteranganResmon, 'id'>,
  ): Promise<{ data: TxKeteranganResmon; message: string; status: boolean }> {

    let hasil = await this.TxKeteranganResmonRepository.create(TxKeteranganResmon)
    let massage = "Data berhasil ditambahkan"

    return {
      status: true,
      message: massage,
      data: hasil
    };

  }

  @get('/tx-keterangan-resmon')
    @response(200, {
        description: 'Array of TxKeteranganResmon model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxKeteranganResmon, {includeRelations: true}),
                },
            },
        },
    })
    async find(
      @param.filter(TxKeteranganResmon) filter?: Filter<TxKeteranganResmon>,
    ): Promise<{ data: (TxKeteranganResmon)[]; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxKeteranganResmonRepository.find(filter)
      };

    }

  @get('/tx-keterangan-resmon/{id}')
  @response(200, {
    description: 'TxKeteranganResmon model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKeteranganResmon, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxKeteranganResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxKeteranganResmon>
  ): Promise<{ data: TxKeteranganResmon; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.TxKeteranganResmonRepository.findById(id, filter)
      };
  }

  @patch('/tx-keterangan-resmon/{id}')
  @response(204, {
    description: 'TxKeteranganResmon PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKeteranganResmon, {partial: true}),
        },
      },
    })
    txRiksaMonitoring: TxKeteranganResmon,
  ): Promise<{ data: any; message: string; status: boolean }> {

      await this.TxKeteranganResmonRepository.updateById(id, txRiksaMonitoring)
      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.TxKeteranganResmonRepository.findById(id)
      };
  }

  @del('/tx-keterangan-resmon/{id}')
  @response(204, {
      description: 'TxKeteranganResmon DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
      await this.TxKeteranganResmonRepository.deleteById(id);
  }

  @get('/tx-keterangan-resmon-by-laporan/{idLaporan}')
  @response(200, {
    description: 'TxKeteranganResmon model by laporan',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKeteranganResmon, {includeRelations: true}),
      },
    },
  })
  async findByLaporan(
    @param.path.string('idLaporan') idLaporan: string,
    @param.filter(TxKeteranganResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxKeteranganResmon>
  ): Promise<{ data: TxKeteranganResmon; message: string; status: boolean }> {

    const sql = `select * from simpel_4.tx_dokumen_resmon\n`+
                `where id_tx_laporan = '${idLaporan}'`
    const result = await this.TxKeteranganResmonRepository.dataSource.execute(sql);

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: result
    };
  }

  @get('/tx-keterangan-resmon-by-riksa/{idRiksa}')
  @response(200, {
    description: 'TxKeteranganResmon model by riksa',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKeteranganResmon, {includeRelations: true}),
      },
    },
  })
  async findByRiksa(
    @param.path.string('idRiksa') idRiksa: string,
    @param.filter(TxKeteranganResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxKeteranganResmon>
  ): Promise<{ data: TxKeteranganResmon; message: string; status: boolean }> {

    const sql = `select * from simpel_4.tx_dokumen_resmon\n`+
                `where id_tx_riksa = '${idRiksa}'\n`+
                `limit 1`

    const result = await this.TxKeteranganResmonRepository.dataSource.execute(sql);
    const resultFinal = result[0]

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: resultFinal
      };
  }

}
