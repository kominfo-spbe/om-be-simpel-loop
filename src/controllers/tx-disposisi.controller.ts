import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxDisposisi, TxDisposisiPvlKaper} from '../models';
import {TxDisposisiRepository} from '../repositories';

export class TxDisposisiController {
  constructor(
    @repository(TxDisposisiRepository)
    public txDisposisiRepository : TxDisposisiRepository,
  ) {}

  @post('/tx-disposisi')
  @response(200, {
    description: 'TxDisposisi model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxDisposisi)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisi, {
            title: 'NewTxDisposisi',
            exclude: ['id'],
          }),
        },
      },
    })
    txDisposisi: Omit<TxDisposisi, 'id'>,
  ): Promise<{ data: TxDisposisi; message: string; status: boolean }> {


      let rs = {
        status: true,
        message: "Data berhasil ditambahkan",
        data: await this.txDisposisiRepository.create(txDisposisi)

      }

      return rs;
  }

  @get('/tx-disposisi/count')
  @response(200, {
    description: 'TxDisposisi model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxDisposisi) where?: Where<TxDisposisi>,
  ): Promise<Count> {
    return this.txDisposisiRepository.count(where);
  }

  @get('/tx-disposisi')
  @response(200, {
    description: 'Array of TxDisposisi model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxDisposisi, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxDisposisi) filter?: Filter<TxDisposisi>,
  ): Promise<{ data: (TxDisposisi)[]; message: string; status: boolean }> {
      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txDisposisiRepository.find(filter)
      };
  }

  @patch('/tx-disposisi')
  @response(200, {
    description: 'TxDisposisi PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisi, {partial: true}),
        },
      },
    })
    txDisposisi: TxDisposisi,
    @param.where(TxDisposisi) where?: Where<TxDisposisi>,
  ): Promise<Count> {
    return this.txDisposisiRepository.updateAll(txDisposisi, where);
  }

  @get('/tx-disposisi/{id}')
  @response(200, {
    description: 'TxDisposisi model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDisposisi, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxDisposisi, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDisposisi>
  ): Promise<{ data: TxDisposisi; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txDisposisiRepository.findById(id, filter)
      };
  }

  @patch('/tx-disposisi/{id}')
  @response(204, {
    description: 'TxDisposisi PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisi, {partial: true}),
        },
      },
    })
    txDisposisi: TxDisposisi,
  ): Promise<{ data: any; message: string; status: boolean }> {
      await this.txDisposisiRepository.updateById(id, txDisposisi);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txDisposisiRepository.findById(id)
      };
  }

  @put('/tx-disposisi/{id}')
  @response(204, {
    description: 'TxDisposisi PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txDisposisi: TxDisposisi,
  ): Promise<{ data: TxDisposisiPvlKaper; message: string; status: boolean }> {
    await this.txDisposisiRepository.replaceById(id, txDisposisi);

    return {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txDisposisiRepository.findById(id)
    };
  }

  @del('/tx-disposisi/{id}')
  @response(204, {
    description: 'TxDisposisi DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txDisposisiRepository.deleteById(id);
  }
}
