import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response, RestBindings, Request,
} from '@loopback/rest';
import {TxDokumen, TxLaporan, TxRiksa, TxSettingProsesResmon} from '../models';
import {TxDokumenRepository, TxLaporanRepository, TxRiksaRepository} from '../repositories';
import fs from "fs";
import https from "https";
import {inject, service} from "@loopback/core";
import {GenerateFileService} from "../services";

export class TxRiksaController {
  constructor(
    @repository(TxRiksaRepository)
    public txRiksaRepository : TxRiksaRepository,
    @repository(TxLaporanRepository)
    public txLaporanRepository: TxLaporanRepository,
    @repository(TxDokumenRepository)
    public txDokumenRepository: TxDokumenRepository,
    @service(GenerateFileService)
    public dokumenService: GenerateFileService,
    @inject(RestBindings.Http.REQUEST) private request: Request
  ) {}

  @post('/tx-riksa')
  @response(200, {
    description: 'TxRiksa model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxRiksa)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksa, {
            title: 'NewTxRiksa',
            exclude: ['id'],
          }),
        },
      },
    })
    txRiksa: Omit<TxRiksa, 'id'>,
  ): Promise<{ data: TxRiksa; message: string; status: boolean }> {

    let resRiksa = await this.txRiksaRepository.create(txRiksa);

    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: resRiksa
    }

    return rs;

  }

  @get('/tx-riksa/count')
  @response(200, {
    description: 'TxRiksa model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxRiksa) where?: Where<TxRiksa>,
  ): Promise<Count> {
    return this.txRiksaRepository.count(where);
  }

  @get('/tx-riksa')
  @response(200, {
    description: 'Array of TxRiksa model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxRiksa, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxRiksa) filter?: Filter<TxRiksa>,
  ): Promise<{ data: (TxRiksa)[]; message: string; status: boolean }> {

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txRiksaRepository.find(filter)
    };

  }

  @patch('/tx-riksa')
  @response(200, {
    description: 'TxRiksa PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksa, {partial: true}),
        },
      },
    })
    txRiksa: TxRiksa,
    @param.where(TxRiksa) where?: Where<TxRiksa>,
  ): Promise<Count> {
    return this.txRiksaRepository.updateAll(txRiksa, where);
  }

  @get('/tx-riksa/{id}')
  @response(200, {
    description: 'TxRiksa model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxRiksa, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxRiksa, {exclude: 'where'}) filter?: FilterExcludingWhere<TxRiksa>
  ): Promise<{ data: TxRiksa; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txRiksaRepository.findById(id, filter)
      };
  }

  @patch('/tx-riksa/{id}')
  @response(204, {
    description: 'TxRiksa PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksa, {partial: true}),
        },
      },
    })
    txRiksa: TxRiksa,
  ): Promise<{ data: any; message: string; status: boolean }> {

    let resRiksa = await this.txRiksaRepository.updateById(id, txRiksa)

    let laporan = await this.txLaporanRepository.findById(txRiksa.id_tx_laporan);

    if (txRiksa.media === "surat") {
      return await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", laporan.id, laporan.status_laporan ?? "", laporan.no_agenda?? "", laporan.kode_kantor ?? "").then(() => {
        return {
          status: true,
          message: "Data berhasil ditambahkan",
          data: resRiksa
        }
      }).catch(() => {
        return {
          status: true,
          message: "Dokumen gagal dibuat",
          data: resRiksa
        }
      });
    }

      return {
        status: true,
        message: "Data berhasil diubah",
        data: resRiksa
      };

  }

  @put('/tx-riksa/{id}')
  @response(204, {
    description: 'TxRiksa PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txRiksa: TxRiksa,
  ): Promise<void> {
    await this.txRiksaRepository.replaceById(id, txRiksa);
  }

  @del('/tx-riksa/{id}')
  @response(204, {
    description: 'TxRiksa DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txRiksaRepository.deleteById(id);
  }


  private async getFileFromReport(namafile: string, idtxlaporan: string, status: string, nomor_agenda: string, kode_kantor: string) {
    const endpoint = 'pemberitahuan_dimulai_pemeriksaan';
    let version = 1

    const tDokumen = await this.txDokumenRepository.find({
      where: {
        id_tx_laporan: idtxlaporan,
        tipe_dokumen: namafile
      },
      order: ['version DESC'],
      limit: 1
    });

    if (tDokumen.length > 0) {
      version = ((tDokumen[0].version ? tDokumen[0].version : 0) + 1);
    }

    let template = namafile;
    if (kode_kantor !== 'JKT') {
      template += '_perwakilan';
    }

    // await this.getDoc(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, template);
    await this.dokumenService.generateDokumen(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, template)
  }

  // private async getDoc(endpoint: string, namafile: string, idtxlaporan: string, version: number, nomor_agenda: string, status: string, template: string) {
  //   return await new Promise((resolve,reject) => {
  //     const folderPath = './public/uploads/dokumen/' + idtxlaporan;
  //     if (!fs.existsSync(folderPath)) {
  //       fs.mkdirSync(folderPath, { recursive: true });
  //     }
  //
  //     const headers = {...this.request.headers};
  //     const token = headers.authorization !== null && headers.authorization?.startsWith("Bearer") ?
  //         headers.authorization?.substring(7, headers.authorization.length) : undefined;
  //     // var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsInN1YnN0YW5zaSI6WyJzdWJzdGFuc2lfMyIsInN1YnN0YW5zaV8zIiwic3Vic3RhbnNpXzEiLCJzdWJzdGFuc2lfMSIsInN1YnN0YW5zaV80Iiwic3Vic3RhbnNpXzQiLCJzdWJzdGFuc2lfNSIsInN1YnN0YW5zaV81Iiwic3Vic3RhbnNpXzYiLCJzdWJzdGFuc2lfOSIsInN1YnN0YW5zaV8xMSIsInN1YnN0YW5zaV8xMyIsInN1YnN0YW5zaV8xNSIsInN1YnN0YW5zaV8xNiIsInN1YnN0YW5zaV8xOCIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8yNiIsInN1YnN0YW5zaV8zMCIsInN1YnN0YW5zaV8zMSIsInN1YnN0YW5zaV8zMiIsInN1YnN0YW5zaV8zNCIsInN1YnN0YW5zaV8xMiIsInN1YnN0YW5zaV8zMyIsInN1YnN0YW5zaV8zNyIsInN1YnN0YW5zaV80OCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MiIsInN1YnN0YW5zaV84Il0sInBlcm1pc3Npb25zIjpbIlBldHVnYXMgUmlrc2EiLCJSZWdpc3RyYXNpLVJlZ2lzdHJhc2ktVGFtYmFoIiwiUmVnaXN0cmFzaS1PbWJ1ZHNtYW4gT25saW5lLVJlZ2lzdHJhc2lrYW4iLCJQZXR1Z2FzIFBWTCIsIlJlZ2lzdHJhc2ktUmVnaXN0cmFzaS1QZW51Z2FzYW4iXSwia2luZCI6ImFjY2VzcyIsInJvbGVzIjpbIktlcGFsYSBUaW0gUmlrc2EgMyIsIktlcGFsYSBUaW0gUmlrc2EgNyIsIlRpbSBQVkwiLCJLZXBhbGEgS2Vhc2lzdGVuYW4gUmlrc2EiLCJLZXBhbGEgVGltIFJpa3NhIDYiLCJBbmdnb3RhIFJpa3NhIDEiLCJLZXBhbGEgS2Vhc2lzdGVuYW4iLCJLZXBhbGEgVGltIFJpa3NhIDIiLCJLZXBhbGEgVGltIFJpa3NhIDQiLCJLZXBhbGEgVGltIFJpa3NhIDUgVGVzdCIsIktlcGFsYSBUaW0gUmlrc2EgMSJdLCJleHAiOjE2NjY3NzI2NDYsInVzZXIiOnsiaWQiOiJlYWM5NzllZC1mNTBkLTQ3MmYtODUwYy1mOWQxNjg1ZWRlYWQiLCJ1c2VybmFtZSI6ImNob2lydWRkaW5AZ21haWwuY29tIiwiZW1haWwiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDMifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNyJ9LHsiYXV0aG9yaXR5IjoiVGltIFBWTCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIFJpa3NhIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDYifSx7ImF1dGhvcml0eSI6IkFuZ2dvdGEgUmlrc2EgMSJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDIifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIFRpbSBSaWtzYSA1IFRlc3QifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgMSJ9XSwiZW5hYmxlZCI6dHJ1ZSwiYWNjb3VudE5vbkxvY2tlZCI6dHJ1ZSwiY3JlZGVudGlhbHNOb25FeHBpcmVkIjp0cnVlLCJhY2NvdW50Tm9uRXhwaXJlZCI6dHJ1ZX0sImlhdCI6MTY2NjY4NjI0Nn0.lhEQneVY6t9xC0YzYXlxl2bPALqhLwT56qLeawBr2hqEq6D4HcwARh35W0qFhmt6LHKR4m-NblNS_lO1OeUzrg'
  //
  //     const options = {
  //       hostname: 'api-report.ombudsman.dev.layanan.go.id',
  //       path: `/v1/report/${endpoint}?jenisSurat=${namafile}&template=${template}&idLaporan=${idtxlaporan}&lang=en&tte=&nik=&passphrase=`,
  //       headers: {
  //         Authorization: 'Bearer ' + token
  //       }
  //     }
  //     const filename: string = folderPath + '/' + namafile + '_' + idtxlaporan + '_' + version + '.pdf'
  //     const file = fs.createWriteStream(filename)
  //
  //     https.get(options, res => {
  //       if (res.statusCode !== 200) {
  //         fs.unlink(filename, () => {
  //           console.log(res.statusCode);
  //           // reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
  //         });
  //         resolve(true)
  //         return;
  //       }
  //
  //       res.pipe(file);
  //       res.on('data', chunk => {
  //         // data.push(chunk);
  //       });
  //       file.on("finish", () => {
  //         file.close();
  //         // @ts-ignore
  //         const dataTxDokumen: TxDokumen = {
  //           filename,
  //           id_tx_laporan: idtxlaporan,
  //           tipe_dokumen: template,
  //           nomor_agenda: nomor_agenda,
  //           status_dokumen: status,
  //           version
  //         }
  //
  //         this.txDokumenRepository.save(dataTxDokumen)
  //         resolve(true);
  //       });
  //     })
  //         .on("error", listeners => {
  //           reject(false);
  //         });
  //   });
  // }



}
