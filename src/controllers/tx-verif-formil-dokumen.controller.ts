import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxLaporan, TxVerifFormilDokumen} from '../models';
import {TxVerifFormilDokumenRepository} from '../repositories';

export class TxVerifFormilDokumenController {
  constructor(
    @repository(TxVerifFormilDokumenRepository)
    public txVerifFormilDokumenRepository : TxVerifFormilDokumenRepository,
  ) {}

  @post('/tx-verif-formil-dokumen')
  @response(200, {
    description: 'TxVerifFormilDokumen model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxVerifFormilDokumen)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifFormilDokumen, {
            title: 'NewTxVerifFormilDokumen',
            exclude: ['id'],
          }),
        },
      },
    })
    txVerifFormilDokumen: Omit<TxVerifFormilDokumen, 'id'>,
  ): Promise<TxVerifFormilDokumen> {
    return this.txVerifFormilDokumenRepository.create(txVerifFormilDokumen);
  }

  @get('/tx-verif-formil-dokumen/count')
  @response(200, {
    description: 'TxVerifFormilDokumen model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxVerifFormilDokumen) where?: Where<TxVerifFormilDokumen>,
  ): Promise<Count> {
    return this.txVerifFormilDokumenRepository.count(where);
  }

  @get('/tx-verif-formil-dokumen')
  @response(200, {
    description: 'Array of TxVerifFormilDokumen model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxVerifFormilDokumen, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxVerifFormilDokumen) filter?: Filter<TxVerifFormilDokumen>,
  ): Promise<{ data: (TxVerifFormilDokumen)[]; message: string; status: boolean }> {
      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txVerifFormilDokumenRepository.find(filter)
      };
  }

  @patch('/tx-verif-formil-dokumen')
  @response(200, {
    description: 'TxVerifFormilDokumen PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifFormilDokumen, {partial: true}),
        },
      },
    })
    txVerifFormilDokumen: TxVerifFormilDokumen,
    @param.where(TxVerifFormilDokumen) where?: Where<TxVerifFormilDokumen>,
  ): Promise<Count> {
    return this.txVerifFormilDokumenRepository.updateAll(txVerifFormilDokumen, where);
  }

  @get('/tx-verif-formil-dokumen/{id}')
  @response(200, {
    description: 'TxVerifFormilDokumen model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxVerifFormilDokumen, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxVerifFormilDokumen, {exclude: 'where'}) filter?: FilterExcludingWhere<TxVerifFormilDokumen>
  ): Promise<TxVerifFormilDokumen> {
    return this.txVerifFormilDokumenRepository.findById(id, filter);
  }

  @patch('/tx-verif-formil-dokumen/{id}')
  @response(204, {
    description: 'TxVerifFormilDokumen PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifFormilDokumen, {partial: true}),
        },
      },
    })
    txVerifFormilDokumen: TxVerifFormilDokumen,
  ): Promise<{ data: any; message: string; status: boolean }> {
      await this.txVerifFormilDokumenRepository.updateById(id, txVerifFormilDokumen);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txVerifFormilDokumenRepository.findById(id)
      };
  }

  @put('/tx-verif-formil-dokumen/{id}')
  @response(204, {
    description: 'TxVerifFormilDokumen PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txVerifFormilDokumen: TxVerifFormilDokumen,
  ): Promise<void> {
    await this.txVerifFormilDokumenRepository.replaceById(id, txVerifFormilDokumen);
  }

  @del('/tx-verif-formil-dokumen/{id}')
  @response(204, {
    description: 'TxVerifFormilDokumen DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txVerifFormilDokumenRepository.deleteById(id);
  }
}
