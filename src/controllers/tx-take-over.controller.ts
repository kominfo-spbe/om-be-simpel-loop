import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxTakeOver} from '../models';
import {TxLaporanRepository, TxTakeOverRepository} from '../repositories';

export class TxTakeOverController {
    constructor(
        @repository(TxTakeOverRepository)
        public txTakeOverRepository: TxTakeOverRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository
    ) {
    }

    @post('/tx-take-over')
    @response(200, {
        description: 'TxTakeOver model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxTakeOver)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxTakeOver, {
                        title: 'NewTxTakeOver',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txTakeOver: Omit<TxTakeOver, 'id'>,
    ): Promise<{ data: TxTakeOver; message: string; status: boolean }> {

        let existingDataLaporan = await this.txLaporanRepository.findById(txTakeOver.id_tx_laporan);
        if (existingDataLaporan) {
            txTakeOver.no_agenda = existingDataLaporan.no_agenda
            txTakeOver.officer_old_by = existingDataLaporan.officer_by
            txTakeOver.take_over_date = new Date().toISOString()
        }


        //save data
        let savedData = await this.txTakeOverRepository.create(txTakeOver);

        //update officer_by di table tx_laporan
        if (savedData) {
            var txLaporan: any = {}
            txLaporan.id = txTakeOver.id_tx_laporan
            txLaporan.officer_by = txTakeOver.officer_new_by

            let updatedLaporan = await this.txLaporanRepository.updateById(txTakeOver.id_tx_laporan, txLaporan);

        }


        let rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedData

        }

        return rs;
    }

    @get('/tx-take-over/count')
    @response(200, {
        description: 'TxTakeOver model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxTakeOver) where?: Where<TxTakeOver>,
    ): Promise<Count> {
        return this.txTakeOverRepository.count(where);
    }

    @get('/tx-take-over')
    @response(200, {
        description: 'Array of TxTakeOver model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxTakeOver, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxTakeOver) filter?: Filter<TxTakeOver>,
    ): Promise<{ data: (TxTakeOver)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txTakeOverRepository.find(filter)
        };
    }

    @patch('/tx-take-over')
    @response(200, {
        description: 'TxTakeOver PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxTakeOver, {partial: true}),
                },
            },
        })
            txTakeOver: TxTakeOver,
        @param.where(TxTakeOver) where?: Where<TxTakeOver>,
    ): Promise<Count> {
        return this.txTakeOverRepository.updateAll(txTakeOver, where);
    }

    @get('/tx-take-over/{id}')
    @response(200, {
        description: 'TxTakeOver model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxTakeOver, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxTakeOver, {exclude: 'where'}) filter?: FilterExcludingWhere<TxTakeOver>
    ): Promise<{ data: TxTakeOver; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txTakeOverRepository.findById(id, filter)
        };
    }

    @patch('/tx-take-over/{id}')
    @response(204, {
        description: 'TxTakeOver PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxTakeOver, {partial: true}),
                },
            },
        })
            txTakeOver: TxTakeOver,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txTakeOverRepository.updateById(id, txTakeOver);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txTakeOverRepository.findById(id)
        };
    }

    @put('/tx-take-over/{id}')
    @response(204, {
        description: 'TxTakeOver PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txTakeOver: TxTakeOver,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txTakeOverRepository.replaceById(id, txTakeOver);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txTakeOverRepository.findById(id)
        };
    }

    @del('/tx-take-over/{id}')
    @response(204, {
        description: 'TxTakeOver DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txTakeOverRepository.deleteById(id);
    }
}
