import {inject} from "@loopback/core";
import {CountSchema, repository} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, post, requestBody, response} from '@loopback/rest';
import {TxLaporanTanggapan} from "../models";
import {TxLaporanTanggapanRepository} from "../repositories";
import {PvlService} from "../services";

export class TxLaporanTanggapanController {
    constructor(
        @repository(TxLaporanTanggapanRepository)
        public txLaporanTanggapanRepository: TxLaporanTanggapanRepository,
        @inject('services.PvlService')
        public pvlService: PvlService
    ) {
    }

    @post('/tx-laporan-tanggapan')
    @response(200, {
        description: 'TxLaporanTanggapan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxLaporanTanggapan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLaporanTanggapan, {
                        title: 'NewTxLaporanTanggapan',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txLaporanTanggapan: Omit<TxLaporanTanggapan, 'id'>,
    ): Promise<{ data: TxLaporanTanggapan; message: string; status: boolean }> {
        const savedTanggapan = await this.txLaporanTanggapanRepository.create(txLaporanTanggapan);
        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedTanggapan
        };
    }

    @get('/tx-laporan-tanggapan/duration')
    @response(200, {
        description: 'TxLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async duration(
        @param.query.string('id_laporan') id: string
    ): Promise<{ data: { durasi: number }; message: string; status: boolean }> {
        const sql = "select \n" +
            "a.id,\n" +
            "a.id_tx_laporan,\n" +
            "a.tanggal,\n" +
            "a.melalui,\n" +
            "a.keterangan, \n" +
            "a.created_date, \n" +
            "a.ket_jenis_tanggapan, \n" +
            "a.file_name_tanggapan \n" +
            "from simpel_4.tx_laporan_tanggapan a\n" +
            "where a.id_tx_laporan = $1";

        const result = await this.txLaporanTanggapanRepository.dataSource.execute(sql, [id]);

        const data = result[0];
        const time = new Date().getTime() - new Date(data['created_date']).getTime();
        let durasi = await this.pvlService.TimeDay(time);
        if (durasi != null) {
            const dr = durasi["durasi"].toString();
            if (dr.toLowerCase() === 'baru') {
                durasi = 1;
            } else {
                if (dr !== 'null') {
                    durasi = Number.parseInt(dr);
                } else {
                    durasi = 0;
                }
            }
        }
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: {
                durasi: durasi
            }
        };
    }

    @get('/tx-laporan-tanggapan')
    @response(200, {
        description: 'TxLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporanTanggapan, {includeRelations: true}),
            },
        },
    })
    async findByLaporanId(
        @param.query.string('id_laporan') id: string
    ): Promise<object> {

        const sql = "select \n" +
            "a.id,\n" +
            "a.id_tx_laporan,\n" +
            "a.tanggal,\n" +
            "a.melalui,\n" +
            "a.keterangan,\n" +
            "a.ket_jenis_tanggapan, \n" +
            "a.file_name_tanggapan \n" +
            "from simpel_4.tx_laporan_tanggapan a\n" +
            "where a.id_tx_laporan = $1";

        const result = await this.txLaporanTanggapanRepository.dataSource.execute(sql, [id]);
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };
    }

    @del('/tx-laporan-tanggapan/{id}')
    @response(204, {
        description: 'TxLaporan DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txLaporanTanggapanRepository.deleteById(id);
    }

}
