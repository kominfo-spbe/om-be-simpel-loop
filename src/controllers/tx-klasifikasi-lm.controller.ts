import {inject} from "@loopback/core";
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {TxKlasifikasiLm} from '../models';
import {TxKlasifikasiLmRepository} from '../repositories';
import {PvlService} from "../services";

export class TxKlasifikasiLmController {
  constructor(
    @repository(TxKlasifikasiLmRepository)
    public txKlasifikasiLmRepository: TxKlasifikasiLmRepository,
    @inject('services.PvlService')
    public pvlService: PvlService
  ) { }

  @post('/tx-klasifikasi-lm')
  @response(200, {
    description: 'TxKlasifikasiLm model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxKlasifikasiLm)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKlasifikasiLm, {
            title: 'NewTxKlasifikasiLm',
            exclude: ['id'],
          }),
        },
      },
    })
    txKlasifikasiLm: Omit<TxKlasifikasiLm, 'id'>,
  ): Promise<object> {

    let dataPost = txKlasifikasiLm

    dataPost.total_skor = await this.pvlService.hitungSkor(dataPost.jml_terlapor_terkait, dataPost.lokasi_terlapor, dataPost.penerima_manfaat, dataPost.permasalahan_dilaporkan)

    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: await this.txKlasifikasiLmRepository.create(dataPost)
    }

    return rs;
  }

  @get('/tx-klasifikasi-lm/count')
  @response(200, {
    description: 'TxKlasifikasiLm model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxKlasifikasiLm) where?: Where<TxKlasifikasiLm>,
  ): Promise<Count> {
    return this.txKlasifikasiLmRepository.count(where);
  }

  @get('/tx-klasifikasi-lm')
  @response(200, {
    description: 'Array of TxKlasifikasiLm model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxKlasifikasiLm, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxKlasifikasiLm) filter?: Filter<TxKlasifikasiLm>,
  ): Promise<object> {

    let listUraian = ['permasalahan_dilaporkan', 'klasifikasi_laporan']
    let resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txKlasifikasiLmRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  /*  @patch('/tx-klasifikasi-lm')
    @response(200, {
      description: 'TxKlasifikasiLm PATCH success count',
      content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
      @requestBody({
        content: {
          'application/json': {
            schema: getModelSchemaRef(TxKlasifikasiLm, {partial: true}),
          },
        },
      })
      txKlasifikasiLm: TxKlasifikasiLm,
      @param.where(TxKlasifikasiLm) where?: Where<TxKlasifikasiLm>,
    ): Promise<Count> {
      return this.txKlasifikasiLmRepository.updateAll(txKlasifikasiLm, where);
    }*/

  @get('/tx-klasifikasi-lm/{id}')
  @response(200, {
    description: 'TxKlasifikasiLm model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKlasifikasiLm, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxKlasifikasiLm, {exclude: 'where'}) filter?: FilterExcludingWhere<TxKlasifikasiLm>
  ): Promise<object> {
    let listUraian = ['permasalahan_dilaporkan', 'klasifikasi_laporan']
    let resultData = await this.pvlService.getMLookUpFindById(id, filter, listUraian, this.txKlasifikasiLmRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  /*@get('/tx-klasifikasi-lm-laporan/{idLaporan}')
  @response(200, {
    description: 'TxKlasifikasiLm model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKlasifikasiLm, {includeRelations: true}),
      },
    },
  })
  async findOne(
    @param.filter(TxKlasifikasiLm) filter?: Filter<TxKlasifikasiLm>
  ): Promise<{  status: boolean; message: string; data: TxKlasifikasiLm; }> {
    return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txKlasifikasiLmRepository.findOne(filter)
      };
  }*/

  @patch('/tx-klasifikasi-lm/{id}')
  @response(204, {
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKlasifikasiLm, {partial: true}),
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKlasifikasiLm, {partial: true}),
        },
      },
    })
    txKlasifikasiLm: TxKlasifikasiLm,
  ): Promise<Object> {

    let currentData = await this.txKlasifikasiLmRepository.findById(id);
    let dataPost = txKlasifikasiLm

    let jml_terlapor_terkait = dataPost.hasOwnProperty('jml_terlapor_terkait') ? dataPost.jml_terlapor_terkait : currentData.jml_terlapor_terkait;
    let lokasi_terlapor = dataPost.hasOwnProperty('lokasi_terlapor') ? dataPost.lokasi_terlapor : currentData.lokasi_terlapor;
    let penerima_manfaat = dataPost.hasOwnProperty('penerima_manfaat') ? dataPost.penerima_manfaat : currentData.penerima_manfaat;
    let permasalahan_dilaporkan = dataPost.hasOwnProperty('permasalahan_dilaporkan') ? dataPost.permasalahan_dilaporkan : currentData.permasalahan_dilaporkan;

    dataPost.total_skor = await this.pvlService.hitungSkor(jml_terlapor_terkait, lokasi_terlapor, penerima_manfaat, permasalahan_dilaporkan)

    let rs = {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txKlasifikasiLmRepository.updateById(id, dataPost)
    }

    return rs;
  }

  /*@put('/tx-klasifikasi-lm/{id}')
  @response(204, {
    description: 'TxKlasifikasiLm PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txKlasifikasiLm: TxKlasifikasiLm,
  ): Promise<void> {
    await this.txKlasifikasiLmRepository.replaceById(id, txKlasifikasiLm);
  }*/

  @del('/tx-klasifikasi-lm/{id}')
  @response(204, {
    description: 'TxKlasifikasiLm DELETE success',
  })
  async deleteById(@param.path.string('id') id: string)
    : Promise<Object> {
    let rs = {
      status: true,
      message: "Data dihapus",
      data: await this.txKlasifikasiLmRepository.deleteById(id)
    }

    //send to log


    return rs;
  }
}
