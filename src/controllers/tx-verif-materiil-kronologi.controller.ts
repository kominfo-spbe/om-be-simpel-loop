import {
  Filter,
  FilterExcludingWhere,
  repository
} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, requestBody, response} from '@loopback/rest';
import {TxVerifMateriilKronologi} from '../models';
import {TxVerifMateriilKronologiRepository} from '../repositories';

export class TxVerifMateriilKronologiController {
  constructor(
    @repository(TxVerifMateriilKronologiRepository)
    public txVerifMateriilKronologiRepository: TxVerifMateriilKronologiRepository
  ) {
  }

  @post('/tx-verif-materiil-kronologi')
  @response(200, {
    description: 'Input Transaksi Verifikasi Materiil Kronologi',
    content: {'application/json': {schema: getModelSchemaRef(TxVerifMateriilKronologi)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifMateriilKronologi, {
            title: 'Transaksi Verifikasi Materiil Kronologi Baru',
            exclude: ['id'],
          }),
        },
      },
    })
    txVerifMateriilKronologi: Omit<TxVerifMateriilKronologi, 'id'>,
  ): Promise<TxVerifMateriilKronologi> {
    return this.txVerifMateriilKronologiRepository.create(txVerifMateriilKronologi);
  }

  @get('/tx-verif-materiil-kronologi')
  @response(200, {
    description: 'List Transaksi Verifikasi Materiil Kronologi',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxVerifMateriilKronologi, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxVerifMateriilKronologi) filter?: Filter<TxVerifMateriilKronologi>,
  ): Promise<{data: (TxVerifMateriilKronologi)[]; message: string; status: boolean}> {
    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txVerifMateriilKronologiRepository.find(filter)
    };
  }

  @get('/tx-verif-materiil-kronologi/{id}')
  @response(200, {
    description: 'Detail Transaksi Verifikasi Materiil Kronologi',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxVerifMateriilKronologi, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxVerifMateriilKronologi, {exclude: 'where'}) filter?: FilterExcludingWhere<TxVerifMateriilKronologi>
  ): Promise<TxVerifMateriilKronologi> {
    return this.txVerifMateriilKronologiRepository.findById(id, filter);
  }

  @patch('/tx-verif-materiil-kronologi/{id}')
  @response(204, {
    description: 'TxVerifMateriilKronologi PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxVerifMateriilKronologi, {partial: true}),
        },
      },
    })
    txVerifMateriilKronologi: TxVerifMateriilKronologi,
  ): Promise<object> {
    const currentData = await this.txVerifMateriilKronologiRepository.findById(id);
    let isUpdated = false;

    if (currentData) {
      txVerifMateriilKronologi.updated_date = new Date().toISOString();
      await this.txVerifMateriilKronologiRepository.updateById(id, txVerifMateriilKronologi);
      isUpdated = true;
    }

    const newData = await this.txVerifMateriilKronologiRepository.findById(id);

    return {
      status: isUpdated,
      message: isUpdated ? 'Data berhasil diubah' : 'Data gagal diubah',
      data: {"data_lama": currentData, "data_baru": newData}
    };
  }

  @del('/tx-verif-materiil-kronologi/{id}')
  @response(204, {
    description: 'TxVerifMateriilKronologi DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
  ): Promise<object> {

    const currentData = await this.txVerifMateriilKronologiRepository.findById(id);
    let isDeleted = false;
    const deletedDate = new Date().toISOString();

    if (currentData) {
      await this.txVerifMateriilKronologiRepository.updateById(id, {"deleted_date": deletedDate});
      isDeleted = true;
    }

    const newData = await this.txVerifMateriilKronologiRepository.findById(id);

    return {
      status: isDeleted,
      message: isDeleted ? 'Data berhasil dihapus' : 'Data gagal dihapus',
      data: {"id": id, "deleted_date": deletedDate, "data": newData}
    };
  }

  @patch('/tx-verif-materiil-kronologi/swap')
  @response(204, {
    description: 'TxVerifMateriilKronologi PATCH success',
  })
  async swap(
    @requestBody() txVerifMateriilKronologi: TxVerifMateriilKronologi[],
  ): Promise<object> {

    for (const key of txVerifMateriilKronologi) {
      await this.txVerifMateriilKronologiRepository.updateById(key.id, key)

      }

    return {
      message: 'Data berhasil diubah'
    };
  }


}


