import {
    Count,
    CountSchema,
    Filter,
    FilterExcludingWhere,
    repository,
    Where
} from '@loopback/repository';
import {
    del, get,
    getModelSchemaRef, param, patch, post, put, requestBody,
    response
} from '@loopback/rest';
import {TxCabutLaporan} from '../models';
import {TxCabutLaporanRepository, TxLaporanRepository} from '../repositories';

export class TxCabutLaporanController {
    constructor(
        @repository(TxCabutLaporanRepository)
        public txCabutLaporanRepository: TxCabutLaporanRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository
    ) {
    }

    @post('/tx-cabut-laporan')
    @response(200, {
        description: 'TxCabutLaporan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxCabutLaporan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxCabutLaporan, {
                        title: 'NewTxCabutLaporan',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txCabutLaporan: Omit<TxCabutLaporan, 'id'>,
    ): Promise<{ data: TxCabutLaporan; message: string; status: boolean }> {

        //save data
        const savedData = await this.txCabutLaporanRepository.create(txCabutLaporan);

        //update is_suspend di table tx_laporan
        if (savedData) {
            const txLaporan: any = {}
            txLaporan.id = txCabutLaporan.id_tx_laporan
            // txLaporan.is_suspended = "1"
            txLaporan.status_laporan = "LT"

            const updatedLaporan = await this.txLaporanRepository.updateById(txCabutLaporan.id_tx_laporan, txLaporan);

        }

        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedData

        }
    }

    @get('/tx-cabut-laporan/count')
    @response(200, {
        description: 'TxCabutLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxCabutLaporan) where?: Where<TxCabutLaporan>,
    ): Promise<Count> {
        return this.txCabutLaporanRepository.count(where);
    }

    @get('/tx-cabut-laporan')
    @response(200, {
        description: 'Array of TxCabutLaporan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxCabutLaporan, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxCabutLaporan) filter?: Filter<TxCabutLaporan>,
    ): Promise<{ data: (TxCabutLaporan)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txCabutLaporanRepository.find(filter)
        };
    }

    @patch('/tx-cabut-laporan')
    @response(200, {
        description: 'TxCabutLaporan PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxCabutLaporan, {partial: true}),
                },
            },
        })
            txCabutLaporan: TxCabutLaporan,
        @param.where(TxCabutLaporan) where?: Where<TxCabutLaporan>,
    ): Promise<Count> {
        return this.txCabutLaporanRepository.updateAll(txCabutLaporan, where);
    }

    @get('/tx-cabut-laporan/{id}')
    @response(200, {
        description: 'TxCabutLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxCabutLaporan, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxCabutLaporan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxCabutLaporan>
    ): Promise<{ data: TxCabutLaporan; message: string; status: boolean }> {
        //return this.txLaporanRepository.findById(id, filter);

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txCabutLaporanRepository.findById(id, filter)
        };
    }

    @patch('/tx-cabut-laporan/{id}')
    @response(204, {
        description: 'TxCabutLaporan PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxCabutLaporan, {partial: true}),
                },
            },
        })
            txCabutLaporan: TxCabutLaporan,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txCabutLaporanRepository.updateById(id, txCabutLaporan);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txCabutLaporanRepository.findById(id)
        };
    }

    @put('/tx-cabut-laporan/{id}')
    @response(204, {
        description: 'TxCabutLaporan PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txCabutLaporan: TxCabutLaporan,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txCabutLaporanRepository.replaceById(id, txCabutLaporan);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txCabutLaporanRepository.findById(id)
        };
    }

    @del('/tx-cabut-laporan/{id}')
    @response(204, {
        description: 'TxCabutLaporan DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txCabutLaporanRepository.deleteById(id);
    }
}
