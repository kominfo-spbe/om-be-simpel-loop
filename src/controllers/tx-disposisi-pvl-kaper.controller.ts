import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxDisposisiPvlKaper, TxLaporan} from '../models';
import {TxDisposisiPvlKaperRepository} from '../repositories';

export class TxDisposisiPvlKaperController {
  constructor(
    @repository(TxDisposisiPvlKaperRepository)
    public txDisposisiPvlKaperRepository : TxDisposisiPvlKaperRepository,
  ) {}

  @post('/tx-disposisi-pvl-kaper')
  @response(200, {
    description: 'TxDisposisiPvlKaper model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxDisposisiPvlKaper)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKaper, {
            title: 'NewTxDisposisiPvlKaper',
            exclude: ['id'],
          }),
        },
      },
    })
    txDisposisiPvlKaper: Omit<TxDisposisiPvlKaper, 'id'>,
  ): Promise<{ data: Promise<TxDisposisiPvlKaper>; message: string; status: boolean }> {


    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: this.txDisposisiPvlKaperRepository.create(txDisposisiPvlKaper)

    }

    return rs;
  }

  @get('/tx-disposisi-pvl-kaper/count')
  @response(200, {
    description: 'TxDisposisiPvlKaper model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxDisposisiPvlKaper) where?: Where<TxDisposisiPvlKaper>,
  ): Promise<Count> {
    return this.txDisposisiPvlKaperRepository.count(where);
  }

  @get('/tx-disposisi-pvl-kaper')
  @response(200, {
    description: 'Array of TxDisposisiPvlKaper model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxDisposisiPvlKaper, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxDisposisiPvlKaper) filter?: Filter<TxDisposisiPvlKaper>,
  ): Promise<{ data: (TxDisposisiPvlKaper)[]; message: string; status: boolean }> {
      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txDisposisiPvlKaperRepository.find(filter)
      };
  }

  @patch('/tx-disposisi-pvl-kaper')
  @response(200, {
    description: 'TxDisposisiPvlKaper PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKaper, {partial: true}),
        },
      },
    })
    txDisposisiPvlKaper: TxDisposisiPvlKaper,
    @param.where(TxDisposisiPvlKaper) where?: Where<TxDisposisiPvlKaper>,
  ): Promise<Count> {
    return this.txDisposisiPvlKaperRepository.updateAll(txDisposisiPvlKaper, where);
  }

  @get('/tx-disposisi-pvl-kaper/{id}')
  @response(200, {
    description: 'TxDisposisiPvlKaper model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDisposisiPvlKaper, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxDisposisiPvlKaper, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDisposisiPvlKaper>
  ): Promise<{ data: TxDisposisiPvlKaper; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txDisposisiPvlKaperRepository.findById(id, filter)
      };
  }

  @patch('/tx-disposisi-pvl-kaper/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKaper PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKaper, {partial: true}),
        },
      },
    })
    txDisposisiPvlKaper: TxDisposisiPvlKaper,
  ): Promise<{ data: any; message: string; status: boolean }> {
      await this.txDisposisiPvlKaperRepository.updateById(id, txDisposisiPvlKaper);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txDisposisiPvlKaperRepository.findById(id)
      };
  }

  @put('/tx-disposisi-pvl-kaper/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKaper PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txDisposisiPvlKaper: TxDisposisiPvlKaper,
  ): Promise<{ data: TxDisposisiPvlKaper; message: string; status: boolean }> {

      // update data
      await this.txDisposisiPvlKaperRepository.replaceById(id, txDisposisiPvlKaper);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txDisposisiPvlKaperRepository.findById(id)
      };
  }

  @del('/tx-disposisi-pvl-kaper/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKaper DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txDisposisiPvlKaperRepository.deleteById(id);
  }
}
