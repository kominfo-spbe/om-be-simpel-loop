import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, requestBody,
  response
} from '@loopback/rest';
import {TxLaporan, TxSettingProsesResmon, TxTakeOver} from '../models';
import {TxSettingProsesResmonRepository} from '../repositories';

export class TxSettingProsesResmonController {
  constructor(
    @repository(TxSettingProsesResmonRepository)
    public txSettingProsesResmonRepository: TxSettingProsesResmonRepository,
  ) { }

  @post('/tx-setting-proses-resmon')
  @response(200, {
    description: 'TxSettingProsesResmon model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxSettingProsesResmon)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxSettingProsesResmon, {
            title: 'NewTxSettingProsesResmon',
            exclude: ['id'],
          }),
        },
      },
    })
    txSettingProsesResmon: Omit<TxSettingProsesResmon, 'id'>,
  ): Promise<{ data: TxSettingProsesResmon; message: string; status: boolean }> {

    let rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: await this.txSettingProsesResmonRepository.create(txSettingProsesResmon)

    }

    return rs;

  }

  @get('/tx-setting-proses-resmon/count')
  @response(200, {
    description: 'TxSettingProsesResmon model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxSettingProsesResmon) where?: Where<TxSettingProsesResmon>,
  ): Promise<Count> {
    return this.txSettingProsesResmonRepository.count(where);
  }

  @get('/tx-setting-proses-resmon')
  @response(200, {
    description: 'Array of TxSettingProsesResmon model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxSettingProsesResmon, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxSettingProsesResmon) filter?: Filter<TxSettingProsesResmon>,
  ): Promise<{ data: (TxSettingProsesResmon)[]; message: string; status: boolean }> {

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txSettingProsesResmonRepository.find(filter)
    };
  }

  @patch('/tx-setting-proses-resmon')
  @response(200, {
    description: 'TxSettingProsesResmon PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxSettingProsesResmon, {partial: true}),
        },
      },
    })
    txSettingProsesResmon: TxSettingProsesResmon,
    @param.where(TxSettingProsesResmon) where?: Where<TxSettingProsesResmon>,
  ): Promise<Count> {
    return this.txSettingProsesResmonRepository.updateAll(txSettingProsesResmon, where);
  }

  @get('/tx-setting-proses-resmon/{id}')
  @response(200, {
    description: 'TxSettingProsesResmon model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxSettingProsesResmon, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxSettingProsesResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxSettingProsesResmon>
  ): Promise<{ data: TxSettingProsesResmon; message: string; status: boolean }> {

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: await this.txSettingProsesResmonRepository.findById(id, filter)
    };
  }

  @patch('/tx-setting-proses-resmon/{id}')
  @response(204, {
    description: 'TxSettingProsesResmon PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxSettingProsesResmon, {partial: true}),
        },
      },
    })
    txSettingProsesResmon: TxSettingProsesResmon,
  ): Promise<{ data: any; message: string; status: boolean }> {

    await this.txSettingProsesResmonRepository.updateById(id, txSettingProsesResmon)

    return {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txSettingProsesResmonRepository.findById(id)
    };
  }

  @put('/tx-setting-proses-resmon/{id}')
  @response(204, {
    description: 'TxSettingProsesResmon PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txSettingProsesResmon: TxSettingProsesResmon,
  ): Promise<void> {
    await this.txSettingProsesResmonRepository.replaceById(id, txSettingProsesResmon);
  }

  @del('/tx-setting-proses-resmon/{id}')
  @response(204, {
    description: 'TxSettingProsesResmon DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txSettingProsesResmonRepository.deleteById(id);
  }
}
