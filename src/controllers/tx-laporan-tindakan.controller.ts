import {inject} from "@loopback/core";
import {CountSchema, repository} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, post, requestBody, response} from '@loopback/rest';
import {TxLaporanTindakan} from "../models";
import {TxLaporanRepository, TxLaporanTindakanRepository} from "../repositories";
import {PvlService} from "../services";

export class TxLaporanTindakanController {
    constructor(
        @repository(TxLaporanTindakanRepository)
        public txLaporanTindakanRepository: TxLaporanTindakanRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository,
        @inject('services.PvlService')
        public pvlService: PvlService
    ) {
    }

    @post('/tx-laporan-tindakan')
    @response(200, {
        description: 'TxLaporanTindakan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxLaporanTindakan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLaporanTindakan, {
                        title: 'NewTxLaporanTindakan',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txLaporanTindakan: Omit<TxLaporanTindakan, 'id'>,
    ): Promise<{ data: TxLaporanTindakan; message: string; status: boolean }> {
        const savedTindakan = await this.txLaporanTindakanRepository.create(txLaporanTindakan);
        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedTindakan
        };
    }

    @get('/tx-laporan-tindakan/duration')
    @response(200, {
        description: 'TxLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async duration(
        @param.query.string('id_laporan') id: string
    ): Promise<{ data: { durasi: number }; message: string; status: boolean }> {
        const sql = "select \n" +
            "a.id,\n" +
            "a.id_tx_laporan,\n" +
            "a.tanggal,\n" +
            "a.jenis_tindakan,\n" +
            "a.created_date,\n" +
            "a.file_name, \n" +
            "a.ket_jenis_tindakan, \n" +
            "a.keterangan \n" +
            "from simpel_4.tx_laporan_tindakan a\n" +
            "where a.id_tx_laporan = $1";

        const result = await this.txLaporanTindakanRepository.dataSource.execute(sql, [id]);

        const query = "select status_laporan ,* from simpel_4.tx_laporan where id = $1";
        const hasil = await this.txLaporanRepository.dataSource.execute(query, [id]);




        let durasi: any;
        if(hasil.length > 0){
            if(hasil[0].status_laporan == 'VFL'){
                if (result.length > 0) {
                    const data = result[0];
                    let dateTime = new Date().getTime();
                    if (data.created_date) {
                        dateTime = new Date(data.created_date).getTime()
                    }
                    const time = new Date().getTime() - dateTime;
                    durasi = await this.pvlService.TimeDay(time)
                    if (durasi != null) {
                        const dr = durasi["durasi"].toString();
                        if (dr.toLowerCase() === 'baru') {
                            durasi = 1;
                        } else {
                            if (dr !== 'null') {
                                durasi = Number.parseInt(dr);
                            } else {
                                durasi = 0;
                            }
                        }
                    }
                } else {
                    durasi = 0;
                }
            }else{
                durasi = -1;
            }
        }else{
            durasi = -2;
        }

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: {
                durasi: durasi
            }
        };
    }

    @get('/tx-laporan-tindakan')
    @response(200, {
        description: 'TxLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporanTindakan, {includeRelations: true}),
            },
        },
    })
    async findByLaporanId(
        @param.query.string('id_laporan') id: string
    ): Promise<object> {

        const sql = "select \n" +
            "a.id,\n" +
            "a.id_tx_laporan,\n" +
            "a.tanggal,\n" +
            "a.jenis_tindakan,\n" +
            "a.file_name, \n" +
            "a.ket_jenis_tindakan, \n" +
            "a.keterangan \n" +
            "from simpel_4.tx_laporan_tindakan a\n" +
            "where a.id_tx_laporan = $1";

        const result = await this.txLaporanTindakanRepository.dataSource.execute(sql, [id]);
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };
    }

    @del('/tx-laporan-tindakan/{id}')
    @response(204, {
        description: 'TxLaporan DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txLaporanTindakanRepository.deleteById(id);
    }

}
