import {
    Filter, FilterExcludingWhere, IsolationLevel, juggler, repository
} from '@loopback/repository';
import {
    del,
    get,
    getModelSchemaRef, HttpErrors, oas, param, patch, post, Request, requestBody, Response, response, RestBindings
} from '@loopback/rest';
import {DbDataSource} from '../datasources';
import {TdTtdSpesimen, Tte, TxDokumen, MPelapor} from '../models';
import {TdTtdSpesimenRepository, MPelaporRepository} from '../repositories';

import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from '../keys';
import {FileUploadHandler} from '../types';

import multer from 'multer';

import {inject} from "@loopback/core";
import path from "path";
import fs from "fs";
import process from "process";
import FormData from "form-data";
import axios from "axios";
import Transaction = juggler.Transaction;

export class TdTtdSpesimenController {

    constructor(
        @repository(TdTtdSpesimenRepository)
        public TdTtdSpesimenRepository : TdTtdSpesimenRepository,
        @repository(MPelaporRepository)
        public mPRepo : MPelaporRepository,
        @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
        @inject('datasources.db') private dataSource: DbDataSource,
        @inject(STORAGE_DIRECTORY) private storageDirectory: string
    ) {}

    @post('/ttd-spesimen')
    @response(200, {
        description: 'TdTtdSpesimen model instance',
        content: {'application/json': {schema: getModelSchemaRef(TdTtdSpesimen)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TdTtdSpesimen, {
                        title: 'newTxCaraTindakLanjutRiksa',
                        exclude: ['id'],
                    }),
                },
            },
        })
            TdTtdSpesimen: Omit<TdTtdSpesimen, 'id'>,
    ): Promise<{ data: TdTtdSpesimen; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: await this.TdTtdSpesimenRepository.create(TdTtdSpesimen)

        };

    }

    @get('/ttd-spesimen')
    @response(200, {
        description: 'Array of TdTtdSpesimen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TdTtdSpesimen, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TdTtdSpesimen) filter?: Filter<TdTtdSpesimen>,
    ): Promise<{ data: (TdTtdSpesimen)[]; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.TdTtdSpesimenRepository.find(filter)
        };

    }

    @get('/ttd-spesimen/{id}')
    @response(200, {
        description: 'TdTtdSpesimen model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TdTtdSpesimen, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TdTtdSpesimen, {exclude: 'where'}) filter?: FilterExcludingWhere<TdTtdSpesimen>
    ): Promise<{ data: TdTtdSpesimen; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.TdTtdSpesimenRepository.findById(id, filter)
        };
    }

    @patch('/ttd-spesimen/{id}')
    @response(204, {
        description: 'TdTtdSpesimen PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TdTtdSpesimen, {partial: true}),
                },
            },
        })
            txRiksaMonitoring: TdTtdSpesimen,
    ): Promise<{ data: any; message: string; status: boolean }> {

        await this.TdTtdSpesimenRepository.updateById(id, txRiksaMonitoring)
        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.TdTtdSpesimenRepository.findById(id)
        };
    }

    @get('/ttd-spesimen/download')
    @oas.response.file()
    async findFile(
        @inject(RestBindings.Http.RESPONSE) response: Response,
        @param.filter(TdTtdSpesimen) filter?: Filter<TdTtdSpesimen>
    ) {
        const data = await this.TdTtdSpesimenRepository.find(filter);
        let surat: TdTtdSpesimen;

        if(data.length > 0){
            surat = data[0];
        }

        // @ts-ignore
        if(surat.spesimen_path != null){
            // @ts-ignore
            const file = path.resolve(this.storageDirectory + '/spesimen/', surat.spesimen_path);
            // @ts-ignore
            response.download(file, surat.spesimen_path);
            return response;
        }

        return {
            status: false,
            message: "Data tidak ditemukan",
            data: null
        };

    }

    @post('/ttd-spesimen/upload', {
        responses: {
            200: {
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                        },
                    },
                },
                description: 'Files and fields',
            },
        },
    })
    async resmonUpload(
        @requestBody.file()
            request: Request,
        @inject(RestBindings.Http.RESPONSE) response: Response,
    ): Promise<object> {
        const folderPath = './public/uploads/spesimen/'
        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath, { recursive: true });
        }
        const storage = multer.diskStorage(
            {
                destination: folderPath,
                filename: function ( req, file, cb ) {
                    //req.body is empty...
                    //How could I get the new_file_name property sent from client here?
                    cb( null, 'ttd_' + req.body.user_id + file.originalname.substring(file.originalname.lastIndexOf('.')));
                }
            }
        );
        const upload = multer({storage});

         var upl = await new Promise<object>((resolve, reject) => {
            upload.any()(request, response, (err: unknown) => {
                if (err) reject(err);
                else {

                    resolve(TdTtdSpesimenController.modifyTindakLanjutFile(request));
                }
            });
        });

        const filter = {
            where: {
                // @ts-ignore
                user_id: upl.fields.user_id
            }
        }
        const exist = await this.TdTtdSpesimenRepository.find(filter);

        if(exist && exist.length > 0){

        } else {
// @ts-ignore
            const spesimen: TdTtdSpesimen = {
                // @ts-ignore
                user_id:  upl.fields.user_id,
                // @ts-ignore
                spesimen_path: 'ttd_' +  upl.fields.user_id + '.png'
            }

            await this.TdTtdSpesimenRepository.create(spesimen)
        }

        return await new Promise<object>((resolve, reject) => {
            resolve({ status: true, message: "success"});
        });
    }

    @patch('/ttd-spesimen/rollback')
    @response(204, {
        description: 'TdTtdSpesimen PATCH success',
    })
    async rollbackDemo(
    ): Promise<{ data: any; message: string; status: boolean }> {
        var tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)

        try{
            await this.TdTtdSpesimenRepository.updateById('zq974da0-642e-11ed-8796-193d0dd69c58', {
                user_id: '02c3a90b-1c8f-4ec6-8dc9-644519679cb5',
                spesimen_path: 'ediiiiit'
            }, {
                transaction: tx
            })

            await this.mPRepo.updateById('00587330-43bd-11ed-82ff-9741966c5ca8', {
                no_telp_pelapor: '11111'
            }, {
                transaction: tx
            })

            tx.commit()

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.TdTtdSpesimenRepository.findById('zq974da0-642e-11ed-8796-193d0dd69c58')
            };
        } catch (e){
            tx.rollback()

            throw new HttpErrors.UnprocessableEntity("Gagal submit data")
        }
    }

    private static async modifyTindakLanjutFile(request: Request) {
        const uploadedFiles = request.files;
        const data = request.body

        const mapper = (f: globalThis.Express.Multer.File) => ({
            fieldname: f.fieldname,
            originalname:  'ttd_' + data.user_id + f.originalname.substring(f.originalname.lastIndexOf('.')),
            encoding: f.encoding,
            mimetype: f.mimetype,
            size: f.size,
        });

        let files: object[] = [];
        if (Array.isArray(uploadedFiles)) {
            files = uploadedFiles.map(mapper);
        } else {
            for (const filename in uploadedFiles) {
                files.push(...uploadedFiles[filename].map(mapper));
            }
        }

        return {files, fields: request.body};
    }
}
