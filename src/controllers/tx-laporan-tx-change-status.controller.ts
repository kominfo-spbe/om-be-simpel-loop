import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  TxLaporan,
  TxChangeStatus,
} from '../models';
import {TxLaporanRepository} from '../repositories';

export class TxLaporanTxChangeStatusController {
  constructor(
    @repository(TxLaporanRepository) protected txLaporanRepository: TxLaporanRepository,
  ) { }

  @get('/tx-laporans/{id}/tx-change-statuses', {
    responses: {
      '200': {
        description: 'Array of TxLaporan has many TxChangeStatus',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(TxChangeStatus)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<TxChangeStatus>,
  ): Promise<TxChangeStatus[]> {
    return this.txLaporanRepository.txChangeStatuses(id).find(filter);
  }

  @post('/tx-laporans/{id}/tx-change-statuses', {
    responses: {
      '200': {
        description: 'TxLaporan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxChangeStatus)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof TxLaporan.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxChangeStatus, {
            title: 'NewTxChangeStatusInTxLaporan',
            exclude: ['id'],
            optional: ['id_tx_laporan']
          }),
        },
      },
    }) txChangeStatus: Omit<TxChangeStatus, 'id'>,
  ): Promise<TxChangeStatus> {
    return this.txLaporanRepository.txChangeStatuses(id).create(txChangeStatus);
  }

  @patch('/tx-laporans/{id}/tx-change-statuses', {
    responses: {
      '200': {
        description: 'TxLaporan.TxChangeStatus PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxChangeStatus, {partial: true}),
        },
      },
    })
    txChangeStatus: Partial<TxChangeStatus>,
    @param.query.object('where', getWhereSchemaFor(TxChangeStatus)) where?: Where<TxChangeStatus>,
  ): Promise<Count> {
    return this.txLaporanRepository.txChangeStatuses(id).patch(txChangeStatus, where);
  }

  @del('/tx-laporans/{id}/tx-change-statuses', {
    responses: {
      '200': {
        description: 'TxLaporan.TxChangeStatus DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(TxChangeStatus)) where?: Where<TxChangeStatus>,
  ): Promise<Count> {
    return this.txLaporanRepository.txChangeStatuses(id).delete(where);
  }
}
