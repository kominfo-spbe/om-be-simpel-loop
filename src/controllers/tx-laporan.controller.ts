import {inject, service} from "@loopback/core";
import {
    Count,
    CountSchema,
    Filter,
    FilterExcludingWhere,
    IsolationLevel,
    Where,
    repository
} from '@loopback/repository';
import {
    HttpErrors,
    Request,
    RestBindings,
    del,
    get,
    getModelSchemaRef,
    param,
    patch,
    post,
    requestBody,
    response
} from '@loopback/rest';
import * as _ from "lodash";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import {v4 as uuidv4} from 'uuid';
import {DbDataSource} from '../datasources';
import {
    FormilCabutLaporan,
    FormilSubmit,
    MKantor,
    Petugas,
    RegistrasiNew,
    RegistrasiOnline,
    SendEmailInit,
    TahapDisposisi,
    TahapKlasifikasi,
    TdUser,
    TrackingLaporan,
    TxChangeStatus,
    TxHistoryStatusLaporan,
    TxLaporan,
    TxOtp,
    TxVerifFormil
} from '../models';
import {FormilDraft} from '../models/formil-draft.model';
import {TahapPleno} from '../models/tahap-pleno.model';
import {TahapRegistrasi} from '../models/tahap-registrasi.model';
import {
    MKantorRepository,
    MPelaporRepository,
    TdUserRepository,
    TxCabutLaporanRepository,
    TxCaraTindakLanjutRiksaRepository,
    TxChangeStatusRepository,
    TxDisposisiPvlKaperRepository,
    TxDisposisiPvlKaptenRepository,
    TxDokumenRepository,
    TxHistoryStatusLaporanRepository,
    TxKlasifikasiLmRepository,
    TxLaporanHistoryRepository,
    TxLaporanRepository,
    TxOtpRepository,
    TxPlenoRepository,
    TxVerifFormilRepository,
    TxVerifMateriilRepository
} from '../repositories';
import {EmailService, GenerateFileService, OtpService, PvlService} from "../services";

const {glob} = require('glob');
const $path = require('path');
const fs = require('fs')

//const HelperClass = require("../src/helpers/util.ts");


class item {
}

export class TxLaporanController {
    constructor(
        @repository(TxDisposisiPvlKaperRepository)
        public txDisposisiPvlKaperRepository: TxDisposisiPvlKaperRepository,
        @repository(TxPlenoRepository)
        public txPlenoRepository: TxPlenoRepository,
        @repository(TxKlasifikasiLmRepository)
        public txKlasifikasiLmRepository: TxKlasifikasiLmRepository,
        @repository(TxCabutLaporanRepository)
        public txCabutLaporanRepository: TxCabutLaporanRepository,
        @repository(TxVerifMateriilRepository)
        public txVerifMateriilRepository: TxVerifMateriilRepository,
        @repository(TxVerifFormilRepository)
        public txVerifFormilRepository: TxVerifFormilRepository,
        @repository(TxChangeStatusRepository)
        public txChangeStatusRepository: TxChangeStatusRepository,
        @repository(TxDisposisiPvlKaptenRepository)
        public txDisposisiPvlKaptenRepository: TxDisposisiPvlKaptenRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository,
        @repository(MPelaporRepository)
        public mPelaporRepository: MPelaporRepository,
        @repository(MKantorRepository)
        public mKantorRepository: MKantorRepository,
        @repository(TxLaporanHistoryRepository)
        public txLaporanHistoryRepository: TxLaporanHistoryRepository,
        @repository(TdUserRepository)
        public tdUserRepository: TdUserRepository,
        @repository(TxHistoryStatusLaporanRepository)
        public txdHistoryStatusLaporanRepository: TxHistoryStatusLaporanRepository,
        @repository(TxOtpRepository)
        public txOtpRepository: TxOtpRepository,
        @repository(TxDokumenRepository)
        public txDokumenRepository: TxDokumenRepository,
        @repository(TxCaraTindakLanjutRiksaRepository)
        public txCaraTindakLanjutRiksaRepository: TxCaraTindakLanjutRiksaRepository,
        @service(GenerateFileService)
        public dokumenService: GenerateFileService,
        @inject('services.EmailService')
        public emailService: EmailService,
        @inject('services.PvlService')
        public pvlService: PvlService,
        @inject('services.OtpService')
        public otpService: OtpService,
        @inject('datasources.db') private dataSource: DbDataSource,
        @inject(RestBindings.Http.REQUEST) private request: Request
    ) {
    }

    @post('/tx-laporan/request-otp')
    @response(200, {
        description: 'Request OTP success',
    })
    async requestOtp(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLaporan, {partial: true}),
                },
            },
        })
        txLaporan: TxLaporan,
    ): Promise<{data: any; message: string; status: boolean}> {
        const OTP = this.otpService.generateOtp();

        const txOtp: TxOtp = new TxOtp({
            tracking_id: txLaporan.tracking_id,
            otp: OTP
        })

        const result = await this.txOtpRepository.save(txOtp);

        const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendOtpMail(
            result,
            txLaporan.email_pelapor
        );

        if (nodeMailer.accepted.length)
            console.log('An email has been sent to the provided email');

        return {
            status: true,
            message: "Request OTP berhasil",
            data: result
        };
    }

    @post('/tx-laporan/verify-otp')
    @response(200, {
        description: 'Verify OTP success',
    })
    async verifyOtp(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxOtp, {partial: true}),
                },
            },
        })
        txOtp: TxOtp,
    ): Promise<{data: any; message: string; status: boolean}> {

        const result = await this.txOtpRepository.find({
            where: {tracking_id: txOtp.tracking_id, otp: txOtp.otp},
            limit: 1
        });

        if (result.length > 0) {
            await this.txOtpRepository.delete(result[0])
            return {
                status: true,
                message: "verify OTP berhasil",
                data: null
            };
        } else {
            return {
                status: false,
                message: "verify OTP gagal",
                data: result
            };
        }
    }

    @post('/tx-laporan')
    @response(200, {
        description: 'TxLaporan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxLaporan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLaporan, {
                        title: 'NewTxLaporan',
                        exclude: ['id'],
                    }),
                },
            },
        })
        txLaporan: Omit<TxLaporan, 'id'>,
    ): Promise<{data: TxLaporan; message: string; status: boolean}> {


        //save to m_pelapor
        const mPelapor = await this.mPelaporRepository.find({
            where: {nomor_identitas_pelapor: txLaporan.nomor_identitas_pelapor},
            limit: 1
        });

        let mIdPelapor = null
        if (mPelapor.length === 0) {
            mIdPelapor = await this.createPelapor(txLaporan)
        } else {
            mIdPelapor = mPelapor[0]['id']
        }


        //save to
        if (txLaporan.status_laporan === "RL" || txLaporan.tipe_laporan === "IN") {
            // dicomment sangkuriang
            txLaporan.no_agenda = await this.generateNomorAgenda()
            txLaporan.token = this.generateToken(10, txLaporan.tipe_laporan)
            let kodeKantor = ""
            let didaftarkanOleh = ""
            if (txLaporan.tipe_laporan == "IN" && txLaporan.proses_laporan) {
                kodeKantor = txLaporan.proses_laporan;
            }


            if (!kodeKantor) {
                if (txLaporan.created_by !== undefined && txLaporan.created_by !== null) {
                    await this.tdUserRepository.findById(txLaporan.created_by).then((value: TdUser) => {
                        if (value !== null && value !== undefined) {
                            if (value.kodeKantor !== null && value.kodeKantor !== undefined) {
                                kodeKantor = value.kodeKantor;
                            }
                        }
                    });
                }
            }

            if (kodeKantor !== null && kodeKantor !== undefined && kodeKantor !== "") {
                txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, txLaporan.provinsi_terlapor, txLaporan.tipe_laporan, txLaporan.cara_penyampaian, txLaporan.klasifikasi_instansi_terlapor)


                // dicomment sangkuriang
                // if (txLaporan.no_agenda) {
                // txLaporan.tgl_agenda = new Date().toISOString()
                // }

                // if (txLaporan.no_arsip && txLaporan.no_arsip !== '-') {
                //     txLaporan.kode_kantor = txLaporan.no_arsip.substr(txLaporan.no_arsip.length - 3);
                // }


                txLaporan.kode_kantor = kodeKantor;

                await this.mKantorRepository.findById(kodeKantor).then((value: MKantor) => {
                    if (value !== null && value.nama_kantor !== null && value.nama_kantor !== undefined) {
                        didaftarkanOleh = value.nama_kantor;
                    }
                });

                txLaporan.didaftarkan_oleh = didaftarkanOleh;


                if (txLaporan.tipe_laporan === "Tembusan" || txLaporan.tipe_laporan === "KNL") {
                    txLaporan.officer_by = txLaporan.created_by;
                    txLaporan.status_petugas = "dikerjakan";
                }
            }
        }

        //save to if status DRAFT
        //  || txLaporan.no_arsip == '-'
        if (txLaporan.status_laporan === "DRF") {
            const kantor = await this.mKantorRepository.findOne({
                where: {'id_provinsi': txLaporan.provinsi_terlapor}
            });
            let kodeKantor = kantor?.kode_kantor;
            let namaKantor = kantor?.nama_kantor;
            if (txLaporan.provinsi_terlapor == '31') {
                kodeKantor = 'JKT';
                await this.mKantorRepository.findById(kodeKantor).then((value: MKantor) => {
                    if (value !== null && value.nama_kantor !== null && value.nama_kantor !== undefined) {
                        namaKantor = value.nama_kantor;
                    }
                });
            }
            //txLaporan.token = this.generateToken(10, txLaporan.tipe_laporan)
            txLaporan.didaftarkan_oleh = namaKantor
            txLaporan.kode_kantor = kodeKantor
        } else {
            if (txLaporan.tgl_agenda == null) {
                txLaporan.tgl_agenda = new Date().toISOString()
            }
        }

        txLaporan.id_m_pelapor = mIdPelapor

        const savedLaporan = await this.txLaporanRepository.create(txLaporan);

        // new update id_file_upload to filename

        const copylaporan = txLaporan;
        if (txLaporan.status_laporan === "DRF" && txLaporan.id_file_upload != null) {
            if (txLaporan.file_identitas_pelapor != null) {
                copylaporan.file_identitas_pelapor = txLaporan.file_identitas_pelapor?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_ktp_pemberi_kuasa != null) {
                copylaporan.filename_ktp_pemberi_kuasa = txLaporan.filename_ktp_pemberi_kuasa?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_ktp_pemberi_kuasa2 != null) {
                copylaporan.filename_ktp_pemberi_kuasa2 = txLaporan.filename_ktp_pemberi_kuasa2?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_ktp_pemberi_kuasa3 != null) {
                copylaporan.filename_ktp_pemberi_kuasa3 = txLaporan.filename_ktp_pemberi_kuasa3?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_uraian != null) {
                copylaporan.filename_uraian = txLaporan.filename_uraian?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_bukti != null) {
                copylaporan.filename_bukti = txLaporan.filename_bukti?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_art != null) {
                copylaporan.filename_art = txLaporan.filename_art?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_akta_pendirian != null) {
                copylaporan.filename_akta_pendirian = txLaporan.filename_akta_pendirian?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_surat_kuasa != null) {
                copylaporan.filename_surat_kuasa = txLaporan.filename_surat_kuasa?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_kartu_keluarga != null) {
                copylaporan.filename_kartu_keluarga = txLaporan.filename_kartu_keluarga?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_upload_bukti_upaya != null) {
                copylaporan.filename_upload_bukti_upaya = txLaporan.filename_upload_bukti_upaya?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }
            if (txLaporan.filename_upload_bukti_upaya_email != null) {
                copylaporan.filename_upload_bukti_upaya_email = txLaporan.filename_upload_bukti_upaya_email?.replace(txLaporan.id_file_upload, savedLaporan.id);
            }

            const dataUpdated = await this.txLaporanRepository.updateById(savedLaporan.id, copylaporan);
        }


        const dirPath = "public/uploads/";
        // const finalPath = dirPath.split("\\").join("/")
        const files = glob.sync(dirPath + '?(*|*-)' + txLaporan.id_file_upload + '?(-*)?*');

        let i = 0;
        for (i = 0; i < files.length; i++) {
            const f = $path.basename(files[i])
            const d = $path.dirname(files[i])
            fs.renameSync(files[i], files[i].replace(txLaporan.id_file_upload, savedLaporan.id), function (err: unknown) {
                if (err) throw err;
                console.log('renamed complete');
            });
        }


        // const idLaporan = savedLaporan.id;
        // const historyLaporan = Object.assign({}, savedLaporan);
        // // @ts-ignore
        // delete historyLaporan['id'];
        // // @ts-ignore
        // historyLaporan["id_laporan"] = idLaporan;
        // const savedHistory = await this.txLaporanHistoryRepository.create(historyLaporan);

        if (txLaporan.email_pelapor) {
            const email = await this.validateEmail(txLaporan.email_pelapor);

            // At this point we are dealing with valid email.
            // Lets check whether there is an associated account
            const filter = {
                where: {'email_pelapor': email},
                order: ['created_date DESC']
            }
            const foundLaporan = await this.txLaporanRepository.findOne(filter);

            // No account found
            if (foundLaporan) {

                // Send an email to the user's email address
                /*const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
                    foundLaporan,
                );

                // Nodemailer has accepted the request. All good
                if (nodeMailer.accepted.length) {
                    console.log('An email has been sent to the provided email');
                } else {
                    console.log('Error sending report email')
                }*/

                if (foundLaporan.tipe_laporan == 'LM' || foundLaporan.tipe_laporan == 'RCO') {
                    try {

                        const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas', 'warga_negara_pelapor']
                        const resultData = await this.pvlService.getMLookUpFindById(foundLaporan.id, filter, listUraian, this.txLaporanRepository)

                        //get daeran pelapor
                        const provPelapor = resultData['provinsi_pelapor']
                        const kabPelapor = resultData['kab_kota_pelapor']
                        const kecPelapor = resultData['kec_pelapor']

                        const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
                        if (daerahPelapor) {
                            resultData['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
                            resultData['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
                            resultData['kec_pelapor_name'] = daerahPelapor['nm_kec'];
                        }


                        //get daeran terlapor
                        const provTerlapor = resultData['provinsi_terlapor']
                        const kabTerlapor = resultData['kab_kota_terlapor']
                        const kecTerlapor = resultData['kec_terlapor']
                        const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
                        if (daerahTerlapor) {
                            resultData['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
                            resultData['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
                            resultData['kec_terlapor_name'] = daerahTerlapor['nm_kec'];
                        }

                        const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
                            resultData,
                        );

                        if (nodeMailer.accepted.length)
                            console.log('An email has been sent to the provided email');


                    } catch (e: unknown) { // <-- note `e` has explicit `unknown` type

                        if (typeof e === "string") {
                            // works, `e` narrowed to string

                            console.log('Error sending report email')
                            console.log(e.toUpperCase())
                        } else if (e instanceof Error) {

                            console.log('Error sending report email')
                            console.log(e.message)
                        }


                    }
                }
            }
        }

        //generate surat
        switch (txLaporan.status_laporan) {
            case 'RL':
                if (txLaporan.tipe_laporan == 'LM' || txLaporan.tipe_laporan == 'RCO') {
                    // @ts-ignore
                    await this.getFileFromReport("cover_map_kuning", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                }
                break;
            case 'VFL':
                // @ts-ignore
                await this.getFileFromReport("form_formil", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'VML':
                // @ts-ignore
                await this.getFileFromReport("form_ringkasan_materiil", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'KLL':
                // @ts-ignore
                await this.getFileFromReport("klasifikasi_lm", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'PMP':
                // @ts-ignore
                await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'lhpd':
                // @ts-ignore
                await this.getFileFromReport("lhpd", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'PLRKS':
                // @ts-ignore
                await this.getFileFromReport("ba_penutupan_laporan", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            case 'TMSF':
                // @ts-ignore
                await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", savedLaporan.id, txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                break;
            default:
                break;
        }

        const rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedLaporan

        }

        return rs;
    }

    @get('/tx-laporan/count')
    @response(200, {
        description: 'TxLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxLaporan) where?: Where<TxLaporan>,
    ): Promise<Count> {
        return this.txLaporanRepository.count(where);
    }

    @get('/tx-laporan/count-data')
    @response(200, {
        description: 'Array of TxLaporan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
            },
        },
    })
    async countData(
        @param.filter(TxLaporan) filter?: Filter<TxLaporan>,
    ): Promise<any> {
        const listUraian = ['kategori_pelapor', 'jenis_identitas'
            , 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor'
            , 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas',
            'pendidikan_pelapor']
        const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txLaporanRepository, true)

        return resultData;
    }

    @get('/tx-laporan/count-relation')
    @response(200, {
        description: 'TxLaporan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async countRelation(
        @param.where(TxLaporan) where?: Where<TxLaporan>,
        @param.filter(TxLaporan) filter?: Filter<TxLaporan>,
    ): Promise<Count> {
        const listUraian = ['kategori_pelapor', 'jenis_identitas'
            , 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor'
            , 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas',
            'pendidikan_pelapor']

        if (filter?.include && filter.include.indexOf('txDisposisiPvlKapten') > -1) {
            filter.include = filter.include.map((it: any) => {
                if (it === 'txDisposisiPvlKapten') {
                    return {
                        relation: 'txDisposisiPvlKapten',
                        required: true,
                    }
                }

                return it
            })
        }

        const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txLaporanRepository, true)

        // let finalData = await this.pvlService.customFilter(resultData, filter)
        // @ts-ignore
        // if(filter.include != undefined && filter.include.length > 0){
        //     // @ts-ignore
        //     for(const inc of filter.include){
        //         // @ts-ignore
        //         if(inc.relation && inc.relation == "txDisposisiPvlKapten"){
        //             // @ts-ignore
        //             resultData = resultData.filter(x => {
        //                 return x.txDisposisiPvlKapten != undefined && x.txDisposisiPvlKapten != null
        //             })
        //         }

        //     }
        // }

        const resp: Count = resultData

        return new Promise((resolve, reject) => {
            resolve(resp);
        });
    }

    @get('/tx-laporan')
    @response(200, {
        description: 'Array of TxLaporan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxLaporan, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxLaporan) filter?: Filter<TxLaporan>,
    ): Promise<object> {

        const listUraian = ['kategori_pelapor', 'jenis_identitas'
            , 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor'
            , 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas',
            'pendidikan_pelapor']
        const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txLaporanRepository)


        // let finalData = await this.pvlService.customFilter(resultData, filter)
        // @ts-ignore
        // if(filter.include != undefined && filter.include.length > 0){
        //     // @ts-ignore
        //     for(const inc of filter.include){
        //         // @ts-ignore
        //         if(inc.relation && inc.relation == "txDisposisiPvlKapten" && inc.required){
        //             // @ts-ignore
        //             resultData = resultData.filter(x => {
        //                 return x.txDisposisiPvlKapten != undefined && x.txDisposisiPvlKapten != null
        //             })
        //         }

        //     }
        // }

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: resultData
        };

    }

    @get('/tx-laporan/{id}')
    @response(200, {
        description: 'TxLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxLaporan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxLaporan>
    ): Promise<object> {
        //return this.txLaporanRepository.findById(id, filter);
        const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas',
            'pendidikan_pelapor']
        const resultData = await this.pvlService.getMLookUpFindById(id, filter, listUraian, this.txLaporanRepository)

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: resultData
        };
    }

    @patch('/tx-laporan/{id}')
    @response(204, {
        description: 'TxLaporan PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLaporan, {partial: true}),
                },
            },
        })
        txLaporan: TxLaporan,
    ): Promise<{data: any; message: string; status: boolean}> {

        const existingData = await this.txLaporanRepository.findById(id);

        const newData = existingData;
        //create data if tipe laporan = KNL
        if (existingData.tipe_laporan === "KNL" && txLaporan.tipe_laporan === "LM" && existingData.status === true) {
            //existingData1.id = '';
            // @ts-ignore
            delete newData.id;
            newData.status = true;
            newData.no_arsip = '-';
            newData.is_duplicated = true;
            const savedLaporan = await this.txLaporanRepository.create(newData);
            //update existingData status = false
            existingData.status = false;
            existingData.is_duplicated = true;
            const dataUpdated = await this.txLaporanRepository.updateById(id, existingData);
            existingData.no_arsip = "";
            id = savedLaporan.id;

        } else if (existingData.tipe_laporan === "KNL" && txLaporan.tipe_laporan === "LM" && existingData.status === false) {
            const sql = `select * from simpel_4.tx_laporan\n` +
                `where no_agenda = '${existingData.no_agenda}' and status = true`

            const result = await this.txLaporanRepository.dataSource.execute(sql);
            id = result[0].id;
            txLaporan.no_arsip = result[0].no_arsip;

        }

        if (existingData.status_laporan == "DRF" && txLaporan.status_laporan == "RL" && existingData.tipe_laporan == null && (txLaporan.tipe_laporan == "LM" || txLaporan.tipe_laporan == "RCO")) {
            if (existingData.email_pelapor) {
                const email = await this.validateEmail(existingData.email_pelapor);

                // At this point we are dealing with valid email.
                // Lets check whether there is an associated account
                const filter = {
                    where: {'email_pelapor': email},
                    order: ['created_date DESC']
                }

                if (txLaporan.tipe_laporan == 'LM' || txLaporan.tipe_laporan == 'RCO') {
                    try {

                        const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas', 'warga_negara_pelapor']
                        const resultData = await this.pvlService.getMLookUpFindById(existingData.id, filter, listUraian, this.txLaporanRepository)

                        //get daeran pelapor
                        const provPelapor = resultData['provinsi_pelapor']
                        const kabPelapor = resultData['kab_kota_pelapor']
                        const kecPelapor = resultData['kec_pelapor']

                        const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
                        if (daerahPelapor) {
                            resultData['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
                            resultData['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
                            resultData['kec_pelapor_name'] = daerahPelapor['nm_kec'];
                        }


                        //get daeran terlapor
                        const provTerlapor = resultData['provinsi_terlapor']
                        const kabTerlapor = resultData['kab_kota_terlapor']
                        const kecTerlapor = resultData['kec_terlapor']
                        const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
                        if (daerahTerlapor) {
                            resultData['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
                            resultData['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
                            resultData['kec_terlapor_name'] = daerahTerlapor['nm_kec'];
                        }

                        const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
                            resultData,
                        );

                        if (nodeMailer.accepted.length)
                            console.log('An email has been sent to the provided email');


                    } catch (e: unknown) { // <-- note `e` has explicit `unknown` type

                        if (typeof e === "string") {
                            // works, `e` narrowed to string

                            console.log('Error sending report email')
                            console.log(e.toUpperCase())
                        } else if (e instanceof Error) {

                            console.log('Error sending report email')
                            console.log(e.message)
                        }

                    }
                }
            }

        }

        if (txLaporan.tgl_agenda == null && existingData.tgl_agenda == null) {
            txLaporan.tgl_agenda = new Date().toISOString()
        }

        if (txLaporan.provinsi_terlapor == undefined) {
            txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
        }
        // update data tx_laporan
        if (txLaporan.status_laporan !== "DRF") {

            let kodeKantor = ""
            let createdBy = ""
            if (txLaporan.created_by !== undefined && txLaporan.created_by !== null) {
                createdBy = txLaporan.created_by;
            } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                createdBy = existingData.created_by;
            }

            if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                    if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                        kodeKantor = value.kodeKantor;
                    }
                });
            }
            // create no-agenda
            if (existingData.no_agenda == null) {
                txLaporan.no_agenda = await this.generateNomorAgenda()
            }
            txLaporan.created_date = existingData.created_date;

            if (existingData.kode_kantor) {
                kodeKantor = existingData.kode_kantor
            }

            let tpLaporan = ""
            if (txLaporan.tipe_laporan !== null && txLaporan.tipe_laporan !== undefined && txLaporan.tipe_laporan !== "") {
                tpLaporan = txLaporan.tipe_laporan;
            } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                tpLaporan = existingData.tipe_laporan;
            }

            if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                    txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                }
            }

            if (existingData.token == null || existingData.token == "-") {
                txLaporan.token = this.generateToken(10, txLaporan.tipe_laporan)
            }

            // if (txLaporan.no_arsip && txLaporan.no_arsip !== '-') {
            //     txLaporan.kode_kantor = txLaporan.no_arsip.substr(txLaporan.no_arsip.length - 3);
            // }

            if (txLaporan.tipe_laporan == "KNL" || txLaporan.tipe_laporan == "Tembusan") {
                txLaporan.status_petugas = 'dikerjakan';
                if (txLaporan.officer_by == null && txLaporan.updated_by != null) {
                    txLaporan.officer_by = txLaporan.updated_by;
                }
            }
        }

        //save to if status DRAFT
        if ((txLaporan.status_laporan === "DRF" || txLaporan.no_arsip == '-')) {
            const kantor = await this.mKantorRepository.findOne({
                where: {'id_provinsi': txLaporan.provinsi_terlapor}
            });

            txLaporan.kode_kantor = kantor?.kode_kantor
        }

        if (existingData.status_laporan != txLaporan.status_laporan && txLaporan.status_laporan != null) {
            const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                id_tx_laporan: existingData.id,
                no_agenda: existingData.no_agenda,
                prev_status: existingData.status_laporan,
                next_status: txLaporan.status_laporan,
                created_by: txLaporan.updated_by != null ? txLaporan.updated_by : existingData.officer_by,
                prev_kantor: existingData.kode_kantor,
                next_kantor: txLaporan.kode_kantor != null ? txLaporan.kode_kantor : existingData.kode_kantor,
                prev_assignee: existingData.officer_by,
                next_assignee: txLaporan.officer_by != null ? txLaporan.officer_by : existingData.officer_by
            });

            await this.txdHistoryStatusLaporanRepository.create(hsl);

            switch (existingData.status_laporan) {
                case 'VFL':
                    // @ts-ignore
                    await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                    break;
                case 'VML':
                    // @ts-ignore
                    await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                    break;
                case 'KLL':
                    // @ts-ignore
                    await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                    break;
                case 'PMP':
                    // @ts-ignore
                    await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                    break;
                case 'lhpd':
                    // @ts-ignore
                    await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                    break;
                case 'PLRKS':
                    // @ts-ignore
                    await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                    break;
                // case 'TMSF':
                //     // @ts-ignore
                //     await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                //     break;
                // case 'TMSM':
                //     // @ts-ignore
                //     await this.getFileFromReport("bapl_tidak_memenuhi_syarat_materiil", existingData.id, 'TMSM', existingData.no_agenda, existingData.kode_kantor)
                //     break;
                default:
                    break;
            }
        }

        if (txLaporan.status_laporan == 'TMSM') {
            if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                await this.getFileFromReport("bapl_tidak_memenuhi_syarat_materiil", existingData.id, 'TMSM', existingData.no_agenda, existingData.kode_kantor)
            }
        } else if (txLaporan.status_laporan == 'TMSF') {
            if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
            }
        } else if (txLaporan.status_laporan == 'LT' && (existingData.status_laporan == 'LT' || existingData.status_laporan == 'VFL' || existingData.status_laporan == 'VML' || existingData.status_laporan == 'KML' || existingData.status_laporan == 'PL')) {
            if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
            }
        }

        // update dokumen
        const isDataExistKantorPusat = existingData.kode_kantor === "JKT"
        const isDataNewKantorPusat = (txLaporan.kode_kantor && txLaporan.kode_kantor !== "" ? txLaporan.kode_kantor : existingData.kode_kantor) === "JKT"
        if (isDataExistKantorPusat !== isDataNewKantorPusat) {
            // update tx dokumen
            let qTxDokumen = '';
            let qTxCaraTindakLanjut = '';
            if (txLaporan.kode_kantor === 'JKT') {
                // pusat
                qTxDokumen = `case tipe_dokumen
                    \twhen 'surat_penutupan_laporan_perwakilan_perwakilan'
                    \tthen 'surat_penutupan_laporan_perwakilan'
                    \twhen 'laporan_akhir_hasil_pemeriksaan_ori_perwakilan_perwakilan'
                    \tthen 'laporan_akhir_hasil_pemeriksaan_ori_perwakilan'
                    \telse split_part(tipe_dokumen, '_perwakilan', 1)
                    \tend as tipe_dokumen`;
                // qTxCaraTindakLanjut = `split_part(jenis_surat, '_perwakilan', 1) as tipe_dokumen`;
                qTxCaraTindakLanjut = `\tcase jenis_surat
                    \twhen 'surat_penutupan_laporan_perwakilan_perwakilan'
                    \tthen 'surat_penutupan_laporan_perwakilan'
                    \twhen 'laporan_akhir_hasil_pemeriksaan_ori_perwakilan_perwakilan'
                    \tthen 'laporan_akhir_hasil_pemeriksaan_ori_perwakilan'
                    \telse split_part(jenis_surat, '_perwakilan', 1)
                    \tend as tipe_dokumen`;
            } else {
                // perwakilan
                qTxDokumen = `concat(tipe_dokumen, '_perwakilan') as tipe_dokumen`
                qTxCaraTindakLanjut = `concat(jenis_surat, '_perwakilan') as tipe_dokumen`
            }

            if (qTxDokumen) {
                const sql = `update simpel_4.tx_dokumen d
                        set tipe_dokumen = r.tipe_dokumen
                        FROM (SELECT id, ${qTxDokumen}
                              FROM simpel_4.tx_dokumen
                              where id_tx_laporan = '${existingData.id}') AS r
                        where d.id = r.id`;
                const result = this.txDokumenRepository.dataSource.execute(sql)
            }

            // update tx cara tindak lanjut
            if (qTxCaraTindakLanjut) {
                const sql = `update simpel_4.tx_cara_tindak_lanjut_riksa d
                        set jenis_surat = r.tipe_dokumen
                        FROM (
                            SELECT id, ${qTxCaraTindakLanjut}
                            FROM simpel_4.tx_cara_tindak_lanjut_riksa
                            where id_tx_laporan = '${existingData.id}'
                        ) AS r
                        where d.id = r.id`;
                const result = this.txCaraTindakLanjutRiksaRepository.dataSource.execute(sql)
            }
        }

        await this.txLaporanRepository.updateById(id, txLaporan);

        if (txLaporan.status_laporan == 'RL') {
            if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                await this.getFileFromReport("cover_map_kuning", existingData.id, 'DRF', existingData.no_agenda, existingData.kode_kantor)
            }
        }

        // const idLaporan = id;
        // const historyLaporan = Object.assign({}, txLaporan);
        // // @ts-ignore
        // delete historyLaporan['id'];
        // // @ts-ignore
        // historyLaporan["id_laporan"] = idLaporan;
        // const savedHistory = await this.txLaporanHistoryRepository.create(historyLaporan);

        const dataResponse = await this.txLaporanRepository.findById(id)

        return {
            status: true,
            message: "Data berhasil diubah",
            data: dataResponse
        };

    }

    @del('/tx-laporan/{id}')
    @response(204, {
        description: 'TxLaporan DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txLaporanRepository.deleteById(id);
    }

    @get('/tx-laporan/petugas', {
        responses: {
            '200': {
                description: 'menampilkan laporan by petugas',
            },
        },
    })
    async laporanByPetugas(
        @param.filter(Petugas) filter?: Filter<Petugas>,
    ): Promise<any> {

        // @ts-ignore
        let kodeKantor = null
        // @ts-ignore
        let q = null
        // @ts-ignore
        let arrSubstansi = []
        let arrSubstansiFinal = ""

        if (filter) {
            // @ts-ignore
            kodeKantor = filter.where.kode_kantor

            // @ts-ignore
            const paramQ = filter.where.q
            if (paramQ != null) {
                q = '%' + paramQ.toLowerCase() + '%'
            }

            // @ts-ignore
            arrSubstansi = filter.where.substansi
            if (arrSubstansi != null) {
                //arrSubstansiSample = "'substansi_3', 'substansi_1'"
                arrSubstansiFinal = "'" + arrSubstansi.join("', '") + "'"
            }
        }

        // let sql = "select distinct u.id_user,\n" +
        //     "                u.nama,\n" +
        //     "                mk.nama_kantor,\n" +
        //     //"                substansi_role.lookup_code,\n" +
        //     "                CASE\n" +
        //     "                    WHEN (l.jumlah_laporan > 0)\n" +
        //     "                        THEN l.jumlah_laporan\n" +
        //     "                    ELSE 0 END jumlah_laporan\n" +
        //     "\n" +
        //     "from apps_manager.td_user u\n" +
        //     "\n" +
        //     "join apps_manager.td_user_detail tud on u.id_user = tud.id_user\n" +
        //     "join apps_manager.td_role tr on tud.id_role = tr.id_role\n" +
        //     "join referensi.m_kantor mk on mk.kode_kantor = u.kode_kantor\n" +
        //     "left join (\n" +
        //     "    select tsr.id_role, c.lookup_code\n" +
        //     "    from apps_manager.td_substansi_role tsr\n" +
        //     "    join referensi.m_lookup c on c.lookup_code = tsr.lookup_code\n" +
        //     "    where c.lookup_group_name = 'substansi'\n" +
        //     ") substansi_role\n" +
        //     "on substansi_role.id_role = tr.id_role\n" +
        //     "\n" +
        //     "left join (\n" +
        //     "    select l.officer_by,\n" +
        //     "           count(*) jumlah_laporan\n" +
        //     "    from simpel_4.tx_laporan l\n" +
        //     "    group by 1\n" +
        //     ") l\n" +
        //     "on u.id_user = l.officer_by\n" +
        //     "\n" +
        //     "where CASE\n" +
        //     "    WHEN $1::text is not null\n" +
        //     "    THEN u.kode_kantor::text = $1\n" +
        //     "    ELSE u.kode_kantor is not null END\n" +
        //     "and CASE\n" +
        //     "    WHEN $2::text is not null\n" +
        //     "    THEN UPPER(u.nama) like ($2)\n" +
        //     "    ELSE UPPER(u.nama) is not null END\n" +
        //     //"and tr.id_role IN ('87945921-23cf-6222-208a-11d9aa20d7ef', 'a1103d18-ea74-49a9-9775-fd66c75a3e18')\n" +
        //     "and CASE\n" +
        //     "    WHEN $3 > 0\n" +
        //     "    THEN substansi_role.lookup_code IN ($4)\n" +
        //     "    ELSE true END\n" +
        //     "\n" +
        //     "order by nama_kantor, jumlah_laporan";

        const sql = "select \n" +
            "distinct \n" +
            "u.id_user, u.nama,\n" +
            "mk.nama_kantor,\n" +
            "case WHEN (l.jumlah_laporan > 0) THEN l.jumlah_laporan ELSE 0 END jumlah_laporan\n" +
            "from apps_manager.td_user u\n" +
            "join apps_manager.td_user_detail tud on u.id_user = tud.id_user\n" +
            "join apps_manager.td_role tr on tud.id_role = tr.id_role\n" +
            "join referensi.m_kantor mk on mk.kode_kantor = u.kode_kantor\n" +
            "left join (\n" +
            "    select tsr.id_role, c.lookup_code\n" +
            "    from apps_manager.td_substansi_role tsr\n" +
            "    join referensi.m_lookup c on c.lookup_code = tsr.lookup_code\n" +
            "    where c.lookup_group_name = 'substansi'\n" +
            ") substansi_role\n" +
            "on substansi_role.id_role = tr.id_role\n" +
            "left join (\n" +
            "    select l.officer_by,\n" +
            "           count(*) jumlah_laporan\n" +
            "    from simpel_4.tx_laporan l \n" +
            "    where is_suspended is null and status_laporan != 'LT'\n" +
            "    and status_laporan != 'TMSM'\n" +
            "    and status_laporan != 'TMSF'\n" +
            "    and status_laporan != 'LTRKS'\n" +
            "    and status_laporan != 'LTRSM'\n" +
            "    and status_laporan != 'DRKS'\n" +
            "    and status_laporan != 'DTRES'\n" +
            "    group by 1\n" +
            ") l\n" +
            "on u.id_user = l.officer_by\n" +
            "where " +
            (q ? "lower(u.nama) like $2::text and " : "") +
            "CASE\n" +
            "    WHEN $1::text is not null\n" +
            "    THEN u.kode_kantor::text = $1\n" +
            "    ELSE u.kode_kantor is not null end\n" +
            "    and tr.id_role in (\n" +
            "    '621dfc56-655f-e867-df0f-4c2fccbb655d',\n" +
            "\t'0b03127c-39c1-6ffb-6111-18cd985b0bf2',\n" +
            "\t'86bca1e1-fc1e-47de-63ec-5fcf00a0d893',\n" +
            "\t'fc2166a0-4caa-3168-3dbf-672209312a29',\n" +
            "\t'42f4c5cd-5222-5f3b-473f-d6390248fda4',\n" +
            "\t'34f9499c-1650-167b-9268-c2f6ca9df369',\n" +
            "\t'338a843c-cc25-5c99-ad14-b9b78f4e7c7d',\n" +
            "\t'c1a7f50a-7854-28c9-df57-9785964dc651',\n" +
            "\t'6b3963af-a8e2-5ae2-4b0b-251fcb0e2558',\n" +
            "\t'd65f9ca7-eb69-a817-6742-9be12568ad15',\n" +
            "\t'8a54c7a3-c3f2-d653-3210-3557e0df6475',\n" +
            "\t'66d8db87-8dae-586d-c166-88e040da0972',\n" +
            "\t'8f46a852-9e55-6d21-7203-7c7329d55bd4',\n" +
            "\t'da5dff82-a242-2b11-9862-8aa21559b227'\n" +
            "    )\n" +
            "order by nama_kantor, jumlah_laporan"

        const sqlFinal = sql
        // if (arrSubstansi) {
        //     sql = sql.replace("$3", arrSubstansi.length)
        //     sqlFinal = sql.replace("$4", arrSubstansiFinal)
        // }

        // let result = await this.txLaporanRepository.dataSource.execute(sqlFinal, [kodeKantor, q]);
        let result;
        if (q) {
            result = await this.txLaporanRepository.dataSource.execute(sqlFinal, [kodeKantor, q]);
        } else {
            result = await this.txLaporanRepository.dataSource.execute(sqlFinal, [kodeKantor]);
        }


        const rs = {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        }
        return rs;
    }

    @get('/tx-laporan/role', {
        responses: {
            '200': {
                description: 'menampilkan laporan by role',
            },
        },
    })
    async laporanByRole(
        @param.filter(Petugas) filter?: Filter<Petugas>,
    ): Promise<any> {

        // @ts-ignore
        const kodeKantor = 'SBY'
        // @ts-ignore
        let q = null
        // @ts-ignore
        let arrSubstansi = []
        let arrSubstansiFinal = ""
        let roleArr = []
        let role = ""

        if (filter) {
            // @ts-ignore
            // kodeKantor = filter.where.kode_kantor

            // @ts-ignore
            const paramQ = filter.where.q
            if (paramQ != null) {
                q = '%' + paramQ.toUpperCase() + '%'
            }

            // @ts-ignore
            arrSubstansi = filter.where.substansi
            if (arrSubstansi != null) {
                //arrSubstansiSample = "'substansi_3', 'substansi_1'"
                arrSubstansiFinal = "'" + arrSubstansi.join("', '") + "'"
            }

            // @ts-ignore
            roleArr = filter.where.role
            if (roleArr != null) {
                //arrSubstansiSample = "'substansi_3', 'substansi_1'"
                role = "'" + roleArr.join("', '") + "'"
            }
        }

        // console.log(role);


        const sql = "select \n" +
            "distinct \n" +
            "u.id_user, u.nama,\n" +
            "mk.nama_kantor,\n" +
            "case WHEN (l.jumlah_laporan > 0) THEN l.jumlah_laporan ELSE 0 END jumlah_laporan\n" +
            "from apps_manager.td_user u\n" +
            "join apps_manager.td_user_detail tud on u.id_user = tud.id_user\n" +
            "join apps_manager.td_role tr on tud.id_role = tr.id_role\n" +
            "join referensi.m_kantor mk on mk.kode_kantor = u.kode_kantor\n" +
            "left join (\n" +
            "    select tsr.id_role, c.lookup_code\n" +
            "    from apps_manager.td_substansi_role tsr\n" +
            "    join referensi.m_lookup c on c.lookup_code = tsr.lookup_code\n" +
            "    where c.lookup_group_name = 'substansi'\n" +
            ") substansi_role\n" +
            "on substansi_role.id_role = tr.id_role\n" +
            "left join (\n" +
            "    select l.officer_by,\n" +
            "           count(*) jumlah_laporan\n" +
            "    from simpel_4.tx_laporan l where is_suspended is null and \n" +
            " status_laporan != 'LT'and status_laporan != 'TMSM'and status_laporan != 'TMSF'and \n" +
            " status_laporan != 'LTRSM'and status_laporan != 'LTRSM' and \n" +
            " status_laporan != 'DRKS'and status_laporan != 'DTRES' \n" +
            "    group by 1\n" +
            ") l\n" +
            "on u.id_user = l.officer_by\n" +
            // "where CASE\n" +
            // "    WHEN $1::text is not null\n" +
            // "    THEN u.kode_kantor::text = $1\n" +
            // "    ELSE u.kode_kantor is not null end\n" +
            "    where tr.id_role in (\n" +
            `${role}` +
            "    )\n" +
            "order by nama_kantor, jumlah_laporan"

        const sqlFinal = sql
        // if (arrSubstansi) {
        //     sql = sql.replace("$3", arrSubstansi.length)
        //     sqlFinal = sql.replace("$4", arrSubstansiFinal)
        // }

        // let result = await this.txLaporanRepository.dataSource.execute(sqlFinal, [kodeKantor, q]);
        const result = await this.txLaporanRepository.dataSource.execute(sqlFinal);

        const rs = {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        }
        return rs;
    }

    async createPelapor(txLaporan: Omit<TxLaporan, "id">) {
        const objMPelapor = {
            kategori_pelapor: txLaporan.kategori_pelapor,
            email_pelapor: txLaporan.email_pelapor,
            no_telp_pelapor: txLaporan.no_telp_pelapor,
            kec_pelapor: txLaporan.kec_pelapor,
            kab_kota_pelapor: txLaporan.kab_kota_pelapor,
            provinsi_pelapor: txLaporan.provinsi_pelapor,
            alamat_lengkap_pelapor: txLaporan.alamat_lengkap_pelapor,
            pendidikan_pelapor: txLaporan.pendidikan_pelapor,
            pekerjaan_pelapor: txLaporan.pekerjaan_pelapor,
            status_perkawinan_pelapor: txLaporan.status_perkawinan_pelapor,
            jenis_kelamin_pelapor: txLaporan.jenis_kelamin_pelapor,
            tanggal_lahir_pelapor: txLaporan.tanggal_lahir_pelapor,
            tempat_lahir_pelapor: txLaporan.tempat_lahir_pelapor,
            file_identitas_pelapor: txLaporan.file_identitas_pelapor,
            nomor_identitas_pelapor: txLaporan.nomor_identitas_pelapor,
            jenis_identitas_pelapor: txLaporan.jenis_identitas_pelapor,
            warga_negara_pelapor: txLaporan.warga_negara_pelapor,
            nama_pelapor: txLaporan.nama_pelapor
        }


        const savedMPelapor = await this.mPelaporRepository.create(objMPelapor)

        return savedMPelapor.id
    }

    // @ts-ignore
    async generateNomorAgenda() {
        //var sampel = '010611.2021'

        // const dataLaporan = await this.txLaporanRepository.find({
        //     fields: ['no_agenda', 'tahun'],
        //     order: ['tahun DESC', 'no_agenda DESC'],
        //     where: {no_agenda: {neq: ""}},
        //     limit: 1,
        // });
        //
        // // @ts-ignore
        // const nomor = dataLaporan.length > 0 ? dataLaporan[0]['no_agenda'].substring(0, dataLaporan[0]['no_agenda'].indexOf('.')) : '0';
        // let nomorFinal = await this.generateNum(1, parseInt(nomor))
        // const tahunLast = dataLaporan.length > 0 ? dataLaporan[0]['tahun'] : '0';
        //
        // // @ts-ignore
        // if (nomor == nomorFinal) { // @ts-ignore
        //     nomorFinal += nomorFinal
        // }
        //
        // const tahun = new Date().getFullYear();
        // // @ts-ignore
        // if (tahun != tahunLast) {
        //     nomorFinal = 1
        // }
        // const nomorAgenda = this.zeroPad(nomorFinal, 100000) + '.' + tahun
        const nomorAgenda = await this.txLaporanRepository.dataSource.execute("select generate_no_agenda()");

        return nomorAgenda[0]["generate_no_agenda"]
    };

    async generateNum(cons: number, max: number) {
        let randNumber = max + cons
        if (randNumber == max) {
            randNumber += await this.generateNum(cons, max);
        }
        return randNumber;
    }

    async generateNomorArsip(kdKantor: string | undefined, idProv: string | undefined, jenisLaporan: string | undefined, caraPenyampaian: string | undefined, klasifikasiTerlapor: string | undefined) {
        //var sampel = '0094/LM/IX/2021/BKL'
        let nomorArsip = '';
        // let laporanNeedArsip = ["RCO", "ÏIN", "LM"]
        const laporanNeedArsip = ["IN", "LM", "RCO"]

        if (jenisLaporan === "RCO") {
            jenisLaporan = "LM";
        }

        if (jenisLaporan != null && laporanNeedArsip.includes(jenisLaporan)) {
            // let kodekantor = 'JKT';
            const month = new Date().getMonth() + 1;
            const tahun = new Date().getFullYear();
            const bulanRomawi = this.convertToRoman(month);

            const sql = `SELECT no_arsip,substr(no_arsip,0,5) as digit
            FROM  simpel_4.tx_laporan
            where no_arsip like '%${tahun}/${kdKantor}'
            order by digit desc
            limit 1`;

            const dataLaporan = await this.txLaporanRepository.execute(sql);
            // const seqNoArsip = await this.txLaporanRepository.execute("select nextval('simpel_4.seq_no_arsip')");
            // const dataLaporan = await this.txLaporanRepository.find({
            //     fields: ['no_arsip'],
            //     order: ['created_date DESC'],
            //     where: {
            //         no_arsip: {
            //             ilike : '%' +tahun+'/'+ kdKantor
            //         }
            //     },
            //     limit: 1,
            // });
            let kodekantor = kdKantor;//dataLaporan[0]['kode_kantor'];
            //console.log(dataLaporan)

            // @ts-ignore
            let nomor = dataLaporan.length > 0 ? dataLaporan[0].digit : '0';

            // @ts-ignore
            if (nomor === undefined || isNaN(nomor)) { // @ts-ignore
                nomor = 0
            }

            const nomorInc = this.zeroPad(parseInt(nomor) + 1, 1000)

            // dicomment sangkuriang
            // if (caraPenyampaian === "cara_penyampaian_06") {
            //     let dataKantor = await this.mKantorRepository.find({
            //         fields: ['kode_kantor'],
            //         order: ['nama_kantor DESC'],
            //         where: {
            //             id_provinsi: idProv
            //         },
            //         limit: 1,
            //     });
            //
            //     kodekantor = dataKantor[0]['kode_kantor'];
            // }


            //klasifikasi instansi terlapor
            const klasifikasiTerlaporJKR = ["pemda", "bpn", "kepolisian"]
            if (klasifikasiTerlaporJKR.includes(<string>klasifikasiTerlapor)) {

                //pemda DKI = JKR, selain itu sesuai provinsi
                if (klasifikasiTerlapor === "pemda" && idProv === "31")
                    kodekantor = 'JKR'

                //tanah selain banten=JKR
                if (klasifikasiTerlapor === "bpn" && idProv !== "36")
                    kodekantor = 'JKR'

                //kepolisian = JKR (sudah sesuai)
                if (klasifikasiTerlapor === "kepolisian")
                    kodekantor = 'JKR'
            }

            nomorArsip = nomorInc + '/' + jenisLaporan + '/' + bulanRomawi + '/' + tahun + '/' + kodekantor
        }

        return nomorArsip
    };


    @post('/send-email/init')
    async seneEmailInit(
        @requestBody() sendEmailInit: SendEmailInit,
    ): Promise<string> {
        // checks whether email is valid as per regex pattern provided
        const email = await this.validateEmail(sendEmailInit.email);

        // At this point we are dealing with valid email.
        // Lets check whether there is an associated account
        const filter = {
            where: {'email_pelapor': email},
            order: ['created_date DESC']
        }

        const foundUser = await this.txLaporanRepository.findOne(filter);

        // No account found
        if (!foundUser) {
            throw new HttpErrors.NotFound(
                'No complainant associated with the provided email address.',
            );
        }

        const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas', 'warga_negara_pelapor']
        const resultData = await this.pvlService.getMLookUpFindById(foundUser.id, filter, listUraian, this.txLaporanRepository)

        //get daeran pelapor
        const provPelapor = resultData['provinsi_pelapor']
        const kabPelapor = resultData['kab_kota_pelapor']
        const kecPelapor = resultData['kec_pelapor']

        const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
        if (daerahPelapor) {

            resultData['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
            resultData['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
            resultData['kec_pelapor_name'] = daerahPelapor['nm_kec'];

        }


        //get daeran terlapor
        const provTerlapor = resultData['provinsi_terlapor']
        const kabTerlapor = resultData['kab_kota_terlapor']
        const kecTerlapor = resultData['kec_terlapor']
        const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
        if (daerahTerlapor) {
            resultData['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
            resultData['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
            resultData['kec_terlapor_name'] = daerahTerlapor['nm_kec'];
        }


        // Send an email to the user's email address
        const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
            resultData,
        );

        // Nodemailer has accepted the request. All good
        if (nodeMailer.accepted.length) {
            return 'An email has been sent to the provided email';
        }

        // Nodemailer did not complete the request alert the user
        throw new HttpErrors.InternalServerError(
            'Error sending report email',
        );
    }

    async validateEmail(email: string): Promise<string> {
        const emailRegPattern = /\S+@\S+\.\S+/;
        if (!emailRegPattern.test(email)) {
            throw new HttpErrors.UnprocessableEntity('Invalid email address');
        }
        return email;
    }

    generateToken(length: number, jenisLaporan: string | undefined) {
        //var sampel = '4tz7bngnmd';

        const laporanNeedToken = ["RCO", "LM"]
        let token = '-';
        let result = '';

        if (jenisLaporan != null && laporanNeedToken.includes(jenisLaporan)) {
            const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            const charactersLength = characters.length;
            for (let i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() *
                    charactersLength));
            }

            token = result
        }

        return token;

    };

    convertToRoman(num: number) {

        const numeralCodes = [["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],         // Ones
        ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],   // Tens
        ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]];

        let numeral = "";
        const digits = num.toString().split('').reverse();
        for (let i = 0; i < digits.length; i++) {
            numeral = numeralCodes[i][parseInt(digits[i])] + numeral;
        }
        return numeral;
    }

    async getDaerah(kdProv: string | undefined, kdKabKota: string | undefined, kdKec: string | undefined): Promise<any> {

        const sql = "select\n" +
            "      prov.nama as nm_provinsi,\n" +
            "      mk.nama as nm_kab_kota,\n" +
            "      mkc.nama as nm_kec\n" +
            "\n" +
            "    from \"referensi\".\"m_provinsi\" prov\n" +
            "           join \"referensi\".\"m_kabkota\" mk on prov.kode = mk.id_provinsi\n" +
            "           join \"referensi\".\"m_kecamatan\" mkc on mkc.id_kabkota = mk.kode\n" +
            "\n" +
            "    where prov.kode::text = $1\n" +
            "    and mk.kode = $2" +
            "    and mkc.kode = $3"


        const data = await this.txLaporanRepository.dataSource.execute(sql,
            [kdProv, kdKabKota, kdKec])

        return data[0];

    }

    zeroPad(nr: number, base: number) {
        const len = (String(base).length - String(nr).length) + 1;
        return len > 0 ? new Array(len).join('0') + nr : nr;
    }

    @get('/tx-laporan/tracking')
    @response(200, {
        description: 'Array of Laporan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TrackingLaporan, {includeRelations: true}),
                },
            },
        },
    })
    async tracking(
        @param.filter(TxLaporan) filter?: Filter<TrackingLaporan>,
    ): Promise<object> {
        // @ts-ignore
        const token = filter.where.token

        if (!token) {
            throw new HttpErrors.Forbidden(
                "Harus menyertakan token",
            );
        }

        const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas',
            'pendidikan_pelapor', 'warga_negara_pelapor']
        const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txLaporanRepository)

        //get daeran pelapor
        const provPelapor = resultData[0]['provinsi_pelapor']
        const kabPelapor = resultData[0]['kab_kota_pelapor']
        const kecPelapor = resultData[0]['kec_pelapor']

        const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
        if (daerahPelapor) {
            resultData[0]['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
            resultData[0]['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
            resultData[0]['kec_pelapor_name'] = daerahPelapor['nm_kab_kota'];
        }

        //get daeran terlapor
        const provTerlapor = resultData[0]['provinsi_terlapor']
        const kabTerlapor = resultData[0]['kab_kota_terlapor']
        const kecTerlapor = resultData[0]['kec_terlapor']
        const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
        if (daerahTerlapor) {
            resultData[0]['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
            resultData[0]['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
            resultData[0]['kec_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
        }

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: resultData
        };
    }

    @get('tx-laporan/daftar-pelapor')
    @response(204, {
        description: 'Menampilkan Daftar Pelapor'
    })
    async daftraPelapor(
        @param.query.number("page") page: number,
        @param.query.number("limit") limit: number,
        @param.query.string("q") q: string
    ): Promise<any> {
        const sql = "select \n" +
            "distinct \n" +
            "a.nama_pelapor, \n" +
            "a.nomor_identitas_pelapor,\n" +
            "a.no_telp_pelapor,\n" +
            "a.email_pelapor,\n" +
            "a.warga_negara_pelapor,\n" +
            "a.jenis_identitas_pelapor,\n" +
            "a.tempat_lahir_pelapor,\n" +
            "a.tanggal_lahir_pelapor,\n" +
            "a.jenis_kelamin_pelapor,\n" +
            "a.status_perkawinan_pelapor,\n" +
            "a.pekerjaan_pelapor,\n" +
            "a.pendidikan_pelapor,\n" +
            "a.alamat_lengkap_pelapor," +
            "a.file_identitas_pelapor, \n" +
            "b.nama as provinsi_pelapor,\n" +
            "c.nama as kab_kota_pelapor,\n" +
            "d.nama as kec_pelapor \n" +
            "From simpel_4.tx_laporan as a\n" +
            "left join referensi.m_provinsi b on a.provinsi_pelapor = b.kode \n" +
            "left join referensi.m_kabkota c on a.kab_kota_pelapor = c.kode \n" +
            "left join referensi.m_kecamatan d on a.kec_pelapor = d.kode \n" +
            "Where a.status = true and (lower(nama_pelapor) like '%'||$1||'%' or \n" +
            "nomor_identitas_pelapor like '%'||$1||'%' or \n" +
            "no_telp_pelapor like '%'||$1||'%' or \n" +
            "lower(email_pelapor) like '%'||$1||'%') \n" +
            "order by nama_pelapor\n" +
            "offset $2 limit $3";

        const result = await this.txLaporanRepository.dataSource.execute(sql, [q, (page <= 0 ? 0 : page) * limit, limit]);

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };
    }

    private async cekStatusLaporan(statusLaporan?: string, dataId?: string, dataNoAgenda?: string, dataKodeKantor?: string) {
        switch (statusLaporan) {
            case 'RL':
                // @ts-ignore
                await this.getFileFromReport("cover_map_kuning", dataId, 'RL', dataNoAgenda, dataKodeKantor)
                break;
            case 'VFL':
                // @ts-ignore
                await this.getFileFromReport("form_formil", dataId, 'VFL', dataNoAgenda, dataKodeKantor)
                break;
            case 'VML':
                // @ts-ignore
                await this.getFileFromReport("form_ringkasan_materiil", dataId, 'VML', dataNoAgenda, dataKodeKantor)
                break;
            case 'KLL':
                // @ts-ignore
                await this.getFileFromReport("klasifikasi_lm", dataId, 'KLL', dataNoAgenda, dataKodeKantor)
                break;
            case 'PMP':
                // @ts-ignore
                await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", dataId, 'PMP', dataNoAgenda, dataKodeKantor)
                break;
            case 'lhpd':
                // @ts-ignore
                await this.getFileFromReport("lhpd", dataId, 'lhpd', dataNoAgenda, dataKodeKantor)
                break;
            case 'PLRKS':
                // @ts-ignore
                await this.getFileFromReport("ba_penutupan_laporan", dataId, 'PLRKS', dataNoAgenda, dataKodeKantor)
                break;
            default:
                break;
        }
    }

    private async getFileFromReport(namafile: string, idtxlaporan: string, status: string, nomor_agenda: string, kode_kantor: string) {
        let endpoint = 'cara_tindak_lanjut'
        let version = 1

        switch (status) {
            case 'DRF':
                endpoint = 'cover_map_kuning'
                namafile = 'cover_map_kuning'
                break;
            case 'RL':
                endpoint = 'cover_map_kuning'
                namafile = 'cover_map_kuning'
                break;
            case 'VFL':
                endpoint = 'form_formil'
                namafile = 'form_formil'
                break;
            case 'VML':
                endpoint = 'form_ringkasan_materiil'
                namafile = 'form_ringkasan_materiil'
                break;
            case 'KLL':
                endpoint = 'klasifikasi_lm'
                namafile = 'klasifikasi_lm'
                break;
            case 'PMP':
                endpoint = 'pemberitahuan_dimulai_pemeriksaan'
                namafile = 'pemberitahuan_dimulai_pemeriksaan'
                break;
            case 'lhpd':
                endpoint = 'lhpd'
                namafile = 'formulir_lhpd'
                break;
            case 'PLRKS':
                endpoint = 'ba_penutupan_laporan'
                namafile = 'ba_penutupan_laporan'
                break;
            case 'TMSF':
                endpoint = 'bapl_tidak_memenuhi_syarat_formil'
                namafile = 'bapl_tidak_memenuhi_syarat_formil'
                break;
            case 'TMSM':
                endpoint = 'bapl_tidak_memenuhi_syarat_materiil'
                namafile = 'bapl_tidak_memenuhi_syarat_materiil'
                break;
            default:
                break;
        }

        const template = "";

        if (kode_kantor !== 'JKT') {
            if (status === 'VFL') {
                // template = namafile;
                namafile += '_perwakilan';
                endpoint += '_perwakilan';
            } else if (status === 'PLRKS') {
                namafile += '_riksaperwakilan';
                endpoint += '_riksaperwakilan';
            } else {
                namafile += '_perwakilan';
                endpoint += '_perwakilan';
            }
        } else {
            if (status === 'PLRKS') {
                namafile += '_riksapusat';
                endpoint += '_riksapusat';
            }
        }

        const TDokumen = await this.txDokumenRepository.find({
            where: {
                id_tx_laporan: idtxlaporan,
                tipe_dokumen: namafile
            },
            order: ['version DESC'],
            limit: 1
        });

        if (TDokumen.length > 0) {
            version = ((TDokumen[0].version ? TDokumen[0].version : 0) + 1);
        }

        await this.dokumenService.generateDokumen(endpoint, namafile, idtxlaporan, version, nomor_agenda, status, template)

        // const fold   erPath = './public/uploads/dokumen/' + idtxlaporan
        // if (!fs.existsSync(folderPath)) {
        //     fs.mkdirSync(folderPath, { recursive: true });
        // }
        // const headers = {...this.request.headers};
        // const token = headers.authorization !== null && headers.authorization?.startsWith("Bearer") ?
        //     headers.authorization?.substring(7, headers.authorization.length) : undefined;
        // // var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsInN1YnN0YW5zaSI6WyJzdWJzdGFuc2lfMyIsInN1YnN0YW5zaV8zIiwic3Vic3RhbnNpXzEiLCJzdWJzdGFuc2lfMSIsInN1YnN0YW5zaV80Iiwic3Vic3RhbnNpXzQiLCJzdWJzdGFuc2lfNSIsInN1YnN0YW5zaV81Iiwic3Vic3RhbnNpXzYiLCJzdWJzdGFuc2lfOSIsInN1YnN0YW5zaV8xMSIsInN1YnN0YW5zaV8xMyIsInN1YnN0YW5zaV8xNSIsInN1YnN0YW5zaV8xNiIsInN1YnN0YW5zaV8xOCIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8xOSIsInN1YnN0YW5zaV8yNiIsInN1YnN0YW5zaV8zMCIsInN1YnN0YW5zaV8zMSIsInN1YnN0YW5zaV8zMiIsInN1YnN0YW5zaV8zNCIsInN1YnN0YW5zaV8xMiIsInN1YnN0YW5zaV8zMyIsInN1YnN0YW5zaV8zNyIsInN1YnN0YW5zaV80OCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MCIsInN1YnN0YW5zaV81MiIsInN1YnN0YW5zaV84Il0sInBlcm1pc3Npb25zIjpbIlBldHVnYXMgUmlrc2EiLCJSZWdpc3RyYXNpLVJlZ2lzdHJhc2ktVGFtYmFoIiwiUmVnaXN0cmFzaS1PbWJ1ZHNtYW4gT25saW5lLVJlZ2lzdHJhc2lrYW4iLCJQZXR1Z2FzIFBWTCIsIlJlZ2lzdHJhc2ktUmVnaXN0cmFzaS1QZW51Z2FzYW4iXSwia2luZCI6ImFjY2VzcyIsInJvbGVzIjpbIktlcGFsYSBUaW0gUmlrc2EgMyIsIktlcGFsYSBUaW0gUmlrc2EgNyIsIlRpbSBQVkwiLCJLZXBhbGEgS2Vhc2lzdGVuYW4gUmlrc2EiLCJLZXBhbGEgVGltIFJpa3NhIDYiLCJBbmdnb3RhIFJpa3NhIDEiLCJLZXBhbGEgS2Vhc2lzdGVuYW4iLCJLZXBhbGEgVGltIFJpa3NhIDIiLCJLZXBhbGEgVGltIFJpa3NhIDQiLCJLZXBhbGEgVGltIFJpa3NhIDUgVGVzdCIsIktlcGFsYSBUaW0gUmlrc2EgMSJdLCJleHAiOjE2NjY3NzI2NDYsInVzZXIiOnsiaWQiOiJlYWM5NzllZC1mNTBkLTQ3MmYtODUwYy1mOWQxNjg1ZWRlYWQiLCJ1c2VybmFtZSI6ImNob2lydWRkaW5AZ21haWwuY29tIiwiZW1haWwiOiJjaG9pcnVkZGluQGdtYWlsLmNvbSIsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDMifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNyJ9LHsiYXV0aG9yaXR5IjoiVGltIFBWTCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIFJpa3NhIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDYifSx7ImF1dGhvcml0eSI6IkFuZ2dvdGEgUmlrc2EgMSJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIEtlYXNpc3RlbmFuIn0seyJhdXRob3JpdHkiOiJLZXBhbGEgVGltIFJpa3NhIDIifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgNCJ9LHsiYXV0aG9yaXR5IjoiS2VwYWxhIFRpbSBSaWtzYSA1IFRlc3QifSx7ImF1dGhvcml0eSI6IktlcGFsYSBUaW0gUmlrc2EgMSJ9XSwiZW5hYmxlZCI6dHJ1ZSwiYWNjb3VudE5vbkxvY2tlZCI6dHJ1ZSwiY3JlZGVudGlhbHNOb25FeHBpcmVkIjp0cnVlLCJhY2NvdW50Tm9uRXhwaXJlZCI6dHJ1ZX0sImlhdCI6MTY2NjY4NjI0Nn0.lhEQneVY6t9xC0YzYXlxl2bPALqhLwT56qLeawBr2hqEq6D4HcwARh35W0qFhmt6LHKR4m-NblNS_lO1OeUzrg'
        //
        // const options = {
        //     hostname: 'api-report.ombudsman.dev.layanan.go.id',
        //     path: `/v1/report/${endpoint}?jenisSurat=${namafile}&idLaporan=${idtxlaporan}&lang=en&tte=&nik=&passphrase=`,
        //     headers: {
        //         Authorization: 'Bearer ' + token
        //     }
        // }
        // const filename: string = folderPath + '/' + namafile + '_' + idtxlaporan + '_' + version + '.pdf'
        // const file = fs.createWriteStream(filename)
        //
        // https.get(options, res => {
        //     if (res.statusCode !== 200) {
        //         fs.unlink(filename, () => {
        //             console.log(res.statusCode);
        //             // reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
        //         });
        //         return;
        //     }
        //
        //     let headers = res.headers
        //     let page = -1
        //     if (headers['page'] && headers['page'] instanceof String) {
        //         page = parseInt((headers['page'] as string))
        //     }
        //
        //     res.pipe(file);
        //     res.on('data', chunk => {
        //         // data.push(chunk);
        //     });
        //     file.on("finish", () => {
        //         file.close();
        //         // @ts-ignore
        //         const dataTxDokumen: TxDokumen = {
        //             filename,
        //             id_tx_laporan: idtxlaporan,
        //             tipe_dokumen: namafile,
        //             nomor_agenda: nomor_agenda,
        //             status_dokumen: status,
        //             version,
        //             page: page
        //         }
        //
        //         this.txDokumenRepository.save(dataTxDokumen)
        //     });
        // })
    }

    @get('/tx-laporan-by-token/{token}')
    @response(200, {
        description: 'TxLaporan model by token',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
            },
        },
    })
    async findByToken(
        @param.path.string('token') token: string,
        @param.filter(TxLaporan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxLaporan>
    ): Promise<{data: any; message: string; status: boolean}> {

        const sql = `select * from simpel_4.tx_laporan\n` +
            `where token = '${token}' and status = true`

        const result = await this.txLaporanRepository.dataSource.execute(sql);

        const statusLap = result[0].status_laporan;

        const proses = await this.pvlService.getMLookUpStatus(statusLap);

        const resultFinal = result[0].proses_status = proses

        result[0].status_laporan = resultFinal

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result[0]
        };
    }

    @get('/tx-laporan-by-email')
    @response(200, {
        description: 'TxLaporan model by token',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
            },
        },
    })
    async findByAlamatEmail(
        @param.query.number("page") page: number,
        @param.query.number("limit") limit: number,
        @param.query.string("email") email: string
    ): Promise<any> {

        const sql = "select * from simpel_4.tx_laporan\n" +
            "where status = true and lower(email_pelapor) = lower($1)" +
            "order by created_date desc offset $2 limit $3";

        const result = await this.txLaporanRepository.dataSource.execute(sql, [email, (page <= 0 ? 0 : page) * limit, limit]);

        // console.log(result)
        for (let j = 0; j < result.length; j++) {
            const proses = await this.pvlService.getMLookUpStatus(result[j].status_laporan);
            result[j].status_laporan = proses
        }

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: result
        };
    }

    @get('/list-file/{id}')
    @response(200, {
        description: 'TxLaporan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLaporan, {includeRelations: true}),
            },
        },
    })
    async listFile(
        @param.path.string('id') id: string
    ): Promise<object> {
        const existingData = await this.txLaporanRepository.findById(id);

        type ModelsRespon = {
            kategoriPelapor: string;
            klasifikasiPelapor: string;
            filenameDokumen: any;
            jenisDokumen: string;
        };

        const hasil: ModelsRespon[] = [];

        // const coba: Hehe = {
        //     name : "aku",
        //     value : "hehe"
        // }
        // const coba2: Hehe = {
        //     name : "aku",
        //     value : "hehe"
        // }
        // kkk.push(coba);
        // kkk.push(coba2);


        if (existingData.kategori_pelapor === "kategori_pelapor_01" && existingData.jenis_pelapor === "klasifikasi_pelapor_01") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data3);

        } else if (existingData.kategori_pelapor === "kategori_pelapor_01" && existingData.jenis_pelapor === "klasifikasi_pelapor_06") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data3);
            const data4: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_art,
                jenisDokumen: "AD / ART"
            }
            hasil.push(data4);
            const data5: ModelsRespon = {
                kategoriPelapor: "Korban Langsung",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_akta_pendirian,
                jenisDokumen: "Akta Pendirian"
            }
            hasil.push(data5);
        } else if (existingData.kategori_pelapor === "kategori_pelapor_02" && existingData.jenis_pelapor === "klasifikasi_pelapor_04") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa,
                jenisDokumen: "Identitas Pemberi Kuasa"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data3);
            const data4: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data4);
            const data5: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.filename_surat_kuasa,
                jenisDokumen: "Surat Kuasa"
            }
            hasil.push(data5);
            const data6: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Anggota Keluarga",
                filenameDokumen: existingData.filename_kartu_keluarga,
                jenisDokumen: "Fotocopy Kartu Keluarga"
            }
            hasil.push(data6);
        } else if (existingData.kategori_pelapor === "kategori_pelapor_02" && existingData.jenis_pelapor === "klasifikasi_pelapor_02") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa,
                jenisDokumen: "Identitas Pemberi Kuasa"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data3);
            const data4: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data4);
            const data5: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_art,
                jenisDokumen: "AD / ART"
            }
            hasil.push(data5);
            const data6: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_akta_pendirian,
                jenisDokumen: "Akta Pendirian"
            }
            hasil.push(data6);
            const data7: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Badan Hukum Organisasi",
                filenameDokumen: existingData.filename_surat_kuasa,
                jenisDokumen: "Surat Kuasa"
            }
            hasil.push(data7);
        } else if (existingData.kategori_pelapor === "kategori_pelapor_02" && existingData.jenis_pelapor === "klasifikasi_pelapor_05") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa,
                jenisDokumen: "Identitas Pemberi Kuasa"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa2,
                jenisDokumen: "Identitas Pemberi Kuasa 2"
            }
            hasil.push(data3);
            const data4: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa3,
                jenisDokumen: "Identitas Pemberi Kuasa 3"
            }
            hasil.push(data4);
            const data5: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data5);
            const data6: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data6);
            const data7: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Kelompok Masyarakat",
                filenameDokumen: existingData.filename_surat_kuasa,
                jenisDokumen: "Surat Kuasa"
            }
            hasil.push(data7);
        } else if (existingData.kategori_pelapor === "kategori_pelapor_02" && existingData.jenis_pelapor === "klasifikasi_pelapor_07") {
            const data1: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.file_identitas_pelapor,
                jenisDokumen: "Identitas (KTP/KITAS/KITAP)"
            }
            hasil.push(data1);
            const data2: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_ktp_pemberi_kuasa,
                jenisDokumen: "Identitas Pemberi Kuasa"
            }
            hasil.push(data2);
            const data3: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_bukti,
                jenisDokumen: "Bukti Dokumen"
            }
            hasil.push(data3);
            const data4: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_uraian,
                jenisDokumen: "Uraian Peristiwa"
            }
            hasil.push(data4);
            const data5: ModelsRespon = {
                kategoriPelapor: "Kuasa Korban",
                klasifikasiPelapor: "Perorangan",
                filenameDokumen: existingData.filename_surat_kuasa,
                jenisDokumen: "Surat Kuasa"
            }
            hasil.push(data5);
        }

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: hasil
        };
    }

    @patch('/tx-laporan/registrasi/{id}')
    @response(204, {
        description: 'TdTtdSpesimen PATCH success',
    })
    async registrasiPastch(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(RegistrasiOnline, {partial: true}),
                },
            },
        })
        registrasiOnline: RegistrasiOnline,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(id);
            if (existingData.status_laporan == "DRF" && registrasiOnline.txLaporan.status_laporan == "RL" && existingData.tipe_laporan == null && (registrasiOnline.txLaporan.tipe_laporan == "LM" || registrasiOnline.txLaporan.tipe_laporan == "RCO")) {
                if (existingData.email_pelapor) {
                    const email = await this.validateEmail(existingData.email_pelapor);

                    // At this point we are dealing with valid email.
                    // Lets check whether there is an associated account
                    const filter = {
                        where: {'email_pelapor': email},
                        order: ['created_date DESC']
                    }

                    if (registrasiOnline.txLaporan.tipe_laporan == 'LM' || registrasiOnline.txLaporan.tipe_laporan == 'RCO') {
                        try {

                            const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas', 'warga_negara_pelapor']
                            const resultData = await this.pvlService.getMLookUpFindById(existingData.id, filter, listUraian, this.txLaporanRepository)

                            //get daeran pelapor
                            const provPelapor = resultData['provinsi_pelapor']
                            const kabPelapor = resultData['kab_kota_pelapor']
                            const kecPelapor = resultData['kec_pelapor']

                            const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
                            if (daerahPelapor) {
                                resultData['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
                                resultData['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
                                resultData['kec_pelapor_name'] = daerahPelapor['nm_kec'];
                            }


                            //get daeran terlapor
                            const provTerlapor = resultData['provinsi_terlapor']
                            const kabTerlapor = resultData['kab_kota_terlapor']
                            const kecTerlapor = resultData['kec_terlapor']
                            const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
                            if (daerahTerlapor) {
                                resultData['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
                                resultData['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
                                resultData['kec_terlapor_name'] = daerahTerlapor['nm_kec'];
                            }

                            const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
                                resultData,
                            );

                            if (nodeMailer.accepted.length)
                                console.log('An email has been sent to the provided email');


                        } catch (e: unknown) { // <-- note `e` has explicit `unknown` type

                            if (typeof e === "string") {
                                // works, `e` narrowed to string

                                console.log('Error sending report email')
                                console.log(e.toUpperCase())
                            } else if (e instanceof Error) {

                                console.log('Error sending report email')
                                console.log(e.message)
                            }


                        }
                    }
                }

            }

            // if (registrasiOnline.txLaporan.tgl_agenda == null && existingData.tgl_agenda == null) {
            //     registrasiOnline.txLaporan.tgl_agenda = new Date().toISOString()
            // }

            if (registrasiOnline.txLaporan.provinsi_terlapor == undefined) {
                registrasiOnline.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (registrasiOnline.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (registrasiOnline.txLaporan.created_by !== undefined && registrasiOnline.txLaporan.created_by !== null) {
                    createdBy = registrasiOnline.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    registrasiOnline.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                registrasiOnline.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (registrasiOnline.txLaporan.tipe_laporan !== null && registrasiOnline.txLaporan.tipe_laporan !== undefined && registrasiOnline.txLaporan.tipe_laporan !== "") {
                    tpLaporan = registrasiOnline.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        registrasiOnline.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, registrasiOnline.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    registrasiOnline.txLaporan.token = this.generateToken(10, registrasiOnline.txLaporan.tipe_laporan)
                }


                if (registrasiOnline.txLaporan.tipe_laporan == "KNL" || registrasiOnline.txLaporan.tipe_laporan == "Tembusan") {
                    registrasiOnline.txLaporan.status_petugas = 'dikerjakan';
                    if (registrasiOnline.txLaporan.officer_by == null && registrasiOnline.txLaporan.updated_by != null) {
                        registrasiOnline.txLaporan.officer_by = registrasiOnline.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != registrasiOnline.txLaporan.status_laporan && registrasiOnline.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: registrasiOnline.txLaporan.status_laporan,
                    created_by: registrasiOnline.txLaporan.updated_by != null ? registrasiOnline.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: registrasiOnline.txLaporan.kode_kantor != null ? registrasiOnline.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: registrasiOnline.txLaporan.officer_by != null ? registrasiOnline.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })
            }

            await this.txLaporanRepository.updateById(id, registrasiOnline.txLaporan, {
                transaction: tx
            })

            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (registrasiOnline.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = registrasiOnline.txChangeStatus.id_tx_laporan;
            }
            if (registrasiOnline.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = registrasiOnline.txChangeStatus.no_agenda;
            }
            if (registrasiOnline.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = registrasiOnline.txChangeStatus.status_laporan_date;
            }
            if (registrasiOnline.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = registrasiOnline.txChangeStatus.status_laporan_old;
            }
            if (registrasiOnline.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = registrasiOnline.txChangeStatus.status_laporan_new;
            }
            if (registrasiOnline.txChangeStatus.catatan != null) {
                existingData2.catatan = registrasiOnline.txChangeStatus.catatan;
            }

            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })


            tx.commit()
            // console.log(registrasiOnline);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(id)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @post('/tx-laporan/registrasi')
    @response(204, {
        description: 'txLaporan Post success',
    })
    async registrasiPost(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(RegistrasiNew, {partial: true}),
                },
            },
        })
        registrasiNew: RegistrasiNew
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {
            //save to m_pelapor
            registrasiNew.txLaporan.id = uuidv4();
            const mPelapor = await this.mPelaporRepository.find({
                where: {nomor_identitas_pelapor: registrasiNew.txLaporan.nomor_identitas_pelapor},
                limit: 1
            });

            let mIdPelapor = null
            if (mPelapor.length === 0) {
                mIdPelapor = await this.createPelapor(registrasiNew.txLaporan)
            } else {
                mIdPelapor = mPelapor[0]['id']
            }


            //save to
            if (registrasiNew.txLaporan.status_laporan === "RL" || registrasiNew.txLaporan.tipe_laporan === "IN") {
                // dicomment sangkuriang
                registrasiNew.txLaporan.no_agenda = await this.generateNomorAgenda()
                registrasiNew.txLaporan.token = this.generateToken(10, registrasiNew.txLaporan.tipe_laporan)
                let kodeKantor = ""
                let didaftarkanOleh = ""
                if (registrasiNew.txLaporan.tipe_laporan == "IN" && registrasiNew.txLaporan.proses_laporan) {
                    kodeKantor = registrasiNew.txLaporan.proses_laporan;
                }


                if (!kodeKantor) {
                    if (registrasiNew.txLaporan.created_by !== undefined && registrasiNew.txLaporan.created_by !== null) {
                        await this.tdUserRepository.findById(registrasiNew.txLaporan.created_by).then((value: TdUser) => {
                            if (value !== null && value !== undefined) {
                                if (value.kodeKantor !== null && value.kodeKantor !== undefined) {
                                    kodeKantor = value.kodeKantor;
                                }
                            }
                        });
                    }
                }

                if (kodeKantor !== null && kodeKantor !== undefined && kodeKantor !== "") {
                    registrasiNew.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, registrasiNew.txLaporan.provinsi_terlapor, registrasiNew.txLaporan.tipe_laporan, registrasiNew.txLaporan.cara_penyampaian, registrasiNew.txLaporan.klasifikasi_instansi_terlapor)

                    registrasiNew.txLaporan.kode_kantor = kodeKantor;

                    await this.mKantorRepository.findById(kodeKantor).then((value: MKantor) => {
                        if (value !== null && value.nama_kantor !== null && value.nama_kantor !== undefined) {
                            didaftarkanOleh = value.nama_kantor;
                        }
                    });

                    registrasiNew.txLaporan.didaftarkan_oleh = didaftarkanOleh;


                    if (registrasiNew.txLaporan.tipe_laporan === "Tembusan" || registrasiNew.txLaporan.tipe_laporan === "KNL") {
                        registrasiNew.txLaporan.officer_by = registrasiNew.txLaporan.created_by;
                        registrasiNew.txLaporan.status_petugas = "dikerjakan";
                    }
                }
            }

            //save to if status DRAFT
            //  || registrasiNew.txLaporan.no_arsip == '-'
            if (registrasiNew.txLaporan.status_laporan === "DRF") {
                const kantor = await this.mKantorRepository.findOne({
                    where: {'id_provinsi': registrasiNew.txLaporan.provinsi_terlapor}
                });
                let kodeKantor = kantor?.kode_kantor;
                let namaKantor = kantor?.nama_kantor;
                if (registrasiNew.txLaporan.provinsi_terlapor == '31') {
                    kodeKantor = 'JKT';
                    await this.mKantorRepository.findById(kodeKantor).then((value: MKantor) => {
                        if (value !== null && value.nama_kantor !== null && value.nama_kantor !== undefined) {
                            namaKantor = value.nama_kantor;
                        }
                    });
                }
                //registrasiNew.txLaporan.token = this.generateToken(10, registrasiNew.txLaporan.tipe_laporan)
                registrasiNew.txLaporan.didaftarkan_oleh = namaKantor
                registrasiNew.txLaporan.kode_kantor = kodeKantor
            }

            registrasiNew.txLaporan.id_m_pelapor = mIdPelapor
            if (registrasiNew.txLaporan.tgl_agenda == null) {
                registrasiNew.txLaporan.tgl_agenda = new Date().toISOString()
            }
            const savedLaporan = await this.txLaporanRepository.create(registrasiNew.txLaporan, {
                transaction: tx
            })

            // new update id_file_upload to filename

            const copylaporan = registrasiNew.txLaporan;
            if (registrasiNew.txLaporan.status_laporan === "DRF" && registrasiNew.txLaporan.id_file_upload != null) {
                if (registrasiNew.txLaporan.file_identitas_pelapor != null) {
                    copylaporan.file_identitas_pelapor = registrasiNew.txLaporan.file_identitas_pelapor?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_ktp_pemberi_kuasa != null) {
                    copylaporan.filename_ktp_pemberi_kuasa = registrasiNew.txLaporan.filename_ktp_pemberi_kuasa?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_ktp_pemberi_kuasa2 != null) {
                    copylaporan.filename_ktp_pemberi_kuasa2 = registrasiNew.txLaporan.filename_ktp_pemberi_kuasa2?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_ktp_pemberi_kuasa3 != null) {
                    copylaporan.filename_ktp_pemberi_kuasa3 = registrasiNew.txLaporan.filename_ktp_pemberi_kuasa3?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_uraian != null) {
                    copylaporan.filename_uraian = registrasiNew.txLaporan.filename_uraian?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_bukti != null) {
                    copylaporan.filename_bukti = registrasiNew.txLaporan.filename_bukti?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_art != null) {
                    copylaporan.filename_art = registrasiNew.txLaporan.filename_art?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_akta_pendirian != null) {
                    copylaporan.filename_akta_pendirian = registrasiNew.txLaporan.filename_akta_pendirian?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_surat_kuasa != null) {
                    copylaporan.filename_surat_kuasa = registrasiNew.txLaporan.filename_surat_kuasa?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_kartu_keluarga != null) {
                    copylaporan.filename_kartu_keluarga = registrasiNew.txLaporan.filename_kartu_keluarga?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_upload_bukti_upaya != null) {
                    copylaporan.filename_upload_bukti_upaya = registrasiNew.txLaporan.filename_upload_bukti_upaya?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }
                if (registrasiNew.txLaporan.filename_upload_bukti_upaya_email != null) {
                    copylaporan.filename_upload_bukti_upaya_email = registrasiNew.txLaporan.filename_upload_bukti_upaya_email?.replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id);
                }

                const dataUpdated = await this.txLaporanRepository.updateById(savedLaporan.id, copylaporan, {
                    transaction: tx
                })
            }


            const dirPath = "public/uploads/";
            // const finalPath = dirPath.split("\\").join("/")
            const files = glob.sync(dirPath + '?(*|*-)' + registrasiNew.txLaporan.id_file_upload + '?(-*)?*');

            let i = 0;
            for (i = 0; i < files.length; i++) {
                const f = $path.basename(files[i])
                const d = $path.dirname(files[i])
                fs.renameSync(files[i], files[i].replace(registrasiNew.txLaporan.id_file_upload, savedLaporan.id), function (err: unknown) {
                    if (err) throw err;
                });
            }


            // const idLaporan = savedLaporan.id;
            // const historyLaporan = Object.assign({}, savedLaporan);
            // // @ts-ignore
            // delete historyLaporan['id'];
            // // @ts-ignore
            // historyLaporan["id_laporan"] = idLaporan;
            // const savedHistory = await this.txLaporanHistoryRepository.create(historyLaporan);


            //generate surat
            switch (registrasiNew.txLaporan.status_laporan) {
                case 'RL':
                    // @ts-ignore
                    await this.getFileFromReport("cover_map_kuning", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'VFL':
                    // @ts-ignore
                    await this.getFileFromReport("form_formil", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'VML':
                    // @ts-ignore
                    await this.getFileFromReport("form_ringkasan_materiil", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'KLL':
                    // @ts-ignore
                    await this.getFileFromReport("klasifikasi_lm", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'PMP':
                    // @ts-ignore
                    await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'lhpd':
                    // @ts-ignore
                    await this.getFileFromReport("lhpd", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'PLRKS':
                    // @ts-ignore
                    await this.getFileFromReport("ba_penutupan_laporan", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                case 'TMSF':
                    // @ts-ignore
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", savedLaporan.id, registrasiNew.txLaporan.status_laporan, savedLaporan.no_agenda, savedLaporan.kode_kantor)
                    break;
                default:
                    break;
            }


            if (registrasiNew.txLaporan.tipe_laporan === "IN") {
                registrasiNew.txDisposisiPvlKapten.id = uuidv4();
                registrasiNew.txDisposisiPvlKapten.id_tx_laporan = registrasiNew.txLaporan.id;

                const createKapten: any =
                    this.txDisposisiPvlKaptenRepository.create(registrasiNew.txDisposisiPvlKapten, {
                        transaction: tx
                    })

                await this.txLaporanRepository.updateById(
                    registrasiNew.txDisposisiPvlKapten.id_tx_laporan,
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    {is_disposisi_kapten: registrasiNew.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                    transaction: tx
                })

            }

            await tx.commit()

            if (registrasiNew.txLaporan.email_pelapor) {
                const email = await this.validateEmail(registrasiNew.txLaporan.email_pelapor);

                // At this point we are dealing with valid email.
                // Lets check whether there is an associated account
                const filter = {
                    where: {'email_pelapor': email},
                    order: ['created_date DESC']
                }
                const foundLaporan = await this.txLaporanRepository.findOne(filter);


                // No account found
                if (foundLaporan) {
                    if (foundLaporan.tipe_laporan == 'LM' || foundLaporan.tipe_laporan == 'RCO') {

                        try {

                            const listUraian = ['kategori_pelapor', 'jenis_identitas', 'status_perkawinan_pelapor', 'kelompok_instansi_terlapor', 'klasifikasi_instansi_terlapor', 'substansi', 'tipe_laporan', 'cara_penyampaian', 'status_laporan', 'status_petugas', 'warga_negara_pelapor']
                            const resultData = await this.pvlService.getMLookUpFindById(foundLaporan.id, filter, listUraian, this.txLaporanRepository)

                            //get daeran pelapor
                            const provPelapor = resultData['provinsi_pelapor']
                            const kabPelapor = resultData['kab_kota_pelapor']
                            const kecPelapor = resultData['kec_pelapor']

                            const daerahPelapor = await this.getDaerah(provPelapor, kabPelapor, kecPelapor);
                            if (daerahPelapor) {
                                resultData['provinsi_pelapor_name'] = daerahPelapor['nm_provinsi'];
                                resultData['kab_kota_pelapor_name'] = daerahPelapor['nm_kab_kota'];
                                resultData['kec_pelapor_name'] = daerahPelapor['nm_kec'];
                            }


                            //get daeran terlapor
                            const provTerlapor = resultData['provinsi_terlapor']
                            const kabTerlapor = resultData['kab_kota_terlapor']
                            const kecTerlapor = resultData['kec_terlapor']
                            const daerahTerlapor = await this.getDaerah(provTerlapor, kabTerlapor, kecTerlapor);
                            if (daerahTerlapor) {
                                resultData['provinsi_terlapor_name'] = daerahTerlapor['nm_provinsi'];
                                resultData['kab_kota_terlapor_name'] = daerahTerlapor['nm_kab_kota'];
                                resultData['kec_terlapor_name'] = daerahTerlapor['nm_kec'];
                            }

                            const nodeMailer: SMTPTransport.SentMessageInfo = await this.emailService.sendReportMail(
                                resultData,
                            );

                            if (nodeMailer.accepted.length)
                                console.log('An email has been sent to the provided email');


                        } catch (e: unknown) { // <-- note `e` has explicit `unknown` type

                            if (typeof e === "string") {
                                // works, `e` narrowed to string

                                console.log('Error sending report email')
                                console.log(e.toUpperCase())
                            } else if (e instanceof Error) {

                                console.log('Error sending report email')
                                console.log(e.message)
                            }


                        }
                    }

                }
            }


            return {
                status: true,
                message: "Data berhasil diubah",
                data: registrasiNew.txLaporan
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-registrasi/{id}')
    @response(204, {
        description: 'tahapRegistrasi PATCH success',
    })
    async tahapRegistrasi(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TahapRegistrasi, {partial: true}),
                },
            },
        })
        tahapRegistrasi: TahapRegistrasi,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(id);
            if (tahapRegistrasi.txLaporan.provinsi_terlapor == undefined) {
                tahapRegistrasi.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (tahapRegistrasi.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (tahapRegistrasi.txLaporan.created_by !== undefined && tahapRegistrasi.txLaporan.created_by !== null) {
                    createdBy = tahapRegistrasi.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    tahapRegistrasi.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                tahapRegistrasi.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (tahapRegistrasi.txLaporan.tipe_laporan !== null && tahapRegistrasi.txLaporan.tipe_laporan !== undefined && tahapRegistrasi.txLaporan.tipe_laporan !== "") {
                    tpLaporan = tahapRegistrasi.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        tahapRegistrasi.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, tahapRegistrasi.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    tahapRegistrasi.txLaporan.token = this.generateToken(10, tahapRegistrasi.txLaporan.tipe_laporan)
                }


                if (tahapRegistrasi.txLaporan.tipe_laporan == 'KNL') {
                    tahapRegistrasi.txLaporan.status_petugas = 'ditugaskan';
                    if (tahapRegistrasi.txLaporan.officer_by == null && tahapRegistrasi.txLaporan.updated_by != null) {
                        tahapRegistrasi.txLaporan.officer_by = tahapRegistrasi.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != tahapRegistrasi.txLaporan.status_laporan && tahapRegistrasi.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: tahapRegistrasi.txLaporan.status_laporan,
                    created_by: tahapRegistrasi.txLaporan.updated_by != null ? tahapRegistrasi.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: tahapRegistrasi.txLaporan.kode_kantor != null ? tahapRegistrasi.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: tahapRegistrasi.txLaporan.officer_by != null ? tahapRegistrasi.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })
            }

            await this.txLaporanRepository.updateById(id, tahapRegistrasi.txLaporan, {
                transaction: tx
            })

            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (tahapRegistrasi.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = tahapRegistrasi.txChangeStatus.id_tx_laporan;
            }
            if (tahapRegistrasi.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = tahapRegistrasi.txChangeStatus.no_agenda;
            }
            if (tahapRegistrasi.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = tahapRegistrasi.txChangeStatus.status_laporan_date;
            }
            if (tahapRegistrasi.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = tahapRegistrasi.txChangeStatus.status_laporan_old;
            }
            if (tahapRegistrasi.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = tahapRegistrasi.txChangeStatus.status_laporan_new;
            }
            if (tahapRegistrasi.txChangeStatus.catatan != null) {
                existingData2.catatan = tahapRegistrasi.txChangeStatus.catatan;
            }

            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })

            const existingData3: TxVerifFormil = new TxVerifFormil();
            existingData3.id_tx_laporan = tahapRegistrasi.txVerifFormil.id_tx_laporan;
            await this.txVerifFormilRepository.create(existingData3, {
                transaction: tx
            })


            await tx.commit()

            // eslint-disable-next-line no-void
            void this.cekStatusLaporan(existingData.status_laporan, existingData.id, existingData.no_agenda, existingData.kode_kantor);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(id)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-formil-submit/{id}')
    @response(204, {
        description: 'tahapFormilSubmit PATCH success',
    })
    async tahapFormilSubmit(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(FormilSubmit, {partial: true}),
                },
            },
        })
        formilSubmit: FormilSubmit,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const {id, ...newFormil} = formilSubmit.txVerifFormil;

            await this.txVerifFormilRepository.updateById(formilSubmit.txVerifFormil.id, newFormil, {
                transaction: tx
            })

            if (formilSubmit.txVerifMateriil.id != "") {
                const newMateriil = _.omit(formilSubmit.txVerifMateriil, ['id']);

                await this.txVerifMateriilRepository.updateById(formilSubmit.txVerifMateriil.id, newMateriil, {
                    transaction: tx
                })
            } else {
                formilSubmit.txVerifMateriil.id = uuidv4();
                const savedData = await this.txVerifMateriilRepository.create(formilSubmit.txVerifMateriil, {
                    transaction: tx
                })
                if (savedData) {
                    const objLaporan = {
                        substansi: formilSubmit.txVerifMateriil.substansi,
                        pokok_masalah: formilSubmit.txVerifMateriil.pokok_permasalahan
                    }

                    await this.txLaporanRepository.updateById(savedData.id_tx_laporan, objLaporan, {
                        transaction: tx
                    })
                }
            }

            const existingData = await this.txLaporanRepository.findById(newId);
            if (formilSubmit.txLaporan.provinsi_terlapor == undefined) {
                formilSubmit.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (formilSubmit.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (formilSubmit.txLaporan.created_by !== undefined && formilSubmit.txLaporan.created_by !== null) {
                    createdBy = formilSubmit.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    formilSubmit.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                formilSubmit.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (formilSubmit.txLaporan.tipe_laporan !== null && formilSubmit.txLaporan.tipe_laporan !== undefined && formilSubmit.txLaporan.tipe_laporan !== "") {
                    tpLaporan = formilSubmit.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        formilSubmit.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, formilSubmit.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    formilSubmit.txLaporan.token = this.generateToken(10, formilSubmit.txLaporan.tipe_laporan)
                }


                if (formilSubmit.txLaporan.tipe_laporan == 'KNL') {
                    formilSubmit.txLaporan.status_petugas = 'ditugaskan';
                    if (formilSubmit.txLaporan.officer_by == null && formilSubmit.txLaporan.updated_by != null) {
                        formilSubmit.txLaporan.officer_by = formilSubmit.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != formilSubmit.txLaporan.status_laporan && formilSubmit.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: formilSubmit.txLaporan.status_laporan,
                    created_by: formilSubmit.txLaporan.updated_by != null ? formilSubmit.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: formilSubmit.txLaporan.kode_kantor != null ? formilSubmit.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: formilSubmit.txLaporan.officer_by != null ? formilSubmit.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })


            }

            await this.txLaporanRepository.updateById(newId, formilSubmit.txLaporan, {
                transaction: tx
            })

            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (formilSubmit.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = formilSubmit.txChangeStatus.id_tx_laporan;
            }
            if (formilSubmit.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = formilSubmit.txChangeStatus.no_agenda;
            }
            if (formilSubmit.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = formilSubmit.txChangeStatus.status_laporan_date;
            }
            if (formilSubmit.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = formilSubmit.txChangeStatus.status_laporan_old;
            }
            if (formilSubmit.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = formilSubmit.txChangeStatus.status_laporan_new;
            }
            if (formilSubmit.txChangeStatus.catatan != null) {
                existingData2.catatan = formilSubmit.txChangeStatus.catatan;
            }

            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })

            await tx.commit()
            // eslint-disable-next-line no-void
            void this.cekStatusLaporan(existingData.status_laporan, existingData.id, existingData.no_agenda, existingData.kode_kantor);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-formil-draft/{id}')
    @response(204, {
        description: 'tahapRegistrasi PATCH success',
    })
    async tahapFormilDraft(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(FormilDraft, {partial: true}),
                },
            },
        })
        formilDraft: FormilDraft,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(newId);
            if (formilDraft.txLaporan.provinsi_terlapor == undefined) {
                formilDraft.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (formilDraft.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (formilDraft.txLaporan.created_by !== undefined && formilDraft.txLaporan.created_by !== null) {
                    createdBy = formilDraft.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    formilDraft.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                formilDraft.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (formilDraft.txLaporan.tipe_laporan !== null && formilDraft.txLaporan.tipe_laporan !== undefined && formilDraft.txLaporan.tipe_laporan !== "") {
                    tpLaporan = formilDraft.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        formilDraft.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, formilDraft.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    formilDraft.txLaporan.token = this.generateToken(10, formilDraft.txLaporan.tipe_laporan)
                }


                if (formilDraft.txLaporan.tipe_laporan == 'KNL') {
                    formilDraft.txLaporan.status_petugas = 'ditugaskan';
                    if (formilDraft.txLaporan.officer_by == null && formilDraft.txLaporan.updated_by != null) {
                        formilDraft.txLaporan.officer_by = formilDraft.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != formilDraft.txLaporan.status_laporan && formilDraft.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: formilDraft.txLaporan.status_laporan,
                    created_by: formilDraft.txLaporan.updated_by != null ? formilDraft.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: formilDraft.txLaporan.kode_kantor != null ? formilDraft.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: formilDraft.txLaporan.officer_by != null ? formilDraft.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

                switch (existingData.status_laporan) {
                    case 'VFL':
                        // @ts-ignore
                        await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'VML':
                        // @ts-ignore
                        await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'KLL':
                        // @ts-ignore
                        await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PMP':
                        // @ts-ignore
                        await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'lhpd':
                        // @ts-ignore
                        await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PLRKS':
                        // @ts-ignore
                        await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    default:
                        break;
                }
            }

            await this.txLaporanRepository.updateById(newId, formilDraft.txLaporan, {
                transaction: tx
            })

            const {id, ...newFormil} = formilDraft.txVerifFormil;

            await this.txVerifFormilRepository.updateById(formilDraft.txVerifFormil.id, newFormil, {
                transaction: tx
            })


            tx.commit()

            // console.log(tahapRegistrasi);


            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/cabut-tutup-laporan/{id}')
    @response(204, {
        description: 'formilCabutLaporan PATCH success',
    })
    async formilCabutLaporan(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(FormilCabutLaporan, {partial: true}),
                },
            },
        })
        formilCabutLaporan: FormilCabutLaporan,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            //save data
            formilCabutLaporan.txCabutLaporan.id = uuidv4();
            const savedData = await this.txCabutLaporanRepository.create(formilCabutLaporan.txCabutLaporan, {
                transaction: tx
            })

            //update is_suspend di table tx_laporan
            if (savedData) {
                const txLaporan: any = {}
                txLaporan.id = formilCabutLaporan.txCabutLaporan.id_tx_laporan
                // txLaporan.is_suspended = "1"
                txLaporan.status_laporan = "LT"

                const updatedLaporan = await this.txLaporanRepository.updateById(savedData.id_tx_laporan, txLaporan, {
                    transaction: tx
                })

            }

            const existingData = await this.txLaporanRepository.findById(newId);

            if (formilCabutLaporan.txLaporan.provinsi_terlapor == undefined) {
                formilCabutLaporan.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (formilCabutLaporan.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (formilCabutLaporan.txLaporan.created_by !== undefined && formilCabutLaporan.txLaporan.created_by !== null) {
                    createdBy = formilCabutLaporan.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    formilCabutLaporan.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                formilCabutLaporan.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (formilCabutLaporan.txLaporan.tipe_laporan !== null && formilCabutLaporan.txLaporan.tipe_laporan !== undefined && formilCabutLaporan.txLaporan.tipe_laporan !== "") {
                    tpLaporan = formilCabutLaporan.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        formilCabutLaporan.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, formilCabutLaporan.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    formilCabutLaporan.txLaporan.token = this.generateToken(10, formilCabutLaporan.txLaporan.tipe_laporan)
                }


                if (formilCabutLaporan.txLaporan.tipe_laporan == 'KNL') {
                    formilCabutLaporan.txLaporan.status_petugas = 'ditugaskan';
                    if (formilCabutLaporan.txLaporan.officer_by == null && formilCabutLaporan.txLaporan.updated_by != null) {
                        formilCabutLaporan.txLaporan.officer_by = formilCabutLaporan.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != formilCabutLaporan.txLaporan.status_laporan && formilCabutLaporan.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: formilCabutLaporan.txLaporan.status_laporan,
                    created_by: formilCabutLaporan.txLaporan.updated_by != null ? formilCabutLaporan.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: formilCabutLaporan.txLaporan.kode_kantor != null ? formilCabutLaporan.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: formilCabutLaporan.txLaporan.officer_by != null ? formilCabutLaporan.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

            }

            await this.txLaporanRepository.updateById(newId, formilCabutLaporan.txLaporan, {
                transaction: tx
            })

            if (formilCabutLaporan.txChangeStatus != null) {
                const existingData2: TxChangeStatus = new TxChangeStatus();
                if (formilCabutLaporan.txChangeStatus.id_tx_laporan != null) {
                    existingData2.id_tx_laporan = formilCabutLaporan.txChangeStatus.id_tx_laporan;
                }
                if (formilCabutLaporan.txChangeStatus.no_agenda != null) {
                    existingData2.no_agenda = formilCabutLaporan.txChangeStatus.no_agenda;
                }
                if (formilCabutLaporan.txChangeStatus.status_laporan_date != null) {
                    existingData2.status_laporan_date = formilCabutLaporan.txChangeStatus.status_laporan_date;
                }
                if (formilCabutLaporan.txChangeStatus.status_laporan_old != null) {
                    existingData2.status_laporan_old = formilCabutLaporan.txChangeStatus.status_laporan_old;
                }
                if (formilCabutLaporan.txChangeStatus.status_laporan_new != null) {
                    existingData2.status_laporan_new = formilCabutLaporan.txChangeStatus.status_laporan_new;
                }
                if (formilCabutLaporan.txChangeStatus.catatan != null) {
                    existingData2.catatan = formilCabutLaporan.txChangeStatus.catatan;
                }

                await this.txChangeStatusRepository.updateAll(existingData2, where, {
                    transaction: tx
                })

            }


            await tx.commit()

            if (formilCabutLaporan.txLaporan.status_laporan == 'TMSM') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_materiil", existingData.id, 'TMSM', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (formilCabutLaporan.txLaporan.status_laporan == 'TMSF') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (formilCabutLaporan.txLaporan.status_laporan == 'LT' && (existingData.status_laporan == 'LT' || existingData.status_laporan == 'VFL' || existingData.status_laporan == 'VML' || existingData.status_laporan == 'KML' || existingData.status_laporan == 'PL')) {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            }

            // eslint-disable-next-line no-void
            void this.cekStatusLaporan(existingData.status_laporan, existingData.id, existingData.no_agenda, existingData.kode_kantor);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-materiil-submit/{id}')
    @response(204, {
        description: 'tahapFormilSubmit PATCH success',
    })
    async tahapMateriilSubmit(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(FormilSubmit, {partial: true}),
                },
            },
        })
        materiilSubmit: FormilSubmit,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(newId);
            if (materiilSubmit.txLaporan.provinsi_terlapor == undefined) {
                materiilSubmit.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (materiilSubmit.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (materiilSubmit.txLaporan.created_by !== undefined && materiilSubmit.txLaporan.created_by !== null) {
                    createdBy = materiilSubmit.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    materiilSubmit.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                materiilSubmit.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (materiilSubmit.txLaporan.tipe_laporan !== null && materiilSubmit.txLaporan.tipe_laporan !== undefined && materiilSubmit.txLaporan.tipe_laporan !== "") {
                    tpLaporan = materiilSubmit.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        materiilSubmit.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, materiilSubmit.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    materiilSubmit.txLaporan.token = this.generateToken(10, materiilSubmit.txLaporan.tipe_laporan)
                }


                if (materiilSubmit.txLaporan.tipe_laporan == 'KNL') {
                    materiilSubmit.txLaporan.status_petugas = 'ditugaskan';
                    if (materiilSubmit.txLaporan.officer_by == null && materiilSubmit.txLaporan.updated_by != null) {
                        materiilSubmit.txLaporan.officer_by = materiilSubmit.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != materiilSubmit.txLaporan.status_laporan && materiilSubmit.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: materiilSubmit.txLaporan.status_laporan,
                    created_by: materiilSubmit.txLaporan.updated_by != null ? materiilSubmit.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: materiilSubmit.txLaporan.kode_kantor != null ? materiilSubmit.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: materiilSubmit.txLaporan.officer_by != null ? materiilSubmit.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

            }

            await this.txLaporanRepository.updateById(newId, materiilSubmit.txLaporan, {
                transaction: tx
            })

            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (materiilSubmit.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = materiilSubmit.txChangeStatus.id_tx_laporan;
            }
            if (materiilSubmit.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = materiilSubmit.txChangeStatus.no_agenda;
            }
            if (materiilSubmit.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = materiilSubmit.txChangeStatus.status_laporan_date;
            }
            if (materiilSubmit.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = materiilSubmit.txChangeStatus.status_laporan_old;
            }
            if (materiilSubmit.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = materiilSubmit.txChangeStatus.status_laporan_new;
            }
            if (materiilSubmit.txChangeStatus.catatan != null) {
                existingData2.catatan = materiilSubmit.txChangeStatus.catatan;
            }

            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })

            const {id, ...newMateriil} = materiilSubmit.txVerifMateriil;

            await this.txVerifMateriilRepository.updateById(materiilSubmit.txVerifMateriil.id, newMateriil, {
                transaction: tx
            })


            await tx.commit()

            // eslint-disable-next-line no-void
            void this.cekStatusLaporan(existingData.status_laporan, existingData.id, existingData.no_agenda, existingData.kode_kantor);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-materiil-draft/{id}')
    @response(204, {
        description: 'tahapMateriilDraft PATCH success',
    })
    async tahapMateriilDraft(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(FormilSubmit, {partial: true}),
                },
            },
        })
        materiilDraft: FormilSubmit,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(newId);
            if (materiilDraft.txLaporan.provinsi_terlapor == undefined) {
                materiilDraft.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (materiilDraft.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (materiilDraft.txLaporan.created_by !== undefined && materiilDraft.txLaporan.created_by !== null) {
                    createdBy = materiilDraft.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    materiilDraft.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                materiilDraft.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (materiilDraft.txLaporan.tipe_laporan !== null && materiilDraft.txLaporan.tipe_laporan !== undefined && materiilDraft.txLaporan.tipe_laporan !== "") {
                    tpLaporan = materiilDraft.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        materiilDraft.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, materiilDraft.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    materiilDraft.txLaporan.token = this.generateToken(10, materiilDraft.txLaporan.tipe_laporan)
                }


                if (materiilDraft.txLaporan.tipe_laporan == 'KNL') {
                    materiilDraft.txLaporan.status_petugas = 'ditugaskan';
                    if (materiilDraft.txLaporan.officer_by == null && materiilDraft.txLaporan.updated_by != null) {
                        materiilDraft.txLaporan.officer_by = materiilDraft.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != materiilDraft.txLaporan.status_laporan && materiilDraft.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: materiilDraft.txLaporan.status_laporan,
                    created_by: materiilDraft.txLaporan.updated_by != null ? materiilDraft.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: materiilDraft.txLaporan.kode_kantor != null ? materiilDraft.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: materiilDraft.txLaporan.officer_by != null ? materiilDraft.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

                switch (existingData.status_laporan) {
                    case 'VFL':
                        // @ts-ignore
                        await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'VML':
                        // @ts-ignore
                        await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'KLL':
                        // @ts-ignore
                        await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PMP':
                        // @ts-ignore
                        await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'lhpd':
                        // @ts-ignore
                        await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PLRKS':
                        // @ts-ignore
                        await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    default:
                        break;
                }
            }

            await this.txLaporanRepository.updateById(newId, materiilDraft.txLaporan, {
                transaction: tx
            })


            const newFormil = _.omit(materiilDraft.txVerifFormil, ['id']);

            await this.txVerifFormilRepository.updateById(materiilDraft.txVerifFormil.id, newFormil, {
                transaction: tx
            })

            const newMateriil = _.omit(materiilDraft.txVerifMateriil, ['id']);

            await this.txVerifMateriilRepository.updateById(materiilDraft.txVerifMateriil.id, newMateriil, {
                transaction: tx
            })


            tx.commit()

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }


    @patch('/tx-laporan/tahap-klasifikasi-submit/{id}')
    @response(204, {
        description: 'tahapKlasifikasiSubmit PATCH success',
    })
    async tahapKlasifikasiSubmit(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TahapKlasifikasi, {partial: true}),
                },
            },
        })
        klasifikasiSubmit: TahapKlasifikasi,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {
            //patch tx-laporan
            if (klasifikasiSubmit.txLaporan.tipe_laporan == 'RCO' && klasifikasiSubmit.txDisposisiPvlKapten != null) {
                klasifikasiSubmit.txLaporan.kode_kantor = klasifikasiSubmit.txDisposisiPvlKapten.kantor_pemeriksaan;
            }
            const existingData = await this.txLaporanRepository.findById(newId);
            if (klasifikasiSubmit.txLaporan.provinsi_terlapor == undefined) {
                klasifikasiSubmit.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (klasifikasiSubmit.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (klasifikasiSubmit.txLaporan.created_by !== undefined && klasifikasiSubmit.txLaporan.created_by !== null) {
                    createdBy = klasifikasiSubmit.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    klasifikasiSubmit.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                klasifikasiSubmit.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (klasifikasiSubmit.txLaporan.tipe_laporan !== null && klasifikasiSubmit.txLaporan.tipe_laporan !== undefined && klasifikasiSubmit.txLaporan.tipe_laporan !== "") {
                    tpLaporan = klasifikasiSubmit.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        klasifikasiSubmit.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, klasifikasiSubmit.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    klasifikasiSubmit.txLaporan.token = this.generateToken(10, klasifikasiSubmit.txLaporan.tipe_laporan)
                }


                if (klasifikasiSubmit.txLaporan.tipe_laporan == 'KNL') {
                    klasifikasiSubmit.txLaporan.status_petugas = 'ditugaskan';
                    if (klasifikasiSubmit.txLaporan.officer_by == null && klasifikasiSubmit.txLaporan.updated_by != null) {
                        klasifikasiSubmit.txLaporan.officer_by = klasifikasiSubmit.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != klasifikasiSubmit.txLaporan.status_laporan && klasifikasiSubmit.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: klasifikasiSubmit.txLaporan.status_laporan,
                    created_by: klasifikasiSubmit.txLaporan.updated_by != null ? klasifikasiSubmit.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: klasifikasiSubmit.txLaporan.kode_kantor != null ? klasifikasiSubmit.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: klasifikasiSubmit.txLaporan.officer_by != null ? klasifikasiSubmit.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

            }

            await this.txLaporanRepository.updateById(newId, klasifikasiSubmit.txLaporan, {
                transaction: tx
            })

            //patch change status
            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (klasifikasiSubmit.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = klasifikasiSubmit.txChangeStatus.id_tx_laporan;
            }
            if (klasifikasiSubmit.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = klasifikasiSubmit.txChangeStatus.no_agenda;
            }
            if (klasifikasiSubmit.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = klasifikasiSubmit.txChangeStatus.status_laporan_date;
            }
            if (klasifikasiSubmit.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = klasifikasiSubmit.txChangeStatus.status_laporan_old;
            }
            if (klasifikasiSubmit.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = klasifikasiSubmit.txChangeStatus.status_laporan_new;
            }
            if (klasifikasiSubmit.txChangeStatus.catatan != null) {
                existingData2.catatan = klasifikasiSubmit.txChangeStatus.catatan;
            }

            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })

            //patch or post klasifikasi lm
            if (klasifikasiSubmit.txKlasifikasiLm.id != "") {
                const currentData = await this.txKlasifikasiLmRepository.findById(klasifikasiSubmit.txKlasifikasiLm.id);
                const dataPost = klasifikasiSubmit.txKlasifikasiLm

                const jml_terlapor_terkait = dataPost.hasOwnProperty('jml_terlapor_terkait') ? dataPost.jml_terlapor_terkait : currentData.jml_terlapor_terkait;
                const lokasi_terlapor = dataPost.hasOwnProperty('lokasi_terlapor') ? dataPost.lokasi_terlapor : currentData.lokasi_terlapor;
                const penerima_manfaat = dataPost.hasOwnProperty('penerima_manfaat') ? dataPost.penerima_manfaat : currentData.penerima_manfaat;
                const permasalahan_dilaporkan = dataPost.hasOwnProperty('permasalahan_dilaporkan') ? dataPost.permasalahan_dilaporkan : currentData.permasalahan_dilaporkan;

                dataPost.total_skor = await this.pvlService.hitungSkor(jml_terlapor_terkait, lokasi_terlapor, penerima_manfaat, permasalahan_dilaporkan)

                await this.txKlasifikasiLmRepository.updateById(klasifikasiSubmit.txKlasifikasiLm.id, dataPost, {
                    transaction: tx
                })

            } else {
                klasifikasiSubmit.txKlasifikasiLm.id = uuidv4();
                const dataPost = klasifikasiSubmit.txKlasifikasiLm

                dataPost.total_skor = await this.pvlService.hitungSkor(dataPost.jml_terlapor_terkait, dataPost.lokasi_terlapor, dataPost.penerima_manfaat, dataPost.permasalahan_dilaporkan)

                await this.txKlasifikasiLmRepository.create(dataPost, {
                    transaction: tx
                })
            }

            //post or patch txPleno
            if (klasifikasiSubmit.txLaporan.tipe_laporan != 'RCO') {
                if (klasifikasiSubmit.txPleno.id === "" || klasifikasiSubmit.txPleno.id == null) {
                    klasifikasiSubmit.txPleno.id = uuidv4();
                    const savedData = this.txPlenoRepository.create(klasifikasiSubmit.txPleno, {
                        transaction: tx
                    })
                } else {
                    await this.txPlenoRepository.updateById(klasifikasiSubmit.txPleno.id, klasifikasiSubmit.txPleno, {
                        transaction: tx
                    })
                }
            }


            //post disposisi pvl kapten if kantor pemeriksaan JKT
            if (klasifikasiSubmit.txDisposisiPvlKapten.kantor_pemeriksaan === "JKT") {
                klasifikasiSubmit.txDisposisiPvlKapten.id = uuidv4();
                const createKapten: any = this.txDisposisiPvlKaptenRepository.create(klasifikasiSubmit.txDisposisiPvlKapten, {
                    transaction: tx
                })

                await this.txLaporanRepository.updateById(
                    klasifikasiSubmit.txDisposisiPvlKapten.id_tx_laporan,
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    {is_disposisi_kapten: klasifikasiSubmit.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                    transaction: tx
                })
            }

            // console.log(klasifikasiSubmit);


            await tx.commit()

            // eslint-disable-next-line no-void
            void this.cekStatusLaporan(existingData.status_laporan, existingData.id, existingData.no_agenda, existingData.kode_kantor);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-klasifikasi-draft/{id}')
    @response(204, {
        description: 'tahapKlasifikasiDraft PATCH success',
    })
    async tahapKlasifikasiDraft(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TahapKlasifikasi, {partial: true}),
                },
            },
        })
        klasifikasiDraft: TahapKlasifikasi,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            const existingData = await this.txLaporanRepository.findById(newId);
            if (klasifikasiDraft.txLaporan.provinsi_terlapor == undefined) {
                klasifikasiDraft.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (klasifikasiDraft.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (klasifikasiDraft.txLaporan.created_by !== undefined && klasifikasiDraft.txLaporan.created_by !== null) {
                    createdBy = klasifikasiDraft.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    klasifikasiDraft.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                klasifikasiDraft.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (klasifikasiDraft.txLaporan.tipe_laporan !== null && klasifikasiDraft.txLaporan.tipe_laporan !== undefined && klasifikasiDraft.txLaporan.tipe_laporan !== "") {
                    tpLaporan = klasifikasiDraft.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        klasifikasiDraft.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, klasifikasiDraft.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    klasifikasiDraft.txLaporan.token = this.generateToken(10, klasifikasiDraft.txLaporan.tipe_laporan)
                }


                if (klasifikasiDraft.txLaporan.tipe_laporan == 'KNL') {
                    klasifikasiDraft.txLaporan.status_petugas = 'ditugaskan';
                    if (klasifikasiDraft.txLaporan.officer_by == null && klasifikasiDraft.txLaporan.updated_by != null) {
                        klasifikasiDraft.txLaporan.officer_by = klasifikasiDraft.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != klasifikasiDraft.txLaporan.status_laporan && klasifikasiDraft.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: klasifikasiDraft.txLaporan.status_laporan,
                    created_by: klasifikasiDraft.txLaporan.updated_by != null ? klasifikasiDraft.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: klasifikasiDraft.txLaporan.kode_kantor != null ? klasifikasiDraft.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: klasifikasiDraft.txLaporan.officer_by != null ? klasifikasiDraft.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

                switch (existingData.status_laporan) {
                    case 'VFL':
                        // @ts-ignore
                        await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'VML':
                        // @ts-ignore
                        await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'KLL':
                        // @ts-ignore
                        await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PMP':
                        // @ts-ignore
                        await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'lhpd':
                        // @ts-ignore
                        await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PLRKS':
                        // @ts-ignore
                        await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    default:
                        break;
                }
            }

            await this.txLaporanRepository.updateById(newId, klasifikasiDraft.txLaporan, {
                transaction: tx
            })

            const newFormil = _.omit(klasifikasiDraft.txVerifFormil, ['id']);

            await this.txVerifFormilRepository.updateById(klasifikasiDraft.txVerifFormil.id, newFormil, {
                transaction: tx
            })

            const newMateriil = _.omit(klasifikasiDraft.txVerifMateriil, ['id']);

            await this.txVerifMateriilRepository.updateById(klasifikasiDraft.txVerifMateriil.id, newMateriil, {
                transaction: tx
            })


            //post klasifikasi lm
            klasifikasiDraft.txKlasifikasiLm.id = uuidv4();
            const dataPost = klasifikasiDraft.txKlasifikasiLm

            dataPost.total_skor = await this.pvlService.hitungSkor(dataPost.jml_terlapor_terkait, dataPost.lokasi_terlapor, dataPost.penerima_manfaat, dataPost.permasalahan_dilaporkan)

            await this.txKlasifikasiLmRepository.create(dataPost, {
                transaction: tx
            })


            tx.commit()

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }


    @patch('/tx-laporan/tahap-pleno-submit/{id}')
    @response(204, {
        description: 'tahapPlenoSubmit POST success',
    })
    async tahapPlenoSubmit(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TahapPleno, {partial: true}),
                },
            },
        })
        plenoSubmit: TahapPleno,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            // patch materill
            const newMateriil = _.omit(plenoSubmit.txVerifMateriil, ['id']);

            await this.txVerifMateriilRepository.updateById(plenoSubmit.txVerifMateriil.id, newMateriil, {
                transaction: tx
            })

            // patch change status
            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (plenoSubmit.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = plenoSubmit.txChangeStatus.id_tx_laporan;
            }
            if (plenoSubmit.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = plenoSubmit.txChangeStatus.no_agenda;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = plenoSubmit.txChangeStatus.status_laporan_date;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = plenoSubmit.txChangeStatus.status_laporan_old;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = plenoSubmit.txChangeStatus.status_laporan_new;
            }
            if (plenoSubmit.txChangeStatus.catatan != null) {
                existingData2.catatan = plenoSubmit.txChangeStatus.catatan;
            }
            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })


            //post or patch txPleno
            if (plenoSubmit.txPleno.id === "" || plenoSubmit.txPleno.id == null) {
                plenoSubmit.txPleno.id = uuidv4();
                const savedData = this.txPlenoRepository.create(plenoSubmit.txPleno, {
                    transaction: tx
                })
            } else {
                await this.txPlenoRepository.updateById(plenoSubmit.txPleno.id, plenoSubmit.txPleno, {
                    transaction: tx
                })
            }

            if (plenoSubmit.txPleno.menolak_laporan) {
                // laporan di tolak
                //save data
                plenoSubmit.txCabutLaporan.id = uuidv4();
                const savedData = await this.txCabutLaporanRepository.create(plenoSubmit.txCabutLaporan, {
                    transaction: tx
                })

                //update is_suspend di table tx_laporan
                if (savedData) {
                    const txLaporan: any = {}
                    txLaporan.id = plenoSubmit.txCabutLaporan.id_tx_laporan
                    // txLaporan.is_suspended = "1"
                    txLaporan.status_laporan = "LT"

                    const updatedLaporan = await this.txLaporanRepository.updateById(savedData.id_tx_laporan, txLaporan, {
                        transaction: tx
                    })

                }

            } else {
                if (plenoSubmit.txDisposisiPvlKapten.id === "" || plenoSubmit.txDisposisiPvlKapten.id == null) {
                    plenoSubmit.txDisposisiPvlKapten.id = uuidv4();
                    const createKapten: any = this.txDisposisiPvlKaptenRepository.create(plenoSubmit.txDisposisiPvlKapten, {
                        transaction: tx
                    })

                    await this.txLaporanRepository.updateById(
                        plenoSubmit.txDisposisiPvlKapten.id_tx_laporan,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        {is_disposisi_kapten: plenoSubmit.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                        transaction: tx
                    })
                } else {
                    const findKapten: any = await this.txDisposisiPvlKaptenRepository.findById(
                        plenoSubmit.txDisposisiPvlKapten.id,
                    );

                    await this.txDisposisiPvlKaptenRepository.updateById(
                        plenoSubmit.txDisposisiPvlKapten.id,
                        plenoSubmit.txDisposisiPvlKapten, {
                        transaction: tx
                    })

                    if (findKapten) {
                        await this.txLaporanRepository.updateById(
                            findKapten.id_tx_laporan,
                            // eslint-disable-next-line @typescript-eslint/naming-convention
                            {is_disposisi_kapten: plenoSubmit.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                            transaction: tx
                        })
                    }
                }
            }

            // patch tx-laporan
            const existingData = await this.txLaporanRepository.findById(newId);
            if (plenoSubmit.txLaporan.provinsi_terlapor == undefined) {
                plenoSubmit.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (plenoSubmit.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (plenoSubmit.txLaporan.created_by !== undefined && plenoSubmit.txLaporan.created_by !== null) {
                    createdBy = plenoSubmit.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    plenoSubmit.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                plenoSubmit.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (plenoSubmit.txLaporan.tipe_laporan !== null && plenoSubmit.txLaporan.tipe_laporan !== undefined && plenoSubmit.txLaporan.tipe_laporan !== "") {
                    tpLaporan = plenoSubmit.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        plenoSubmit.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, plenoSubmit.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    plenoSubmit.txLaporan.token = this.generateToken(10, plenoSubmit.txLaporan.tipe_laporan)
                }


                if (plenoSubmit.txLaporan.tipe_laporan == 'KNL') {
                    plenoSubmit.txLaporan.status_petugas = 'ditugaskan';
                    if (plenoSubmit.txLaporan.officer_by == null && plenoSubmit.txLaporan.updated_by != null) {
                        plenoSubmit.txLaporan.officer_by = plenoSubmit.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != plenoSubmit.txLaporan.status_laporan && plenoSubmit.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: plenoSubmit.txLaporan.status_laporan,
                    created_by: plenoSubmit.txLaporan.updated_by != null ? plenoSubmit.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: plenoSubmit.txLaporan.kode_kantor != null ? plenoSubmit.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: plenoSubmit.txLaporan.officer_by != null ? plenoSubmit.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

                switch (existingData.status_laporan) {
                    case 'VFL':
                        // @ts-ignore
                        await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'VML':
                        // @ts-ignore
                        await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'KLL':
                        // @ts-ignore
                        await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PMP':
                        // @ts-ignore
                        await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'lhpd':
                        // @ts-ignore
                        await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PLRKS':
                        // @ts-ignore
                        await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    default:
                        break;
                }
            }

            if (plenoSubmit.txLaporan.status_laporan == 'TMSM') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_materiil", existingData.id, 'TMSM', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (plenoSubmit.txLaporan.status_laporan == 'TMSF') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (plenoSubmit.txLaporan.status_laporan == 'LT' && (existingData.status_laporan == 'LT' || existingData.status_laporan == 'VFL' || existingData.status_laporan == 'VML' || existingData.status_laporan == 'KML' || existingData.status_laporan == 'PL')) {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            }

            await this.txLaporanRepository.updateById(newId, plenoSubmit.txLaporan, {
                transaction: tx
            })

            tx.commit()

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

    @patch('/tx-laporan/tahap-disposisi-submit/{id}')
    @response(204, {
        description: 'tahapDisposisiSubmit patch success',
    })
    async tahapDisposisiSubmit(
        @param.path.string('id') newId: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TahapDisposisi, {partial: true}),
                },
            },
        })
        plenoSubmit: TahapDisposisi,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>
    ): Promise<{data: any; message: string; status: boolean}> {
        const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
        try {

            // patch tx-laporan
            const existingData = await this.txLaporanRepository.findById(newId);
            if (plenoSubmit.txLaporan.provinsi_terlapor == undefined) {
                plenoSubmit.txLaporan.provinsi_terlapor = existingData.provinsi_terlapor;
            }

            if (plenoSubmit.txLaporan.status_laporan !== "DRF") {

                let kodeKantor = ""
                let createdBy = ""
                if (plenoSubmit.txLaporan.created_by !== undefined && plenoSubmit.txLaporan.created_by !== null) {
                    createdBy = plenoSubmit.txLaporan.created_by;
                } else if (existingData.created_by !== undefined && existingData.created_by !== null) {
                    createdBy = existingData.created_by;
                }

                if (createdBy !== undefined && createdBy !== null && createdBy !== "") {
                    await this.tdUserRepository.findById(createdBy).then((value: TdUser) => {
                        if (value !== null && value !== undefined && value.kodeKantor !== null && value.kodeKantor !== undefined) {
                            kodeKantor = value.kodeKantor;
                        }
                    });
                }
                // create no-agenda
                if (existingData.no_agenda == null) {
                    plenoSubmit.txLaporan.no_agenda = await this.generateNomorAgenda()
                }
                plenoSubmit.txLaporan.created_date = existingData.created_date;

                if (existingData.kode_kantor) {
                    kodeKantor = existingData.kode_kantor
                }

                let tpLaporan = ""
                if (plenoSubmit.txLaporan.tipe_laporan !== null && plenoSubmit.txLaporan.tipe_laporan !== undefined && plenoSubmit.txLaporan.tipe_laporan !== "") {
                    tpLaporan = plenoSubmit.txLaporan.tipe_laporan;
                } else if (existingData.tipe_laporan !== null && existingData.tipe_laporan !== undefined && existingData.tipe_laporan !== "") {
                    tpLaporan = existingData.tipe_laporan;
                }

                if (tpLaporan !== "KNL" && tpLaporan !== "Tembusan") {
                    if (kodeKantor != undefined && kodeKantor !== null && (existingData.no_arsip === null || existingData.no_arsip === "")) {
                        plenoSubmit.txLaporan.no_arsip = await this.generateNomorArsip(kodeKantor, existingData.provinsi_terlapor, plenoSubmit.txLaporan.tipe_laporan, existingData.cara_penyampaian, existingData.klasifikasi_instansi_terlapor)
                    }
                }

                if (existingData.token == null || existingData.token == "-") {
                    plenoSubmit.txLaporan.token = this.generateToken(10, plenoSubmit.txLaporan.tipe_laporan)
                }


                if (plenoSubmit.txLaporan.tipe_laporan == 'KNL') {
                    plenoSubmit.txLaporan.status_petugas = 'ditugaskan';
                    if (plenoSubmit.txLaporan.officer_by == null && plenoSubmit.txLaporan.updated_by != null) {
                        plenoSubmit.txLaporan.officer_by = plenoSubmit.txLaporan.updated_by;
                    }
                }
            }

            if (existingData.status_laporan != plenoSubmit.txLaporan.status_laporan && plenoSubmit.txLaporan.status_laporan != null) {
                const hsl: TxHistoryStatusLaporan = new TxHistoryStatusLaporan({
                    id_tx_laporan: existingData.id,
                    no_agenda: existingData.no_agenda,
                    prev_status: existingData.status_laporan,
                    next_status: plenoSubmit.txLaporan.status_laporan,
                    created_by: plenoSubmit.txLaporan.updated_by != null ? plenoSubmit.txLaporan.updated_by : existingData.officer_by,
                    prev_kantor: existingData.kode_kantor,
                    next_kantor: plenoSubmit.txLaporan.kode_kantor != null ? plenoSubmit.txLaporan.kode_kantor : existingData.kode_kantor,
                    prev_assignee: existingData.officer_by,
                    next_assignee: plenoSubmit.txLaporan.officer_by != null ? plenoSubmit.txLaporan.officer_by : existingData.officer_by
                });

                await this.txdHistoryStatusLaporanRepository.create(hsl, {
                    transaction: tx
                })

                switch (existingData.status_laporan) {
                    case 'VFL':
                        // @ts-ignore
                        await this.getFileFromReport("form_formil", existingData.id, 'VFL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'VML':
                        // @ts-ignore
                        await this.getFileFromReport("form_ringkasan_materiil", existingData.id, 'VML', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'KLL':
                        // @ts-ignore
                        await this.getFileFromReport("klasifikasi_lm", existingData.id, 'KLL', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PMP':
                        // @ts-ignore
                        await this.getFileFromReport("pemberitahuan_dimulai_pemeriksaan", existingData.id, 'PMP', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'lhpd':
                        // @ts-ignore
                        await this.getFileFromReport("lhpd", existingData.id, 'lhpd', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    case 'PLRKS':
                        // @ts-ignore
                        await this.getFileFromReport("ba_penutupan_laporan", existingData.id, 'PLRKS', existingData.no_agenda, existingData.kode_kantor)
                        break;
                    default:
                        break;
                }
            }

            if (plenoSubmit.txLaporan.status_laporan == 'TMSM') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_materiil", existingData.id, 'TMSM', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (plenoSubmit.txLaporan.status_laporan == 'TMSF') {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            } else if (plenoSubmit.txLaporan.status_laporan == 'LT' && (existingData.status_laporan == 'LT' || existingData.status_laporan == 'VFL' || existingData.status_laporan == 'VML' || existingData.status_laporan == 'KML' || existingData.status_laporan == 'PL')) {
                if (existingData.no_agenda != null && existingData.kode_kantor != null) {
                    await this.getFileFromReport("bapl_tidak_memenuhi_syarat_formil", existingData.id, 'TMSF', existingData.no_agenda, existingData.kode_kantor)
                }
            }

            await this.txLaporanRepository.updateById(newId, plenoSubmit.txLaporan, {
                transaction: tx
            })

            // patch change status
            const existingData2: TxChangeStatus = new TxChangeStatus();
            if (plenoSubmit.txChangeStatus.id_tx_laporan != null) {
                existingData2.id_tx_laporan = plenoSubmit.txChangeStatus.id_tx_laporan;
            }
            if (plenoSubmit.txChangeStatus.no_agenda != null) {
                existingData2.no_agenda = plenoSubmit.txChangeStatus.no_agenda;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_date != null) {
                existingData2.status_laporan_date = plenoSubmit.txChangeStatus.status_laporan_date;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_old != null) {
                existingData2.status_laporan_old = plenoSubmit.txChangeStatus.status_laporan_old;
            }
            if (plenoSubmit.txChangeStatus.status_laporan_new != null) {
                existingData2.status_laporan_new = plenoSubmit.txChangeStatus.status_laporan_new;
            }
            if (plenoSubmit.txChangeStatus.catatan != null) {
                existingData2.catatan = plenoSubmit.txChangeStatus.catatan;
            }
            await this.txChangeStatusRepository.updateAll(existingData2, where, {
                transaction: tx
            })


            // patch or post kapten
            if (plenoSubmit.txDisposisiPvlKapten != null) {
                if (plenoSubmit.txDisposisiPvlKapten.id === "" || plenoSubmit.txDisposisiPvlKapten.id == null) {
                    plenoSubmit.txDisposisiPvlKapten.id = uuidv4();
                    const createKapten: any = this.txDisposisiPvlKaptenRepository.create(plenoSubmit.txDisposisiPvlKapten, {
                        transaction: tx
                    })

                    await this.txLaporanRepository.updateById(
                        plenoSubmit.txDisposisiPvlKapten.id_tx_laporan,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        {is_disposisi_kapten: plenoSubmit.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                        transaction: tx
                    })
                } else {
                    const findKapten: any = await this.txDisposisiPvlKaptenRepository.findById(
                        plenoSubmit.txDisposisiPvlKapten.id,
                    );

                    await this.txDisposisiPvlKaptenRepository.updateById(
                        plenoSubmit.txDisposisiPvlKapten.id,
                        plenoSubmit.txDisposisiPvlKapten, {
                        transaction: tx
                    })

                    if (findKapten) {
                        await this.txLaporanRepository.updateById(
                            findKapten.id_tx_laporan,
                            // eslint-disable-next-line @typescript-eslint/naming-convention
                            {is_disposisi_kapten: plenoSubmit.txDisposisiPvlKapten?.tim_pemeriksaan ?? ''}, {
                            transaction: tx
                        })
                    }
                }
            }

            // patch or post kaper
            if (plenoSubmit.txDisposisiPvlKaper != null) {
                if (plenoSubmit.txDisposisiPvlKaper.id === "" || plenoSubmit.txDisposisiPvlKaper.id == null) {
                    plenoSubmit.txDisposisiPvlKaper.id = uuidv4();
                    const createKaper: any = this.txDisposisiPvlKaperRepository.create(plenoSubmit.txDisposisiPvlKaper, {
                        transaction: tx
                    })

                } else {
                    const findKapten: any = await this.txDisposisiPvlKaperRepository.updateById(plenoSubmit.txDisposisiPvlKaper.id, plenoSubmit.txDisposisiPvlKaper, {
                        transaction: tx
                    })
                }
            }

            tx.commit()

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLaporanRepository.findById(newId)
            };

        } catch (e) {
            tx.rollback()

            throw new HttpErrors.InternalServerError("Gagal submit data")
        }
    }

}
