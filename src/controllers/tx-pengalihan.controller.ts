import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxPengalihan} from '../models';
import {TxLaporanRepository, TxPengalihanRepository} from '../repositories';

export class TxPengalihanController {
    constructor(
        @repository(TxPengalihanRepository)
        public txPengalihanRepository: TxPengalihanRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository
    ) {
    }

    @post('/tx-pengalihan')
    @response(200, {
        description: 'TxPengalihan model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxPengalihan)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxPengalihan, {
                        title: 'NewTxPengalihan',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txPengalihan: Omit<TxPengalihan, 'id'>,
    ): Promise<{ data: TxPengalihan; message: string; status: boolean }> {

        //save data
        let savedData = await this.txPengalihanRepository.create(txPengalihan);

        //update is_suspend di table tx_laporan
        if (savedData && txPengalihan.id_tx_laporan != undefined) {
            var txLaporan: any = {}
            txLaporan.id = txPengalihan.id_tx_laporan
            txLaporan.status_pengalihan = true

            let updatedLaporan = await this.txLaporanRepository.updateById(txPengalihan.id_tx_laporan, txLaporan);

        }

        let rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedData

        }

        return rs;
    }

    @get('/tx-pengalihan/count')
    @response(200, {
        description: 'TxPengalihan model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxPengalihan) where?: Where<TxPengalihan>,
    ): Promise<Count> {
        return this.txPengalihanRepository.count(where);
    }

    @get('/tx-pengalihan')
    @response(200, {
        description: 'Array of TxPengalihan model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxPengalihan, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxPengalihan) filter?: Filter<TxPengalihan>,
    ): Promise<{ data: (TxPengalihan)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txPengalihanRepository.find(filter)
        };
    }

    @patch('/tx-pengalihan')
    @response(200, {
        description: 'TxPengalihan PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxPengalihan, {partial: true}),
                },
            },
        })
            txPengalihan: TxPengalihan,
        @param.where(TxPengalihan) where?: Where<TxPengalihan>,
    ): Promise<Count> {
        return this.txPengalihanRepository.updateAll(txPengalihan, where);
    }

    @get('/tx-pengalihan/{id}')
    @response(200, {
        description: 'TxPengalihan model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxPengalihan, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxPengalihan, {exclude: 'where'}) filter?: FilterExcludingWhere<TxPengalihan>
    ): Promise<{ data: TxPengalihan; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txPengalihanRepository.findById(id, filter)
        };
    }

    @patch('/tx-pengalihan/{id}')
    @response(204, {
        description: 'TxPengalihan PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxPengalihan, {partial: true}),
                },
            },
        })
            txPengalihan: TxPengalihan,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txPengalihanRepository.updateById(id, txPengalihan);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txPengalihanRepository.findById(id)
        };
    }

    @put('/tx-pengalihan/{id}')
    @response(204, {
        description: 'TxPengalihan PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txPengalihan: TxPengalihan,
    ): Promise<void> {
        await this.txPengalihanRepository.replaceById(id, txPengalihan);
    }

    @del('/tx-pengalihan/{id}')
    @response(204, {
        description: 'TxPengalihan DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txPengalihanRepository.deleteById(id);
    }
}
