import {inject} from "@loopback/core";
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  IsolationLevel,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, HttpErrors, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {DbDataSource} from '../datasources';
import {TxPleno} from '../models';
import {TxPlenoRepository} from '../repositories';
import {PvlService} from "../services";

export class TxPlenoController {
  constructor(
    @repository(TxPlenoRepository)
    public txPlenoRepository: TxPlenoRepository,
    @inject('services.PvlService')
    public pvlService: PvlService,
    @inject('datasources.db') private dataSource: DbDataSource
  ) { }

  @post('/tx-pleno')
  @response(200, {
    description: 'TxPleno model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxPleno)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxPleno, {
            title: 'NewTxPleno',
            exclude: ['id'],
          }),
        },
      },
    })
    txPleno: Omit<TxPleno, 'id'>,
  ): Promise<TxPleno> {
    const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
      try{
          const hasil = await this.txPlenoRepository.create(txPleno, {
            transaction: tx
        })

          tx.commit()

          return hasil;

        }catch (e){
          tx.rollback()

          throw new HttpErrors.InternalServerError("Gagal submit data")
      }
  }

  @get('/tx-pleno/count')
  @response(200, {
    description: 'TxPleno model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxPleno) where?: Where<TxPleno>,
  ): Promise<Count> {
    return this.txPlenoRepository.count(where);
  }

  @get('/tx-pleno')
  @response(200, {
    description: 'Array of TxPleno model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxPleno, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxPleno) filter?: Filter<TxPleno>,
  ): Promise<object> {

    const listUraian = ['proses_laporan', 'tim_pemeriksaan']
    const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txPlenoRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  @get('/tx-pleno/{id}')
  @response(200, {
    description: 'TxPleno model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxPleno, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxPleno, {exclude: 'where'}) filter?: FilterExcludingWhere<TxPleno>
  ): Promise<object> {

    const listUraian = ['proses_laporan', 'tim_pemeriksaan']
    const resultData = await this.pvlService.getMLookUpFindById(id, filter, listUraian, this.txPlenoRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }

  @patch('/tx-pleno/{id}')
  @response(204, {
    description: 'TxPleno PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxPleno, {partial: true}),
        },
      },
    })
    txPleno: TxPleno,
  ): Promise<{data: any; message: string; status: boolean}> {
    const tx = await this.dataSource.beginTransaction(IsolationLevel.READ_COMMITTED)
      try{
          await this.txPlenoRepository.updateById(id, txPleno, {
            transaction: tx
        })

          tx.commit()

          return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txPlenoRepository.findById(id)
          };
        }catch (e){
          tx.rollback()

          throw new HttpErrors.InternalServerError("Gagal submit data")
      }
  }

  @del('/tx-pleno/{id}')
  @response(204, {
    description: 'TxPleno DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txPlenoRepository.deleteById(id);
  }

  // @patch('/tx-pleno')
  // @response(200, {
  //   description: 'TxPleno PATCH success count',
  //   content: {'application/json': {schema: CountSchema}},
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(TxPleno, {partial: true}),
  //       },
  //     },
  //   })
  //   txPleno: TxPleno,
  //   @param.where(TxPleno) where?: Where<TxPleno>,
  // ): Promise<Count> {
  //   return this.txPlenoRepository.updateAll(txPleno, where);
  // }

  // @put('/tx-pleno/{id}')
  // @response(204, {
  //   description: 'TxPleno PUT success',
  // })
  // async replaceById(
  //   @param.path.string('id') id: string,
  //   @requestBody() txPleno: TxPleno,
  // ): Promise<void> {
  //   await this.txPlenoRepository.replaceById(id, txPleno);
  // }
}
