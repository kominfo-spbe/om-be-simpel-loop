import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, requestBody, response} from '@loopback/rest';
import {TxProsesResmon} from '../models';
import {TxProsesResmonRepository} from '../repositories';

export class TxProsesResmonController {
    constructor(
        @repository(TxProsesResmonRepository)
        public txProsesResmonRepository: TxProsesResmonRepository,
    ) {
    }

    @post('/tx-proses-resmon')
    @response(200, {
        description: 'TxProsesResmon model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxProsesResmon)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxProsesResmon, {
                        title: 'NewTxProsesResmon',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txProsesResmon: Omit<TxProsesResmon, 'id'>,
    ): Promise<{ data: TxProsesResmon; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: await this.txProsesResmonRepository.create(txProsesResmon)

        }

    }

    @get('/tx-proses-resmon/count')
    @response(200, {
        description: 'TxProsesResmon model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxProsesResmon) where?: Where<TxProsesResmon>,
    ): Promise<Count> {
        return this.txProsesResmonRepository.count(where);
    }

    @get('/tx-proses-resmon')
    @response(200, {
        description: 'Array of TxProsesResmon model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxProsesResmon, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxProsesResmon) filter?: Filter<TxProsesResmon>,
    ): Promise<{ data: (TxProsesResmon)[]; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txProsesResmonRepository.find(filter)
        };
    }

    @get('/tx-proses-resmon/{id}')
    @response(200, {
        description: 'TxProsesResmon model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxProsesResmon, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxProsesResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxProsesResmon>
    ): Promise<{ data: TxProsesResmon; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txProsesResmonRepository.findById(id, filter)
        };
    }

    @patch('/tx-proses-resmon/{id}')
    @response(204, {
        description: 'TxProsesResmon PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxProsesResmon, {partial: true}),
                },
            },
        })
            txProsesResmon: TxProsesResmon,
    ): Promise<{ data: any; message: string; status: boolean }> {

        await this.txProsesResmonRepository.updateById(id, txProsesResmon)

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txProsesResmonRepository.findById(id)
        };
    }

    @del('/tx-proses-resmon/{id}')
    @response(204, {
        description: 'TxProsesResmon DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txProsesResmonRepository.deleteById(id);
    }
}
