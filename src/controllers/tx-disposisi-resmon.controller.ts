import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, requestBody, response} from '@loopback/rest';
import {TxDisposisiResmon, TxTakeOver} from '../models';
import {TxDisposisiResmonRepository} from '../repositories';

export class TxDisposisiResmonController {
    constructor(
        @repository(TxDisposisiResmonRepository)
        public txDisposisiResmonRepository: TxDisposisiResmonRepository,
    ) {
    }

    @post('/tx-disposisi-resmon')
    @response(200, {
        description: 'TxDisposisiResmon model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxDisposisiResmon)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxDisposisiResmon, {
                        title: 'NewTxDisposisiResmon',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txDisposisiResmon: Omit<TxDisposisiResmon, 'id'>,
    ): Promise<{ data: TxDisposisiResmon; message: string; status: boolean }> {

        let savedData = await this.txDisposisiResmonRepository.create(txDisposisiResmon);
        let rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedData

        }

        return rs;

    }

    @get('/tx-disposisi-resmon/count')
    @response(200, {
        description: 'TxDisposisiResmon model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxDisposisiResmon) where?: Where<TxDisposisiResmon>,
    ): Promise<Count> {
        return this.txDisposisiResmonRepository.count(where);
    }

    @get('/tx-disposisi-resmon')
    @response(200, {
        description: 'Array of TxDisposisiResmon model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxDisposisiResmon, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxDisposisiResmon) filter?: Filter<TxDisposisiResmon>,
    ): Promise<{ data: (TxDisposisiResmon)[]; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txDisposisiResmonRepository.find(filter)
        };

    }

    @get('/tx-disposisi-resmon/{id}')
    @response(200, {
        description: 'TxDisposisiResmon model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxDisposisiResmon, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxDisposisiResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxDisposisiResmon>
    ): Promise<{ data: TxDisposisiResmon; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txDisposisiResmonRepository.findById(id, filter)
        };
    }

    @patch('/tx-disposisi-resmon/{id}')
    @response(204, {
        description: 'TxDisposisiResmon PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxDisposisiResmon, {partial: true}),
                },
            },
        })
            txDisposisiResmon: TxDisposisiResmon,
    ): Promise<{ data: any; message: string; status: boolean }> {

      await this.txDisposisiResmonRepository.updateById(id, txDisposisiResmon);

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txDisposisiResmonRepository.findById(id)
      };

    }

    @del('/tx-disposisi-resmon/{id}')
    @response(204, {
        description: 'TxDisposisiResmon DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txDisposisiResmonRepository.deleteById(id);
    }
}
