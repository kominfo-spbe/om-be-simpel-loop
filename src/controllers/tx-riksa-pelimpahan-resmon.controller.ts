import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxRiksaPelimpahanResmon, TxSettingProsesResmon} from '../models';
import {TxRiksaPelimpahanResmonRepository} from '../repositories';

export class TxRiksaPelimpahanResmonController {
  constructor(
    @repository(TxRiksaPelimpahanResmonRepository)
    public txRiksaPelimpahanResmonRepository : TxRiksaPelimpahanResmonRepository,
  ) {}

  @post('/tx-riksa-pelimpahan-resmon')
  @response(200, {
    description: 'TxRiksaPelimpahanResmon model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxRiksaPelimpahanResmon)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaPelimpahanResmon, {
            title: 'NewTxRiksaPelimpahanResmon',
            exclude: ['id'],
          }),
        },
      },
    })
    txRiksaPelimpahanResmon: Omit<TxRiksaPelimpahanResmon, 'id'>,
  ): Promise<{ data: TxRiksaPelimpahanResmon; message: string; status: boolean }> {

      let rs = {
        status: true,
        message: "Data berhasil ditambahkan",
        data: await this.txRiksaPelimpahanResmonRepository.create(txRiksaPelimpahanResmon)

      }

      return rs;

  }

  @get('/tx-riksa-pelimpahan-resmon/count')
  @response(200, {
    description: 'TxRiksaPelimpahanResmon model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxRiksaPelimpahanResmon) where?: Where<TxRiksaPelimpahanResmon>,
  ): Promise<Count> {
    return this.txRiksaPelimpahanResmonRepository.count(where);
  }

  @get('/tx-riksa-pelimpahan-resmon')
  @response(200, {
    description: 'Array of TxRiksaPelimpahanResmon model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxRiksaPelimpahanResmon, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxRiksaPelimpahanResmon) filter?: Filter<TxRiksaPelimpahanResmon>,
  ): Promise<{ data: (TxRiksaPelimpahanResmon)[]; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txRiksaPelimpahanResmonRepository.find(filter)
      };
  }

  @patch('/tx-riksa-pelimpahan-resmon')
  @response(200, {
    description: 'TxRiksaPelimpahanResmon PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaPelimpahanResmon, {partial: true}),
        },
      },
    })
    txRiksaPelimpahanResmon: TxRiksaPelimpahanResmon,
    @param.where(TxRiksaPelimpahanResmon) where?: Where<TxRiksaPelimpahanResmon>,
  ): Promise<Count> {
    return this.txRiksaPelimpahanResmonRepository.updateAll(txRiksaPelimpahanResmon, where);
  }

  @get('/tx-riksa-pelimpahan-resmon/{id}')
  @response(200, {
    description: 'TxRiksaPelimpahanResmon model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxRiksaPelimpahanResmon, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxRiksaPelimpahanResmon, {exclude: 'where'}) filter?: FilterExcludingWhere<TxRiksaPelimpahanResmon>
  ): Promise<{ data: TxRiksaPelimpahanResmon; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txRiksaPelimpahanResmonRepository.findById(id, filter)
      };
  }

  @patch('/tx-riksa-pelimpahan-resmon/{id}')
  @response(204, {
    description: 'TxRiksaPelimpahanResmon PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaPelimpahanResmon, {partial: true}),
        },
      },
    })
    txRiksaPelimpahanResmon: TxRiksaPelimpahanResmon,
  ): Promise<{ data: any; message: string; status: boolean }> {

      await this.txRiksaPelimpahanResmonRepository.updateById(id, txRiksaPelimpahanResmon)

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txRiksaPelimpahanResmonRepository.findById(id)
      };
  }

  @put('/tx-riksa-pelimpahan-resmon/{id}')
  @response(204, {
    description: 'TxRiksaPelimpahanResmon PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txRiksaPelimpahanResmon: TxRiksaPelimpahanResmon,
  ): Promise<void> {
    await this.txRiksaPelimpahanResmonRepository.replaceById(id, txRiksaPelimpahanResmon);
  }

  @del('/tx-riksa-pelimpahan-resmon/{id}')
  @response(204, {
    description: 'TxRiksaPelimpahanResmon DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txRiksaPelimpahanResmonRepository.deleteById(id);
  }
}
