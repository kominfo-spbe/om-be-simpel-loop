import {
    Count,
    CountSchema,
    Filter, FilterExcludingWhere, repository, Where
} from '@loopback/repository';
import {
    del,
    get,
    getModelSchemaRef, param, patch, post, requestBody, response
} from '@loopback/rest';
import {TxLaporan, TxManajemenDokumen} from '../models';
import {TxManajemenDokumenRepository} from '../repositories';

export class TxDokumenResmonController {
    constructor(
        @repository(TxManajemenDokumenRepository)
        public TxManajemenDokumenRepository : TxManajemenDokumenRepository,
    ) {}

    @get('/tx-manajemen-dokumen')
    @response(200, {
        description: 'Array of TxManajemenDokumen model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxManajemenDokumen, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxManajemenDokumen) filter?: Filter<TxManajemenDokumen>,
    ): Promise<{ data: (TxManajemenDokumen)[]; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.TxManajemenDokumenRepository.find(filter)
        };

    }

    @get('/tx-manajemen-dokumen/{id}')
    @response(200, {
        description: 'TxManajemenDokumen model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxManajemenDokumen, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxManajemenDokumen, {exclude: 'where'}) filter?: FilterExcludingWhere<TxManajemenDokumen>
    ): Promise<{ data: TxManajemenDokumen; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.TxManajemenDokumenRepository.findById(id, filter)
        };
    }

    @patch('/tx-manajemen-dokumen/{id}')
    @response(204, {
        description: 'TxManajemenDokumen PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxManajemenDokumen, {partial: true}),
                },
            },
        })
            txRiksaMonitoring: TxManajemenDokumen,
    ): Promise<{ data: any; message: string; status: boolean }> {

        await this.TxManajemenDokumenRepository.updateById(id, txRiksaMonitoring)
        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.TxManajemenDokumenRepository.findById(id)
        };
    }

    @get('/tx-manajemen-dokumen/count')
    @response(200, {
        description: 'TxManajemenDokumen model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxLaporan) where?: Where<TxManajemenDokumen>,
    ): Promise<Count> {
        return this.TxManajemenDokumenRepository.count(where);
    }

}
