// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';


import {getModelSchemaRef, post, requestBody, response} from "@loopback/rest";
import {JwtModel} from "../models";
import {inject} from "@loopback/core";
import {AuthService} from "../services";

export class AuthController {
    constructor(@inject('services.AuthService')
                public authService: AuthService) {
    }

    @post('/inspect-token')
    @response(200, {
        description: 'Jwt Model instance',
        content: {'application/json': {schema: getModelSchemaRef(JwtModel)}},
    })
    async jwtDecode(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(JwtModel, {
                        title: 'NewJwtModel',
                        //exclude: ['token'],
                    }),
                },
            },
        })
            jwtModel: JwtModel,
    ): Promise<{ data: any; message: string; status: boolean }> {

        let decodedToken = {}
        if (jwtModel.token != undefined)
            decodedToken = await AuthService.parseJwt(jwtModel.token)

/*        let result = {
            validate : await this.authService.validateJwt(jwtModel.token),
            decodedToken : decodedToken
        }*/

        let rs = {
            status: true,
            message: "Data berhasil ditampilkan",
            data: decodedToken

        }

        return rs;

    }


}
