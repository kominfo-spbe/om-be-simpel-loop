import {inject} from "@loopback/core";
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, requestBody,
  response
} from '@loopback/rest';
import {TxKlasifikasiLmRiksa} from '../models';
import {TxKlasifikasiLmRiksaRepository} from '../repositories';
import {PvlService} from "../services";

export class TxKlasifikasiLmRiksaController {
  constructor(
    @repository(TxKlasifikasiLmRiksaRepository)
    public txKlasifikasiLmRiksaRepository: TxKlasifikasiLmRiksaRepository,
    @inject('services.PvlService')
    public pvlService: PvlService
  ) { }

  @post('/tx-klasifikasi-lm-riksa')
  @response(200, {
    description: 'TxKlasifikasiLmRiksa model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxKlasifikasiLmRiksa)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKlasifikasiLmRiksa, {
            title: 'NewTxKlasifikasiLmRiksa',
            exclude: ['id'],
          }),
        },
      },
    })
    txKlasifikasiLmRiksa: Omit<TxKlasifikasiLmRiksa, 'id'>,
  ): Promise<object> {

    const dataPost = txKlasifikasiLmRiksa

    dataPost.total_skor = await this.pvlService.hitungSkor(dataPost.jml_terlapor_terkait, dataPost.lokasi_terlapor, dataPost.penerima_manfaat, dataPost.permasalahan_dilaporkan)

    const rs = {
      status: true,
      message: "Data berhasil ditambahkan",
      data: await this.txKlasifikasiLmRiksaRepository.create(dataPost)
    }

    return rs;
  }

  @get('/tx-klasifikasi-lm-riksa/count')
  @response(200, {
    description: 'TxKlasifikasiLmRiksa model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async countRiksa(
    @param.where(TxKlasifikasiLmRiksa) where?: Where<TxKlasifikasiLmRiksa>,
  ): Promise<Count> {
    return this.txKlasifikasiLmRiksaRepository.count(where);
  }

  @get('/tx-klasifikasi-lm-riksa')
  @response(200, {
    description: 'Array of TxKlasifikasiLmRiksa model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxKlasifikasiLmRiksa, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxKlasifikasiLmRiksa) filter?: Filter<TxKlasifikasiLmRiksa>,
  ): Promise<object> {

    const listUraian = ['permasalahan_dilaporkan', 'klasifikasi_laporan']
    const resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txKlasifikasiLmRiksaRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }


  @get('/tx-klasifikasi-lm-riksa/{id}')
  @response(200, {
    description: 'TxKlasifikasiLmRiksa model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKlasifikasiLmRiksa, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxKlasifikasiLmRiksa, {exclude: 'where'}) filter?: FilterExcludingWhere<TxKlasifikasiLmRiksa>
  ): Promise<object> {
    const listUraian = ['permasalahan_dilaporkan', 'klasifikasi_laporan']
    const resultData = await this.pvlService.getMLookUpFindById(id, filter, listUraian, this.txKlasifikasiLmRiksaRepository)

    return {
      status: true,
      message: "Data berhasil ditampilkan",
      data: resultData
    };
  }


  @patch('/tx-klasifikasi-lm-riksa/{id}')
  @response(204, {
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxKlasifikasiLmRiksa, {partial: true}),
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxKlasifikasiLmRiksa, {partial: true}),
        },
      },
    })
    txKlasifikasiLmRiksa: TxKlasifikasiLmRiksa,
  ): Promise<Object> {

    const currentData = await this.txKlasifikasiLmRiksaRepository.findById(id);
    const dataPost = txKlasifikasiLmRiksa

    const jml_terlapor_terkait = dataPost.hasOwnProperty('jml_terlapor_terkait') ? dataPost.jml_terlapor_terkait : currentData.jml_terlapor_terkait;
    const lokasi_terlapor = dataPost.hasOwnProperty('lokasi_terlapor') ? dataPost.lokasi_terlapor : currentData.lokasi_terlapor;
    const penerima_manfaat = dataPost.hasOwnProperty('penerima_manfaat') ? dataPost.penerima_manfaat : currentData.penerima_manfaat;
    const permasalahan_dilaporkan = dataPost.hasOwnProperty('permasalahan_dilaporkan') ? dataPost.permasalahan_dilaporkan : currentData.permasalahan_dilaporkan;

    dataPost.total_skor = await this.pvlService.hitungSkor(jml_terlapor_terkait, lokasi_terlapor, penerima_manfaat, permasalahan_dilaporkan)

    const rs = {
      status: true,
      message: "Data berhasil diubah",
      data: await this.txKlasifikasiLmRiksaRepository.updateById(id, dataPost)
    }

    return rs;
  }


  @del('/tx-klasifikasi-lm-riksa/{id}')
  @response(204, {
    description: 'TxKlasifikasiLmRiksa DELETE success',
  })
  async deleteById(@param.path.string('id') id: string)
    : Promise<Object> {
    const rs = {
      status: true,
      message: "Data dihapus",
      data: await this.txKlasifikasiLmRiksaRepository.deleteById(id)
    }

    //send to log


    return rs;
  }



}
