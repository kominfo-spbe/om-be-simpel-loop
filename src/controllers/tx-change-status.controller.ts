import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxChangeStatus} from '../models';
import {TxChangeStatusRepository, TxLaporanRepository} from '../repositories';

export class TxChangeStatusController {
    constructor(
        @repository(TxChangeStatusRepository)
        public txChangeStatusRepository: TxChangeStatusRepository,
        @repository(TxLaporanRepository)
        public txLaporanRepository: TxLaporanRepository
    ) {
    }

    @post('/tx-change-status')
    @response(200, {
        description: 'TxChangeStatus model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxChangeStatus)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxChangeStatus, {
                        title: 'NewTxChangeStatus',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txChangeStatus: Omit<TxChangeStatus, 'id'>,
    ):
        Promise<{ data: TxChangeStatus; message: string; status: boolean }> {

        if (txChangeStatus.no_agenda == null) {
            let laporan = await this.txLaporanRepository.findById(txChangeStatus.id_tx_laporan)
            txChangeStatus.no_agenda = laporan.no_agenda
        }

        //save change status
        txChangeStatus.status_laporan_date = new Date().toISOString()
        txChangeStatus.created_date = new Date().toISOString()
        let savedData = await this.txChangeStatusRepository.create(txChangeStatus);


        //update status laporan
        if (savedData) {
            let objLaporan = {
                status_laporan: txChangeStatus.status_laporan_new,
            }

            await this.updateStatusLaporan(txChangeStatus.id_tx_laporan, objLaporan)
        }

        let rs = {
            status: true,
            message: "Data berhasil ditambahkan",
            data: await this.txChangeStatusRepository.findById(savedData.id)

        }

        return rs;
    }

    @get('/tx-change-status/count')
    @response(200, {
        description: 'TxChangeStatus model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>,
    ): Promise<Count> {
        return this.txChangeStatusRepository.count(where);
    }

    @get('/tx-change-status')
    @response(200, {
        description: 'Array of TxChangeStatus model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxChangeStatus, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxChangeStatus) filter?: Filter<TxChangeStatus>,
    ): Promise<{ data: (TxChangeStatus)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txChangeStatusRepository.find(filter)
        };
    }

    @patch('/tx-change-status')
    @response(200, {
        description: 'TxChangeStatus PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxChangeStatus, {partial: true}),
                },
            },
        })
            txChangeStatus: TxChangeStatus,
        @param.where(TxChangeStatus) where?: Where<TxChangeStatus>,
    ): Promise<Count> {
        return this.txChangeStatusRepository.updateAll(txChangeStatus, where);
    }

    @get('/tx-change-status/{id}')
    @response(200, {
        description: 'TxChangeStatus model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxChangeStatus, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxChangeStatus, {exclude: 'where'}) filter?: FilterExcludingWhere<TxChangeStatus>
    ): Promise<{ data: TxChangeStatus; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txChangeStatusRepository.findById(id, filter)
        };
    }

    @patch('/tx-change-status/{id}')
    @response(204, {
        description: 'TxChangeStatus PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxChangeStatus, {partial: true}),
                },
            },
        })
            txChangeStatus: TxChangeStatus,
    ): Promise<{ data: any; message: string; status: boolean }> {
        await this.txChangeStatusRepository.updateById(id, txChangeStatus);

        return {
            status: true,
            message: "Data berhasil diubah",
            data: await this.txChangeStatusRepository.findById(id)
        };
    }

    @put('/tx-change-status/{id}')
    @response(204, {
        description: 'TxChangeStatus PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txChangeStatus: TxChangeStatus,
    ): Promise<void> {
        await this.txChangeStatusRepository.replaceById(id, txChangeStatus);
    }

    @del('/tx-change-status/{id}')
    @response(204, {
        description: 'TxChangeStatus DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txChangeStatusRepository.deleteById(id);
    }

    async updateStatusLaporan(idLaporan: string, txLaporan: { status_laporan: string | undefined }) {

        let laporan = await this.txLaporanRepository.findById(idLaporan)

        if (laporan) {

            // update data tx_laporan
            await this.txLaporanRepository.updateById(idLaporan, txLaporan);

        }

        return true;


    }


}
