import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TxRiksaMonitoring, TxSettingProsesResmon} from '../models';
import {TxRiksaMonitoringRepository} from '../repositories';

export class TxRiksaMonitoringController {
  constructor(
    @repository(TxRiksaMonitoringRepository)
    public txRiksaMonitoringRepository : TxRiksaMonitoringRepository,
  ) {}

  @post('/tx-riksa-monitoring')
  @response(200, {
    description: 'TxRiksaMonitoring model instance',
    content: {'application/json': {schema: getModelSchemaRef(TxRiksaMonitoring)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaMonitoring, {
            title: 'NewTxRiksaMonitoring',
            exclude: ['id'],
          }),
        },
      },
    })
    txRiksaMonitoring: Omit<TxRiksaMonitoring, 'id'>,
  ): Promise<{ data: TxRiksaMonitoring; message: string; status: boolean }> {

      let rs = {
        status: true,
        message: "Data berhasil ditambahkan",
        data: await this.txRiksaMonitoringRepository.create(txRiksaMonitoring)

      }

      return rs;

  }

  @get('/tx-riksa-monitoring/count')
  @response(200, {
    description: 'TxRiksaMonitoring model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxRiksaMonitoring) where?: Where<TxRiksaMonitoring>,
  ): Promise<Count> {
    return this.txRiksaMonitoringRepository.count(where);
  }

  @get('/tx-riksa-monitoring')
  @response(200, {
    description: 'Array of TxRiksaMonitoring model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxRiksaMonitoring, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TxRiksaMonitoring) filter?: Filter<TxRiksaMonitoring>,
  ): Promise<{ data: (TxRiksaMonitoring)[]; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txRiksaMonitoringRepository.find(filter)
      };
  }

  @patch('/tx-riksa-monitoring')
  @response(200, {
    description: 'TxRiksaMonitoring PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaMonitoring, {partial: true}),
        },
      },
    })
    txRiksaMonitoring: TxRiksaMonitoring,
    @param.where(TxRiksaMonitoring) where?: Where<TxRiksaMonitoring>,
  ): Promise<Count> {
    return this.txRiksaMonitoringRepository.updateAll(txRiksaMonitoring, where);
  }

  @get('/tx-riksa-monitoring/{id}')
  @response(200, {
    description: 'TxRiksaMonitoring model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxRiksaMonitoring, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxRiksaMonitoring, {exclude: 'where'}) filter?: FilterExcludingWhere<TxRiksaMonitoring>
  ): Promise<{ data: TxRiksaMonitoring; message: string; status: boolean }> {

      return {
        status: true,
        message: "Data berhasil ditampilkan",
        data: await this.txRiksaMonitoringRepository.findById(id, filter)
      };
  }

  @patch('/tx-riksa-monitoring/{id}')
  @response(204, {
    description: 'TxRiksaMonitoring PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxRiksaMonitoring, {partial: true}),
        },
      },
    })
    txRiksaMonitoring: TxRiksaMonitoring,
  ): Promise<{ data: any; message: string; status: boolean }> {

      await this.txRiksaMonitoringRepository.updateById(id, txRiksaMonitoring)

      return {
        status: true,
        message: "Data berhasil diubah",
        data: await this.txRiksaMonitoringRepository.findById(id)
      };
  }

  @put('/tx-riksa-monitoring/{id}')
  @response(204, {
    description: 'TxRiksaMonitoring PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txRiksaMonitoring: TxRiksaMonitoring,
  ): Promise<void> {
    await this.txRiksaMonitoringRepository.replaceById(id, txRiksaMonitoring);
  }

  @del('/tx-riksa-monitoring/{id}')
  @response(204, {
    description: 'TxRiksaMonitoring DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txRiksaMonitoringRepository.deleteById(id);
  }
}
