import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where,} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody, response,} from '@loopback/rest';
import {TxLampiran} from '../models';
import {TxLampiranRepository} from '../repositories';
import {inject} from "@loopback/core";
import {PvlService} from "../services";

export class TxLampiranController {
    constructor(
        @repository(TxLampiranRepository)
        public txLampiranRepository: TxLampiranRepository,
        @inject('services.PvlService')
        public pvlService: PvlService
    ) {
    }

    @post('/tx-lampiran')
    @response(200, {
        description: 'TxLampiran model instance',
        content: {'application/json': {schema: getModelSchemaRef(TxLampiran)}},
    })
    async create(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLampiran, {
                        title: 'NewTxLampiran',
                        exclude: ['id'],
                    }),
                },
            },
        })
            txLampiran: Omit<TxLampiran, 'id'>,
    ):
        Promise<{ data: TxLampiran; message: string; status: boolean }> {

        let savedData = await this.txLampiranRepository.create(txLampiran);

        return {
            status: true,
            message: "Data berhasil ditambahkan",
            data: savedData

        }
    }

    @get('/tx-lampiran/count')
    @response(200, {
        description: 'TxLampiran model count',
        content: {'application/json': {schema: CountSchema}},
    })
    async count(
        @param.where(TxLampiran) where?: Where<TxLampiran>,
    ): Promise<Count> {
        return this.txLampiranRepository.count(where);
    }

    @get('/tx-lampiran')
    @response(200, {
        description: 'Array of TxLampiran model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: getModelSchemaRef(TxLampiran, {includeRelations: true}),
                },
            },
        },
    })
    async find(
        @param.filter(TxLampiran) filter?: Filter<TxLampiran>,
    ):/* Promise<{ data: (TxLampiran)[]; message: string; status: boolean }> {
        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txLampiranRepository.find(filter)
        };*/


        Promise<object> {

            let listUraian = ['status_laporan']
            let resultData = await this.pvlService.getMLookUpFindAll(filter, listUraian, this.txLampiranRepository)

            return {
                status: true,
                message: "Data berhasil ditampilkan",
                data: resultData
            };

    }

    @patch('/tx-lampiran')
    @response(200, {
        description: 'TxLampiran PATCH success count',
        content: {'application/json': {schema: CountSchema}},
    })
    async updateAll(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLampiran, {partial: true}),
                },
            },
        })
            txLampiran: TxLampiran,
        @param.where(TxLampiran) where?: Where<TxLampiran>,
    ): Promise<Count> {
        return this.txLampiranRepository.updateAll(txLampiran, where);
    }

    @get('/tx-lampiran/{id}')
    @response(200, {
        description: 'TxLampiran model instance',
        content: {
            'application/json': {
                schema: getModelSchemaRef(TxLampiran, {includeRelations: true}),
            },
        },
    })
    async findById(
        @param.path.string('id') id: string,
        @param.filter(TxLampiran, {exclude: 'where'}) filter?: FilterExcludingWhere<TxLampiran>
    ): Promise<{ data: TxLampiran; message: string; status: boolean }> {

        return {
            status: true,
            message: "Data berhasil ditampilkan",
            data: await this.txLampiranRepository.findById(id, filter)
        };

    }

    @patch('/tx-lampiran/{id}')
    @response(204, {
        description: 'TxLampiran PATCH success',
    })
    async updateById(
        @param.path.string('id') id: string,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(TxLampiran, {partial: true}),
                },
            },
        })
            txLampiran: TxLampiran,
    ): Promise<{ data: any; message: string; status: boolean }> {
            await this.txLampiranRepository.updateById(id, txLampiran);

            return {
                status: true,
                message: "Data berhasil diubah",
                data: await this.txLampiranRepository.findById(id)
            };

    }

    @put('/tx-lampiran/{id}')
    @response(204, {
        description: 'TxLampiran PUT success',
    })
    async replaceById(
        @param.path.string('id') id: string,
        @requestBody() txLampiran: TxLampiran,
    ): Promise<void> {
        await this.txLampiranRepository.replaceById(id, txLampiran);
    }

    @del('/tx-lampiran/{id}')
    @response(204, {
        description: 'TxLampiran DELETE success',
    })
    async deleteById(@param.path.string('id') id: string): Promise<void> {
        await this.txLampiranRepository.deleteById(id);
    }
}
