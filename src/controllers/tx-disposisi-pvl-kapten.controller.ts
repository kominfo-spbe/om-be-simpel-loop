import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {TxDisposisiPvlKapten} from '../models';
import {
  TxDisposisiPvlKaptenRepository,
  TxLaporanRepository,
} from '../repositories';

export class TxDisposisiPvlKaptenController {
  constructor(
    @repository(TxDisposisiPvlKaptenRepository)
    public txDisposisiPvlKaptenRepository: TxDisposisiPvlKaptenRepository,

    @repository(TxLaporanRepository)
    public txLaporanRepository: TxLaporanRepository,
  ) {}

  @post('/tx-disposisi-pvl-kapten')
  @response(200, {
    description: 'TxDisposisiPvlKapten model instance',
    content: {
      'application/json': {schema: getModelSchemaRef(TxDisposisiPvlKapten)},
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKapten, {
            title: 'NewTxDisposisiPvlKapten',
            exclude: ['id'],
          }),
        },
      },
    })
    txDisposisiPvlKapten: Omit<TxDisposisiPvlKapten, 'id'>,
  ): Promise<{
    data: Promise<TxDisposisiPvlKapten>;
    message: string;
    status: boolean;
  }> {
    const createKapten: any =
      this.txDisposisiPvlKaptenRepository.create(txDisposisiPvlKapten);
    await this.txLaporanRepository.updateById(
      txDisposisiPvlKapten.id_tx_laporan,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      {is_disposisi_kapten: txDisposisiPvlKapten?.tim_pemeriksaan ?? ''},
    );

    const rs = {
      status: true,
      message: 'Data berhasil ditambahkan',
      data: createKapten,
    };

    return rs;
  }

  @get('/tx-disposisi-pvl-kapten/count')
  @response(200, {
    description: 'TxDisposisiPvlKapten model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TxDisposisiPvlKapten) where?: Where<TxDisposisiPvlKapten>,
  ): Promise<Count> {
    return this.txDisposisiPvlKaptenRepository.count(where);
  }

  @get('/tx-disposisi-pvl-kapten')
  @response(200, {
    description: 'Array of TxDisposisiPvlKapten model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TxDisposisiPvlKapten, {
            includeRelations: true,
          }),
        },
      },
    },
  })
  async find(
    @param.filter(TxDisposisiPvlKapten) filter?: Filter<TxDisposisiPvlKapten>,
  ): Promise<{data: TxDisposisiPvlKapten[]; message: string; status: boolean}> {
    return {
      status: true,
      message: 'Data berhasil ditampilkan',
      data: await this.txDisposisiPvlKaptenRepository.find(filter),
    };
  }

  @patch('/tx-disposisi-pvl-kapten')
  @response(200, {
    description: 'TxDisposisiPvlKapten PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKapten, {partial: true}),
        },
      },
    })
    txDisposisiPvlKapten: TxDisposisiPvlKapten,
    @param.where(TxDisposisiPvlKapten) where?: Where<TxDisposisiPvlKapten>,
  ): Promise<Count> {
    return this.txDisposisiPvlKaptenRepository.updateAll(
      txDisposisiPvlKapten,
      where,
    );
  }

  @get('/tx-disposisi-pvl-kapten/{id}')
  @response(200, {
    description: 'TxDisposisiPvlKapten model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TxDisposisiPvlKapten, {
          includeRelations: true,
        }),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TxDisposisiPvlKapten, {exclude: 'where'})
    filter?: FilterExcludingWhere<TxDisposisiPvlKapten>,
  ): Promise<{data: TxDisposisiPvlKapten; message: string; status: boolean}> {
    return {
      status: true,
      message: 'Data berhasil ditampilkan',
      data: await this.txDisposisiPvlKaptenRepository.findById(id, filter),
    };
  }

  @patch('/tx-disposisi-pvl-kapten/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKapten PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TxDisposisiPvlKapten, {partial: true}),
        },
      },
    })
    txDisposisiPvlKapten: TxDisposisiPvlKapten,
  ): Promise<{data: any; message: string; status: boolean}> {
    const findKapten: any = await this.txDisposisiPvlKaptenRepository.findById(
      id,
    );

    await this.txDisposisiPvlKaptenRepository.updateById(
      id,
      txDisposisiPvlKapten,
    );

    if (findKapten) {
      await this.txLaporanRepository.updateById(
        findKapten.id_tx_laporan,
        // eslint-disable-next-line @typescript-eslint/naming-convention
        {is_disposisi_kapten: txDisposisiPvlKapten?.tim_pemeriksaan ?? ''},
      );
    }

    return {
      status: true,
      message: 'Data berhasil diubah',
      data: findKapten,
    };
  }

  @put('/tx-disposisi-pvl-kapten/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKapten PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() txDisposisiPvlKapten: TxDisposisiPvlKapten,
  ): Promise<{data: TxDisposisiPvlKapten; message: string; status: boolean}> {
    // update data
    await this.txDisposisiPvlKaptenRepository.replaceById(
      id,
      txDisposisiPvlKapten,
    );

    return {
      status: true,
      message: 'Data berhasil diubah',
      data: await this.txDisposisiPvlKaptenRepository.findById(id),
    };
  }

  @del('/tx-disposisi-pvl-kapten/{id}')
  @response(204, {
    description: 'TxDisposisiPvlKapten DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.txDisposisiPvlKaptenRepository.deleteById(id);
  }
}
