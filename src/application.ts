import {BootMixin} from '@loopback/boot';
import {ApplicationConfig, createBindingFromClass} from '@loopback/core';
import {RepositoryMixin, repository} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import multer from 'multer';
import path from 'path';
import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from './keys';
import {authMiddleware} from './middleware/auth.middleware';
import {MySequence} from './sequence';
import {SECURITY_SCHEME_SPEC, SECURITY_SPEC} from './utils/security-spec';
//import {AuthenticationComponent} from '@loopback/authentication';
import {CronComponent, CronJob, cronJob} from '@loopback/cron';
import {TxLaporan, TxVerifFormil} from './models';
import {TxLaporanRepository, TxVerifFormilRepository} from './repositories';

export {ApplicationConfig};

export class Simpel4WsApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      indexTemplatePath: path.resolve(
        __dirname,
        '../public/explorer/index.html.ejs',
      ),
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.component(CronComponent);
    this.add(createBindingFromClass(ScheduleService));
    // this.myCronJob.start();
    // this.addScheduler();

    // Configure file upload with multer options
    this.configureFileUpload(options.fileStorageDirectory);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    // this.bootOptions = {
    //   controllers: {
    //     // Customize ControllerBooter Conventions here
    //     dirs: ['controllers'],
    //     extensions: ['.controller.js'],
    //     nested: true,
    //   },
    // };

    // register auth middleware
    this.middleware(authMiddleware);

    // ------ ADD SNIPPET  ---------
    // Add security spec
    this.addSecuritySpec();
    // ------------- END OF SNIPPET -------------
  }

  /**
   * Configure `multer` options for file upload
   */
  protected configureFileUpload(destination?: string) {
    // Upload files to `dist/.sandbox` by default
    destination = destination ?? path.join(__dirname, '../public/uploads');
    this.bind(STORAGE_DIRECTORY).to(destination);
    const multerOptions: multer.Options = {
      storage: multer.diskStorage({
        destination,
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    };
    // Configure the file upload service with multer options
    this.configure(FILE_UPLOAD_SERVICE).to(multerOptions);
  }

  // ---------- ADD THIS FUNCTION ------------
  // Add this function so that we can later test the
  // JWT authentication using API Explorer
  addSecuritySpec(): void {
    this.api({
      openapi: '3.0.0',
      info: {
        title: 'Simpel 4 WS',
        version: '0.0.13.13',
      },
      paths: {},
      components: {securitySchemes: SECURITY_SCHEME_SPEC},
      security: SECURITY_SPEC,
      /*security: [
        {
          // secure all endpoints with 'jwt'
          jwt: [],
        },
      ],*/
      servers: [{url: '/'}],
    });
  }
  // -------------- END OF SNIPPET -----------
}

@cronJob()
export class ScheduleService extends CronJob {
  constructor(
    @repository(TxLaporanRepository)
    public txLaporanRepository: TxLaporanRepository,
    @repository(TxVerifFormilRepository)
    public txVerifFormilRepository: TxVerifFormilRepository,
  ) {
    super({
      name: 'job-penutupan-laporan',
      onTick: () => {
        // do the work
        this.performMyJob().then(r => {});
      },
      cronTime: '* */5 * * * *',
      start: true,
    });
  }

  async performMyJob() {
    const sql =
      'select a.id \n' +
      'from simpel_4.tx_laporan as a\n' +
      'join simpel_4.tx_laporan_tindakan as b on a.id = b.id_tx_laporan\n' +
      'where \n' +
      "(b.created_date + INTERVAL '60 day') < current_date and \n" +
      "a.status_laporan in ('VFL')" +
      'group by a.id';

    const idTxLaporan: Array<string> = [];
    await this.txLaporanRepository.dataSource
      .execute(sql)
      .then((value: [TxLaporan]) => {
        for (let i = 0; i < value.length; i++) {
          const item = value[i];
          const data = {
            status_laporan: 'PBAP',
            status_count: 1,
          };
          idTxLaporan.push(item.id);
          this.txLaporanRepository.updateById(item.id, data);
        }
      });

    for (let i = 0; i < idTxLaporan.length; i++) {
      const sqlFormil =
        'select a.id \n' +
        'from simpel_4.tx_verif_formil as a\n' +
        'where id_tx_laporan = $1';
      const id = idTxLaporan[i];
      await this.txVerifFormilRepository.dataSource
        .execute(sqlFormil, [id])
        .then((value: [TxVerifFormil]) => {
          for (let x = 0; i < value.length; x++) {
            const item = value[x];
            const data = {
              status_verif_formil: 'status_verif_05',
            };
            this.txVerifFormilRepository.updateById(item.id, data);
          }
        });
    }
  }
}
