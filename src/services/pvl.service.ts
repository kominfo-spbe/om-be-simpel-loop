import {BindingScope, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  MKantorRepository,
  MLookupRepository,
  TdUserRepository,
  TxKlasifikasiLmRepository,
  TxKlasifikasiLmRiksaRepository,
  TxVerifMateriilRepository
} from '../repositories';

@injectable({scope: BindingScope.TRANSIENT})
export class PvlService {
  constructor(
    @repository(TxVerifMateriilRepository)
    public txVerifMateriilRepository: TxVerifMateriilRepository,
    @repository(TxKlasifikasiLmRepository)
    public txKlasifikasiLmRepository: TxKlasifikasiLmRepository,
    @repository(TxKlasifikasiLmRiksaRepository)
    public txKlasifikasiLmRiksaRepository: TxKlasifikasiLmRiksaRepository,
    @repository(MLookupRepository)
    public mLookupRepository: MLookupRepository,
    @repository(TdUserRepository)
    public tdUserRepository: TdUserRepository,
    @repository(MKantorRepository)
    public mKantorRepository: MKantorRepository,
  ) {}

  async getMLookUpStatus(status: string) {
    const mLookUp = await this.mLookupRepository.findOne({
      where: {lookupCode: status, lookupGroupName: 'status_laporan'},
    });

    return mLookUp?.additional_1;
  }

  //Get data from m_lookup by list attributes from id
  async getMLookUpFindById(
    id: string,
    filter: any,
    listUraian: any = [],
    txRepository: any,
  ) {
    const resultData = await txRepository.findById(id, filter);
    const rebuildData: any = {};

    for (const list of listUraian) {
      const mLookUp = await this.mLookupRepository.findOne({
        where: {lookupCode: resultData[list]},
      });

      if (mLookUp) {
        rebuildData[list + '_name'] = mLookUp.lookupName;
      }
    }

    if (
      resultData['officer_by'] &&
      (typeof resultData['officer_by'] != 'undefined' ||
        resultData['officer_by'] != null)
    ) {
      const tdUser = await this.tdUserRepository.findOne({
        where: {idUser: resultData['officer_by']},
      });
      if (tdUser) {
        rebuildData['officer_by' + '_name'] = tdUser.nama;
      }
    }

    const time =
      new Date().getTime() - new Date(resultData['created_date']).getTime();
    const durasi = await this.TimeStructure(time);

    rebuildData['durasi'] = durasi;

    return {...resultData, ...rebuildData};
  }

  //Get data from m_lookup by list attributes from list
  async getMLookUpFindAll(
    filter: any,
    listUraian: any = [],
    txRepository: any,
    isCount = false,
  ) {
    if (filter.where && Object.keys(filter.where).length < 1 && !filter.limit) {
      filter.limit = 10;
      filter.skip = 0;
    }

    let limit;
    let skip;
    if (filter.limit != null) {
      limit = filter.limit;
      // filter.limit = null;
    }
    if (filter.skip != null) {
      skip = filter.skip;
      // filter.skip = 0;
    }

    let cFilter = filter;

    if (typeof filter !== 'undefined') {
      cFilter = await this.customFilter(filter);
    }

    if (!isCount) {
      const resultData = await txRepository.find(cFilter);
      const rebuildData: any = {};
      let mergedData: any = {};
      const finalData: any = [];
      let lookupDatas: any[] = [];

      if (listUraian.length > 0) {
        lookupDatas = await this.mLookupRepository.find({
          fields: {
            lookupCode: true,
            lookupName: true,
          },
        });
      }

      const tdUser = await this.tdUserRepository.find();
      const kantorRepo = await this.mKantorRepository.find();

      for (const data of resultData) {
        for (const list of listUraian) {
          const findLookup: any = lookupDatas.find(
            (itLookup: any) => itLookup.lookupCode === data[list],
          );

          if (findLookup) {
            rebuildData[`${list}_name`] = findLookup.lookupName;
          }

          mergedData = {...data, ...rebuildData};
        }

        if (
          data['officer_by'] &&
          (typeof data['officer_by'] != 'undefined' ||
            data['officer_by'] != null)
        ) {
          // const tdUser = await this.tdUserRepository.findOne({
          //   where: {idUser: data['officer_by']},
          // });
          const user = tdUser.find(x => x.idUser == data['officer_by'])
          if (user) {
            mergedData['officer_by' + '_name'] = user.nama;
          }
        }

        if (
          data['kode_kantor'] &&
          (typeof data['kode_kantor'] != 'undefined' ||
            data['kode_kantor'] != null)
        ) {
          const nKantor = kantorRepo.find(x => x.kode_kantor == data['kode_kantor'])
          if (nKantor) {
            mergedData['nama' + '_kantor'] = nKantor.nama_kantor;
          }
        }

        if(data.status_laporan == 'LT' || data.status_laporan == 'LTRKS' || data.status_laporan == 'TMSM' || data.status_laporan == 'TMSF' || data.status_laporan == 'LTRSM'){
          const duras: any = {};
          duras.durasi = '-'
          duras.keterangan = ''
          mergedData['durasi'] = duras
        }else{
          const time =
          new Date().getTime() - new Date(data['tgl_agenda']).getTime();
        mergedData['durasi'] = await this.TimeDay(time);
        }

        finalData.push(mergedData);
      }

      return finalData;
    }

    const countData = await txRepository.count(cFilter.where);

    return countData;
  }

  async getHistoryRelation(filter: any, txRepository: any) {
    let cFilter = filter;

    if (typeof filter !== 'undefined') {
      cFilter = await this.customFilter(filter);
    }

    const resultData = await txRepository.find(cFilter);
    const rebuildData: any = {};
    let mergedData: any = {};
    const finalData: any = [];

    for (const data of resultData) {
      mergedData = {...data, ...rebuildData};
      if (
        data['prev_assignee'] &&
        (typeof data['prev_assignee'] != 'undefined' ||
          data['prev_assignee'] != null)
      ) {
        const tdUser = await this.tdUserRepository.findOne({
          where: {idUser: data['prev_assignee']},
        });
        if (tdUser) {
          mergedData['prev_assignee' + '_name'] = tdUser.nama;
        }
      }

      if (
        data['next_assignee'] &&
        (typeof data['next_assignee'] != 'undefined' ||
          data['next_assignee'] != null)
      ) {
        const tdUser = await this.tdUserRepository.findOne({
          where: {idUser: data['next_assignee']},
        });
        if (tdUser) {
          mergedData['next_assignee' + '_name'] = tdUser.nama;
        }
      }

      finalData.push(mergedData);
    }

    return finalData;
  }

  async TimeStructure(time: any) {
    const data: any = {};

    const diffSeconds = time / 1000;
    const diffMinutes = time / (60 * 1000);
    const diffHours = time / (60 * 60 * 1000);
    const diffDays = time / (24 * 60 * 60 * 1000);
    const diffMonths = time / (24 * 60 * 60 * 1000) / 30;
    const diffYears = time / (24 * 60 * 60 * 1000) / 30 / 12;

    // let output = diffYears + ' tahun ' + diffMonths + ' bulan ' + diffDays + ' hari ' + diffHours + ' jam ' + diffMinutes + ' menit ' + diffSeconds + ' detik '

    let output = 'Baru';

    if (diffYears >= 1) {
      output = Number(diffYears.toFixed(0)) + ' tahun';
    }

    if (diffMonths >= 1 && diffMonths <= 12) {
      output = Number(diffMonths.toFixed(0)) + ' bulan';
    }

    if (diffDays >= 1 && diffDays <= 30) {
      output = Number(diffDays.toFixed(0)) + ' hari';
    }

    if (diffHours > 1 && diffHours <= 24) {
      output = Number(diffHours.toFixed(0)) + ' jam';
    }

    if (diffMinutes >= 1 && diffMinutes <= 60) {
      output = Number(diffMinutes.toFixed(0)) + ' menit';
    }

    if (diffSeconds >= 1 && diffSeconds <= 60) {
      output = Number(diffSeconds.toFixed(0)) + ' detik';
    }

    data.durasi = output;
    data.keterangan = diffSeconds <= 3600 ? 'Baru' : '';

    return data;
  }

  async TimeDay(time: any) {
    const data: any = {};

    const diffSeconds = time / 1000;
    const diffDays = time / (24 * 60 * 60 * 1000);

    let output = 'Baru';

    if (diffDays >= 1) {
      output = Number(diffDays.toFixed(0)) + ' hari';
    }

    data.durasi = output;
    data.keterangan = diffSeconds <= 3600 ? 'Baru' : '';

    return data;
  }

  async customFilter(filter: any) {
    const newFilter: any = {};
    const newFilterContainer: any = [];

    if (typeof filter !== 'undefined') {
      if (filter.hasOwnProperty('where')) {
        if (filter.where.officer_by_name?.ilike) {
          let officerNameFilter: any[] = [];

          officerNameFilter = await this.tdUserRepository.find({
            where: {nama: {ilike: filter.where.officer_by_name.ilike}},
          });

          filter.where.officer_by = {
            inq: officerNameFilter.map((it: any) => it.idUser),
          };
        }

        if (filter.where.hasOwnProperty('tipe_laporan_name')) {
          let tipeLaporanFilter: any[] = [];

          if (filter.where.tipe_laporan_name.hasOwnProperty('ilike')) {
            tipeLaporanFilter = await this.mLookupRepository.find({
              where: {
                lookupName: {ilike: filter.where.tipe_laporan_name.ilike},
                lookupGroupName: 'tipe_laporan',
              },
            });
          }

          filter.where.tipe_laporan = {
            inq: tipeLaporanFilter.map((it: any) => it.lookupCode),
          };
        }

        // added
        if (filter.where?.nama_kantor?.ilike) {
          const namaKantorFilter: any[] = await this.mKantorRepository.find({
            where: {
              nama_kantor: {ilike: filter.where.nama_kantor.ilike},
            },
          });

          filter.where.kode_kantor = {
            inq: namaKantorFilter.map((it: any) => it.kode_kantor),
          };
        }

        if (filter.where?.status_petugas_name?.ilike) {
          const statusPetugasFilter: any[] = await this.mLookupRepository.find({
            where: {
              lookupName: {ilike: filter.where.status_petugas_name.ilike},
              lookupGroupName: 'status_petugas',
            },
          });

          filter.where.status_petugas = {
            inq: statusPetugasFilter.map((it: any) => it.lookup_name),
          };
        }

        if (filter.where?.status_laporan_name?.ilike) {
          const statusLaporanFilter: any[] = await this.mLookupRepository.find({
            where: {
              lookupName: {ilike: filter.where.status_laporan_name.ilike},
              lookupGroupName: 'status_laporan',
            },
          });

          filter.where.status_laporan = {
            inq: statusLaporanFilter.map((it: any) => it.lookupCode),
          };
        }

        // @ts-ignore
        if (filter.include != undefined && filter.include.length > 0) {
          const findKapten = filter.include.find(
            (it: any) =>
              it.relation &&
              it.relation === 'txDisposisiPvlKapten' &&
              it.required,
          );

          if (findKapten) {
            if (findKapten.scope?.where?.tim_pemeriksaan) {
              filter.where.is_disposisi_kapten =
                findKapten.scope.where.tim_pemeriksaan;
            } else {
              filter.where.is_disposisi_kapten = {
                neq: null,
              };
            }
          }
        }
      }
    }

    return filter;
  }

  async hitungSkor(
    jml_terlapor: any,
    lokasi_terlapor: any,
    penerima_manfaat: any,
    permasalahan_dilaporkan: any,
  ) {
    const jumlah_skor =
      parseInt(jml_terlapor === null ? '0' : jml_terlapor) +
      parseInt(lokasi_terlapor === null ? '0' : lokasi_terlapor) +
      parseInt(penerima_manfaat === null ? '0' : penerima_manfaat) +
      parseInt(
        permasalahan_dilaporkan === null
          ? '0'
          : permasalahan_dilaporkan == null
          ? 0
          : permasalahan_dilaporkan.charAt(permasalahan_dilaporkan.length - 1),
      ) *
        2;

    let klasifikasi_laporan = '';

    if (8 >= jumlah_skor) {
      klasifikasi_laporan = 'Laporan Sederhana';
    } else if (15 >= jumlah_skor && jumlah_skor >= 9) {
      klasifikasi_laporan = 'Laporan Sedang';
    } else {
      klasifikasi_laporan = 'Laporan Berat';
    }

    return jumlah_skor;
  }
}
