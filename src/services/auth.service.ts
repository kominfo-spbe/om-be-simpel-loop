import {BindingScope, injectable} from '@loopback/core';
import {HttpErrors} from "@loopback/rest";
import {promisify} from "util";

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

@injectable({scope: BindingScope.TRANSIENT})
export class AuthService {
    constructor(/* Add @inject to inject parameters */) {
    }

    static async parseJwt(token: string) {
        var base64Payload = token.split('.')[1];
        var payload = Buffer.from(base64Payload, 'base64');
        return JSON.parse(payload.toString());
    }

    static async validateJwt(token: string | undefined): Promise<boolean> {
        let PRIV_KEY = process.env.PRIV_KEY ?? "4ppsM4n463rSecretKey";
        let isValid = false;

        try {
            if (!token)
                isValid =  false;
            else if (token) {
                isValid = await verifyAsync(token, PRIV_KEY, { algorithms: ['HS512'] });
            }
        } catch (error) {
            isValid = false;
        }
        return isValid;
    }

    static async thisThrows(message: string) {
        throw new HttpErrors.Unauthorized(
            message,
        );
    }

}
