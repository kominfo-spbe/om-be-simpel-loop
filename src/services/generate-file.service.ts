// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-file-transfer
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
    BindingScope,
    config,
    ContextTags, inject,
    injectable,
    Provider, service
} from '@loopback/core';
import multer from 'multer';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {FileUploadHandler} from '../types';
import {repository} from "@loopback/repository";
import {TxDokumenRepository} from "../repositories";
import fs from "fs";
import https from "https";
import http from "http";
import {TxDokumen} from "../models";
import {Request, RestBindings} from "@loopback/rest";
import * as process from "process";

/**
 * A provider to return an `Express` request handler from `multer` middleware
 */
@injectable({
    scope: BindingScope.TRANSIENT
})
export class GenerateFileService {
    constructor(
        @repository(TxDokumenRepository)
        public txDokumenRepository: TxDokumenRepository,
        @inject(RestBindings.Http.REQUEST) private request: Request
    ) {
    }

    public async generateDokumen(
        endpoint: string, namafile: string, idtxlaporan: string, version: number,
        nomor_agenda: string, status: string, template: string
    ) {
        return await new Promise((resolve,reject) => {
            const folderPath = './public/uploads/dokumen/' + idtxlaporan;
            if (!fs.existsSync(folderPath)) {
                fs.mkdirSync(folderPath, { recursive: true });
            }

            const headers = {...this.request.headers};
            const token = headers.authorization !== null && headers.authorization?.startsWith("Bearer") ?
                headers.authorization?.substring(7, headers.authorization.length) : undefined;

            const options = {
                hostname: process.env.REPORT_URL,
                path: `/v1/report/${endpoint}?jenisSurat=${namafile}&template=${template}&idLaporan=${idtxlaporan}&lang=en&tte=&nik=&passphrase=`,
                port: 3000,
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
            console.log("RFM TEST", options.hostname + options.path)
            const filename: string = folderPath + '/' + namafile + '_' + idtxlaporan + '_' + version + '.pdf'
            const file = fs.createWriteStream(filename)
            let tipeDokumen = template;
            if (template == null || template == "") {
                tipeDokumen = namafile
            }
            console.log("RFM TEMPLATE", tipeDokumen)

            http.get(options, res => {
                if (res.statusCode !== 200) {
                    console.log("RFM RESPONSE FAILED", res.statusCode)
                    fs.unlink(filename, () => {
                        console.log(res.statusCode);
                        // reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
                    });
                    resolve(true)
                    return;
                }
                console.log("RFM RESPONSE SUCCESS", res.statusCode)
                let headers = res.headers
                console.log("RFM RESPONSE HEADERS", headers)
                console.log("RFM RESPONSE HEADERS-PAGE", headers.page)
                let page = -1
                if (headers.page) {
                    page = parseInt((headers.page as string))
                }

                res.pipe(file);
                res.on('data', chunk => {
                    // data.push(chunk);
                });
                file.on("finish", async () => {
                    file.close();
                    // @ts-ignore
                    const dataTxDokumen: TxDokumen = {
                        filename,
                        id_tx_laporan: idtxlaporan,
                        tipe_dokumen: tipeDokumen,
                        nomor_agenda: nomor_agenda,
                        status_dokumen: status,
                        version,
                        page: page
                    }

                    console.log("RFM RESPONSE", "PREPARE INSERT DOKUMEN", dataTxDokumen)

                    let data = await this.txDokumenRepository.save(dataTxDokumen)
                    console.log("RFM RESPONSE", "SUCCESS INSERT DOKUMEN", data)
                    resolve(true);
                });
            })
                .on("error", (e) => {
                    console.log("RFM RESPONSE", e.message)
                    reject(false);
                });
        });
    };

}
