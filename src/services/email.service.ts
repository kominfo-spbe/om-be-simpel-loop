// inside src/services/email.service.ts
import {bind, BindingScope} from '@loopback/core';
import {createTransport} from 'nodemailer';
import SMTPTransport from "nodemailer/lib/smtp-transport";
import {EmailTemplate, TxLaporanCombined, TxOtp} from '../models';


@bind({scope: BindingScope.TRANSIENT})
export class EmailService {
    /**
     * If using gmail see https://nodemailer.com/usage/using-gmail/
     */
    private static async setupTransporter() {
        let secure = true
        if(+process.env.SMTP_PORT! != 465)
            secure = false
        return createTransport({
            host: process.env.SMTP_SERVER,
            port: +process.env.SMTP_PORT!,
            secure, // upgrade later with STARTTLS
            // secure: true,
            tls: {rejectUnauthorized: false},
            auth: {
                user: process.env.SMTP_USERNAME,
                pass: process.env.SMTP_PASSWORD,
            },
        });
    }

    async sendReportMail(txLaporan: TxLaporanCombined): Promise<SMTPTransport.SentMessageInfo> {
        const transporter = await EmailService.setupTransporter();

        let createdDate = null
        if (txLaporan.created_date != null) {
            createdDate = new Date(txLaporan.created_date).toLocaleDateString()
        }
        const emailTemplate = new EmailTemplate({
            to: txLaporan.email_pelapor,
            subject: 'Laporan #' + createdDate,
            html: `
      <div>
          <p>Yth. Sdr/i ${txLaporan.nama_pelapor}</p>
          <!--<p style="color: red;">Terimakasih telah menyampaikan ke ombudsman</p>-->
          <p>Selamat datang,</p>
          <p>Terima Kasih telah menyampaikan laporannya pada Aplikasi Simpel Ombudsman.</p>
          <p>Berikut data laporan Anda : </p>
          <p>Nama Pelapor : ${txLaporan.nama_pelapor}</p>
          <p>Kewarganegaraan : ${txLaporan.warga_negara_pelapor_name}</p>
          <p>Alamat Pelapor : ${txLaporan.alamat_lengkap_pelapor}</p>
          <p>Provinsi: ${txLaporan.provinsi_pelapor_name}</p>
          <p>Kabupaten/Kota : ${txLaporan.kab_kota_pelapor_name}</p>
          <p>Kecamatan : ${txLaporan.kec_pelapor_name}</p>
          <p>Telp : ${txLaporan.no_telp_pelapor}</p>
          <br/>
          <p>Kelompok Instansi Terlapor : ${txLaporan.kelompok_instansi_terlapor_name}</p>
          <p>Klasifikasi Instansi Terlapor : ${txLaporan.klasifikasi_instansi_terlapor_name}</p>
          <p>Nama Instansi : ${txLaporan.nama_instansi_terlapor}</p>
          <p>Alamat Terlapor: ${txLaporan.alamat_terlapor}</p>
          <p>Provinsi: ${txLaporan.provinsi_terlapor_name}</p>
          <p>Kabupaten/Kota : ${txLaporan.kab_kota_terlapor_name}</p>
          <p>Kecamatan : ${txLaporan.kec_terlapor_name}</p>
          <p>Perihal : ${txLaporan.perihal}</p>

          <br/>
          Saat ini laporan Anda sudah masuk dan dalam proses pengecekan Petugas Ombudsman.
          <br/>

          <p>Salam,</p>
          <p>Ombudsman</p>
      </div>
      `,
        });
        return transporter.sendMail(emailTemplate);
    }

    async sendOtpMail(txOtp: TxOtp, email?: string): Promise<SMTPTransport.SentMessageInfo> {
        const transporter = await EmailService.setupTransporter();

        const emailTemplate = new EmailTemplate({
            to: email,
            subject: 'OTP #' + txOtp.tracking_id,
            html: `
      <div>
          <p>Berikut OTP untuk laporan # ${txOtp.tracking_id} : ${txOtp.otp}</p>
      </div>
      `,
        });
        return transporter.sendMail(emailTemplate);
    }
}
