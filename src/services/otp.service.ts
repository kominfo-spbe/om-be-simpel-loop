import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import {repository} from "@loopback/repository";
import {TxLaporanRepository} from "../repositories";

@injectable({scope: BindingScope.TRANSIENT})
export class OtpService {
    constructor(/* Add @inject to inject parameters */
                @repository(TxLaporanRepository)
                public txLaporanRepository : TxLaporanRepository,
    ) {}

    /*
     * Add service methods here
     */
    generateOtp = () => {
        var digits = '0123456789';
        let OTP = '';
        for (let i = 0; i < 6; i++ ) {
            OTP += digits[Math.floor(Math.random() * 10)];
        }

        return OTP;
    }
}
