import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import {repository} from "@loopback/repository";
import {TxLaporanRepository} from "../repositories";

@injectable({scope: BindingScope.TRANSIENT})
export class LogService {
  constructor(/* Add @inject to inject parameters */
              @repository(TxLaporanRepository)
              public txLaporanRepository : TxLaporanRepository,
  ) {}

  /*
   * Add service methods here
   */
  sendToLog = async (obj: any) => {

    obj.data_lama = JSON.stringify(obj.data_lama)
    obj.data_baru = JSON.stringify(obj.data_baru)

    return this.txLaporanRepository.create(obj)

  }
}
