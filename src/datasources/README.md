# Datasources

This directory contains config for datasources used by this app.

Log History:

2021-09-07
1. Tambah relasi table materiil_kronologi ke table materill (foreign key)
   'ALTER TABLE "simpel_4"."tx_verif_materiil_kronologi"
   ADD FOREIGN KEY ("id_tx_laporan") REFERENCES "simpel_4"."tx_laporan" ("id");
   ADD FOREIGN KEY ("id_tx_verif_materiil") REFERENCES "simpel_4"."tx_verif_materiil" ("id");'
2. Tambah relasi table materill ke table laporan
   'ALTER TABLE "simpel_4"."tx_verif_materiil"
   ADD FOREIGN KEY ("id_tx_laporan") REFERENCES "simpel_4"."tx_laporan" ("id");'
3. Tambah kolom kurang tanggal_kronologi di table verif_materiil
   'ALTER TABLE "simpel_4"."tx_verif_materiil_kronologi"
   ADD COLUMN "tanggal_kronologi" date;'
