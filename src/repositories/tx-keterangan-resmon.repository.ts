import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxKeteranganResmon, TxKeteranganResmonRelations} from "../models/tx-keterangan-resmon.model";

export class TxKeteranganResmonRepository extends DefaultCrudRepository<
  TxKeteranganResmon,
  typeof TxKeteranganResmon.prototype.id,
    TxKeteranganResmonRelations
> {

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxKeteranganResmon, dataSource);

  }
}
