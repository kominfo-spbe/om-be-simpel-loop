import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, HasManyRepositoryFactory, HasOneRepositoryFactory, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {
  TdUser,
  TxChangeStatus,
  TxDisposisi,
  TxDisposisiPvlKaper,
  TxDisposisiPvlKapten,
  TxKlasifikasiLm,
  TxKlasifikasiLmRiksa,
  TxLaporan,
  TxLaporanPerubahan,
  TxLaporanRelations,
  TxPleno, TxRiksa, TxRiksaPelimpahanResmon, TxTakeOver,
  TxVerifFormil,
  TxVerifMateriil
} from '../models';
import {
  TdUserRepository,
  TxChangeStatusRepository,
  TxDisposisiPvlKaperRepository,
  TxDisposisiPvlKaptenRepository,
  TxDisposisiRepository,
  TxKlasifikasiLmRepository,
  TxKlasifikasiLmRiksaRepository,
  TxLaporanPerubahanRepository,
  TxPlenoRepository, TxRiksaRepository, TxTakeOverRepository,
  TxVerifFormilRepository,
  TxVerifMateriilRepository
} from '../repositories';
import {TxRiksaPelimpahanResmonRepository} from './tx-riksa-pelimpahan-resmon.repository';

// import {TxChangeStatusRepository} from './tx-change-status.repository';
// import {TxDisposisiPvlKaperRepository} from "./tx-disposisi-pvl-kaper.repository";
// import {TxDisposisiPvlKaptenRepository} from "./tx-disposisi-pvl-kapten.repository";
// import {TxKlasifikasiLmRepository} from "./tx-klasifikasi-lm.repository";
// import {TxLaporanPerubahanRepository} from "./tx-laporan-perubahan.repository";
// import {TxPlenoRepository} from "./tx-pleno.repository";
// import {TxTakeOverRepository} from "./tx-take-over.repository";
// import {TxVerifFormilRepository} from './tx-verif-formil.repository';
// import {TxVerifMateriilRepository} from "./tx-verif-materiil.repository";

export class TxLaporanRepository extends DefaultCrudRepository<
  TxLaporan,
  typeof TxLaporan.prototype.id,
  TxLaporanRelations
> {

  public readonly txVerifFormil: HasOneRepositoryFactory<TxVerifFormil, typeof TxLaporan.prototype.id>;
  public readonly txVerifMateriil: HasOneRepositoryFactory<TxVerifMateriil, typeof TxLaporan.prototype.id>;
  public readonly txKlasifikasiLm: HasOneRepositoryFactory<TxKlasifikasiLm, typeof TxLaporan.prototype.id>;
  public readonly txKlasifikasiLmRiksa: HasOneRepositoryFactory<TxKlasifikasiLmRiksa, typeof TxLaporan.prototype.id>;
  public readonly txPleno: HasOneRepositoryFactory<TxPleno, typeof TxLaporan.prototype.id>;
  public readonly txLaporanPerubahans: HasManyRepositoryFactory<TxLaporanPerubahan, typeof TxLaporan.prototype.id>;
  public readonly txChangeStatuses: HasManyRepositoryFactory<TxChangeStatus, typeof TxLaporan.prototype.id>;
  public readonly txTakeOvers: HasManyRepositoryFactory<TxTakeOver, typeof TxLaporan.prototype.id>;
  public readonly txDisposisiPvlKaper: HasOneRepositoryFactory<TxDisposisiPvlKaper, typeof TxLaporan.prototype.id>;
  public readonly txDisposisiPvlKapten: HasOneRepositoryFactory<TxDisposisiPvlKapten, typeof TxLaporan.prototype.id>;
  public readonly txDisposisi: HasOneRepositoryFactory<TxDisposisi, typeof TxLaporan.prototype.id>;
  public readonly UserDetail: BelongsToAccessor<TdUser, typeof TxLaporan.prototype.id>;
  public readonly txRiksaPelimpahanResmon: HasOneRepositoryFactory<TxRiksaPelimpahanResmon, typeof TxLaporan.prototype.id>;
  public readonly txRiksa: HasOneRepositoryFactory<TxRiksa, typeof TxLaporan.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('TxVerifFormilRepository') protected txVerifFormilRepositoryGetter: Getter<TxVerifFormilRepository>,
    @repository.getter('TxVerifMateriilRepository') protected txVerifMateriilRepositoryGetter: Getter<TxVerifMateriilRepository>,
    @repository.getter('TxKlasifikasiLmRepository') protected txKlasifikasiLmRepositoryGetter: Getter<TxKlasifikasiLmRepository>,
    @repository.getter('TxKlasifikasiLmRiksaRepository') protected txKlasifikasiLmRiksaRepositoryGetter: Getter<TxKlasifikasiLmRiksaRepository>,
    @repository.getter('TxPlenoRepository') protected txPlenoRepositoryGetter: Getter<TxPlenoRepository>,
    @repository.getter('TxLaporanPerubahanRepository') protected txLaporanPerubahanRepositoryGetter: Getter<TxLaporanPerubahanRepository>,
    @repository.getter('TxChangeStatusRepository') protected txChangeStatusRepositoryGetter: Getter<TxChangeStatusRepository>,
    @repository.getter('TxTakeOverRepository') protected txTakeOverRepositoryGetter: Getter<TxTakeOverRepository>,
    @repository.getter('TxDisposisiPvlKaperRepository') protected txDisposisiPvlKaperRepositoryGetter: Getter<TxDisposisiPvlKaperRepository>,
    @repository.getter('TxDisposisiPvlKaptenRepository') protected txDisposisiPvlKaptenRepositoryGetter: Getter<TxDisposisiPvlKaptenRepository>,
    @repository.getter('TxDisposisiRepository') protected txDisposisiRepositoryGetter: Getter<TxDisposisiRepository>,
    @repository.getter('TdUserRepository') protected tdUserRepositoryGetter: Getter<TdUserRepository>,
    @repository.getter('TxRiksaPelimpahanResmonRepository') protected txRiksaPelimpahanResmonRepositoryGetter: Getter<TxRiksaPelimpahanResmonRepository>,
    @repository.getter('TxRiksaRepository') protected txRiksaRepositoryGetter: Getter<TxRiksaRepository>,
  ) {
    super(TxLaporan, dataSource);
    this.txRiksaPelimpahanResmon = this.createHasOneRepositoryFactoryFor('txRiksaPelimpahanResmon', txRiksaPelimpahanResmonRepositoryGetter);
    this.registerInclusionResolver('txRiksaPelimpahanResmon', this.txRiksaPelimpahanResmon.inclusionResolver);
    this.txChangeStatuses = this.createHasManyRepositoryFactoryFor('txChangeStatuses', txChangeStatusRepositoryGetter,);
    this.registerInclusionResolver('txChangeStatuses', this.txChangeStatuses.inclusionResolver);
    this.txVerifFormil = this.createHasOneRepositoryFactoryFor('txVerifFormil', txVerifFormilRepositoryGetter);
    this.registerInclusionResolver('txVerifFormil', this.txVerifFormil.inclusionResolver);
    this.txVerifMateriil = this.createHasOneRepositoryFactoryFor('txVerifMateriil', txVerifMateriilRepositoryGetter);
    this.registerInclusionResolver('txVerifMateriil', this.txVerifMateriil.inclusionResolver);
    this.txKlasifikasiLm = this.createHasOneRepositoryFactoryFor('txKlasifikasiLm', txKlasifikasiLmRepositoryGetter);
    this.registerInclusionResolver('txKlasifikasiLm', this.txKlasifikasiLm.inclusionResolver);
    this.txKlasifikasiLmRiksa = this.createHasOneRepositoryFactoryFor('txKlasifikasiLmRiksa', txKlasifikasiLmRiksaRepositoryGetter);
    this.registerInclusionResolver('txKlasifikasiLmRiksa', this.txKlasifikasiLmRiksa.inclusionResolver);
    this.txPleno = this.createHasOneRepositoryFactoryFor('txPleno', txPlenoRepositoryGetter);
    this.registerInclusionResolver('txPleno', this.txPleno.inclusionResolver);
    this.txLaporanPerubahans = this.createHasManyRepositoryFactoryFor('txLaporanPerubahans', txLaporanPerubahanRepositoryGetter);
    this.registerInclusionResolver('txLaporanPerubahans', this.txLaporanPerubahans.inclusionResolver);
    this.txTakeOvers = this.createHasManyRepositoryFactoryFor('txTakeOvers', txTakeOverRepositoryGetter);
    this.registerInclusionResolver('txTakeOvers', this.txTakeOvers.inclusionResolver);
    this.txDisposisiPvlKaper = this.createHasOneRepositoryFactoryFor('txDisposisiPvlKaper', txDisposisiPvlKaperRepositoryGetter);
    this.registerInclusionResolver('txDisposisiPvlKaper', this.txDisposisiPvlKaper.inclusionResolver);
    this.txDisposisiPvlKapten = this.createHasOneRepositoryFactoryFor('txDisposisiPvlKapten', txDisposisiPvlKaptenRepositoryGetter);
    this.registerInclusionResolver('txDisposisiPvlKapten', this.txDisposisiPvlKapten.inclusionResolver);
    this.txDisposisi = this.createHasOneRepositoryFactoryFor('txDisposisi', txDisposisiRepositoryGetter);
    this.registerInclusionResolver('txDisposisi', this.txDisposisi.inclusionResolver);
    this.UserDetail = this.createBelongsToAccessorFor('UserDetail', tdUserRepositoryGetter,);
    this.registerInclusionResolver('UserDetail', this.UserDetail.inclusionResolver);
    this.txRiksa = this.createHasOneRepositoryFactoryFor('txRiksa', txRiksaRepositoryGetter);
    this.registerInclusionResolver('txRiksa', this.txRiksa.inclusionResolver);
  }
}
