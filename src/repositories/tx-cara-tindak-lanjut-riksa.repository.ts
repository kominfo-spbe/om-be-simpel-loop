import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TdUser, TxCaraTindakLanjutRiksa, TxCaraTindakLanjutRiksaRelations, TxLaporan} from '../models';
import {TdUserRepository} from "./td-user.repository";

export class TxCaraTindakLanjutRiksaRepository extends DefaultCrudRepository<
  TxCaraTindakLanjutRiksa,
  typeof TxCaraTindakLanjutRiksa.prototype.id,
  TxCaraTindakLanjutRiksaRelations
> {

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxCaraTindakLanjutRiksa, dataSource);

  }
}
