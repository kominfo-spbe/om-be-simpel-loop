import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDisposisi, TxDisposisiRelations, TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';

export class TxDisposisiRepository extends DefaultCrudRepository<
  TxDisposisi,
  typeof TxDisposisi.prototype.id,
  TxDisposisiRelations
> {

  public readonly txLaporan: BelongsToAccessor<TxLaporan, typeof TxDisposisi.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxDisposisi, dataSource);
    this.txLaporan = this.createBelongsToAccessorFor('txLaporan', txLaporanRepositoryGetter,);
    this.registerInclusionResolver('txLaporan', this.txLaporan.inclusionResolver);
  }
}
