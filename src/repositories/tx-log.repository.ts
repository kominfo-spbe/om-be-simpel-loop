import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxLog} from '../models';

export class TxLogRepository extends DefaultCrudRepository<
  TxLog,
  typeof TxLog.prototype.id_tx_log
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxLog, dataSource);
  }
}
