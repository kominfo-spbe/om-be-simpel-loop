import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxChangeStatus, TxChangeStatusRelations, TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';

export class TxChangeStatusRepository extends DefaultCrudRepository<
  TxChangeStatus,
  typeof TxChangeStatus.prototype.id,
  TxChangeStatusRelations
> {

  public readonly id: BelongsToAccessor<TxLaporan, typeof TxChangeStatus.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxChangeStatus, dataSource);
    this.id = this.createBelongsToAccessorFor('id', txLaporanRepositoryGetter,);
    this.registerInclusionResolver('id', this.id.inclusionResolver);
  }
}
