import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDokumen, TxDokumenTte,TxDokumenRelations} from '../models';
import {TxDokumenTteRepository} from './tx-dokumen-tte.repository';

export class TxDokumenRepository extends DefaultCrudRepository<
    TxDokumen,
    typeof TxDokumen.prototype.id,
    TxDokumenRelations
    > {

    public readonly TxDokumenTte: HasManyRepositoryFactory<TxDokumenTte, typeof TxDokumen.prototype.id>;

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
        @repository.getter('TxDokumenTteRepository') protected txDokumenTteRepositoryGetter: Getter<TxDokumenTteRepository>,
    ) {
        super(TxDokumen, dataSource);
        this.TxDokumenTte = this.createHasManyRepositoryFactoryFor('TxDokumenTte', txDokumenTteRepositoryGetter,);
        this.registerInclusionResolver('TxDokumenTte', this.TxDokumenTte.inclusionResolver);
    }
}
