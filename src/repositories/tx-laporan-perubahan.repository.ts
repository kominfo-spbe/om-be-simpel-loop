import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxLaporanPerubahan, TxLaporanPerubahanRelations} from '../models';

export class TxLaporanPerubahanRepository extends DefaultCrudRepository<
  TxLaporanPerubahan,
  typeof TxLaporanPerubahan.prototype.id,
  TxLaporanPerubahanRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxLaporanPerubahan, dataSource);
  }
}
