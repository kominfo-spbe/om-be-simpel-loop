import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDisposisiResmon, TxDisposisiResmonRelations} from '../models';

export class TxDisposisiResmonRepository extends DefaultCrudRepository<
  TxDisposisiResmon,
  typeof TxDisposisiResmon.prototype.id,
  TxDisposisiResmonRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxDisposisiResmon, dataSource);
  }
}
