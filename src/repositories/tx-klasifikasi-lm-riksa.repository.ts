import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {MLookup, TxKlasifikasiLmRiksa, TxKlasifikasiLmRiksaRelations} from '../models';
import {MLookupRepository} from './m-lookup.repository';

export class TxKlasifikasiLmRiksaRepository extends DefaultCrudRepository<
  TxKlasifikasiLmRiksa,
  typeof TxKlasifikasiLmRiksa.prototype.id,
  TxKlasifikasiLmRiksaRelations
> {

  public readonly PermasalahanLapor: BelongsToAccessor<MLookup, typeof TxKlasifikasiLmRiksa.prototype.id>;

  public readonly KlasifikasiLapor: BelongsToAccessor<MLookup, typeof TxKlasifikasiLmRiksa.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('MLookupRepository') protected mLookupRepositoryGetter: Getter<MLookupRepository>,
  ) {
    super(TxKlasifikasiLmRiksa, dataSource);
  }
}
