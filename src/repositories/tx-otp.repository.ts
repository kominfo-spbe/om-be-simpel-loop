import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxOtp} from '../models';

export class TxOtpRepository extends DefaultCrudRepository<
    TxOtp,
    typeof TxOtp.prototype.id
    > {

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
    ) {
        super(TxOtp, dataSource);

    }
}
