import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxVerifFormil, TxVerifFormilRelations, TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';

export class TxVerifFormilRepository extends DefaultCrudRepository<
  TxVerifFormil,
  typeof TxVerifFormil.prototype.id,
  TxVerifFormilRelations
> {

  public readonly id: BelongsToAccessor<TxLaporan, typeof TxVerifFormil.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxVerifFormil, dataSource);
    this.id = this.createBelongsToAccessorFor('id', txLaporanRepositoryGetter,);
    this.registerInclusionResolver('id', this.id.inclusionResolver);
  }
}
