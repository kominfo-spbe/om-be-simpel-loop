import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {MLookup, TxKlasifikasiLm, TxKlasifikasiLmRelations} from '../models';
import {MLookupRepository} from './m-lookup.repository';

export class TxKlasifikasiLmRepository extends DefaultCrudRepository<
  TxKlasifikasiLm,
  typeof TxKlasifikasiLm.prototype.id,
  TxKlasifikasiLmRelations
> {

  public readonly PermasalahanLapor: BelongsToAccessor<MLookup, typeof TxKlasifikasiLm.prototype.id>;

  public readonly KlasifikasiLapor: BelongsToAccessor<MLookup, typeof TxKlasifikasiLm.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('MLookupRepository') protected mLookupRepositoryGetter: Getter<MLookupRepository>,
  ) {
    super(TxKlasifikasiLm, dataSource);
  }
}
