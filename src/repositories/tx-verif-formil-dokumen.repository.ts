import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxVerifFormilDokumen, TxVerifFormilDokumenRelations} from '../models';

export class TxVerifFormilDokumenRepository extends DefaultCrudRepository<
  TxVerifFormilDokumen,
  typeof TxVerifFormilDokumen.prototype.id,
  TxVerifFormilDokumenRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxVerifFormilDokumen, dataSource);
  }
}
