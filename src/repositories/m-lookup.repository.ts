import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {MLookup, MLookupRelations} from '../models';

export class MLookupRepository extends DefaultCrudRepository<
  MLookup,
  typeof MLookup.prototype.lookupId,
  MLookupRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(MLookup, dataSource);
  }
}
