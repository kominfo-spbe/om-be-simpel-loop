import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxVerifMateriilDokumen, TxVerifMateriilDokumenRelations} from '../models';

export class TxVerifMateriilDokumenRepository extends DefaultCrudRepository<
  TxVerifMateriilDokumen,
  typeof TxVerifMateriilDokumen.prototype.id,
  TxVerifMateriilDokumenRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxVerifMateriilDokumen, dataSource);
  }
}
