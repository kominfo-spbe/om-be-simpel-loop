import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxManajemenDokumen} from '../models';

export class TxManajemenDokumenRepository extends DefaultCrudRepository<
    TxManajemenDokumen,
    typeof TxManajemenDokumen.prototype.id
    > {

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
    ) {
        super(TxManajemenDokumen, dataSource);

    }
}
