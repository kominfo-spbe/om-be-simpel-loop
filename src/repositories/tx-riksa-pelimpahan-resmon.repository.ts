import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxRiksaPelimpahanResmon, TxRiksaPelimpahanResmonRelations} from '../models';

export class TxRiksaPelimpahanResmonRepository extends DefaultCrudRepository<
  TxRiksaPelimpahanResmon,
  typeof TxRiksaPelimpahanResmon.prototype.id,
  TxRiksaPelimpahanResmonRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxRiksaPelimpahanResmon, dataSource);
  }
}
