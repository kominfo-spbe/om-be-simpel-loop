import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxPengalihan, TxPengalihanRelations} from '../models';

export class TxPengalihanRepository extends DefaultCrudRepository<
  TxPengalihan,
  typeof TxPengalihan.prototype.id,
  TxPengalihanRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxPengalihan, dataSource);
  }
}
