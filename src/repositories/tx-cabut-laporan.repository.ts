import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxCabutLaporan, TxCabutLaporanRelations} from '../models';

export class TxCabutLaporanRepository extends DefaultCrudRepository<
  TxCabutLaporan,
  typeof TxCabutLaporan.prototype.id,
  TxCabutLaporanRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxCabutLaporan, dataSource);
  }
}
