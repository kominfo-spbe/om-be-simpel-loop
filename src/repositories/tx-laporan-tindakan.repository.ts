import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';
import {TxLaporanTindakan, TxLaporanTindakanRelations} from "../models";

export class TxLaporanTindakanRepository extends DefaultCrudRepository<
  TxLaporanTindakan,
  typeof TxLaporanTindakan.prototype.id,
  TxLaporanTindakanRelations
> {

  // public readonly txLaporan: BelongsToAccessor<TxLaporan, typeof TxLaporanTindakan.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    // @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxLaporanTindakan, dataSource);
    // this.txLaporan = this.createBelongsToAccessorFor('txLaporan', txLaporanRepositoryGetter,);
    // this.registerInclusionResolver('txLaporan', this.txLaporan.inclusionResolver);
  }
}
