import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDisposisiPvlKapten, TxDisposisiPvlKaptenRelations} from '../models';

export class TxDisposisiPvlKaptenRepository extends DefaultCrudRepository<
  TxDisposisiPvlKapten,
  typeof TxDisposisiPvlKapten.prototype.id,
  TxDisposisiPvlKaptenRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxDisposisiPvlKapten, dataSource);
  }
}
