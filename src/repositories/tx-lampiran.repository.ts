import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxLampiran, TxLampiranRelations} from '../models';

export class TxLampiranRepository extends DefaultCrudRepository<
  TxLampiran,
  typeof TxLampiran.prototype.id,
  TxLampiranRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxLampiran, dataSource);
  }
}
