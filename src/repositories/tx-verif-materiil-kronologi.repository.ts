import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxVerifMateriilKronologi, TxVerifMateriilKronologiRelations} from '../models';

export class TxVerifMateriilKronologiRepository extends DefaultCrudRepository<
  TxVerifMateriilKronologi,
  typeof TxVerifMateriilKronologi.prototype.id,
  TxVerifMateriilKronologiRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxVerifMateriilKronologi, dataSource);
  }
}
