import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxLaporanTanggapan, TxLaporanTanggapanRelations} from '../models';

export class TxLaporanTanggapanRepository extends DefaultCrudRepository<
  TxLaporanTanggapan,
  typeof TxLaporanTanggapan.prototype.id,
    TxLaporanTanggapanRelations
> {

  // public readonly txLaporan: BelongsToAccessor<TxLaporan, typeof TxLaporanTindakan.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    // @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxLaporanTanggapan, dataSource);
    // this.txLaporan = this.createBelongsToAccessorFor('txLaporan', txLaporanRepositoryGetter,);
    // this.registerInclusionResolver('txLaporan', this.txLaporan.inclusionResolver);
  }
}
