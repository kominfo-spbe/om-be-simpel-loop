import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TdUser, TxHistoryStatusLaporan, TxHistoryStatusLaporanRelations, TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';
import {TdUserRepository} from "./td-user.repository";

export class TxHistoryStatusLaporanRepository extends DefaultCrudRepository<
    TxHistoryStatusLaporan,
    typeof TxHistoryStatusLaporan.prototype.id,
    TxHistoryStatusLaporanRelations
    > {

    public readonly id: BelongsToAccessor<TxLaporan, typeof TxHistoryStatusLaporan.prototype.id>;
    public readonly UserDetail: BelongsToAccessor<TdUser, typeof TxLaporan.prototype.id>;

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
        @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
        @repository.getter('TdUserRepository') protected tdUserRepositoryGetter: Getter<TdUserRepository>,
    ) {
        super(TxHistoryStatusLaporan, dataSource);
        this.id = this.createBelongsToAccessorFor('id', txLaporanRepositoryGetter,);
        this.registerInclusionResolver('id', this.id.inclusionResolver);
        this.UserDetail = this.createBelongsToAccessorFor('UserDetail', tdUserRepositoryGetter,);
        this.registerInclusionResolver('UserDetail', this.UserDetail.inclusionResolver);
    }
}
