import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxRiksaTindaklanjut, TxRiksaTindaklanjutRelations} from '../models';

export class TxRiksaTindaklanjutRepository extends DefaultCrudRepository<
  TxRiksaTindaklanjut,
  typeof TxRiksaTindaklanjut.prototype.id,
  TxRiksaTindaklanjutRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxRiksaTindaklanjut, dataSource);
  }
}
