import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxProsesResmon, TxProsesResmonRelations} from '../models';

export class TxProsesResmonRepository extends DefaultCrudRepository<
  TxProsesResmon,
  typeof TxProsesResmon.prototype.id,
  TxProsesResmonRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxProsesResmon, dataSource);
  }
}
