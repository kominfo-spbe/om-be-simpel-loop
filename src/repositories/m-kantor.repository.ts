import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {MKantor, MKantorRelations} from '../models';

export class MKantorRepository extends DefaultCrudRepository<
  MKantor,
  typeof MKantor.prototype.kode_kantor,
  MKantorRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(MKantor, dataSource);
  }
}
