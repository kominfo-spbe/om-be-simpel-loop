import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxFileUpload} from '../models';

export class TxFileUploadRepository extends DefaultCrudRepository<
  TxFileUpload,
    typeof TxFileUpload.prototype.id
    > {

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
    ) {
        super(TxFileUpload, dataSource);

    }
}
