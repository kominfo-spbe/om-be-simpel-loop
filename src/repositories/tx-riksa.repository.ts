import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxRiksa, TxRiksaRelations, TxLaporan} from '../models';
import {TxLaporanRepository} from './tx-laporan.repository';

export class TxRiksaRepository extends DefaultCrudRepository<
  TxRiksa,
  typeof TxRiksa.prototype.id,
  TxRiksaRelations
> {

  public readonly txLaporan: BelongsToAccessor<TxLaporan, typeof TxRiksa.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('TxLaporanRepository') protected txLaporanRepositoryGetter: Getter<TxLaporanRepository>,
  ) {
    super(TxRiksa, dataSource);
    this.txLaporan = this.createBelongsToAccessorFor('txLaporan', txLaporanRepositoryGetter,);
    this.registerInclusionResolver('txLaporan', this.txLaporan.inclusionResolver);
  }
}
