import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {MPelapor, MPelaporRelations} from '../models';

export class MPelaporRepository extends DefaultCrudRepository<
  MPelapor,
  typeof MPelapor.prototype.id,
  MPelaporRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(MPelapor, dataSource);
  }
}
