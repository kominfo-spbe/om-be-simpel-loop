import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDokumenResmon, TxDokumenResmonRelations} from '../models';

export class TxDokumenResmonRepository extends DefaultCrudRepository<
  TxDokumenResmon,
  typeof TxDokumenResmon.prototype.id,
  TxDokumenResmonRelations
> {

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxDokumenResmon, dataSource);

  }
}
