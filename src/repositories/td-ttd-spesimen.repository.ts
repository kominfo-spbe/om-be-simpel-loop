import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TdTtdSpesimen, TdTtdSpesimenRelations} from '../models';
import {TdUserRepository} from "./td-user.repository";

export class TdTtdSpesimenRepository extends DefaultCrudRepository<
    TdTtdSpesimen,
    typeof TdTtdSpesimen.prototype.id,
    TdTtdSpesimenRelations
    > {

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
    ) {
        super(TdTtdSpesimen, dataSource);

    }
}
