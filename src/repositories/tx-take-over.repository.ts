import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxTakeOver, TxTakeOverRelations} from '../models';

export class TxTakeOverRepository extends DefaultCrudRepository<
  TxTakeOver,
  typeof TxTakeOver.prototype.id,
  TxTakeOverRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxTakeOver, dataSource);
  }
}
