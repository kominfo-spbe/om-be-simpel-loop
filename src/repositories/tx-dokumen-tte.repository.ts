import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDokumenTte} from '../models';

export class TxDokumenTteRepository extends DefaultCrudRepository<
    TxDokumenTte,
    typeof TxDokumenTte.prototype.id
    > {

    constructor(
        @inject('datasources.db') dataSource: DbDataSource,
    ) {
        super(TxDokumenTte, dataSource);

    }
}
