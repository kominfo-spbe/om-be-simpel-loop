import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxSettingProsesResmon, TxSettingProsesResmonRelations} from '../models';

export class TxSettingProsesResmonRepository extends DefaultCrudRepository<
  TxSettingProsesResmon,
  typeof TxSettingProsesResmon.prototype.id,
  TxSettingProsesResmonRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxSettingProsesResmon, dataSource);
  }
}
