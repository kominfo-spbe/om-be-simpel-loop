import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxRiksaMonitoring, TxRiksaMonitoringRelations} from '../models';

export class TxRiksaMonitoringRepository extends DefaultCrudRepository<
  TxRiksaMonitoring,
  typeof TxRiksaMonitoring.prototype.id,
  TxRiksaMonitoringRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxRiksaMonitoring, dataSource);
  }
}
