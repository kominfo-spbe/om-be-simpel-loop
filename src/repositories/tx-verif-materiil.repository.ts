import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxVerifMateriil, TxVerifMateriilDokumen, TxVerifMateriilKronologi, TxVerifMateriilRelations} from '../models';
import {MLookupRepository} from './m-lookup.repository';
import {TxVerifMateriilDokumenRepository} from './tx-verif-materiil-dokumen.repository';
import {TxVerifMateriilKronologiRepository} from './tx-verif-materiil-kronologi.repository';

export class TxVerifMateriilRepository extends DefaultCrudRepository<
  TxVerifMateriil,
  typeof TxVerifMateriil.prototype.id,
  TxVerifMateriilRelations
> {

  public readonly TxVerifMateriilKronologi: HasManyRepositoryFactory<TxVerifMateriilKronologi, typeof TxVerifMateriil.prototype.id>;
  public readonly TxVerifMateriilDokumen: HasManyRepositoryFactory<TxVerifMateriilDokumen, typeof TxVerifMateriil.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('MLookupRepository') protected mLookupRepositoryGetter: Getter<MLookupRepository>, @repository.getter('TxVerifMateriilKronologiRepository') protected txVerifMateriilKronologiRepositoryGetter: Getter<TxVerifMateriilKronologiRepository>, @repository.getter('TxVerifMateriilDokumenRepository') protected txVerifMateriilDokumenRepositoryGetter: Getter<TxVerifMateriilDokumenRepository>,
  ) {
    super(TxVerifMateriil, dataSource);
    this.TxVerifMateriilKronologi = this.createHasManyRepositoryFactoryFor('TxVerifMateriilKronologi', txVerifMateriilKronologiRepositoryGetter,);
    this.registerInclusionResolver('TxVerifMateriilKronologi', this.TxVerifMateriilKronologi.inclusionResolver);
    this.TxVerifMateriilDokumen = this.createHasManyRepositoryFactoryFor('TxVerifMateriilDokumen', txVerifMateriilDokumenRepositoryGetter,);
    this.registerInclusionResolver('TxVerifMateriilDokumen', this.TxVerifMateriilDokumen.inclusionResolver);
  }
}

export class TxVerifMateriilRepositoryIdLaporan extends DefaultCrudRepository<
  TxVerifMateriil,
  typeof TxVerifMateriil.prototype.id_tx_laporan,
  TxVerifMateriilRelations
> {

  public readonly TxVerifMateriilKronologi: HasManyRepositoryFactory<TxVerifMateriilKronologi, typeof TxVerifMateriil.prototype.id>;
  public readonly TxVerifMateriilDokumen: HasManyRepositoryFactory<TxVerifMateriilDokumen, typeof TxVerifMateriil.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('MLookupRepository') protected mLookupRepositoryGetter: Getter<MLookupRepository>, @repository.getter('TxVerifMateriilKronologiRepository') protected txVerifMateriilKronologiRepositoryGetter: Getter<TxVerifMateriilKronologiRepository>, @repository.getter('TxVerifMateriilDokumenRepository') protected txVerifMateriilDokumenRepositoryGetter: Getter<TxVerifMateriilDokumenRepository>,
  ) {
    super(TxVerifMateriil, dataSource);
    this.TxVerifMateriilKronologi = this.createHasManyRepositoryFactoryFor('TxVerifMateriilKronologi', txVerifMateriilKronologiRepositoryGetter,);
    this.registerInclusionResolver('TxVerifMateriilKronologi', this.TxVerifMateriilKronologi.inclusionResolver);
    this.TxVerifMateriilDokumen = this.createHasManyRepositoryFactoryFor('TxVerifMateriilDokumen', txVerifMateriilDokumenRepositoryGetter,);
    this.registerInclusionResolver('TxVerifMateriilDokumen', this.TxVerifMateriilDokumen.inclusionResolver);
  }
}
