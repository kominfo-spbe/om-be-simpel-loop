import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxDisposisiPvlKaper, TxDisposisiPvlKaperRelations} from '../models';

export class TxDisposisiPvlKaperRepository extends DefaultCrudRepository<
  TxDisposisiPvlKaper,
  typeof TxDisposisiPvlKaper.prototype.id,
  TxDisposisiPvlKaperRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxDisposisiPvlKaper, dataSource);
  }
}
