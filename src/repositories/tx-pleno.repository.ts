import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TxPleno, TxPlenoRelations} from '../models';

export class TxPlenoRepository extends DefaultCrudRepository<
  TxPleno,
  typeof TxPleno.prototype.id,
  TxPlenoRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TxPleno, dataSource);
  }
}
