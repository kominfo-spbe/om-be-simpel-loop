import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {TdUser, TdUserRelations} from '../models';

export class TdUserRepository extends DefaultCrudRepository<
  TdUser,
  typeof TdUser.prototype.idUser,
  TdUserRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TdUser, dataSource);
  }
}
