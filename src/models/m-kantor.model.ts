import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'referensi', table: 'm_kantor'}
  }
})
export class MKantor extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  kode_kantor: string;

  @property({
    type: 'string',
  })
  nama_kantor?: string;

  @property({
    type: 'string',
  })
  deskripsi_kantor?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  distribution_list?: string;

  @property({
    type: 'date',
  })
  created_date?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  id_provinsi?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<MKantor>) {
    super(data);
  }
}

export interface MKantorRelations {
  // describe navigational properties here
}

export type MKantorWithRelations = MKantor & MKantorRelations;
