import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'simpel_4', table: 'tx_disposisi_resmon'}
  }
})
export class TxDisposisiResmon extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 36,
    id: 1,
    defaultFn: "uuid",
    postgresql: {columnName: 'id', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 36,
    postgresql: {columnName: 'id_tx_laporan', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  idTxLaporan?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'tgl_disposisi', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tglDisposisi?: string;

  @property({
    type: 'boolean',
    postgresql: {columnName: 'ceklis_menerima', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  ceklisMenerima?: boolean;

  @property({
    type: 'string',
    length: 4000,
    postgresql: {columnName: 'catatan', dataType: 'character varying', dataLength: 4000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  catatan?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'created_date', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
    defaultFn: 'now',
  })
  createdDate?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'created_by', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdBy?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'officer_date', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  officerDate?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'officer_by', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  officerBy?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxDisposisiResmon>) {
    super(data);
  }
}

export interface TxDisposisiResmonRelations {
  // describe navigational properties here
}

export type TxDisposisiResmonWithRelations = TxDisposisiResmon & TxDisposisiResmonRelations;
