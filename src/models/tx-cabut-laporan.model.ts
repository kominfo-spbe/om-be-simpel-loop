import {Entity, model, property} from '@loopback/repository';

@model({
    settings: {
        strict: true,
        postgresql: {schema: 'simpel_4', table: 'tx_penutupan_laporan'}
    }
})
export class TxCabutLaporan extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
        defaultFn: "uuid"
    })
    id: string;

    @property({
        type: 'string',
    })
    id_tx_laporan: string;

    @property({
        type: 'string',
    })
    no_agenda?: string;

    @property({
        type: 'string',
    })
    no_berita_acara?: string;

    @property({
        type: 'date',
    })
    tgl_berita_acara?: string;

    @property({
        type: 'string',
    })
    alasan_pencabutan?: string;

    @property({
        type: 'string',
    })
    alasan_penutupan?: string;

    @property({
        type: 'string',
    })
    ringkasan?: string;

    @property({
        type: 'string',
    })
    nama_file?: string;

    @property({
        type: 'string',
    })
    ukuran_file?: string;

    @property({
        type: 'date',
    })
    created_date?: string;

    @property({
        type: 'string',
    })
    created_by?: string;

    @property({
        type: 'string',
    })
    status_laporan_awal?: string;

    @property({
        type: 'string',
    })
    no_pencabutan?: string;

    @property({
        type: 'date',
    })
    tgl_pencabutan_laporan?: string;

    @property({
        type: 'string',
    })
    media_pencabutan?: string;

    @property({
        type: 'string',
    })
    ringkasan_pencabutan?: string;

    @property({
        type: 'date',
    })
    tgl_penutupan_laporan?: string;

    @property({
        type: 'string',
    })
    media_penutupan?: string;

    @property({
        type: 'string',
    })
    mediasosial?: string;

    @property({
        type: 'string',
    })
    ringkasan_penutupan?: string;

    @property({
        type: 'string',
    })
    surat?: string;

    @property({
        type: 'string',
    })
    email?: string;

    @property({
        type: 'string',
    })
    lain_lain?: string;

    @property({
        type: 'string',
    })
    keterangan_media_lain?: string;

    @property({
        type: 'string',
    })
    filename_upload?: string;

    @property({
        type: 'string',
    })
    no_arsip?: string;

    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxCabutLaporan>) {
        super(data);
    }
}

export interface TxCabutLaporanRelations {
    // describe navigational properties here
}

export type TxCabutLaporanWithRelations = TxCabutLaporan & TxCabutLaporanRelations;
