import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_klasifikasi_lm'}
  },
})
export class TxKlasifikasiLm extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: 'uuid'
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
  })
  jml_terlapor_terkait?: string;

  @property({
    type: 'string',
  })
  isu_atendi_publik?: string;

  @property({
    type: 'string',
  })
  pihak_terkait?: string;

  @property({
    type: 'string',
  })
  permasalahan_dilaporkan?: string;

  @property({
    type: 'string',
  })
  detail_permasalahan_dilaporkan?: string;

  @property({
    type: 'string',
  })
  lokasi_terlapor?: string;

  @property({
    type: 'string',
  })
  penerima_manfaat?: string;

  @property({
    type: 'number',
  })
  total_skor?: number;

  @property({
    type: 'string',
  })
  klasifikasi_laporan?: string;

  @property({
    type: 'string',
  })
  kesimpulan?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  tipe_klasifikasi_lm?: string;

  @property({
    type: 'date',
  })
  tgl_persetujuan?: string;

  // @belongsTo(() => MLookup, {name: 'PermasalahanLapor', keyTo : 'lookupCode'})
  // permasalahan_dilaporkan: string;

  // @belongsTo(() => MLookup, {name: 'KlasifikasiLapor', keyTo : 'lookupCode'})
  // klasifikasi_laporan: string;

  constructor(data?: Partial<TxKlasifikasiLm>) {
    super(data);
  }
}

export interface TxKlasifikasiLmRelations {
  // describe navigational properties here
}

export type TxKlasifikasiLmWithRelations = TxKlasifikasiLm & TxKlasifikasiLmRelations;
