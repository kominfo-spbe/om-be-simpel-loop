import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_keterangan_resmon'}
  },
})
export class TxKeteranganResmon extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan?: string;

  @property({
    type: 'string',
  })
  id_tx_riksa?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  judul?: string;

  @property({
    type: 'string',
  })
  keterangan?: string;

  @property({
    type: 'string',
  })
  status?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxKeteranganResmon>) {
    super(data);
  }
}

export interface TxKeteranganResmonRelations {
  // describe navigational properties here
}

export type TxDokumenResmonWithRelations = TxKeteranganResmon & TxKeteranganResmonRelations;
