import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'simpel_4', table: 'tx_proses_resmon'}
  }
})
export class TxProsesResmon extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 36,
    id: 1,
    defaultFn: "uuid",
    postgresql: {columnName: 'id', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 36,
    postgresql: {columnName: 'id_dokumen', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  idDokumen?: string;

  @property({
    type: 'string',
    length: 36,
    postgresql: {columnName: 'id_tx_laporan', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  idTxLaporan?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filename_word', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filenameWord?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filesize_word', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filesizeWord?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filename_final', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filenameFinal?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filesize_final', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filesizeFinal?: string;

  @property({
    type: 'boolean',
    postgresql: {columnName: 'is_submit', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  isSubmit?: boolean;

  @property({
    type: 'date',
    postgresql: {columnName: 'created_date', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
    defaultFn: 'now',
  })
  createdDate?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'created_by', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdBy?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxProsesResmon>) {
    super(data);
  }
}

export interface TxProsesResmonRelations {
  // describe navigational properties here
}

export type TxProsesResmonWithRelations = TxProsesResmon & TxProsesResmonRelations;
