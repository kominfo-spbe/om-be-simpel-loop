import {Entity, model, property} from '@loopback/repository';

@model({
    settings: {
        strict: true,
        postgresql: {schema: 'simpel_4', table: 'tx_dokumen_tte'}
    }
})
export class TxDokumenTte extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        defaultFn: "uuid"
    })
    id: string;

    @property({
        type: 'string',
    })
    id_tx_dokumen?: string;

    @property({
        type: 'string',
    })
    id_penandatangan?: string;

    @property({
        type: 'string',
    })
    nama_penandatangan?: string;

    @property({
        type: 'string',
    })
    id_role?: string;

    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxDokumenTte>) {
        super(data);
    }
}
