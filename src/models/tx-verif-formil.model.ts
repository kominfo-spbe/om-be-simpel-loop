import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_verif_formil'}},
})
export class TxVerifFormil extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;
  @property({
    type: 'string',
  })
  kriteria_pelapor?: string;

  @property({
    type: 'string',
  })
  kategori_pelapor?: string;

  @property({
    type: 'string',
  })
  klasifikasi_pelapor?: string;

  @property({
    type: 'string',
  })
  hasil_verif_formil?: string;

  @property({
    type: 'string',
  })
  status_verif_formil?: string;

  @property({
    type: 'date',
    defaultFn: "now"
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'boolean',
  })
  ceklis_bukti_dukung?: boolean;
  @property({
    type: 'boolean',
  })
  ceklis_uraian?: boolean;
  @property({
    type: 'boolean',
  })
  ceklis_tahun_laporan?: boolean;
  @property({
    type: 'boolean',
  })
  ceklis_bukti_upaya?: boolean;

  @property({
    type: 'string',
  })
  bukti_upaya?: string;

  @property({
    type: 'string',
  })
  filename_upload_bukti_upaya?: string;

  @property({
    type: 'boolean',
  })
  cb_fotocopy_ktp?: boolean;

  @property({
    type: 'boolean',
  })
  cb_fotocopy_ktp_pemberi?: boolean;

  @property({
    type: 'boolean',
  })
  cb_fotocopy_ktp_penerima?: boolean;

  @property({
    type: 'boolean',
  })
  cb_ceklis_tahun_laporan?: boolean;

  @property({
    type: 'boolean',
  })
  cb_ceklis_bukti_dukung?: boolean;

  @property({
    type: 'boolean',
  })
  cb_ceklis_uraian?: boolean;

  @property({
    type: 'boolean',
  })
  cb_ceklis_bukti_upaya?: boolean;

  @property({
    type: 'boolean',
  })
  cb_surat_kuasa_asli?: boolean;

  @property({
    type: 'string',
  })
  nama_organisasi?: string;

  @property({
    type: 'string',
  })
  alamat_organisasi?: string;

  @property({
    type: 'string',
  })
  provinsi_organisasi?: string;

  @property({
    type: 'string',
  })
  kota_organisasi?: string;

  @property({
    type: 'string',
  })
  kecamatan_organisasi?: string;

  @property({
    type: 'string',
  })
  telepon_organisasi?: string;

  @property({
    type: 'string',
  })
  email_organisasi?: string;

  @property({
    type: 'string',
  })
  nama_korban?: string;

  @property({
    type: 'string',
  })
  alamat_korban?: string;

  @property({
    type: 'string',
  })
  no_telepon_korban?: string;

  @property({
    type: 'string',
  })
  nama_masyarakat?: string;

  @property({
    type: 'string',
  })
  alamat_masyarakat?: string;

  @property({
    type: 'string',
  })
  provinsi_masyarakat?: string;

  @property({
    type: 'string',
  })
  kota_masyarakat?: string;

  @property({
    type: 'string',
  })
  kecamatan_masyarakat?: string;

  @property({
    type: 'string',
  })
  telp_masyarakat?: string;

  @property({
    type: 'string',
  })
  email_masyarakat?: string;

  @property({
    type: 'string',
  })
  lainnya?: string;

  @property({
    type: 'string',
  })
  instansi_dituju?: string;

  @property({
    type: 'date',
  })
  tanggal_upaya?: string;

  @property({
    type: 'string',
  })
  keterangan_upaya?: string;

  @property({
    type: 'string',
  })
  filename_upload_ktpformil?: string;

  @property({
    type: 'string',
  })
  filename_lampiran_lapor?: string;

  @property({
    type: 'date',
  })
  tanggal_upaya_terakhir?: string;

  @property({
    type: 'boolean',
  })
  cb_ad_art?: boolean;

  @property({
    type: 'boolean',
  })
  cb_akta_pendirian?: boolean;

  @property({
    type: 'boolean',
  })
  cb_fotocopy_kartukeluarga?: boolean;

  @property({
    type: 'string',
  })
  filename_laporan_tahunan?: string;

  @property({
    type: 'string',
  })
  file_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  filename_ktp_pemberi_kuasa?: string;

  @property({
    type: 'string',
  })
  filename_ktp_pemberi_kuasa2?: string;

  @property({
    type: 'string',
  })
  filename_ktp_pemberi_kuasa3?: string;

  @property({
    type: 'string',
  })
  filename_bukti?: string;

  @property({
    type: 'string',
  })
  filename_uraian?: string;

  @property({
    type: 'string',
  })
  filename_art?: string;

  @property({
    type: 'string',
  })
  filename_akta_pendirian?: string;

  @property({
    type: 'string',
  })
  filename_surat_kuasa?: string;

  @property({
    type: 'string',
  })
  filename_kartu_keluarga?: string;

  @property({
    type: 'string',
  })
  formil_update_status?: string;

  @property({
    type: 'string',
  })
  filename_upload_bukti_surat?: string;

  @property({
    type: 'date',
  })
  tanggal_formil?: string;


  @belongsTo(() => TxLaporan, {name: 'id'})
  id_tx_laporan: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxVerifFormil>) {
    super(data);
  }
}

export interface TxVerifFormilRelations {
  // describe navigational properties here
}

export type TxVerifFormilWithRelations = TxVerifFormil & TxVerifFormilRelations;
