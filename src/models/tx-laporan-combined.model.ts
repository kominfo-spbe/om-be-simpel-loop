import {Model, model, property} from '@loopback/repository';

@model()
export class TxLaporanCombined extends Model {

  @property({
    type: 'string',
  })
  id: string;

  @property({
    type: 'string',
  })
  id_m_pelapor?: string;

  @property({
    type: 'string',
    required: false,
  })
  kategori_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  nama_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  warga_negara_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  jenis_identitas_pelapor: string;

  @property({
    type: 'string'
  })
  jenis_identitas_name: string;

  @property({
    type: 'string'
  })
  warga_negara_pelapor_name: string;

  @property({
    type: 'string',
    required: false,
  })
  nomor_identitas_pelapor: string;

  @property({
    type: 'string',
  })
  file_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  tempat_lahir_pelapor?: string;

  @property({
    type: 'date',
  })
  tanggal_lahir_pelapor?: string;

  @property({
    type: 'string',
  })
  jenis_kelamin_pelapor?: string;

  @property({
    type: 'string',
  })
  status_perkawinan_pelapor?: string;

  @property({
    type: 'string',
  })
  pekerjaan_pelapor?: string;

  @property({
    type: 'string',
  })
  pendidikan_pelapor?: string;

  @property({
    type: 'string',
  })
  alamat_lengkap_pelapor?: string;

  @property({
    type: 'string',
  })
  provinsi_pelapor?: string;

  @property({
    type: 'string',
  })
  provinsi_pelapor_name?: string;

  @property({
    type: 'string',
  })
  kab_kota_pelapor?: string;

  @property({
    type: 'string',
  })
  kab_kota_pelapor_name?: string;

  @property({
    type: 'string',
  })
  kec_pelapor?: string;

  @property({
    type: 'string',
  })
  kec_pelapor_name?: string;

  @property({
    type: 'string',
  })
  no_telp_pelapor?: string;

  @property({
    type: 'string',
  })
  email_pelapor?: string;

  @property({
    type: 'string',
  })
  identitas_pelapor_rahasia?: string;

  @property({
    type: 'string',
  })
  nama_terlapor?: string;

  @property({
    type: 'string',
  })
  jabatan_terlapor?: string;

  @property({
    type: 'string',
  })
  kelompok_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  kelompok_instansi_terlapor_name?: string;

  @property({
    type: 'string',
  })
  klasifikasi_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  klasifikasi_instansi_terlapor_name?: string;

  @property({
    type: 'string',
  })
  nama_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  alamat_terlapor?: string;

  @property({
    type: 'string',
  })
  provinsi_terlapor?: string;

  @property({
    type: 'string',
  })
  provinsi_terlapor_name?: string;

  @property({
    type: 'string',
  })
  kab_kota_terlapor?: string;

  @property({
    type: 'string',
  })
  kab_kota_terlapor_name?: string;

  @property({
    type: 'string',
  })
  kec_terlapor?: string;

  @property({
    type: 'string',
  })
  kec_terlapor_name?: string;

  @property({
    type: 'string',
  })
  substansi?: string;

  @property({
    type: 'string',
  })
  perihal?: string;

  @property({
    type: 'string',
  })
  pokok_masalah?: string;

  @property({
    type: 'date',
  })
  tgl_agenda?: string;

  @property({
    type: 'string',
  })
  no_agenda?: string;

  @property({
    type: 'string',
  })
  no_arsip?: string;

  @property({
    type: 'string',
  })
  tipe_laporan?: string;

  @property({
    type: 'string',
  })
  cara_penyampaian?: string;

  @property({
    type: 'string',
  })
  is_suspended?: string;

  @property({
    type: 'string',
  })
  token?: string;

  @property({
    type: 'string',
  })
  status_laporan?: string;

  @property({
    type: 'string',
  })
  status_petugas?: string;

  @property({
    type: 'date',
    default: "$now"
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'string',
  })
  officer_by?: string;

  @property({
    type: 'string',
  })
  filesize_pelapor?: string;

  @property({
    type: 'string',
  })
  harapan?: string;

  @property({
    type: 'string',
  })
  sudah_melapor?: string;

  @property({
    type: 'date',
  })
  tgl_upaya_melapor?: string;

  @property({
    type: 'string',
  })
  filename_uraian?: string;

  @property({
    type: 'string',
  })
  filesize_uraian?: string;

  @property({
    type: 'string',
  })
  filename_bukti?: string;

  @property({
    type: 'string',
  })
  filesize_bukti?: string;

    @property({
      type: 'object',
      itemType: 'object',
    })
    daerah_pelapor?: object;

  constructor(data?: Partial<TxLaporanCombined>) {
    super(data);
  }
}

export interface TxLaporanCombinedRelations {
  // describe navigational properties here
}

export type TxLaporanCombinedWithRelations = TxLaporanCombined & TxLaporanCombinedRelations;
