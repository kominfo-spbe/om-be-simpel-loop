import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_laporan_perubahan'}},
})
export class TxLaporanPerubahan extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
    required: true,
  })
  no_agenda: string;

  @property({
    type: 'string',
  })
  perubahan_lama?: string;

  @property({
    type: 'string',
  })
  perubahan_baru?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;


  constructor(data?: Partial<TxLaporanPerubahan>) {
    super(data);
  }
}

export interface TxLaporanPerubahanRelations {
  // describe navigational properties here
}

export type TxLaporanPerubahanWithRelations = TxLaporanPerubahan & TxLaporanPerubahanRelations;
