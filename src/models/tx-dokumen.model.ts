import {Entity, hasMany, model, property} from '@loopback/repository';
import {TxDokumenTte} from '../models';

@model({
    settings: {postgresql: {schema: 'simpel_4', table: 'tx_dokumen'}},
})

export class TxDokumen extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        defaultFn: "uuid",
    })
    id: string;

    @property({
        type: 'string',
    })
    id_tx_laporan: string;

    @property({
        type: 'string',
    })
    filename?: string;

    @property({
        type: 'string',
    })
    nomor_agenda?: string;

    @property({
        type: 'string',
    })
    tipe_dokumen?: string;

    @property({
        type: 'string',
    })
    status_dokumen?: string;

    @property({
        type: 'string',
    })
    penandatangan_1?: string;

    @property({
        type: 'string',
    })
    penandatangan_2?: string;

    @property({
        type: 'string',
    })
    penandatangan_3?: string;

    @property({
        type: 'string',
    })
    nama_penandatangan_1?: string;

    @property({
        type: 'string',
    })
    nama_penandatangan_2?: string;

    @property({
        type: 'string',
    })
    nama_penandatangan_3?: string;

    @property({
        type: 'number',
    })
    version?: number;

    @property({
        type: 'date',
        defaultFn: 'now'
    })
    created_date?: string;

    @property({
        type: 'number',
        defaultFn: '-1'
    })
    page?: number;

    // Define well-known properties here
    @hasMany(() => TxDokumenTte, {keyTo: 'id_tx_dokumen'})
    TxDokumenTte: TxDokumenTte[];
    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxDokumen>) {
        super(data);
    }
}

export interface TxDokumenRelations {
    // describe navigational properties here
}

export type TxDokumenWithRelations = TxDokumen & TxDokumenRelations;
