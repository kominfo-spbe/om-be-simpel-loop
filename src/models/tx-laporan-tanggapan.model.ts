import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TxLaporan} from '../models';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_laporan_tanggapan'}
  },
})
export class TxLaporanTanggapan extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'date',
  })
  tanggal?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  melalui?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  keterangan?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  ket_jenis_tanggapan?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  file_name_tanggapan?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  // @property({
  //   type: 'string',
  //   required: true,
  //   jsonSchema: {nullable: true}
  // })
  // id_tx_laporan?: string;

  @belongsTo(() => TxLaporan, {name: 'id'})
  id_tx_laporan: string;

  constructor(data?: Partial<TxLaporanTanggapan>) {
    super(data);
  }
}

export interface TxLaporanTanggapanRelations {
  // describe navigational properties here
}

export type TxLaporanTanggapanWithRelations = TxLaporanTanggapan & TxLaporanTanggapanRelations;
