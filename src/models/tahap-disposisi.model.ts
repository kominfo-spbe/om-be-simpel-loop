import {Model, model, property} from '@loopback/repository';
import {TxChangeStatus} from './tx-change-status.model';
import {TxDisposisiPvlKaper} from './tx-disposisi-pvl-kaper.model';
import {TxDisposisiPvlKapten} from './tx-disposisi-pvl-kapten.model';
import {TxLaporan} from './tx-laporan.model';

@model()
export class TahapDisposisi extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;

    @property({
      type: TxDisposisiPvlKapten,
      jsonSchema: {nullable: true},
    })
    txDisposisiPvlKapten: TxDisposisiPvlKapten;

    @property({
      type: TxDisposisiPvlKaper,
      jsonSchema: {nullable: true},
    })
    txDisposisiPvlKaper: TxDisposisiPvlKaper;

  constructor(data?: Partial<TahapDisposisi>) {
    super(data);
  }
}
