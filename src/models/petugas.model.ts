import {Model, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Petugas extends Model {
    @property({
        type: 'string'
    })
    kode_kantor?: string;

    @property({
        type: 'string'
    })
    q?: string;

    @property({
        type: 'array',
        required: true
    })
    substansi?: string;

    @property({
        type: 'array'
    })
    role?: string;


    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<Petugas>) {
        super(data);
    }
}

export interface PetugasRelations {
    // describe navigational properties here
}

export type PetugasWithRelations = Petugas & PetugasRelations;
