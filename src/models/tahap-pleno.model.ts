import {Model, model, property} from '@loopback/repository';
import {TxCabutLaporan} from './tx-cabut-laporan.model';
import {TxChangeStatus} from './tx-change-status.model';
import {TxDisposisiPvlKapten} from './tx-disposisi-pvl-kapten.model';
import {TxLaporan} from './tx-laporan.model';
import {TxPleno} from './tx-pleno.model';
import {TxVerifMateriil} from './tx-verif-materiil.model';

@model()
export class TahapPleno extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;

    @property({
      type: TxPleno,
      jsonSchema: {nullable: true},
    })
    txPleno: TxPleno;

    @property({
      type: TxDisposisiPvlKapten,
      jsonSchema: {nullable: true},
    })
    txDisposisiPvlKapten: TxDisposisiPvlKapten;

    @property({
      type: TxVerifMateriil,
      jsonSchema: {nullable: true},
    })
    txVerifMateriil: TxVerifMateriil;

    @property({
      type: TxCabutLaporan,
      jsonSchema: {nullable: true},
    })
    txCabutLaporan: TxCabutLaporan;



  constructor(data?: Partial<TahapPleno>) {
    super(data);
  }
}
