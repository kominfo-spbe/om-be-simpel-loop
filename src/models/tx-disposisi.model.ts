import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_disposisi'}},
})
export class TxDisposisi extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;
  @property({
    type: 'date',
  })
  tgl_disposisi?: string;

  @property({
    type: 'string',
  })
  kode_kantor?: string;

  @property({
    type: 'string',
  })
  alasan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  catatan?: string;

  @property({
    type: 'string',
  })
  status_laporan?: string;

  @property({
    type: 'boolean',
  })
  status_disposisi?: boolean;

  @property({
    type: 'string',
  })
  officer_by?: string;

  @property({
    type: 'date',
  })
  officer_date?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  tgl_terima_berkas?: string;

  @belongsTo(() => TxLaporan, {name: 'txLaporan'})
  id_tx_laporan: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxDisposisi>) {
    super(data);
  }
}

export interface TxDisposisiRelations {
  // describe navigational properties here
}

export type TxDisposisiWithRelations = TxDisposisi & TxDisposisiRelations;
