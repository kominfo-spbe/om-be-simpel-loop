import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_disposisi_pvl_kapten'},
  },
})
export class TxDisposisiPvlKapten extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: 'uuid',
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan: string;

  @property({
    type: 'date',
  })
  tgl_penyerahan_riksa?: string;

  @property({
    type: 'string',
  })
  kantor_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  catatan?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxDisposisiPvlKapten>) {
    super(data);
  }
}

export interface TxDisposisiPvlKaptenRelations {
  // describe navigational properties here
}

export type TxDisposisiPvlKaptenWithRelations = TxDisposisiPvlKapten &
  TxDisposisiPvlKaptenRelations;
