import {Model, model, property} from '@loopback/repository';

@model()
export class TrackingLaporan extends Model {
  @property({
    type: 'string',
  })
  token?: string;


  constructor(data?: Partial<TrackingLaporan>) {
    super(data);
  }
}

export interface TrackingLaporanRelations {
  // describe navigational properties here
}

export type TrackingLaporanWithRelations = TrackingLaporan & TrackingLaporanRelations;
