import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_verif_formil_dokumen'}},
})
export class TxVerifFormilDokumen extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_verif_formil: string;

  @property({
    type: 'string',
    required: true,
  })
  id_dokumen: string;

  @property({
    type: 'string',
    required: true,
  })
  filename_dokumen: string;

  @property({
    type: 'string',
  })
  filesize_dokumen?: string;

  @property({
    type: 'string',
  })
  notes_dokumen?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxVerifFormilDokumen>) {
    super(data);
  }
}

export interface TxVerifFormilDokumenRelations {
  // describe navigational properties here
}

export type TxVerifFormilDokumenWithRelations = TxVerifFormilDokumen & TxVerifFormilDokumenRelations;
