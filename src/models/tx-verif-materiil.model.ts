import {Entity, hasMany, model, property} from '@loopback/repository';
import {TxVerifMateriilDokumen, TxVerifMateriilKronologi} from '../models';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_verif_materiil'}},
})

export class TxVerifMateriil extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid",
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
  })
  substansi?: string;

  @property({
    type: 'string',
  })
  pokok_permasalahan?: string;

  @property({
    type: 'string',
  })
  legal_standing_pelapor?: string;

  @property({
    type: 'string',
  })
  harapan_pelapor?: string;

  @property({
    type: 'boolean',
  })
  kewenangan_ombudsman?: boolean;

  @property({
    type: 'boolean',
  })
  menolak_laporan?: boolean;

  @property({
    type: 'boolean',
  })
  verif_identitas_pelapor_dirahasiakan?: boolean;

  @property({
    type: 'boolean',
  })
  verif_pelapor_keberatan?: boolean;

  @property({
    type: 'boolean',
  })
  verif_laporan_objek_pemeriksaan?: boolean;

  @property({
    type: 'boolean',
  })
  verif_laporan_ditindaklanjuti?: boolean;

  @property({
    type: 'boolean',
  })
  verif_proses_penyelesaian?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    default: null
  })
  updated_date?: string;

  @property({
    type: 'string',
    default: null
  })
  updated_by?: string;

  @property({
    type: 'string',
    default: null
  })
  deleted_by?: string;

  @property({
    type: 'date',
    default: null
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  catatan_kewenangan_ombudsman?: string;

  @property({
    type: 'string',
  })
  alasan?: string;

  @property({
    type: 'string',
  })
  alasan_melanjutkan_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  proses_laporan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'date',
  })
  tanggal_materiil?: string;


  // Define well-known properties here
  @hasMany(() => TxVerifMateriilKronologi, {keyTo: 'id_tx_verif_materiil'})
  TxVerifMateriilKronologi: TxVerifMateriilKronologi[];

  @hasMany(() => TxVerifMateriilDokumen, {keyTo: 'id_tx_verif_materiil'})
  TxVerifMateriilDokumen: TxVerifMateriilDokumen[];

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxVerifMateriil>) {
    super(data);
  }
}

export interface TxVerifMateriilRelations {
  // describe navigational properties here
}

export type TxVerifMateriilWithRelations = TxVerifMateriil & TxVerifMateriilRelations;
