import {Model, model, property} from '@loopback/repository';

@model()
export class SendEmailInit extends Model {

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  constructor(data?: Partial<SendEmailInit>) {
    super(data);
  }
}

export interface SendEmailInitRelations {
  // describe navigational properties here
}

export type SendEmailInitWithRelations = SendEmailInit & SendEmailInitRelations;
