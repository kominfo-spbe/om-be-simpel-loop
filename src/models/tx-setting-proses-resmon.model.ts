import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'simpel_4', table: 'tx_setting_proses_resmon'}
  }
})
export class TxSettingProsesResmon extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 36,
    id: 1,
    defaultFn: "uuid",
    postgresql: {columnName: 'id', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 1000,
    postgresql: {columnName: 'nama_dokumen', dataType: 'character varying', dataLength: 1000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  namaDokumen?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'tipe_dokumen', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tipeDokumen?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filename_dokumen', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filenameDokumen?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'filesize_dokumen', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  filesizeDokumen?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'dasar_hukum', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  dasarHukum?: string;

  @property({
    type: 'boolean',
    postgresql: {columnName: 'status_dokumen', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  statusDokumen?: boolean;

  @property({
    type: 'string',
    length: 4000,
    postgresql: {columnName: 'catatan', dataType: 'character varying', dataLength: 4000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  catatan?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'created_date', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
    defaultFn: 'now',
  })
  createdDate?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'created_by', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdBy?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxSettingProsesResmon>) {
    super(data);
  }
}

export interface TxSettingProsesResmonRelations {
  // describe navigational properties here
}

export type TxSettingProsesResmonWithRelations = TxSettingProsesResmon & TxSettingProsesResmonRelations;
