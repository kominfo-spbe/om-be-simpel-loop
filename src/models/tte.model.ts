import {Model, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Tte extends Model {
    @property({
        type: 'string'
    })
    id_tx_laporan?: string;

    @property({
        type: 'string'
    })
    tipe_dokumen?: string;

    @property({
        type: 'string'
    })
    nik?: string;

    @property({
        type: 'string'
    })
    passphrase?: string;

    // @property({
    //     type: 'string'
    // })
    // id_role?: string;

    @property({
        type: 'string'
    })
    user_id?: string;
    @property({
        type: 'string'
    })
    nama_penandatangan?: string;

    @property({
        type: 'number'
    })
    urutan_penandatangan?: number;
    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<Tte>) {
        super(data);
    }
}
