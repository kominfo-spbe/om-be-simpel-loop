import {Entity, model, property} from '@loopback/repository';

@model({
    settings: {idInjection: false, postgresql: {schema: 'apps_manager', table: 'td_ttd_spesimen'}}
})
export class TdTtdSpesimen extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
        defaultFn: "uuid"
    })
    id: string;

    @property({
        type: 'string',
    })
    user_id?: string;

    @property({
        type: 'string',
    })
    spesimen_path?: string;

    @property({
        type: 'date',
        defaultFn: 'now',
    })
    created_date?: string;

    @property({
        type: 'date',
    })
    updated_date?: string;

    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TdTtdSpesimen>) {
        super(data);
    }
}

export interface TdTtdSpesimenRelations {
    // describe navigational properties here
}

export type TdTtdSpesimenWithRelations = TdTtdSpesimen & TdTtdSpesimenRelations;
