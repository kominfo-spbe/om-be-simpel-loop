import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_take_over'}},
})
export class TxTakeOver extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan?: string;

  @property({
    type: 'string',
  })
  no_agenda?: string;

  @property({
    type: 'date',
  })
  take_over_date?: string;

  @property({
    type: 'string',
  })
  officer_old_by?: string;

  @property({
    type: 'string',
  })
  officer_new_by?: string;

  @property({
    type: 'string',
  })
  alasan?: string;

  @property({
    type: 'date',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'string',
  })
  nama_pemberi_tugas?: string;

  @property({
    type: 'string',
  })
  jabatan_pemberi_tugas?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxTakeOver>) {
    super(data);
  }
}

export interface TxTakeOverRelations {
  // describe navigational properties here
}

export type TxTakeOverWithRelations = TxTakeOver & TxTakeOverRelations;
