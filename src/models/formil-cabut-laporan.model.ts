import {Model, model, property} from '@loopback/repository';
import {TxCabutLaporan} from './tx-cabut-laporan.model';
import {TxChangeStatus} from './tx-change-status.model';
import {TxLaporan} from './tx-laporan.model';

@model()
export class FormilCabutLaporan extends Model {

    @property({
      type: TxCabutLaporan,
      jsonSchema: {nullable: true},
    })
    txCabutLaporan: TxCabutLaporan;

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;


  constructor(data?: Partial<FormilCabutLaporan>) {
    super(data);
  }
}
