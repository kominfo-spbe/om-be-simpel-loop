import {Model, model, property} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';
import {TxVerifFormil} from './tx-verif-formil.model';

@model()
export class FormilDraft extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxVerifFormil,
      jsonSchema: {nullable: true},
    })
    txVerifFormil: TxVerifFormil;

  constructor(data?: Partial<FormilDraft>) {
    super(data);
  }
}
