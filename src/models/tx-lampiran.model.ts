import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_lampiran'}
  },
})
export class TxLampiran extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan?: string;

  @property({
    type: 'string',
  })
  status_laporan?: string;

  @property({
    type: 'string',
  })
  filename_dokumen?: string;

  @property({
    type: 'string',
  })
  identitas_dokumen?: string;

  @property({
    type: 'date',
    default: "$now"
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'string',
  })
  catatan?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  id_reference?: string;


  @property({
    type: 'string',
  })
  step_flag?: string;

  @property({
    type: 'string',
  })
  field_flag?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxLampiran>) {
    super(data);
  }
}

export interface TxLampiranRelations {
  // describe navigational properties here
}

export type TxLampiranWithRelations = TxLampiran & TxLampiranRelations;
