import {Model, model, property} from '@loopback/repository';
import {TxDisposisiPvlKapten} from './tx-disposisi-pvl-kapten.model';
import {TxLaporan} from './tx-laporan.model';

@model()
export class RegistrasiNew extends Model {

  @property({
    type: TxLaporan,
    jsonSchema: {nullable: true},
  })
  txLaporan: TxLaporan;

  @property({
    type: TxDisposisiPvlKapten,
    jsonSchema: {nullable: true},
  })
  txDisposisiPvlKapten: TxDisposisiPvlKapten;

}
