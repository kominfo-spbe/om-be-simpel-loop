import {Model, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class JwtResponse extends Model {
  @property({
    type: 'string',
  })
  sub?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  role?: string[];

  @property({
    type: 'string',
  })
  kind?: string;

  @property({
    type: 'number',
  })
  exp?: number;

  @property({
    type: 'object',
  })
  user?: object;

  @property({
    type: 'number',
  })
  iat?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<JwtResponse>) {
    super(data);
  }
}

export interface JwtResponseRelations {
  // describe navigational properties here
}

export type JwtResponseWithRelations = JwtResponse & JwtResponseRelations;
