import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_pengalihan'}},
})
export class TxPengalihan extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan?: string;

  @property({
    type: 'date',
  })
  tgl_disposisi?: string;

  @property({
    type: 'string',
  })
  kode_kantor?: string;

  @property({
    type: 'string',
  })
  alasan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  catatan?: string;

  @property({
    type: 'string',
  })
  status_laporan?: string;

  @property({
    type: 'boolean',
  })
  status_disposisi?: boolean;

  @property({
    type: 'string',
  })
  officer_by?: string;

  @property({
    type: 'date',
  })
  officer_date?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  constructor(data?: Partial<TxPengalihan>) {
    super(data);
  }
}

export interface TxPengalihanRelations {
  // describe navigational properties here
}

export type TxPengalihanWithRelations = TxPengalihan & TxPengalihanRelations;
