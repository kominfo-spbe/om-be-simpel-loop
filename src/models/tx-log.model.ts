import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    strict: false,
    postgresql: { schema: 'simpel_4', table: 'tx_log'}
  },

})
export class TxLog extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: "uuid"
  })
  id_tx_log?: string;

  @property({
    type: 'string',
  })
  nama_tabel?: string;

  @property({
    type: 'string',
  })
  nama_aksi?: string;

  @property({
    type: 'string',
  })
  data_lama?: string;

  @property({
    type: 'string',
  })
  data_baru?: string;

  @property({
    type: 'string',
  })
  nip_rekam?: string;

  @property({
    type: 'date',
  })
  waktu_rekam?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxLog>) {
    super(data);
  }
}

export interface LogRelations {
  // describe navigational properties here
}

export type LogWithRelations = TxLog & LogRelations;
