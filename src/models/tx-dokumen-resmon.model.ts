import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_dokumen_resmon'}
  },
})
export class TxDokumenResmon extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  id_tx_laporan?: string;

  @property({
    type: 'string',
  })
  id_tx_riksa?: string;

  @property({
    type: 'string',
  })
  jenis_surat?: string;

  @property({
    type: 'string',
  })
  data?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  keterangan?: string;

  @property({
    type: 'string',
  })
  filename?: string;

  @property({
    type: 'date',
  })
  created_date_lahp?: string;

  @property({
    type: 'date',
  })
  created_date_resolusi?: string;

  @property({
    type: 'date',
  })
  created_date_rekomendasi?: string;

  @property({
    type: 'date',
  })
  created_date_monitoring?: string;

  @property({
    type: 'date',
  })
  created_date_penutupan?: string;

  @property({
    type: 'date',
  })
  updated_date_lahp?: string;

  @property({
    type: 'date',
  })
  updated_date_resolusi?: string;

  @property({
    type: 'date',
  })
  updated_date_rekomendasi?: string;

  @property({
    type: 'date',
  })
  updated_date_monitoring?: string;

  @property({
    type: 'date',
  })
  updated_date_penutupan?: string;

@property({
    type: 'string',
  })
  nama_dokumen?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxDokumenResmon>) {
    super(data);
  }
}

export interface TxDokumenResmonRelations {
  // describe navigational properties here
}

export type TxDokumenResmonWithRelations = TxDokumenResmon & TxDokumenResmonRelations;
