import {Entity, model, property, belongsTo} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';
import {MKantor} from "./m-kantor.model";
import {TdUser} from "./td-user.model";

@model({
    settings: {
        strict: true,
        postgresql: {schema: 'simpel_4', table: 'tx_history_status_laporan'}
    }
})
export class TxHistoryStatusLaporan extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
        defaultFn: "uuid"
    })
    id: string;
    @property({
        type: 'string',
    })
    no_agenda?: string;

    @property({
        type: 'string',
    })
    prev_status?: string;

    @property({
        type: 'string',
    })
    next_status?: string;

    @property({
        type: 'date',
        default: "$now"
    })
    created_date?: string;

    @property({
        type: 'string',
    })
    created_by?: string;

    @belongsTo(() => TdUser, {name: 'UserDetail', keyTo: 'idUser'})
    prev_assignee?: string;

    @belongsTo(() => TdUser, {name: 'UserDetail', keyTo: 'idUser'})
    next_assignee?: string;

    @belongsTo(() => TxLaporan, {name: 'id'})
    id_tx_laporan: string;

    @belongsTo(() => MKantor, {name: 'kode_kantor'})
    prev_kantor: string;

    @belongsTo(() => MKantor, {name: 'kode_kantor'})
    next_kantor: string;
    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxHistoryStatusLaporan>) {
        super(data);
    }
}

export interface TxHistoryStatusLaporanRelations {
    // describe navigational properties here
}

export type TxHistoryStatusLaporanWithRelations = TxHistoryStatusLaporan & TxHistoryStatusLaporanRelations;
