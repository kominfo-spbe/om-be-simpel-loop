import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'referensi', table: 'm_pelapor'}}
})
export class MPelapor extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
  })
  kategori_pelapor?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  email_pelapor?: string;

  @property({
    type: 'string',
  })
  no_telp_pelapor?: string;

  @property({
    type: 'string',
  })
  kec_pelapor?: string;

  @property({
    type: 'string',
  })
  kab_kota_pelapor?: string;

  @property({
    type: 'string',
  })
  provinsi_pelapor?: string;

  @property({
    type: 'string',
  })
  alamat_lengkap_pelapor?: string;

  @property({
    type: 'string',
  })
  pendidikan_pelapor?: string;

  @property({
    type: 'string',
  })
  pekerjaan_pelapor?: string;

  @property({
    type: 'string',
  })
  status_perkawinan_pelapor?: string;

  @property({
    type: 'string',
  })
  jenis_kelamin_pelapor?: string;

  @property({
    type: 'date',
  })
  tanggal_lahir_pelapor?: string;

  @property({
    type: 'string',
  })
  tempat_lahir_pelapor?: string;

  @property({
    type: 'string',
  })
  file_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  nomor_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  jenis_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  warga_negara_pelapor?: string;

  @property({
    type: 'string',
  })
  nama_pelapor?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<MPelapor>) {
    super(data);
  }
}

export interface MPelaporRelations {
  // describe navigational properties here
}

export type MPelaporWithRelations = MPelapor & MPelaporRelations;
