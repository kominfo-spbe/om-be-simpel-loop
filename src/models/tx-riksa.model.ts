import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_riksa'}
  },
})
export class TxRiksa extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;
  @property({
    type: 'string',
  })
  media?: string;

  @property({
    type: 'string',
  })
  tertuju?: string;

  @property({
    type: 'string',
  })
  tlp?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  nomor_surat?: string;

  @property({
    type: 'string',
  })
  catatan_lainnya?: string;

  @property({
    type: 'string',
  })
  substansi_laporan?: string;

  @property({
    type: 'string',
  })
  pokok_permasalahan?: string;

  @property({
    type: 'string',
  })
  dugaan_maladministrasi?: string;

  @property({
    type: 'string',
  })
  kronologis?: string;

  @property({
    type: 'string',
  })
  harapan_pelapor?: string;

  @property({
    type: 'string',
  })
  peraturan_terkait?: string;

  @property({
    type: 'string',
  })
  data_dukung?: string;

  @property({
    type: 'string',
  })
  analisis?: string;

  @property({
    type: 'string',
  })
  kesimpulan_sementara?: string;

  @property({
    type: 'string',
  })
  rencana_tindaklanjut?: string;

  @property({
    type: 'date',
  })
  tgl_bedah_laporan?: string;

  @property({
    type: 'string',
  })
  kode_kantor?: string;

  @property({
    type: 'string',
  })
  officer_by?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date_pemberitahuan?: string;

  @property({
    type: 'string',
  })
  created_by_pemberitahuan?: string;

  @property({
    type: 'date',
  })
  created_date_lhpd?: string;

  @property({
    type: 'string',
  })
  created_by_lhpd?: string;

  @property({
    type: 'boolean',
  })
  ceklis_alihkan?: boolean;

  @property({
    type: 'string',
  })
  konfirmasi_lahp_diterima?: string;

  @property({
    type: 'boolean',
  })
  ceklis_tindakan_korektif?: boolean;

  @property({
    type: 'date',
  })
  created_date_monitoring?: string;

  @property({
    type: 'string',
  })
  created_by_monitoring?: string;

  @property({
    type: 'string',
  })
  putusan_dugaan_maladministrasi?: string;

  @property({
    type: 'string',
  })
  catatan_alihkan?: string;

  @property({
    type: 'date',
  })
  created_date_lahp?: string;

  @property({
    type: 'string',
  })
  created_by_lahp?: string;

  @property({
    type: 'date',
  })
  tgl_konfirmasi_lahp?: string;

  @property({
    type: 'string',
  })
  ringkasan_lahp?: string;

  @property({
    type: 'string',
  })
  proses_laporan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  keterangan?: string;

  @property({
    type: 'string',
  })
  ket_media_lain?: string;

  @property({
    type: 'string',
  })
  filename_media_surat?: string;

  @property({
    type: 'string',
  })
  sifat_surat?: string;

  @property({
    type: 'string',
  })
  lampiran?: string;

  @property({
    type: 'string',
  })
  hal_surat?: string;

  @property({
    type: 'date',
  })
  tanggal_surat?: string;

  @property({
    type: 'string',
  })
  tlp2?: string;

  @property({
    type: 'string',
  })
  email2?: string;

  @property({
    type: 'string',
  })
  whatsapp?: string;

  @property({
    type: 'string',
  })
  nama?: string;

  @property({
    type: 'string',
  })
  instansijabatan?: string;

  @property({
    type: 'date',
  })
  tgl_terima_berkas?: string;

  @property({
    type: 'string',
  })
  kesimpulan_hasil_monitoring?: string;

  @belongsTo(() => TxLaporan, {name: 'txLaporan'})
  id_tx_laporan: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxRiksa>) {
    super(data);
  }
}

export interface TxRiksaRelations {
  // describe navigational properties here
}

export type TxRiksaWithRelations = TxRiksa & TxRiksaRelations;
