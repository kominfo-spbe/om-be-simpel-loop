import {belongsTo, Entity, hasMany, hasOne, model, property} from '@loopback/repository';
import {
  TdUser,
  TxChangeStatus,
  TxDisposisi,
  TxDisposisiPvlKaper,
  TxDisposisiPvlKapten,
  TxKlasifikasiLm,
  TxLaporanPerubahan,
  TxPleno, TxRiksa, TxTakeOver,
  TxVerifFormil,
  TxVerifMateriil
} from '../models';
import {TxRiksaPelimpahanResmon} from './tx-riksa-pelimpahan-resmon.model';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_laporan_history'}
  },
})
export class TxLaporanHistory extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id_laporan: string;

  @property({
    type: 'string',
  })
  id_m_pelapor?: string;

  @property({
    type: 'string',
    required: false,
  })
  kategori_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  nama_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  warga_negara_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  jenis_identitas_pelapor: string;

  @property({
    type: 'string',
    required: false,
  })
  nomor_identitas_pelapor: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  file_identitas_pelapor?: string;

  @property({
    type: 'string',
  })
  tempat_lahir_pelapor?: string;

  @property({
    type: 'date',
  })
  tanggal_lahir_pelapor?: string;

  @property({
    type: 'string',
  })
  jenis_kelamin_pelapor?: string;

  @property({
    type: 'string',
  })
  status_perkawinan_pelapor?: string;

  @property({
    type: 'string',
  })
  pekerjaan_pelapor?: string;

  @property({
    type: 'string',
  })
  pendidikan_pelapor?: string;

  @property({
    type: 'string',
  })
  alamat_lengkap_pelapor?: string;

  @property({
    type: 'string',
  })
  provinsi_pelapor?: string;

  @property({
    type: 'string',
  })
  kab_kota_pelapor?: string;

  @property({
    type: 'string',
  })
  kec_pelapor?: string;

  @property({
    type: 'string',
  })
  no_telp_pelapor?: string;

  @property({
    type: 'string',
  })
  email_pelapor?: string;

  @property({
    type: 'string',
  })
  identitas_pelapor_rahasia?: string;

  @property({
    type: 'string',
  })
  nama_terlapor?: string;

  @property({
    type: 'string',
  })
  jabatan_terlapor?: string;

  @property({
    type: 'string',
  })
  kelompok_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  klasifikasi_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  nama_instansi_terlapor?: string;

  @property({
    type: 'string',
  })
  alamat_terlapor?: string;

  @property({
    type: 'string',
  })
  provinsi_terlapor?: string;

  @property({
    type: 'string',
  })
  kab_kota_terlapor?: string;

  @property({
    type: 'string',
  })
  kec_terlapor?: string;

  @property({
    type: 'string',
  })
  substansi?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true}
  })
  perihal?: string;

  @property({
    type: 'string',
  })
  pokok_masalah?: string;

  @property({
    type: 'date',
  })
  tgl_agenda?: string;

  @property({
    type: 'string',
  })
  no_agenda?: string;

  @property({
    type: 'string',
  })
  no_arsip?: string;

  @property({
    type: 'string',
  })
  tipe_laporan?: string;

  @property({
    type: 'string',
  })
  cara_penyampaian?: string;

  @property({
    type: 'string',
  })
  is_suspended?: string;

  @property({
    type: 'string',
  })
  token?: string;

  @property({
    type: 'string',
  })
  status_laporan?: string;

  @property({
    type: 'string',
  })
  status_petugas?: string;

  @property({
    type: 'date',
    default: "$now"
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  // @property({
  //   type: 'string',
  // })
  // officer_by?: string;

  @property({
    type: 'string',
    required: false,
  })
  filesize_pelapor?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  harapan?: string;

  @property({
    type: 'string',
  })
  sudah_melapor?: string;

  @property({
    type: 'date',
    required: false,
    jsonSchema: {nullable: true}
  })
  tgl_upaya_melapor?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true}
  })
  filename_uraian?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true}
  })
  filesize_uraian?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true}
  })
  filename_bukti?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true}
  })
  filesize_bukti?: string;

  @property({
    type: 'string',
  })
  kode_kantor?: string;

  @property({
    type: 'boolean',
  })
  status_pengalihan?: boolean;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  ringkasan_hasil_konsultasi?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_art?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_art?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_akta_pendirian?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_akta_pendirian?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_ktp_pemberi_kuasa?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_ktp_pemberi_kuasa?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_ktp_pemberi_kuasa2?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_ktp_pemberi_kuasa2?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_ktp_pemberi_kuasa3?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_ktp_pemberi_kuasa3?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_surat_kuasa?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_surat_kuasa?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_kartu_keluarga?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_kartu_keluarga?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_upload_bukti_upaya?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_upload_bukti_upaya?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filename_upload_bukti_upaya_email?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  filesize_upload_bukti_upaya_email?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  jenis_pelapor?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  ceklis_bukti_upaya?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  bukti_upaya?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  tindak_lanjut_konsultasi?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  pemberi_disposisi?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  alasan_respon_cepat?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  tanggal_disposisi?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  hasil_pleno?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  didaftarkan_oleh?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  proses_laporan?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  tim_pemeriksaan?:string;

  @hasOne(() => TxVerifFormil, {keyTo: 'id_tx_laporan'})
  txVerifFormil: TxVerifFormil;

  @hasOne(() => TxVerifMateriil, {keyTo: 'id_tx_laporan'})
  txVerifMateriil: TxVerifMateriil;

  @hasOne(() => TxKlasifikasiLm, {keyTo: 'id_tx_laporan'})
  txKlasifikasiLm: TxKlasifikasiLm;

  @hasOne(() => TxPleno, {keyTo: 'id_tx_laporan'})
  txPleno: TxPleno;

  @hasOne(() => TxDisposisiPvlKaper, {keyTo: 'id_tx_laporan'})
  txDisposisiPvlKaper: TxDisposisiPvlKaper;

  @hasOne(() => TxDisposisiPvlKapten, {keyTo: 'id_tx_laporan'})
  txDisposisiPvlKapten: TxDisposisiPvlKapten;

  @hasOne(() => TxDisposisi, {keyTo: 'id_tx_laporan'})
  txDisposisi: TxDisposisi;

  @hasOne(() => TxRiksa, {keyTo: 'id_tx_laporan'})
  txRiksa: TxRiksa;
  @hasOne(() => TxRiksaPelimpahanResmon, {keyTo: 'id_tx_laporan'})
  txRiksaPelimpahanResmon: TxRiksaPelimpahanResmon;
  @hasMany(() => TxLaporanPerubahan, {keyTo: 'id_tx_laporan'})
  txLaporanPerubahans: TxLaporanPerubahan[];

  @hasMany(() => TxChangeStatus, {keyTo: 'id_tx_laporan'})
  txChangeStatuses: TxChangeStatus[];

  @hasMany(() => TxTakeOver, {keyTo: 'id_tx_laporan'})
  txTakeOvers: TxTakeOver[];

  @belongsTo(() => TdUser, {name: 'UserDetail', keyTo: 'idUser'})
  officer_by?: string;

  constructor(data?: Partial<TxLaporanHistory>) {
    super(data);
  }
}

export interface TxLaporanHistoryRelations {
  // describe navigational properties here
}

export type TxLaporanHistoryWithRelations = TxLaporanHistory & TxLaporanHistoryRelations;
