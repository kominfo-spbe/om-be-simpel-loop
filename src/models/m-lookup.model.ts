import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'referensi', table: 'm_lookup'}}
})
export class MLookup extends Entity {
  @property({
    type: 'string',
    length: 4000,
    postgresql: {columnName: 'lookup_name', dataType: 'character varying', dataLength: 4000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  lookupName?: string;

  @property({
    type: 'string',
    length: 4000,
    postgresql: {columnName: 'lookup_group_name', dataType: 'character varying', dataLength: 4000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  lookupGroupName?: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'lookup_code', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  lookupCode?: string;

  @property({
    type: 'string',
    required: true,
    length: 200,
    id: 1,
    postgresql: {columnName: 'lookup_id', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  lookupId: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'additional_1', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  additional_1?: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'additional_2', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  additional_2?: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'meta', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  meta?: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'parent_key', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  parentKey?: string;

  @property({
    type: 'string',
    length: 4000,
    postgresql: {columnName: 'description', dataType: 'character varying', dataLength: 4000, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  description?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<MLookup>) {
    super(data);
  }
}

export interface MLookupRelations {
  // describe navigational properties here
}

export type MLookupWithRelations = MLookup & MLookupRelations;
