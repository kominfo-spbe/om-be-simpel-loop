import {Entity, model, property, belongsTo} from '@loopback/repository';
import {TxLaporan} from './tx-laporan.model';

@model({
  settings: {
    strict: true,
    postgresql: {schema: 'simpel_4', table: 'tx_change_status'}
  }
})
export class TxChangeStatus extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;
  @property({
    type: 'string',
  })
  no_agenda?: string;

  @property({
    type: 'date',
  })
  status_laporan_date?: string;

  @property({
    type: 'string',
  })
  status_laporan_old?: string;

  @property({
    type: 'string',
  })
  status_laporan_new?: string;

  @property({
    type: 'string',
  })
  catatan?: string;

  @property({
    type: 'date',
    default: "$now"
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @belongsTo(() => TxLaporan, {name: 'id'})
  id_tx_laporan: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxChangeStatus>) {
    super(data);
  }
}

export interface TxChangeStatusRelations {
  // describe navigational properties here
}

export type TxChangeStatusWithRelations = TxChangeStatus & TxChangeStatusRelations;
