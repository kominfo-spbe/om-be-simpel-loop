import {Model, model, property} from '@loopback/repository';
import {TxChangeStatus} from './tx-change-status.model';
import {TxDisposisiPvlKapten} from './tx-disposisi-pvl-kapten.model';
import {TxKlasifikasiLm} from './tx-klasifikasi-lm.model';
import {TxLaporan} from './tx-laporan.model';
import {TxPleno} from './tx-pleno.model';
import {TxVerifFormil} from './tx-verif-formil.model';
import {TxVerifMateriil} from './tx-verif-materiil.model';

@model()
export class TahapKlasifikasi extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;

    @property({
      type: TxKlasifikasiLm,
      jsonSchema: {nullable: true},
    })
    txKlasifikasiLm: TxKlasifikasiLm;

    @property({
      type: TxPleno,
      jsonSchema: {nullable: true},
    })
    txPleno: TxPleno;

    @property({
      type: TxDisposisiPvlKapten,
      jsonSchema: {nullable: true},
    })
    txDisposisiPvlKapten: TxDisposisiPvlKapten;

    @property({
      type: TxVerifFormil,
      jsonSchema: {nullable: true},
    })
    txVerifFormil: TxVerifFormil;

    @property({
      type: TxVerifMateriil,
      jsonSchema: {nullable: true},
    })
    txVerifMateriil: TxVerifMateriil;



  constructor(data?: Partial<TahapKlasifikasi>) {
    super(data);
  }
}
