import {Model, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class ListTte extends Model {
    @property({
        type: 'array',
        required: true
    })
    idrole?: string;
    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<ListTte>) {
        super(data);
    }
}
