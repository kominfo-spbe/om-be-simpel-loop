import {Entity, model, property} from '@loopback/repository';

@model({
    settings: {
        strict: true,
        postgresql: {schema: 'simpel_4', table: 'tx_manajemen_dokumen'}
    }
})
export class TxManajemenDokumen extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
        defaultFn: "uuid"
    })
    id: string;

    @property({
        type: 'string',
    })
    nama_dokumen?: string;

    @property({
        type: 'number',
    })
    jumlah_penandatangan?: number;

    @property({
        type: 'string',
    })
    penandatangan?: string;
    @property({
        type: 'string',
    })
    penandatangan_1?: string;
    @property({
        type: 'string',
    })
    penandatangan_2?: string;
    @property({
        type: 'string',
    })
    penandatangan_3?: string;

    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxManajemenDokumen>) {
        super(data);
    }
}
