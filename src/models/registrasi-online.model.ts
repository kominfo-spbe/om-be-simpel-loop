import {Model, model, property} from '@loopback/repository';
import {TxChangeStatus} from './tx-change-status.model';
import {TxLaporan} from './tx-laporan.model';

@model()
export class RegistrasiOnline extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;


  constructor(data?: Partial<RegistrasiOnline>) {
    super(data);
  }
}
