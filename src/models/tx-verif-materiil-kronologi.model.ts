import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_verif_materiil_kronologi'}},
})
export class TxVerifMateriilKronologi extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_verif_materiil: string;

  @property({
    type: 'string',
  })
  tanggal_kronologi?: string;

  @property({
    type: 'string',
  })
  uraian_kronologi?: string;

  @property({
    type: 'string',
  })
  catatan_bukti?: string;

  @property({
    type: 'string',
  })
  filename_bukti?: string;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    default: null
  })
  updated_date?: string;

  @property({
    type: 'string',
    default: null
  })
  updated_by?: string;

  @property({
    type: 'string',
    default: null
  })
  deleted_by?: string;

  @property({
    type: 'date',
    default: null
  })
  deleted_date?: string;

  @property({
    type: 'number',
  })
  no_urut?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxVerifMateriilKronologi>) {
    super(data);
  }
}

export interface TxVerifMateriilKronologiRelations {
  // describe navigational properties here
}

export type TxVerifMateriilKronologiWithRelations = TxVerifMateriilKronologi & TxVerifMateriilKronologiRelations;
