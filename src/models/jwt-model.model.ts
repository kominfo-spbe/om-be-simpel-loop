import {Model, model, property} from '@loopback/repository';

@model()
export class JwtModel extends Model {
  @property({
    type: 'string',
  })
  token?: string;


  constructor(data?: Partial<JwtModel>) {
    super(data);
  }
}

export interface JwtModelRelations {
  // describe navigational properties here
}

export type JwtModelWithRelations = JwtModel & JwtModelRelations;
