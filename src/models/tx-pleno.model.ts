import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {postgresql: {schema: 'simpel_4', table: 'tx_pleno'}},
})
export class TxPleno extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'boolean',
  })
  kewenangan_ombudsman?: boolean;

  @property({
    type: 'string',
  })
  ket_kewenangan_ombudsman?: string;

  @property({
    type: 'boolean',
  })
  menolak_laporan?: boolean;

  @property({
    type: 'string',
  })
  proses_laporan?: string;

  @property({
    type: 'string',
  })
  tim_pemeriksaan?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
  })
  updated_date?: string;

  @property({
    type: 'string',
  })
  updated_by?: string;

  @property({
    type: 'string',
  })
  deleted_by?: string;

  @property({
    type: 'date',
  })
  deleted_date?: string;

  @property({
    type: 'string',
  })
  nomor_berita_acara?: string;

  @property({
    type: 'date',
  })
  tgl_berita_acara?: string;

  @property({
    type: 'string',
  })
  keterangan_berita_acara?: string;

  @property({
    type: 'string',
  })
  filename_berita_acara?: string;

  @property({
    type: 'string',
  })
  filesize_berita_acara?: string;

  @property({
    type: 'string',
  })
  filename_dokumen?: string;

  @property({
    type: 'string',
  })
  filesize_dokumen?: string;

  @property({
    type: 'string',
  })
  nomor_berita_acara_pleno?: string;
  @property({
    type: 'string',
  })
  alasan_penutupan?: string;
  @property({
    type: 'string',
  })
  alasan_penundaan?: string;
  @property({
    type: 'string',
  })
  status_pleno?: string;

  @property({
    type: 'string',
  })
  hasil_kesimpulan_pleno?: string;

  @property({
    type: 'date',
  })
  tgl_berita_acara_penutupan?: string;

  @property({
    type: 'date',
  })
  tanggal_pleno?: string;

  //

  @property({
    type: 'string',
  })
  no_agenda?: string;

  @property({
    type: 'string',
  })
  no_berita_acara?: string;

  @property({
    type: 'date',
    required: false,
    jsonSchema: {nullable: true},
  })
  tgl_penutupan_laporan?: string;

  @property({
    type: 'date',
    required: false,
    jsonSchema: {nullable: true},
  })
  tgl_pencabutan_laporan?: string;

  @property({
    type: 'string',
  })
  media_penutupan?: string;

  @property({
    type: 'string',
  })
  media_pencabutan?: string;

  @property({
    type: 'string',
  })
  mediasosial?: string;

  @property({
    type: 'string',
  })
  ringkasan_penutupan?: string;

  @property({
    type: 'string',
  })
  ringkasan?: string;

  @property({
    type: 'date',
    required: false,
    jsonSchema: {nullable: true},
  })
  tgl_penutupan_pleno?: string;

  @property({
    type: 'string',
  })
  alasan_penutupan_laporan?: string;

  @property({
    type: 'string',
  })
  alasan_melanjutkan_pemeriksaan?: string;

  @property({
    type: 'string',
  })
  nomor_berita_acara_pemeriksaan?: string;

  @property({
    type: 'date',
  })
  tanggal_pleno_riksa?: string;

  constructor(data?: Partial<TxPleno>) {
    super(data);
  }
}

export interface TxPlenoRelations {
  // describe navigational properties here
}

export type TxPlenoWithRelations = TxPleno & TxPlenoRelations;
