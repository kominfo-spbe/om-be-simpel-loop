import {Model, model, property} from '@loopback/repository';
import {TxChangeStatus} from './tx-change-status.model';
import {TxLaporan} from './tx-laporan.model';
import {TxVerifFormil} from './tx-verif-formil.model';
import {TxVerifMateriil} from './tx-verif-materiil.model';

@model()
export class FormilSubmit extends Model {

    @property({
      type: TxLaporan,
      jsonSchema: {nullable: true},
    })
    txLaporan: TxLaporan;

    @property({
      type: TxChangeStatus,
      jsonSchema: {nullable: true},
    })
    txChangeStatus: TxChangeStatus;

    @property({
      type: TxVerifFormil,
      jsonSchema: {nullable: true},
    })
    txVerifFormil: TxVerifFormil;

    @property({
      type: TxVerifMateriil,
      jsonSchema: {nullable: true},
    })
    txVerifMateriil: TxVerifMateriil;


  constructor(data?: Partial<FormilSubmit>) {
    super(data);
  }
}
