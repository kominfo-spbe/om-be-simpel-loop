import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'simpel_4', table: 'tx_verif_materiil_dokumen'}
  }
})
export class TxVerifMateriilDokumen extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_laporan: string;

  @property({
    type: 'string',
    required: true,
  })
  id_tx_verif_materiil: string;

  @property({
    type: 'string',
  })
  id_dokumen?: string;

  @property({
    type: 'string',
  })
  filename_dokumen?: string;

  @property({
    type: 'string',
  })
  filesize_dokumen?: string;

  @property({
    type: 'string',
  })
  notes_dokumen?: string;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  created_date?: string;

  @property({
    type: 'string',
  })
  created_by?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    default: null
  })
  updated_date?: string;

  @property({
    type: 'string',
    default: null
  })
  updated_by?: string;

  @property({
    type: 'string',
    default: null
  })
  deleted_by?: string;

  @property({
    type: 'date',
    default: null
  })
  deleted_date?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TxVerifMateriilDokumen>) {
    super(data);
  }
}

export interface TxVerifMateriilDokumenRelations {
  // describe navigational properties here
}

export type TxVerifMateriilDokumenWithRelations = TxVerifMateriilDokumen & TxVerifMateriilDokumenRelations;
