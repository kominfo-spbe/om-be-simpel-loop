import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TxLaporan} from '../models';

@model({
  settings: {
    postgresql: {schema: 'simpel_4', table: 'tx_laporan_tindakan'}
  },
})
export class TxLaporanTindakan extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    defaultFn: "uuid"
  })
  id: string;

  @property({
    type: 'date',
  })
  tanggal?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  jenis_tindakan?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  file_name?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  ket_jenis_tindakan?: string;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {nullable: true}
  })
  keterangan?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  created_date?: string;

  @belongsTo(() => TxLaporan, {name: 'id'})
  id_tx_laporan: string;

  constructor(data?: Partial<TxLaporanTindakan>) {
    super(data);
  }
}

export interface TxLaporanTindakanRelations {
  // describe navigational properties here
}

export type TxLaporanTindakanWithRelations = TxLaporanTindakan & TxLaporanTindakanRelations;
