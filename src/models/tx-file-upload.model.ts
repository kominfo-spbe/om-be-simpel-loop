import {Entity, model, property} from '@loopback/repository';

@model({
    settings: {
        strict: true,
        postgresql: {schema: 'simpel_4', table: 'tx_file_upload'}
    }
})
export class TxFileUpload extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
        defaultFn: "uuid"
    })
    id: string;
    @property({
      type: 'date',
      defaultFn: 'now',
    })
    created_date?: string;

    // Define well-known properties here

    // Indexer property to allow additional data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;

    constructor(data?: Partial<TxFileUpload>) {
        super(data);
    }
}
