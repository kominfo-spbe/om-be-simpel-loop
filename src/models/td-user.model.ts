import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'apps_manager', table: 'td_user'}}
})
export class TdUser extends Entity {
  @property({
    type: 'string',
    length: 50,
    postgresql: {columnName: 'kode_kantor', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  kodeKantor?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'waktu_update', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  waktuUpdate?: string;

  @property({
    type: 'string',
    length: 18,
    postgresql: {columnName: 'nip_update', dataType: 'character varying', dataLength: 18, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  nipUpdate?: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'waktu_rekam', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  waktuRekam: string;

  @property({
    type: 'string',
    required: true,
    length: 18,
    postgresql: {columnName: 'nip_rekam', dataType: 'character varying', dataLength: 18, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  nipRekam: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'waktu_aktivasi', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  waktuAktivasi?: string;

  @property({
    type: 'string',
    length: 1,
    postgresql: {columnName: 'flag_aktivasi', dataType: 'character varying', dataLength: 1, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  flagAktivasi?: string;

  @property({
    type: 'string',
    length: 1,
    postgresql: {columnName: 'flag_blokir', dataType: 'character varying', dataLength: 1, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  flagBlokir?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'tanggal_expired', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tanggalExpired?: string;

  @property({
    type: 'string',
    length: 100,
    postgresql: {columnName: 'answer', dataType: 'character varying', dataLength: 100, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  answer?: string;

  @property({
    type: 'string',
    length: 100,
    postgresql: {columnName: 'ask', dataType: 'character varying', dataLength: 100, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  ask?: string;

  @property({
    type: 'string',
    length: 100,
    postgresql: {columnName: 'pin', dataType: 'character varying', dataLength: 100, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  pin?: string;

  @property({
    type: 'string',
    required: true,
    length: 100,
    postgresql: {columnName: 'user_password', dataType: 'character varying', dataLength: 100, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  userPassword: string;

  @property({
    type: 'string',
    required: true,
    length: 25,
    postgresql: {columnName: 'user_name', dataType: 'character varying', dataLength: 25, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  userName: string;

  @property({
    type: 'string',
    required: true,
    length: 75,
    postgresql: {columnName: 'email', dataType: 'character varying', dataLength: 75, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  email: string;

  @property({
    type: 'string',
    required: true,
    length: 25,
    postgresql: {columnName: 'handphone', dataType: 'character varying', dataLength: 25, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  handphone: string;

  @property({
    type: 'string',
    required: true,
    length: 250,
    postgresql: {columnName: 'alamat', dataType: 'character varying', dataLength: 250, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  alamat: string;

  @property({
    type: 'string',
    required: true,
    length: 75,
    postgresql: {columnName: 'nama', dataType: 'character varying', dataLength: 75, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  nama: string;

  @property({
    type: 'string',
    length: 18,
    postgresql: {columnName: 'nip', dataType: 'character varying', dataLength: 18, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  nip?: string;

  @property({
    type: 'string',
    length: 250,
    postgresql: {columnName: 'divisi_detail', dataType: 'character varying', dataLength: 250, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  divisiDetail?: string;

  @property({
    type: 'string',
    length: 75,
    postgresql: {columnName: 'divisi', dataType: 'character varying', dataLength: 75, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  divisi?: string;

  @property({
    type: 'string',
    length: 3,
    postgresql: {columnName: 'kode_level', dataType: 'character varying', dataLength: 3, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  kodeLevel?: string;

  @property({
    type: 'string',
    length: 36,
    postgresql: {columnName: 'identitas', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  identitas?: string;

  @property({
    type: 'string',
    length: 2,
    postgresql: {columnName: 'kode_identitas', dataType: 'character varying', dataLength: 2, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  kodeIdentitas?: string;

  @property({
    type: 'string',
    required: true,
    length: 1,
    postgresql: {columnName: 'jenis_user', dataType: 'character varying', dataLength: 1, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  jenisUser: string;

  @property({
    type: 'string',
    required: true,
    length: 36,
    id: 1,
    postgresql: {columnName: 'id_user', dataType: 'character varying', dataLength: 36, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  idUser: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'jabatan', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  jabatan?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TdUser>) {
    super(data);
  }
}

export interface TdUserRelations {
  // describe navigational properties here
}

export type TdUserWithRelations = TdUser & TdUserRelations;
